NAME = loroco-stm32-sdk

LIB_RELEASE = $(NAME).a
LIB_DEBUG   = $(NAME)-debug.a

CROSS_COMPILE   = arm-none-eabi-
CC              = $(CROSS_COMPILE)gcc
LD              = $(CROSS_COMPILE)gcc

INCLUDE          = -I include
FLAGS_ARCH       = -mlittle-endian -mcpu=cortex-m4 -mfpu=fpv5-d16
CFLAGS_WARN      = -Wall -Wextra -Werror -Wno-address-of-packed-member
CFLAGS_COMMON    =  $(FLAGS_ARCH) $(INCLUDE) -MD -MP $(CFLAGS_WARN)
CFLAGS_RELEASE   = -O3 $(CFLAGS_COMMON)
CFLAGS_DEBUG     = -O0 -g $(CFLAGS_COMMON)

SRC = $(shell find src -type f)
OBJ_RELEASE = $(patsubst src/%.c, .build/%.o, $(SRC))
DEP_RELEASE = $(patsubst src/%.c, .build/%.d, $(SRC))
OBJ_DEBUG = $(patsubst src/%.c, .build/debug/%.o, $(SRC))
DEP_DEBUG = $(patsubst src/%.c, .build/debug/%.d, $(SRC))

all: release

release: $(LIB_RELEASE)

debug: $(LIB_DEBUG)

.build/init.o: link/init.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS_RELEASE) -o $@ $<

.build/debug/init.o: link/init.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS_DEBUG) -o $@ $<

.build/%.o: src/%.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS_RELEASE) -o $@ $<

.build/debug/%.o: src/%.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS_DEBUG) -o $@ $<

$(LIB_RELEASE): .build/init.o $(OBJ_RELEASE)
	$(AR) -crs $@ $^

$(LIB_DEBUG): .build/debug/init.o $(OBJ_DEBUG)
	$(AR) -crs $@ $^

clean:
	rm -rf .build $(LIB_RELEASE) $(LIB_DEBUG)

.build/cppcheck.log: $(SRC)
	cppcheck -I include --enable=all --force --quiet $^ 2> $@

cppcheck: .build/cppcheck.log
	cat $<

loc:
	wc -l $(SRC)

-include $(DEP_RELEASE) $(DEP_DEBUG) $(DEP_TEST)

.PHONY : all release debug clean cppcheck loc
