#ifndef __CM4_H__
#define __CM4_H__

#include <stdbool.h>
#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned       intlinesnum :  4;
		const unsigned res_31_4    : 28;
	};

	uint32_t mask;
} cm4_ictr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           dismcycint :  1;
		bool           disdefwbuf :  1;
		bool           disfold    :  1;
		const unsigned res_7_3    :  5;
		bool           disoofp    :  1;
		unsigned       disfpca    :  1;
		const unsigned res_31_10  : 22;
	};

	uint32_t mask;
} cm4_actlr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           enable    :  1;
		bool           tickint   :  1;
		unsigned       clksource :  1;
		const unsigned res_15_3  : 13;
		const bool     countflag :  1;
		const unsigned res_31_17 : 15;
	};

	uint32_t mask;
} cm4_syst_csr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned       reload    : 24;
		const unsigned res_31_24 :  8;
	};

	uint32_t mask;
} cm4_syst_rvr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned       current   : 24;
		const unsigned res_31_24 :  8;
	};

	uint32_t mask;
} cm4_syst_cvr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned tenms     : 24;
		const unsigned res_29_24 :  6;
		const bool     skew      :  1;
		const bool     nmoref    :  1;
	};

	uint32_t mask;
} cm4_syst_calib_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned revision    :  4;
		const unsigned partno      : 12;
		const unsigned constant    :  4;
		const unsigned variant     :  4;
		const unsigned implementer :  8;
	};

	uint32_t mask;
} cm4_cpuid_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned vectactive  : 9;
		const unsigned res_10_9    : 2;
		const bool     rettobase   : 1;
		const unsigned vectpending : 9;
		const unsigned res_21      : 1;
		const bool     isrpending  : 1;
		const bool     debug       : 1;
		const unsigned res_24      : 1;
		bool           pendstclr   : 1;
		bool           pendstset   : 1;
		bool           pendsvclr   : 1;
		bool           pendsvset   : 1;
		const unsigned res_30_29   : 1;
		bool           nmipendset  : 1;
	};

	uint32_t mask;
} cm4_icsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned       tbloff  : 23;
		const unsigned res_8_0 :  9;
	};

	uint32_t mask;
} cm4_vtor_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           vectreset     :  1;
		bool           vectclractive :  1;
		bool           sysresetreq   :  1;
		const unsigned res_7_3       :  5;
		unsigned       prigroup      :  3;
		const unsigned res_14_11     :  4;
		const bool     endianness    :  1;
		unsigned       vectkeystat   : 16;
	};

	uint32_t mask;
} cm4_aircr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned res_0         :  1;
		bool           sleeponexit   :  1;
		bool           sleepdeep     :  1;
		const unsigned res_3         :  1;
		bool           sevonpend     :  1;
		const unsigned res_31_5      : 27;
	};

	uint32_t mask;
} cm4_scr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           nonbasethrdena :  1;
		bool           usersetmpend   :  1;
		const unsigned res_2          :  1;
		bool           unalign_trp    :  1;
		bool           div_0_trp      :  1;
		const unsigned res_7_5        :  3;
		bool           bfhfnmign      :  1;
		const bool     stkalign       :  1;
		const unsigned res_15_10      :  6;
		bool           dc             :  1;
		bool           ic             :  1;
		const bool     bp             :  1;
		const unsigned res_31_19      : 23;
	};

	uint32_t mask;
} cm4_ccr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint8_t       pri_4;
		uint8_t       pri_5;
		uint8_t       pri_6;
		const uint8_t pri_7;
	};

	uint32_t mask;
} cm4_shpr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint8_t pri_8;
		const uint8_t pri_9;
		const uint8_t pri_10;
		uint8_t       pri_11;
	};

	uint32_t mask;
} cm4_shpr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint8_t pri_12;
		const uint8_t pri_13;
		uint8_t       pri_14;
		uint8_t       pri_15;
	};

	uint32_t mask;
} cm4_shpr3_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           memfaultact    :  1;
		bool           busfaultact    :  1;
		const unsigned res_2          :  1;
		bool           usgfaultact    :  1;
		const unsigned res_6_4        :  3;
		bool           svccallact     :  1;
		bool           monitoract     :  1;
		const unsigned res_9          :  1;
		bool           pendsvact      :  1;
		bool           systickact     :  1;
		bool           usgfaultpended :  1;
		bool           memfaultpended :  1;
		bool           busfaultpended :  1;
		bool           svcallpended   :  1;
		bool           memfaultena    :  1;
		bool           busfaultena    :  1;
		bool           usgfaultena    :  1;
		const unsigned res_31_19      : 23;
	};

	uint32_t mask;
} cm4_shcsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
			bool           iaccviol  : 1;
			bool           daccviol  : 1;
			const unsigned res_2     : 1;
			bool           munstkerr : 1;
			bool           mstker    : 1;
			bool           mlsperr   : 1;
			const unsigned res_6     : 1;
			bool           mmarvalid : 1;
	};

	uint8_t mask;
} cm4_mmfsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
			bool           ibuserr     : 1;
			bool           preciserr   : 1;
			bool           impreciserr : 1;
			bool           unstkerr    : 1;
			bool           stker       : 1;
			bool           lsperr      : 1;
			const unsigned res_6       : 1;
			bool           bfarvalid   : 1;
	};

	uint8_t mask;
} cm4_bfsr_t;


typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
			bool           undefinstr : 1;
			bool           invstate   : 1;
			bool           invpc      : 1;
			bool           nocp       : 1;
			const unsigned res_7_4    : 4;
			bool           unaligned  : 1;
			bool           divbyzero  : 1;
			const unsigned res_15_10  : 6;
	};

	uint16_t mask;
} cm4_ufsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		cm4_mmfsr_t mmfsr;
		cm4_bfsr_t  bfsr;
		cm4_ufsr_t  ufsr;
	};

	uint32_t mask;
} cm4_cfsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned res_0    :  1;
		bool           vecttbl  :  1;
		const unsigned res_29_2 : 28;
		bool           forced   :  1;
		bool           debugevt :  1;
	};

	uint32_t mask;
} cm4_hfsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           halted   :  1;
		bool           bkpt     :  1;
		bool           dwttrap  :  1;
		bool           vcatch   :  1;
		bool           external :  1;
		const unsigned res_31_5 : 27;
	};

	uint32_t mask;
} cm4_dfsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned cl1       : 3;
		const unsigned cl2       : 3;
		const unsigned cl3       : 3;
		const unsigned cl4       : 3;
		const unsigned cl5       : 3;
		const unsigned cl6       : 3;
		const unsigned cl7       : 3;
		const unsigned louis     : 3;
		const unsigned loc       : 3;
		const unsigned lou       : 3;
		const unsigned res_31_29 : 2;
	};

	uint32_t mask;
} cm4_clidr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned iminlione :  4;
		const unsigned res_15_4  : 12;
		const unsigned dminlione :  4;
		const unsigned erg       :  4;
		const unsigned cwg       :  4;
		const unsigned res_28    :  1;
		const unsigned format    :  3;
	};

	uint32_t mask;
} cm4_ctr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned linesize      :  3;
		const unsigned associativity : 10;
		const unsigned numsets       : 15;
		const bool     wa            :  1;
		const bool     ra            :  1;
		const bool     wb            :  1;
		const bool     wt            :  1;
	};

	uint32_t mask;
} cm4_ccsidr_t;

typedef enum
{
	CM4_CACHE_INSTRUCTION = 0,
	CM4_CACHE_DATA        = 1,
} cm4_cache_e;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		cm4_cache_e    ind      :  1;
		unsigned       level    :  3;
		const unsigned res_31_4 : 28;
	};

	uint32_t mask;
} cm4_csselr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned res_19_0  : 20;
		unsigned       cp10      :  2;
		unsigned       cp11      :  2;
		const unsigned res_31_24 :  8;
	};

	uint32_t mask;
} cm4_cpacr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const bool     separate  : 1;
		const unsigned res_7_1   : 7;
		const unsigned dregion   : 8;
		const unsigned iregion   : 8;
		const unsigned res_31_24 : 8;
	};

	uint32_t mask;
} cm4_mpu_type_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           enable     :  1;
		bool           hfnmiena   :  1;
		bool           privdefena :  1;
		const unsigned res_31_3   : 25;
	};

	uint32_t mask;
} cm4_mpu_ctrl_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		unsigned region :  4;
		bool     valid  :  1;
		unsigned addr   : 27;
	};

	uint32_t mask;
} cm4_mpu_rbar_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           enable    : 1;
		unsigned       size      : 5;
		const unsigned res_7_6   : 2;
		unsigned       srd       : 8;
		bool           c         : 1;
		bool           b         : 1;
		bool           s         : 1;
		unsigned       tex       : 3;
		const unsigned res_23_22 : 2;
		unsigned       ap        : 3;
		const unsigned res_27    : 1;
		bool           xn        : 1;
		unsigned       res_31_29 : 3;
	};

	uint32_t mask;
} cm4_mpu_rasr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           lspact   :  1;
		bool           user     :  1;
		const unsigned res_2    :  1;
		bool           thread   :  1;
		bool           hfrdy    :  1;
		bool           mmrdy    :  1;
		bool           bfrdy    :  1;
		const unsigned res_7    :  1;
		bool           monrdy   :  1;
		const unsigned res_29_9 : 21;
		bool           lspen    :  1;
		bool           aspen    :  1;
	};

	uint32_t mask;
} cm4_fpccr_t;

typedef enum
{
	CM4_FP_RMODE_RN = 0,
	CM4_FP_RMODE_RP = 1,
	CM4_FP_RMODE_RM = 2,
	CM4_FP_RMODE_RZ = 3,
} cm4_fp_rmode_e;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		bool           ioc      :  1;
		bool           dzc      :  1;
		bool           ofc      :  1;
		bool           ufc      :  1;
		bool           ixc      :  1;
		const unsigned res_6_5  :  2;
		bool           idc      :  1;
		const unsigned res_21_8 : 14;
		cm4_fp_rmode_e rmode    :  2;
		bool           fz       :  1;
		bool           dn       :  1;
		bool           ahp      :  1;
		const unsigned res_27   :  1;
		bool           v        :  1;
		bool           c        :  1;
		bool           z        :  1;
		bool           n        :  1;
	};

	uint32_t mask;
} cm4_fpscr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const unsigned res_21_0  : 22;
		cm4_fp_rmode_e rmode     :  2;
		bool           fz        :  1;
		bool           dn        :  1;
		bool           ahp       :  1;
		const unsigned res_31_27 :  5;
	};

	uint32_t mask;
} cm4_fpdscr_t;

// TODO: I think we should break this up into a set
//       of structs
typedef struct
{
	volatile const uint32_t res_0;
	volatile       uint32_t ictr;
	volatile       uint32_t actlr;
	volatile const uint32_t res_3;
	volatile       uint32_t syst_csr;
	volatile       uint32_t syst_rvr;
	volatile       uint32_t syst_cvr;
	volatile const uint32_t syst_calib;
	volatile const uint32_t res_8_63[56];
	volatile       uint32_t nvic_iser[8];
	volatile const uint32_t res_72_95[24];
	volatile       uint32_t nvic_icer[8];
	volatile const uint32_t res_104_128[24];
	volatile       uint32_t nvic_ispr[8];
	volatile const uint32_t res_136_159[24];
	volatile       uint32_t nvic_icpr[8];
	volatile const uint32_t res_168_192[24];
	volatile       uint32_t nvic_iabr[8];
	volatile const uint32_t res_200_255[56];
	volatile       uint32_t nvic_ipr[60];
	volatile const uint32_t res_316_831[516];
	volatile const uint32_t cpuid;
	volatile       uint32_t icsr;
	volatile       uint32_t vtor;
	volatile       uint32_t aircr;
	volatile       uint32_t scr;
	volatile       uint32_t ccr;
	volatile       uint32_t shpr1;
	volatile       uint32_t shpr2;
	volatile       uint32_t shpr3;
	volatile       uint32_t shcsr;
	volatile       uint32_t cfsr;
	volatile       uint32_t hfsr;
	volatile       uint32_t dfsr;
	void * volatile         mmfar;
	void * volatile         bfar;
	volatile       uint32_t afsr;
	volatile       uint32_t id_pfr0;
	volatile       uint32_t id_pfr1;
	volatile       uint32_t id_dfr0;
	volatile       uint32_t id_afr0;
	volatile       uint32_t id_mfr0;
	volatile       uint32_t id_mfr1;
	volatile       uint32_t id_mfr2;
	volatile       uint32_t id_mfr3;
	volatile       uint32_t id_isar0;
	volatile       uint32_t id_isar1;
	volatile       uint32_t id_isar2;
	volatile       uint32_t id_isar3;
	volatile       uint32_t id_isar4;
	volatile const uint32_t res_861_865[5];
	volatile       uint32_t cpacr;
	volatile const uint32_t res_867;
	volatile const uint32_t mpu_type;
	volatile       uint32_t mpu_ctrl;
	volatile       uint32_t mpu_rnr;
	volatile       uint32_t mpu_rbar_a0;
	volatile       uint32_t mpu_rasr_a0;
	volatile       uint32_t mpu_rbar_a1;
	volatile       uint32_t mpu_rasr_a1;
	volatile       uint32_t mpu_rbar_a2;
	volatile       uint32_t mpu_rasr_a2;
	volatile       uint32_t mpu_rbar_a3;
	volatile       uint32_t mpu_rasr_a3;
	volatile const uint32_t res_879_959[81];
	volatile       uint32_t stir;
	volatile const uint32_t res_961_972[12];
	volatile       uint32_t fpccr;
	void * volatile         fpcar;
	volatile       uint32_t fpdscr;
	volatile const uint32_t res_976_1023[48];
} cm4_t;

/* TODO - Cache registers. */
/* TODO - Access registers. */

static volatile cm4_t * const cm4
	= (volatile cm4_t*)0xE000E000;

#define CM4_FIELD_READ(r, f) \
	((cm4_##r##_t)cm4->r).f
#define CM4_FIELD_WRITE(r, f, v) \
	do { cm4_##r##_t r = { .mask = cm4->r }; \
	r.f = v; cm4->r = r.mask; } while (0)

#define CM4_SHPR_READ(x) \
	((uint8_t*)&cm4->shpr1)[(x) - 4]
#define CM4_SHPR_WRITE(x, v) \
	((uint8_t*)&cm4->shpr1)[(x) - 4] = v

#define CM4_MMFSR_READ() \
	((cm4_cfsr_t*)&cm4->cfsr)->mmfsr
#define CM4_MMFSR_WRITE(v) \
	((cm4_cfsr_t*)&cm4->cfsr)->mmfsr.mask = v
#define CM4_BFSR_READ() \
	((cm4_cfsr_t*)&cm4->cfsr)->bfsr
#define CM4_BFSR_WRITE(v) \
	((cm4_cfsr_t*)&cm4->cfsr)->bfsr.mask = v
#define CM4_UFSR_READ() \
	((cm4_cfsr_t*)&cm4->cfsr)->ufsr
#define CM4_UFSR_WRITE() \
	((cm4_cfsr_t*)&cm4->cfsr)->ufsr.mask = v

#define CM4_RBAR_READ(a, f) \
	((cm4_rbar_t)cm4->rbar_a##a).f
#define CM4_RBAR_WRITE(a, f, v) \
	do { cm4_rbar_t rbar = { .mask = cm4->rbar_a##a }; \
	rbar.f = v; cm4->rbar_a##a = rbar.mask; } while (0)
#define CM4_RASR_READ(a, f) \
	((cm4_rasr_t)cm4->rasr_a##a).f
#define CM4_RASR_WRITE(a, f, v) \
	do { cm4_rasr_t rasr = { .mask = cm4->rasr_a##a }; \
	rasr.f = v; cm4->rasr_a##a = rasr.mask; } while (0)

static inline void* CM4_MMAR_READ(void)
{
	void* mmar;
	bool valid;
	do
	{
		mmar = cm4->mmfar;
		valid = CM4_MMFSR_READ().mmarvalid;
	} while (valid);
	return mmar;
}

static inline void* CM4_BFAR_READ(void)
{
	void* bfar;
	bool valid;
	do
	{
		bfar = cm4->bfar;
		valid = CM4_BFSR_READ().bfarvalid;
	} while (valid);
	return bfar;
}

static inline cm4_fpscr_t CM4_FPSCR_READ(void)
{
	cm4_fpscr_t fpscr;
	asm("vmrs %0, FPSCR" : "=r" (fpscr.mask));
	return fpscr;
}

static inline void CM4_FPSCR_WRITE(cm4_fpscr_t fpscr)
{
	asm("vmsr FPSCR, %0" : : "r" (fpscr.mask));
}

#define CM4_FPSCR_FIELD_READ(f) \
	CM4_FPSCR_READ().f
#define CM4_FPSCR_FIELD_WRITE(f, v) \
	do { cm4_fpscr_t fpscr = CM4_FPSCR_READ(); \
	fpscr.f = v; CM4_FPSCR_WRITE(fpscr); } while (0)

#endif
