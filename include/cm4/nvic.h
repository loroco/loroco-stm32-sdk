#ifndef __CM4_NVIC_H__
#define __CM4_NVIC_H__

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
	CM4_NVIC_EXC_RESET      =  1,
	CM4_NVIC_EXC_NMI        =  2,
	CM4_NVIC_EXC_HARDFAULT  =  3,
	CM4_NVIC_EXC_MEMMANAGE  =  4,
	CM4_NVIC_EXC_BUSFAULT   =  5,
	CM4_NVIC_EXC_USAGEFAULT =  6,
	CM4_NVIC_EXC_SVCALL     = 11,
	CM4_NVIC_EXC_PENDSV     = 14,
	CM4_NVIC_EXC_SYSTICK    = 15,
	CM4_NVIC_EXC_COUNT
} cm4_nvic_exc_e;

void cm4_nvic_irq_enable(uint8_t irq, bool enable);
void cm4_nvic_irq_pending_set(uint8_t irq, bool pending);
void cm4_nvic_irq_priority_set(uint8_t irq, uint8_t priority);

bool    cm4_nvic_irq_is_enabled(uint8_t irq);
bool    cm4_nvic_irq_is_pending(uint8_t irq);
bool    cm4_nvic_irq_is_active(uint8_t irq);
uint8_t cm4_nvic_irq_priority_get(uint8_t irq);

#endif
