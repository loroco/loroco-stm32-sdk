#ifndef __CM4_SYSTICK_H__
#define __CM4_SYSTICK_H__

#include <stdbool.h>
#include <stdint.h>

bool cm4_systick_enable(uint32_t divide, void (*callback)(void));
void cm4_systick_disable(void);

#endif
