#ifndef __PROTOCOLS_CAN_H__
#define __PROTOCOLS_CAN_H__

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#define CAN_DLC_MAX 8

static inline bool can_dlc_from_size(size_t size, uint8_t* dlc)
{
	if (size > 8) return false;
	if (dlc) *dlc = size;
	return true;
}

static inline size_t can_dlc_to_size(uint8_t dlc)
{
	dlc &= 0x0F;
	if (dlc <= 8) return dlc;
	return 8;
}

static inline bool can_dlc_fd_from_size(size_t size, uint8_t* dlc)
{
	uint8_t l;
	if (size <= 8)
	{
		l = size;
	}
	else
	{
		switch (size)
		{
			case 12: l =  9; break;
			case 16: l = 10; break;
			case 20: l = 11; break;
			case 24: l = 12; break;
			case 32: l = 13; break;
			case 48: l = 14; break;
			case 64: l = 15; break;
			default: return false;
		}
	}
	if (dlc) *dlc = l;
	return true;
}

static inline size_t can_fd_dlc_to_size(uint8_t dlc)
{
	dlc &= 0x0F;
	if (dlc <= 8) return dlc;
	size_t fdcan_dlc[] = { 12, 16, 20, 24, 32, 48, 64 };
	return fdcan_dlc[dlc - 9];
}


#endif
