#ifndef __PROTOCOLS_SLCAN_H__
#define __PROTOCOLS_SLCAN_H__

#include <stdbool.h>
#include <stdint.h>

#include <protocols/can.h>

/* SLCAN Format (from linux driver):
 * A CAN frame has a can_id (11 bit standard frame format OR 29 bit extended
 * frame format) a data length code (can_dlc) which can be from 0 to 8
 * and up to <can_dlc> data bytes as payload.
 * Additionally a CAN frame may become a remote transmission frame if the
 * RTR-bit is set. This causes another ECU to send a CAN frame with the
 * given can_id.
 *
 * The SLCAN ASCII representation of these different frame types is:
 * <type> <id> <dlc> <data>*
 *
 * Extended frames (29 bit) are defined by capital characters in the type.
 * RTR frames are defined as 'r' types - normal frames have 't' type:
 * t => 11 bit data frame
 * r => 11 bit RTR frame
 * T => 29 bit data frame
 * R => 29 bit RTR frame
 *
 * The <id> is 3 (standard) or 8 (extended) bytes in ASCII Hex (base64).
 * The <dlc> is a one byte ASCII number ('0' - '8')
 * The <data> section has at much ASCII Hex bytes as defined by the <dlc>
 *
 * Examples:
 *
 * t1230 : can_id 0x123, can_dlc 0, no data
 * t4563112233 : can_id 0x456, can_dlc 3, data 0x11 0x22 0x33
 * T12ABCDEF2AA55 : extended can_id 0x12ABCDEF, can_dlc 2, data 0xAA 0x55
 * r1230 : can_id 0x123, can_dlc 0, no data, remote transmission request
 */

/* This implementation supports both classic slcan (of the above form) and
 * Binary slcan, the format of which is:
 *
 * <type> <id> <dlc> '&'<data>
 * ----------------- =========
 * ^ the header is   ^ the data is now binary, and prefixed with '&'
 *   still ASCII       to indicate binary format to the parser.
 *
 * Note that classic slcan drivers will fail if fed a binary slcan frame.
 */


typedef enum
{
	SLCAN_MSG_TYPE_DATA,
	SLCAN_MSG_TYPE_REMOTE
} slcan_msg_type_e;

typedef enum
{
	SLCAN_ADDRESS_SPACE_11_BIT,
	SLCAN_ADDRESS_SPACE_29_BIT
} slcan_address_space_e;

typedef enum
{
	SLCAN_FORMAT_CLASSIC,
	SLCAN_FORMAT_BINARY
} slcan_format_e;

static inline uint8_t slcan_address_space_size(slcan_address_space_e space)
{
	uint8_t size = 0;
	switch (space) {
		case SLCAN_ADDRESS_SPACE_11_BIT:
			size = 3;
			break;

		case SLCAN_ADDRESS_SPACE_29_BIT:
			size = 8;
			break;

		default:
			break;
	}
	return size;
}


typedef struct
{
	slcan_msg_type_e      type;
	slcan_address_space_e address_space;
	uint32_t              msg_id;
	uint8_t               dlc;
	bool                  binary;
} slcan_header_t;

typedef struct
{
	slcan_header_t header;
	uint8_t        data[0];
} slcan_frame_t;

slcan_frame_t* slcan_frame_create(slcan_header_t header, const uint8_t *data);
void           slcan_frame_delete(slcan_frame_t *frame);

slcan_frame_t* slcan_frame_from_stream(
	const uint8_t *stream, uint32_t stream_len);
uint8_t*       slcan_frame_to_stream  (
	const slcan_frame_t *frame, uint32_t *stream_len);

#endif
