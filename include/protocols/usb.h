#ifndef __PROTOCOLS_USB_H__
#define __PROTOCOLS_USB_H__

#include <stdbool.h>
#include <stdint.h>

#include <protocols/usb/configuration.h>
#include <protocols/usb/device.h>
#include <protocols/usb/endpoint.h>
#include <protocols/usb/interface.h>
#include <protocols/usb/request.h>

typedef enum
{
	USB_VERSION_1_0 = 0x0100,
	USB_VERSION_1_1 = 0x0110,
	USB_VERSION_2_0 = 0x0200,
	USB_VERSION_3_0 = 0x0300,
	USB_VERSION_3_1 = 0x0310,
} usb_version_e;

typedef enum
{
	USB_PORT_TYPE_NONE = 0,
	USB_PORT_TYPE_SDP,
	USB_PORT_TYPE_CDP,
	USB_PORT_TYPE_DCP,
	USB_PORT_TYPE_PS2,
} usb_port_type_e;

static inline bool usb_port_type_has_data(usb_port_type_e type)
	{ return (type == USB_PORT_TYPE_SDP) || (type == USB_PORT_TYPE_CDP); }


#endif
