typedef struct
#ifndef __PROTOCOLS_USB_CONFIGURATION_H__
#define __PROTCCOLS_USB_CONFIGURATION_H__

#include <stdint.h>

__attribute__((__packed__))
{
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t wTotalLength;
	uint8_t  bNumInterfaces;
	uint8_t  bConfigurationValue;
	uint8_t  iConfiguration;
	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			uint8_t res_4_0  : 5;// must be set to 0
			uint8_t remWkup  : 1;
			uint8_t selfPow  : 1;
			uint8_t res_7    : 1;// must be set to 1
		};
		uint8_t mask;
	} bmAttributes;
	uint8_t  bMaxPower;
} usb_configuration_descriptor_t;

#endif
