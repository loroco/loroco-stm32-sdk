#ifndef __PROTOCOLS_USB_DEVICE_H__
#define __PROTCCOLS_USB_DEVICE_H__

#include <stdint.h>

typedef enum
{
	USB_DEVICE_CLASS_DEVICE               = 0x00,
	USB_DEVICE_CLASS_AUDIO                = 0x01,
	USB_DEVICE_CLASS_COMMUNICATIONS       = 0x02,
	USB_DEVICE_CLASS_CDC_CONTROL          = 0x02,
	USB_DEVICE_CLASS_HID                  = 0x03,
	USB_DEVICE_CLASS_PHYSICAL             = 0x05,
	USB_DEVICE_CLASS_STILL_IMAGING        = 0x06,
	USB_DEVICE_CLASS_PRINTER              = 0x07,
	USB_DEVICE_CLASS_MASS_STORAGE         = 0x08,
	USB_DEVICE_CLASS_HUB                  = 0x09,
	USB_DEVICE_CLASS_CDC_DATA             = 0x0A,
	USB_DEVICE_CLASS_SMART_CARD           = 0x0B,
	USB_DEVICE_CLASS_CONTENT_SECURITY     = 0x0D,
	USB_DEVICE_CLASS_VIDEO                = 0x0E,
	USB_DEVICE_CLASS_PERSONAL_HEALTHCARE  = 0x0F,
	USB_DEVICE_CLASS_AUDIO_VIDEO          = 0x10,
	USB_DEVICE_CLASS_BILLBOARD            = 0x11,
	USB_DEVICE_CLASS_USB_TYPE_C_BRIDGE    = 0x12,
	USB_DEVICE_CLASS_DIAGNOSTIC_DEVICE    = 0xDC,
	USB_DEVICE_CLASS_WIRELESS_CONTROLLER  = 0xE0,
	USB_DEVICE_CLASS_MISCELLANEOUS        = 0xEF,
	USB_DEVICE_CLASS_APPLICATION_SPECIFIC = 0xFE,
	USB_DEVICE_CLASS_VENDOR_SPECIFIC      = 0xFF,
} usb_device_class_e;

typedef struct
__attribute__((__packed__))
{
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t bcdUSB;
	uint8_t  bDeviceClass;
	uint8_t  bDeviceSubClass;
	uint8_t  bDeviceProtocol;
	uint8_t  bMaxPacketSize;
	uint16_t idVendor;
	uint16_t idProduct;
	uint16_t bcdDevice;
	uint8_t  iManufaturer;
	uint8_t  iProduct;
	uint8_t  iSerialNumber;
	uint8_t  bNumConfigurations;
} usb_device_descriptor_t;

#endif
