#ifndef __PROTOCOLS_USB_ENDPOINT_H__
#define __PROTCCOLS_USB_ENDPOINT_H__

#include <stdint.h>

typedef enum
{
	USB_ENDPOINT_DIR_OUT = 0,
	USB_ENDPOINT_DIR_IN  = 1,
} usb_endpoint_dir_e;

typedef enum
{
	USB_ENDPOINT_TYPE_CONTROL     = 0,
	USB_ENDPOINT_TYPE_ISOCHRONOUS = 1,
	USB_ENDPOINT_TYPE_BULK        = 2,
	USB_ENDPOINT_TYPE_INTERRUPT   = 3,
} usb_endpoint_type_e;

typedef enum
{
	USB_ENDPOINT_SYNC_NONE         = 0,
	USB_ENDPOINT_SYNC_ASYNCHRONOUS = 1,
	USB_ENDPOINT_SYNC_ADAPTIVE     = 2,
	USB_ENDPOINT_SYNC_SYNCHRONOUS  = 3,
} usb_endpoint_sync_e;

typedef enum
{
	USB_ENDPOINT_USAGE_DATA                   = 0,
	USB_ENDPOINT_USAGE_FEEDBACK               = 1,
	USB_ENDPOINT_USAGE_IMPLICIT_FEEDBACK_DATA = 2,
} usb_endpoint_usage_e;

typedef struct
__attribute__((__packed__))
{
	uint8_t  bLength;
	uint8_t  bDescriptorType;

	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			unsigned           number    : 4;
			unsigned           reserved  : 3;
			usb_endpoint_dir_e direction : 1;
		};

		uint8_t mask;
	} bEndpointAddress;

	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			usb_endpoint_type_e  type     : 2;
			usb_endpoint_sync_e  sync     : 2;
			usb_endpoint_usage_e usage    : 2;
			unsigned             reserved : 2;
		};

		uint8_t mask;
	} bmAttributes;

	uint16_t wMaxPacketSize;
	uint8_t  bInterval;
} usb_endpoint_descriptor_t;

#endif
