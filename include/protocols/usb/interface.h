#ifndef __PROTOCOLS_USB_INTERFACE_H__
#define __PROTCCOLS_USB_INTERFACE_H__

#include <stdint.h>

typedef struct
__attribute__((__packed__))
{
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bInterfaceNum;
	uint8_t bAlternateSetting;
	uint8_t bNumEndpoints;
	uint8_t bInterfaceClass;
	uint8_t bInterfaceSubClass;
	uint8_t bInterfaceProtocol;
	uint8_t iInterface;
} usb_interface_descriptor_t;

#endif
