#ifndef __PROTOCOLS_USB_REQUEST_H__
#define __PROTCCOLS_USB_REQUEST_H__

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
	USB_FEATURE_SELECTOR_EP_HALT         = 0x00,
	USB_FEATURE_SELECTOR_DEV_REMOTE_WKUP = 0x01,
	USB_FEATURE_SELECTOR_DEV_TEST_MODE   = 0x02,
	USB_FEATURE_SELECTOR_COUNT
} usb_feature_selector_e;


typedef enum
{
	USB_DESCRIPTOR_DEVICE                    = 1,
	USB_DESCRIPTOR_CONFIGURATION             = 2,
	USB_DESCRIPTOR_STRING                    = 3,
	USB_DESCRIPTOR_INTERFACE                 = 4,
	USB_DESCRIPTOR_ENDPOINT                  = 5,
	USB_DESCRIPTOR_DEVICE_QUALIFIER          = 6,
	USB_DESCRIPTOR_OTHER_SPEED_CONFIGURATION = 7,
	USB_DESCRIPTOR_INTERFACE_POWER           = 8,
	USB_DESCRIPTOR_ON_THE_GO                 = 9,
} usb_descriptor_e;

typedef enum
{
	USB_REQUEST_DIR_HOST_TO_DEVICE = 0,
	USB_REQUEST_DIR_DEVICE_TO_HOST = 1,
} usb_request_dir_e;

typedef enum
{
	USB_REQUEST_TYPE_STANDARD = 0,
	USB_REQUEST_TYPE_CLASS    = 1,
	USB_REQUEST_TYPE_VENDOR   = 2,
} usb_request_type_e;

typedef enum
{
	USB_REQUEST_RECIPIENT_DEVICE    = 0,
	USB_REQUEST_RECIPIENT_INTERFACE = 1,
	USB_REQUEST_RECIPIENT_ENDPOINT  = 2,
	USB_REQUEST_RECIPIENT_OTHER     = 3,
} usb_request_recipient_e;

typedef enum
{
	USB_REQUEST_GET_STATUS        =  0,
	USB_REQUEST_CLEAR_FEATURE     =  1,
	USB_REQUEST_SET_FEATURE       =  3,
	USB_REQUEST_SET_ADDRESS       =  5,
	USB_REQUEST_GET_DESCRIPTOR    =  6,
	USB_REQUEST_SET_DESCRIPTOR    =  7,
	USB_REQUEST_GET_CONFIGURATION =  8,
	USB_REQUEST_SET_CONFIGURATION =  9,
	USB_REQUEST_GET_INTERFACE     = 10,
	USB_REQUEST_SET_INTERFACE     = 11,
	USB_REQUEST_SYNCH_FRAME       = 12,
} usb_request_e;

typedef struct
__attribute__((__packed__))
{
	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			usb_request_recipient_e recipient : 5;
			usb_request_type_e      type      : 2;
			usb_request_dir_e       direction : 1;
		};
		uint8_t mask;
	} bmRequestType;
	usb_request_e bRequest : 8;
	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			uint16_t         index      : 8;
			usb_descriptor_e descType  : 8;
		};
		uint16_t mask;
	} wValue;
	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			uint16_t          epNum     : 4;
			uint16_t          res_6_4   : 3;
			usb_request_dir_e direction : 1;
			uint16_t          res_15_8  : 8;
		};
		uint16_t mask;
	} wIndex;
	uint16_t wLength;
} usb_setup_packet_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint16_t selfPow  :  1;
		uint16_t remWkup  :  1;
		uint16_t res_15_2  : 14;
	};

	struct
	__attribute__((__packed__))
	{
		uint16_t halt      :  1;
		uint16_t res_15_1  : 15;
	};

	uint16_t mask;
} usb_status_value_t;

#endif
