#ifndef __STDLIB_H__
#define __STDLIB_H__

#include <stddef.h>

void* malloc(size_t size);
void  free(void *ptr);

#endif
