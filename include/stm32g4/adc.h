#ifndef __STM32G4_ADC_H__
#define __STM32G4_ADC_H__

#include <stdbool.h>

#include <stm32g4/reg/adc.h>

void stm32_adc_group_reset(stm32g4_adc_group_e group);
void stm32_adc_group_enable(stm32g4_adc_group_e group, bool enable);

void stm32_adc_group_mode_set(stm32g4_adc_group_e group, stm32g4_adc_group_mode_e mode);
bool stm32_adc_group_clock_set(stm32g4_adc_group_e group, bool external, unsigned divide);
void stm32_adc_group_enable_internal(stm32g4_adc_group_e group, bool vbat, bool vts, bool vrefint);

bool stm32_adc_group_freq(stm32g4_adc_group_e group, unsigned* freq);

void stm32_adc_power(stm32g4_adc_e adc, bool enable);
bool stm32_adc_calibrate(stm32g4_adc_e adc, bool single, bool differential);

bool stm32_adc_channel_config(
	stm32g4_adc_e adc, unsigned channel,
	bool differential, unsigned sample_period);
bool stm32_adc_channel_sequence_set(
	stm32g4_adc_e adc, bool injected, unsigned count, unsigned* channel);

bool stm32_adc_channel_sample_freq(
	stm32g4_adc_e adc, unsigned channel, unsigned* freq);

bool stm32g4_adc_config(
	stm32g4_adc_e adc, unsigned resolution, unsigned oversample);

#endif
