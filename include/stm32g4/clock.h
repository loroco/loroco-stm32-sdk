#ifndef __STM32G4_CLOCK_H__
#define __STM32G4_CLOCK_H__

#include <stdbool.h>

typedef enum
{
	STM32G4_CLOCK_NONE = 0,

	STM32G4_CLOCK_HSE,
	STM32G4_CLOCK_HSI16,
	STM32G4_CLOCK_HSI16_KER,
	STM32G4_CLOCK_HSI48,
	STM32G4_CLOCK_LSE,
	STM32G4_CLOCK_LSI,
	STM32G4_CLOCK_I2S_CKIN,

	STM32G4_CLOCK_SYS,

	STM32G4_CLOCK_PLL,
	STM32G4_CLOCK_PLL_P,
	STM32G4_CLOCK_PLL_Q,
	STM32G4_CLOCK_PLL_R,

	STM32G4_CLOCK_MCO,
	STM32G4_CLOCK_LSCO,

	STM32G4_CLOCK_AHB,
	STM32G4_CLOCK_SYSTICK,
	STM32G4_CLOCK_APB1,
	STM32G4_CLOCK_APB2,

	STM32G4_CLOCK_RTC,

	STM32G4_CLOCK_USART1,
	STM32G4_CLOCK_USART2,
	STM32G4_CLOCK_USART3,
	STM32G4_CLOCK_UART4,
	STM32G4_CLOCK_UART5,
	STM32G4_CLOCK_LPUART1,
	STM32G4_CLOCK_I2C1,
	STM32G4_CLOCK_I2C2,
	STM32G4_CLOCK_I2C3,
	STM32G4_CLOCK_LPTIM1,
	STM32G4_CLOCK_SAI1,
	STM32G4_CLOCK_I2S23,
	STM32G4_CLOCK_FDCAN,
	STM32G4_CLOCK_CLK48,
	STM32G4_CLOCK_ADC12,
	STM32G4_CLOCK_ADC345,
	STM32G4_CLOCK_I2C4,
	STM32G4_CLOCK_QSPI,

	STM32G4_CLOCK_TIM1,
	STM32G4_CLOCK_TIM2,
	STM32G4_CLOCK_TIM3,
	STM32G4_CLOCK_TIM4,
	STM32G4_CLOCK_TIM5,
	STM32G4_CLOCK_TIM6,
	STM32G4_CLOCK_TIM7,
	STM32G4_CLOCK_TIM8,
	STM32G4_CLOCK_TIM15,
	STM32G4_CLOCK_TIM16,
	STM32G4_CLOCK_TIM17,
	STM32G4_CLOCK_TIM20,

	STM32G4_CLOCK_COUNT,
} stm32g4_clock_e;


bool stm32g4_clock_enable(stm32g4_clock_e clock, bool enable);
void stm32g4_clock_hse_set(unsigned freq, bool bypass);
void stm32g4_clock_lse_set(unsigned freq, bool bypass, unsigned drive);

void stm32g4_clock_hsi48_auto_trim_set(bool auto_trim);

bool stm32g4_clock_src_set(stm32g4_clock_e clock, stm32g4_clock_e src);
bool stm32g4_clock_div_set(stm32g4_clock_e clock, unsigned div);

bool stm32g4_clock_pll_set(
	stm32g4_clock_e src,
	unsigned m, unsigned n,
	unsigned p, unsigned q, unsigned r);

bool stm32g4_clock_is_enabled(stm32g4_clock_e clock);
bool stm32g4_clock_is_stable(stm32g4_clock_e clock);
bool stm32g4_clock_freq(stm32g4_clock_e clock, unsigned* freq);

bool stm32g4_clock_wait_stable(stm32g4_clock_e clock);

#endif
