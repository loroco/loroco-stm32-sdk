#ifndef __STM32G4_DMA_H__
#define __STM32G4_DMA_H__

#include <stdbool.h>
#include <stdint.h>

#include <stm32g4/reg/dma.h>

typedef enum
{
	STM32G4_DMA1 = 0,
	STM32G4_DMA2 = 1,
	STM32G4_DMA_COUNT
} stm32g4_dma_instance_e;

#define STM32G4_DMA_CHANNEL_COUNT 6

void stm32g4_dma_reset (stm32g4_dma_instance_e instance);
void stm32g4_dma_enable(stm32g4_dma_instance_e instance, bool enable);

bool stm32g4_dma_channel_config_set_full(
	stm32g4_dma_instance_e instance, uint8_t channel, unsigned data_count,
	uintptr_t p_address, uintptr_t m_address, bool p_inc, bool m_inc,
	stm32g4_dma_data_sz_e p_size, stm32g4_dma_data_sz_e m_size,
	stm32g4_dma_pl_e priority, stm32g4_dma_dir_e dir,
	bool circular, bool mem_to_mem,
	void (*tfr_compl)     (void*, stm32g4_dma_instance_e instance, uint8_t channel),
	void (*tfr_half_compl)(void*, stm32g4_dma_instance_e instance, uint8_t channel),
	void (*tfr_err)       (void*, stm32g4_dma_instance_e instance, uint8_t channel),
	void *user_data);

bool stm32g4_dma_channel_enable(
	stm32g4_dma_instance_e instance, uint8_t channel, bool enable);

bool stm32g4_dma_channel_is_enabled(
	stm32g4_dma_instance_e instance, uint8_t channel);


typedef enum
{
	STM32G4_DMA_MEM_REGION_USART1,
	STM32G4_DMA_MEM_REGION_COUNT,

	STM32G4_DMA_MEM_REGION_NONE
} stm32g4_dma_mem_region_e;

size_t stm32g4_dma_mem_reacquire(
	stm32g4_dma_mem_region_e region, uint8_t** data);
size_t stm32g4_dma_mem_acquire(
	stm32g4_dma_mem_region_e region, uint8_t** data);
void   stm32g4_dma_mem_release(stm32g4_dma_mem_region_e region);

#endif