#ifndef __STM32G4_FDCAN_H__
#define __STM32G4_FDCAN_H__

#include <stdbool.h>
#include <stdint.h>

#include <stm32g4/reg/fdcan.h>
#include <stm32g4/reg/fdcan_mram.h>
#include <stm32g4/error.h>

typedef enum
{
    STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE = STM32G4_ERROR_SPECIFIC,
    STM32G4_ERROR_FDCAN_INVALID_LENGTH,
    STM32G4_ERROR_FDCAN_FIFO_FULL,
} stm32g4_error_fdcan_e;

typedef enum
{
	FDCAN_UNIT_1,
	FDCAN_UNIT_COUNT
} fdcan_unit_e;

typedef enum
{
	FDCAN_FRAME_FORMAT_CLASSIC,
	FDCAN_FRAME_FORMAT_FD_NO_BR_SWITCHING,
	FDCAN_FRAME_FORMAT_FD_BR_SWITCHING,
	FDCAN_FRAME_FORMAT_COUNT
} fdcan_frame_format_e;

typedef enum
{
	FDCAN_OP_MODE_NORMAL,
	FDCAN_OP_MODE_RESTRICTED,
	FDCAN_OP_MODE_BUS_MONITORING,
	FDCAN_OP_MODE_EXT_LOOPBACK,
	FDCAN_OP_MODE_INT_LOOPBACK,
	FDCAN_OP_MODE_COUNT
} fdcan_op_mode_e;

typedef enum
{
	FDCAN_TX_MODE_FIFO,
	FDCAN_TX_MODE_QUEUE,
	FDCAN_TX_MODE_COUNT
} fdcan_tx_mode_e;

typedef struct
{
	uint32_t sync_jump_width;
	uint32_t prescaler;
	uint32_t timeseg1;
	uint32_t timeseg2;
} fdcan_time_config_t;

typedef enum
{
	FDCAN_FILTER_TYPE_STD,
	FDCAN_FILTER_TYPE_EXT,
	FDCAN_FILTER_TYPE_COUNT
} fdcan_filter_types_e;

typedef enum
{
	FDCAN_FILTER_NON_MATCH_ACCEPT_FIFO0 = 0,
	FDCAN_FILTER_NON_MATCH_ACCEPT_FIFO1 = 1,
	FDCAN_FILTER_NON_MATCH_REJECT       = 2
} fdcan_filter_non_matching_e;

typedef enum
{
	FDCAN_RX_FIFO_0,
	FDCAN_RX_FIFO_1,
	FDCAN_RX_FIFO_COUNT
} fdcan_rx_fifo_e;

typedef struct
{
	bool                        reject_remote_frames[FDCAN_FILTER_TYPE_COUNT];
	fdcan_filter_non_matching_e filter_non_match    [FDCAN_FILTER_TYPE_COUNT];
	bool                        overwrite_on_full   [FDCAN_RX_FIFO_COUNT];
	// TODO: do we need list size, or is it fixed for the G4?
	// uint8_t                     filter_list_size    [FDCAN_FILTER_TYPE_COUNT];
} fdcan_global_filter_config_t;

typedef enum
{
	FDCAN_TIMESTAMP_MODE_OFF  = 0,
	FDCAN_TIMESTAMP_MODE_TCP  = 1,
	FDCAN_TIMESTAMP_MODE_TIM3 = 2,
	FDCAN_TIMESTAMP_MODE_COUNT
} fdcan_timestamp_mode_e;

typedef struct
{
	uint8_t                prescaler;
	fdcan_timestamp_mode_e mode;
} fdcan_timestamp_config_t;

typedef enum
{
	FDCAN_TIMEOUT_MODE_CONTINUOUS = 0,
	FDCAN_TIMEOUT_MODE_TX_EVENT   = 1,
	FDCAN_TIMEOUT_MODE_RX_FIFO_0  = 2,
	FDCAN_TIMEOUT_MODE_RX_FIFO_1  = 3,
	FDCAN_TIMEOUT_MODE_COUNT
} fdcan_timeout_mode_e;

typedef struct
{
	uint16_t             period;
	fdcan_timeout_mode_e mode;
	bool                 enabled;
} fdcan_timeout_config_t;

typedef struct
{
	bool    enabled;
	uint8_t offset;
	uint8_t filter_window_length;
} fdcan_tx_delay_comp_config_t;

typedef struct
{
	uint8_t                      clkdiv;
	fdcan_frame_format_e         frame_format;
	fdcan_op_mode_e              op_mode;
	bool                         auto_retx;
	bool                         tx_pause;
	bool                         protocol_exception;
	fdcan_time_config_t          nominal_time;
	fdcan_time_config_t          data_time;
	fdcan_tx_mode_e              tx_mode;
	fdcan_global_filter_config_t global_filter;
	uint8_t                      ram_watchdog_start;
	fdcan_timestamp_config_t     timestamp;
	fdcan_timeout_config_t       timeout;
	fdcan_tx_delay_comp_config_t tx_delay_comp;
	bool                         iso_mode;
	bool                         edge_filtering;
} fdcan_attr_t;

// TODO: check if user passes NULL, that we can load the default config from
//       the device

typedef enum
{
	FDCAN_STATE_CLOSED,
	FDCAN_STATE_OPEN,
	FDCAN_STATE_BUSY,
	FDCAN_STATE_ERROR,
	FDCAN_STATE_COUNT
} fdcan_state_e;

typedef struct fdcan_handle_s fdcan_handle_t;

typedef void (*fdcan_cb)(fdcan_handle_t *handle);

typedef enum
{
	FDCAN_INT_LINE_0,
	FDCAN_INT_LINE_1,
	FDCAN_INT_LINE_COUNT
} fdcan_int_line_e;

typedef struct
{
	fdcan_cb         callback;
	fdcan_int_line_e int_line;
	bool             enabled;
} fdcan_callback_data_t;

typedef enum
{
	FDCAN_CB_ID_RX_FIFO0,
	FDCAN_CB_ID_RX_FIFO1,
	FDCAN_CB_ID_TX_HIGH_PRIORITY_MSG,
	FDCAN_CB_ID_TX_COMPLETE,
	FDCAN_CB_ID_TX_CANCELLED,
	FDCAN_CB_ID_TX_FIFO_EMPTY,
	FDCAN_CB_ID_TX_EVENT,
	FDCAN_CB_ID_TIMESTAMP_WRAP,
	FDCAN_CB_ID_MRAM_ACCESS_FAIL,
	FDCAN_CB_ID_TIMEOUT_OCCURRED,
	FDCAN_CB_ID_ERROR,
	FDCAN_CB_ID_PROTOCOL_ERROR,
	FDCAN_CB_ID_COUNT
} fdcan_callback_id_t;

typedef struct
{
	fdcan_callback_data_t cb_data[FDCAN_CB_ID_COUNT];
} fdcan_callback_set_t;

void fdcan_rx_fifo0_default_cb            (fdcan_handle_t *handle);
void fdcan_rx_fifo1_default_cb            (fdcan_handle_t *handle);
void fdcan_tx_high_priority_msg_default_cb(fdcan_handle_t *handle);
void fdcan_tx_complete_default_cb         (fdcan_handle_t *handle);
void fdcan_tx_cancelled_default_cb        (fdcan_handle_t *handle);
void fdcan_tx_fifo_empty_default_cb       (fdcan_handle_t *handle);
void fdcan_tx_event_default_cb            (fdcan_handle_t *handle);
void fdcan_timestamp_wrap_default_cb      (fdcan_handle_t *handle);
void fdcan_mram_access_fail_default_cb    (fdcan_handle_t *handle);
void fdcan_timeout_occurred_default_cb    (fdcan_handle_t *handle);
void fdcan_error_default_cb               (fdcan_handle_t *handle);
void fdcan_protocol_error_default_cb      (fdcan_handle_t *handle);

static const fdcan_callback_set_t fdcan_callback_default =
{
	.cb_data =
	{
		[FDCAN_CB_ID_RX_FIFO0]             =
		{
			.callback = fdcan_rx_fifo0_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_RX_FIFO1]             =
		{
			.callback = fdcan_rx_fifo1_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TX_HIGH_PRIORITY_MSG] =
		{
			.callback = fdcan_tx_high_priority_msg_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TX_COMPLETE]          =
		{
			.callback = fdcan_tx_complete_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TX_CANCELLED]         =
		{
			.callback = fdcan_tx_cancelled_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TX_FIFO_EMPTY]        =
		{
			.callback = fdcan_tx_fifo_empty_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TX_EVENT]             =
		{
			.callback = fdcan_tx_event_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TIMESTAMP_WRAP]       =
		{
			.callback = fdcan_timestamp_wrap_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_MRAM_ACCESS_FAIL]       =
		{
			.callback = fdcan_mram_access_fail_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_TIMEOUT_OCCURRED]     =
		{
			.callback = fdcan_timeout_occurred_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_ERROR]                =
		{
			.callback = fdcan_error_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		},
		[FDCAN_CB_ID_PROTOCOL_ERROR]         =
		{
			.callback = fdcan_protocol_error_default_cb,
			.int_line = FDCAN_INT_LINE_0,
			.enabled  = true
		}
	}
};

/* Initialisation methods */

fdcan_handle_t* fdcan_handle_open(
	fdcan_unit_e          id,
	fdcan_attr_t         *attr,
	fdcan_callback_set_t *callbacks);

stm32g4_error_e fdcan_handle_close(fdcan_handle_t* handle);

stm32g4_error_e fdcan_handle_set_powerdown_mode(
	fdcan_handle_t *handle, bool enable);

/* Note: can be used to assign new callbacks, disable existing ones,
 * and set interrupt lines. NULL entries are ignored */
stm32g4_error_e fdcan_handle_configure_callbacks(
	fdcan_handle_t       *handle,
	fdcan_callback_set_t  callbacks);

stm32g4_error_e fdcan_handle_deregister_callback(
	fdcan_handle_t      *handle,
	fdcan_callback_id_t  callback_id);

/* Config Methods */
// TODO: determine if we need config getters at all

// clkdiv between 1-30
stm32g4_error_e fdcan_handle_set_clkdiv(fdcan_handle_t *handle, uint8_t clkdiv);
stm32g4_error_e fdcan_handle_set_frame_format(
	fdcan_handle_t       *handle,
	fdcan_frame_format_e  frame_format);
stm32g4_error_e fdcan_handle_set_op_mode(
	fdcan_handle_t  *handle,
	fdcan_op_mode_e  op_mode);
stm32g4_error_e fdcan_handle_get_op_mode(
	fdcan_handle_t  *handle,
	fdcan_op_mode_e *op_mode);
stm32g4_error_e fdcan_handle_set_auto_retx(
	fdcan_handle_t *handle,
	bool            auto_rtx);
stm32g4_error_e fdcan_handle_set_tx_pause(
	fdcan_handle_t *handle,
	bool            tx_pause);
stm32g4_error_e fdcan_handle_set_protocol_exception(
	fdcan_handle_t *handle,
	bool            protocol_exception);
stm32g4_error_e fdcan_handle_set_nominal_time_config(
	fdcan_handle_t      *handle,
	fdcan_time_config_t  nominal_time);
stm32g4_error_e fdcan_handle_set_data_time_config(
	fdcan_handle_t      *handle,
	fdcan_time_config_t  data_time);
stm32g4_error_e fdcan_handle_set_tx_mode(
	fdcan_handle_t  *handle,
	fdcan_tx_mode_e  tx_mode);
stm32g4_error_e fdcan_handle_set_global_filter_config(
	fdcan_handle_t               *handle,
	fdcan_global_filter_config_t  global_filter);
// ram_watchdog_start == 0 disables the watchdog
stm32g4_error_e fdcan_handle_set_ram_watchdog_start(
	fdcan_handle_t *handle,
	uint8_t         ram_watchdog_start);
stm32g4_error_e fdcan_handle_set_timestamp_config(
	fdcan_handle_t           *handle,
	fdcan_timestamp_config_t  timestamp);
stm32g4_error_e fdcan_handle_set_timeout_config(
	fdcan_handle_t         *handle,
	fdcan_timeout_config_t  timeout);
stm32g4_error_e fdcan_handle_set_tx_delay_comp_config(
	fdcan_handle_t               *handle,
	fdcan_tx_delay_comp_config_t  tx_delay_comp);
stm32g4_error_e fdcan_handle_set_iso_mode(
	fdcan_handle_t *handle,
	bool            iso_mode);
stm32g4_error_e fdcan_handle_set_edge_filtering(
	fdcan_handle_t *handle,
	bool            edge_filtering);


/* Control Methods */

stm32g4_error_e fdcan_handle_get_state(fdcan_handle_t *handle, fdcan_state_e *state);
stm32g4_error_e fdcan_handle_start    (fdcan_handle_t *handle);
stm32g4_error_e fdcan_handle_stop     (fdcan_handle_t *handle);


typedef struct {
	uint32_t id;
	uint8_t  length_code;
	bool     bit_rate_switching;
	bool     fd_format;
	bool     store_tx_events;
	uint8_t  msg_marker;
	bool     remote_tx_req;
	bool     error_bit_tx_recessive;
} fdcan_msg_header_t;

stm32g4_error_e fdcan_handle_add_tx_msg_to_fifo(
	fdcan_handle_t                 *handle,
	fdcan_msg_header_t              header_data,
	const uint32_t                 *data,
	uint32_t                       *put_index);
stm32g4_error_e fdcan_handle_abort_tx(
	fdcan_handle_t *handle,
	uint32_t        buffer_index);
stm32g4_error_e fdcan_handle_get_tx_event(
	fdcan_handle_t                     *handle,
	stm32g4_fdcan_mram_tx_event_fifo_t *event);

stm32g4_error_e fdcan_handle_get_rx_msg_from_fifo(
	fdcan_handle_t               *handle,
	fdcan_rx_fifo_e               rx_fifo,
	stm32g4_fdcan_mram_rx_fifo_t *msg);

typedef enum
{
	FDCAN_MSG_STORAGE_INFO_NO_FIFO     = 0,
	FDCAN_MSG_STORAGE_INFO_FIFO_OVERUN = 1,
	FDCAN_MSG_STORAGE_INFO_FIFO_0      = 2,
	FDCAN_MSG_STORAGE_INFO_FIFO_1      = 3,
} fdcan_msg_storage_info_e;

typedef struct
{
	fdcan_filter_types_e     filter_type;
	uint32_t                 filter_match;
	fdcan_msg_storage_info_e storage_info;
	uint32_t                 buffer_index;
} fdcan_hp_msg_details_t;

stm32g4_error_e fdcan_handle_get_high_priority_msg_status(
	fdcan_handle_t         *handle,
	fdcan_hp_msg_details_t *hp_msg_details);

typedef enum
{
	FDCAN_PS_LEC_ERROR_NONE      = 0,
	FDCAN_PS_LEC_ERROR_STUFF     = 1,
	FDCAN_PS_LEC_ERROR_FORM      = 2,
	FDCAN_PS_LEC_ERROR_ACK       = 3,
	FDCAN_PS_LEC_ERROR_BIT1      = 4,
	FDCAN_PS_LEC_ERROR_BIT0      = 5,
	FDCAN_PS_LEC_ERROR_CRC       = 6,
	FDCAN_PS_LEC_ERROR_NO_CHANGE = 7,
} fdcan_ps_last_error_code_e;

typedef enum
{
	FDCAN_PS_ACT_SYNC = 0,
	FDCAN_PS_ACT_IDLE = 1,
	FDCAN_PS_ACT_RX   = 2,
	FDCAN_PS_ACT_TX   = 3,
} fdcan_ps_activity_e;

typedef struct
{
	fdcan_ps_last_error_code_e last_error;
	fdcan_ps_last_error_code_e last_data_error;
	fdcan_ps_activity_e        activity;
	bool                       error_passive;
	bool                       warning_status;
	bool                       bus_off_status;
	bool                       last_esi;
	bool                       last_brs;
	bool                       received_msg;
	bool                       protocol_exception_event;
	uint8_t                    tx_delay_comp;
} fdcan_procol_status_t;

stm32g4_error_e fdcan_handle_get_protocol_status(
	fdcan_handle_t        *handle,
	fdcan_procol_status_t *protocol_status);

typedef struct
{
	uint8_t tx_error_count; // max 255
	uint8_t rx_error_count; // max 127
	bool    rx_error_passive; // rx_error_count > 128
	uint8_t error_logging;
} fdcan_error_counters_t;

stm32g4_error_e fdcan_handle_get_error_counters(
	fdcan_handle_t         *handle,
	fdcan_error_counters_t *error_counters);

uint32_t fdcan_handle_get_rx_fifo_elem_count(
	fdcan_handle_t  *handle,
	fdcan_rx_fifo_e  fifo);
uint32_t fdcan_handle_get_rx_fifo_get_index(
	fdcan_handle_t  *handle,
	fdcan_rx_fifo_e  fifo);

uint32_t fdcan_handle_get_tx_fifo_space(fdcan_handle_t *handle);
bool     fdcan_handle_tx_fifo_is_full  (fdcan_handle_t *handle);
uint32_t fdcan_handle_get_tx_put_index (fdcan_handle_t *handle);
bool     fdcan_handle_tx_in_progress   (fdcan_handle_t *handle, uint32_t index);

#endif
