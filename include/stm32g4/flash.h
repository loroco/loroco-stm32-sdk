#ifndef __STM32G4_FLASH_H__
#define __STM32G4_FLASH_H__

#include <stdbool.h>

unsigned stm32g4_flash_latency_calc(unsigned freq, unsigned range);
bool     stm32g4_flash_latency_set(unsigned latency);
unsigned stm32g4_flash_latency_get(void);

#endif
