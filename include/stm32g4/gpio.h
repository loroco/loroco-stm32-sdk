#ifndef __STM32G4_GPIO_H__
#define __STM32G4_GPIO_H__

#include <stdbool.h>
#include <stdint.h>

#include <stm32g4/reg/gpio.h>
#include <stm32g4/reg/alt_func.h>

typedef enum
{
	STM32G4_GPIO_OTYPE_PUSH_PULL  = 0,
	STM32G4_GPIO_OTYPE_OPEN_DRAIN = 1,
} stm32g4_gpio_otype_e;

typedef enum
{
	STM32G4_GPIO_SPEED_LOW       = 0,
	STM32G4_GPIO_SPEED_MEDIUM    = 1,
	STM32G4_GPIO_SPEED_HIGH      = 2,
	STM32G4_GPIO_SPEED_VERY_HIGH = 3,
} stm32g4_gpio_speed_e;

typedef enum
{
	STM32G4_GPIO_PULL_NONE     = 0,
	STM32G4_GPIO_PULL_UP       = 1,
	STM32G4_GPIO_PULL_DOWN     = 2,
	STM32G4_GPIO_PULL_RESERVED = 3,
} stm32g4_gpio_pull_e;


void     stm32g4_gpio_port_reset(stm32g4_gpio_port_e port);
void     stm32g4_gpio_port_enable(stm32g4_gpio_port_e port, bool enable);
uint16_t stm32g4_gpio_port_read(stm32g4_gpio_port_e port);
void     stm32g4_gpio_port_write(stm32g4_gpio_port_e port, uint16_t value);
void     stm32g4_gpio_port_set_clear(stm32g4_gpio_port_e port, uint16_t set, uint16_t clear);
bool     stm32g4_gpio_port_lock(stm32g4_gpio_port_e port, uint16_t lock);


void stm32g4_gpio_pin_config_input(stm32g4_gpio_port_e port, unsigned pin, stm32g4_gpio_pull_e pull);
void stm32g4_gpio_pin_config_output(stm32g4_gpio_port_e port, unsigned pin,
	stm32g4_gpio_otype_e otype, stm32g4_gpio_speed_e speed, stm32g4_gpio_pull_e pull);
void stm32g4_gpio_pin_config_analog(stm32g4_gpio_port_e port, unsigned pin);
void stm32g4_gpio_pin_config_alternate(stm32g4_gpio_port_e port, unsigned pin, unsigned func);

bool stm32g4_gpio_pin_read(stm32g4_gpio_port_e port, unsigned pin);
void stm32g4_gpio_pin_write(stm32g4_gpio_port_e port, unsigned pin, bool value);

#endif
