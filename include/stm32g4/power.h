#ifndef __STM32G4_POWER_H__
#define __STM32G4_POWER_H__

#include <stdbool.h>

void     stm32g4_power_backup_write_protect_set(bool enable);
bool     stm32g4_power_backup_write_protect_get(void);

bool     stm32g4_power_voltage_scale_calc(unsigned freq, unsigned* range, bool* boost);
bool     stm32g4_power_voltage_scale_set(unsigned range);
unsigned stm32g4_power_voltage_scale_get(void);
void     stm32g4_power_voltage_scale_wait(void);

void     stm32g4_power_voltage_boost_set(bool boost);
bool     stm32g4_power_voltage_boost_get(void);

#endif
