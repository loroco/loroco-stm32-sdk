#ifndef __STM32G4_RCC_H__
#define __STM32G4_RCC_H__

#include <stdbool.h>

typedef enum
{
	STM32G4_RCC_DMA1          = ((0 * 32) +  0),
	STM32G4_RCC_DMA2          = ((0 * 32) +  1),
	STM32G4_RCC_DMAMUX1       = ((0 * 32) +  2),
	STM32G4_RCC_CORDIC        = ((0 * 32) +  3),
	STM32G4_RCC_FMAC          = ((0 * 32) +  4),
	STM32G4_RCC_FLASH         = ((0 * 32) +  8),
	STM32G4_RCC_CRC           = ((0 * 32) + 12),

	STM32G4_RCC_GPIOA         = ((1 * 32) +  0),
	STM32G4_RCC_GPIOB         = ((1 * 32) +  1),
	STM32G4_RCC_GPIOC         = ((1 * 32) +  2),
	STM32G4_RCC_GPIOD         = ((1 * 32) +  3),
	STM32G4_RCC_GPIOE         = ((1 * 32) +  4),
	STM32G4_RCC_GPIOF         = ((1 * 32) +  5),
	STM32G4_RCC_GPIOG         = ((1 * 32) +  6),
	STM32G4_RCC_ADC12         = ((1 * 32) + 13),
	STM32G4_RCC_ADC345        = ((1 * 32) + 14),
	STM32G4_RCC_DAC1          = ((1 * 32) + 16),
	STM32G4_RCC_DAC2          = ((1 * 32) + 17),
	STM32G4_RCC_DAC3          = ((1 * 32) + 18),
	STM32G4_RCC_DAC4          = ((1 * 32) + 19),
	STM32G4_RCC_AES           = ((1 * 32) + 24),
	STM32G4_RCC_RNG           = ((1 * 32) + 26),

	STM32G4_RCC_FMC           = ((2 * 32) + 0),
	STM32G4_RCC_QSPI          = ((2 * 32) + 8),

	STM32G4_RCC_TIM2          = ((4 * 32) +  0),
	STM32G4_RCC_TIM3          = ((4 * 32) +  1),
	STM32G4_RCC_TIM4          = ((4 * 32) +  2),
	STM32G4_RCC_TIM5          = ((4 * 32) +  3),
	STM32G4_RCC_TIM6          = ((4 * 32) +  4),
	STM32G4_RCC_TIM7          = ((4 * 32) +  5),
	STM32G4_RCC_CRS           = ((4 * 32) +  8),
	STM32G4_RCC_SPI2          = ((4 * 32) + 14),
	STM32G4_RCC_SPI3          = ((4 * 32) + 15),
	STM32G4_RCC_USART2        = ((4 * 32) + 17),
	STM32G4_RCC_USART3        = ((4 * 32) + 18),
	STM32G4_RCC_UART4         = ((4 * 32) + 19),
	STM32G4_RCC_UART5         = ((4 * 32) + 20),
	STM32G4_RCC_I2C1          = ((4 * 32) + 21),
	STM32G4_RCC_I2C2          = ((4 * 32) + 22),
	STM32G4_RCC_USB           = ((4 * 32) + 23),
	STM32G4_RCC_FDCAN         = ((4 * 32) + 25),
	STM32G4_RCC_PWR           = ((4 * 32) + 28),
	STM32G4_RCC_I2C3          = ((4 * 32) + 30),
	STM32G4_RCC_LPTIM1        = ((4 * 32) + 31),

	STM32G4_RCC_LPUART1       = ((5 * 32) + 0),
	STM32G4_RCC_I2C4          = ((5 * 32) + 1),
	STM32G4_RCC_UCPD1         = ((5 * 32) + 8),// TODO: work out duplication in datasheet here

	STM32G4_RCC_SYSCFG        = ((6 * 32) +  0),
	STM32G4_RCC_TIM1          = ((6 * 32) + 11),
	STM32G4_RCC_SPI1          = ((6 * 32) + 12),
	STM32G4_RCC_TIM8          = ((6 * 32) + 13),
	STM32G4_RCC_USART1        = ((6 * 32) + 14),
	STM32G4_RCC_SPI4          = ((6 * 32) + 15),
	STM32G4_RCC_TIM15         = ((6 * 32) + 16),
	STM32G4_RCC_TIM16         = ((6 * 32) + 17),
	STM32G4_RCC_TIM17         = ((6 * 32) + 18),
	STM32G4_RCC_TIM20         = ((6 * 32) + 20),
	STM32G4_RCC_SAI1          = ((6 * 32) + 21),
	STM32G4_RCC_HRTIM         = ((6 * 32) + 26),
} stm32g4_rcc_unit_e;

void stm32g4_rcc_reset (stm32g4_rcc_unit_e unit);
void stm32g4_rcc_enable(stm32g4_rcc_unit_e unit, bool enable);

#endif
