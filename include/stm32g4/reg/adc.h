#ifndef __STM32G4_REG_ADC_H__
#define __STM32G4_REG_ADC_H__

#include <stdint.h>
#include <stddef.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       acrdy     :  1;
		uint32_t       eosmp     :  1;
		uint32_t       eoc       :  1;
		uint32_t       eos       :  1;
		uint32_t       ovr       :  1;
		uint32_t       jeoc      :  1;
		uint32_t       jeos      :  1;
		uint32_t       awd1      :  1;
		uint32_t       awd2      :  1;
		uint32_t       awd3      :  1;
		uint32_t       jqovf     :  1;
		const uint32_t res_31_11 : 21;
	};

	uint32_t mask;
} stm32g4_adc_ir_t;

typedef stm32g4_adc_ir_t stm32g4_adc_isr_t;
typedef stm32g4_adc_ir_t stm32g4_adc_ier_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       aden     :  1;
		uint32_t       addis    :  1;
		uint32_t       adstart  :  1;
		uint32_t       jadstart :  1;
		uint32_t       adstp    :  1;
		uint32_t       jadstp   :  1;
		const uint32_t res_27_6 : 22;
		uint32_t       advregen :  1;
		uint32_t       deeppwd  :  1;
		uint32_t       adcaldif :  1;
		uint32_t       adcal    :  1;
	};

	uint32_t mask;
} stm32g4_adc_cr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       dmaen   : 1;
		uint32_t       dmacfg  : 1;
		const uint32_t res_2   : 1;
		uint32_t       res     : 2;
		uint32_t       extsel0 : 1;
		uint32_t       extsel1 : 1;
		uint32_t       extsel2 : 1;
		uint32_t       extsel3 : 1;
		uint32_t       extsel4 : 1;
		uint32_t       exten   : 2;
		uint32_t       ovrmod  : 1;
		uint32_t       cont    : 1;
		uint32_t       autdly  : 1;
		uint32_t       align   : 1;
		uint32_t       discen  : 1;
		uint32_t       discnum : 3;
		uint32_t       jdiscen : 1;
		uint32_t       jqm     : 1;
		uint32_t       awd1sgl : 1;
		uint32_t       awd1en  : 1;
		uint32_t       jawden  : 1;
		uint32_t       jauto   : 1;
		uint32_t       awd1ch  : 5;
		uint32_t       jqdis   : 1;
	};

	uint32_t mask;
} stm32g4_adc_cfgr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       rovse     : 1;
		uint32_t       jovse     : 1;
		uint32_t       ovsr      : 3;
		uint32_t       ovss      : 4;
		uint32_t       trovs     : 1;
		uint32_t       rovsm     : 1;
		const uint32_t res_15_11 : 5;
		uint32_t       gcomp     : 1;
		const uint32_t res_24_17 : 8;
		uint32_t       swtrig    : 1;
		uint32_t       bulb      : 1;
		uint32_t       smptrig   : 1;
		const uint32_t res_31_28 : 4;
	};

	uint32_t mask;
} stm32g4_adc_cfgr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       smp0    : 3;
		uint32_t       smp1    : 3;
		uint32_t       smp2    : 3;
		uint32_t       smp3    : 3;
		uint32_t       smp4    : 3;
		uint32_t       smp5    : 3;
		uint32_t       smp6    : 3;
		uint32_t       smp7    : 3;
		uint32_t       smp8    : 3;
		uint32_t       smp9    : 3;
		const uint32_t res_30  : 1;
		uint32_t       smpplus : 1;
	};

	uint32_t mask;
} stm32g4_adc_smpr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       smp10     : 3;
		uint32_t       smp11     : 3;
		uint32_t       smp12     : 3;
		uint32_t       smp13     : 3;
		uint32_t       smp14     : 3;
		uint32_t       smp15     : 3;
		uint32_t       smp16     : 3;
		uint32_t       smp17     : 3;
		uint32_t       smp18     : 3;
		const uint32_t res_31_27 : 5;
	};

	uint32_t mask;
} stm32g4_adc_smpr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       lt1       : 12;
		uint32_t       awdfilt   :  3;
		const uint32_t res_15    :  1;
		uint32_t       ht1       : 12;
		const uint32_t res_31_28 :  4;
	};

	uint32_t mask;
} stm32g4_adc_tr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       lt2       : 8;
		const uint32_t res_15_8  : 8;
		uint32_t       ht2       : 8;
		const uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_adc_tr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       lt3       : 8;
		const uint32_t res_15_8  : 8;
		uint32_t       ht3       : 8;
		const uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_adc_tr3_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       l         : 4;
		const uint32_t res_5_4   : 2;
		uint32_t       sq1       : 5;
		const uint32_t res_11    : 1;
		uint32_t       sq2       : 5;
		const uint32_t res_17    : 1;
		uint32_t       sq3       : 5;
		const uint32_t res_23    : 1;
		uint32_t       sq4       : 5;
		const uint32_t res_31_29 : 3;
	};

	uint32_t mask;
} stm32g4_adc_sqr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       sq5       : 5;
		const uint32_t res_5     : 1;
		uint32_t       sq6       : 5;
		const uint32_t res_11    : 1;
		uint32_t       sq7       : 5;
		const uint32_t res_17    : 1;
		uint32_t       sq8       : 5;
		const uint32_t res_23    : 1;
		uint32_t       sq9       : 5;
		const uint32_t res_31_29 : 3;
	};

	uint32_t mask;
} stm32g4_adc_sqr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       sq10      : 5;
		const uint32_t res_5     : 1;
		uint32_t       sq11      : 5;
		const uint32_t res_11    : 1;
		uint32_t       sq12      : 5;
		const uint32_t res_17    : 1;
		uint32_t       sq13      : 5;
		const uint32_t res_23    : 1;
		uint32_t       sq14      : 5;
		const uint32_t res_31_29 : 3;
	};

	uint32_t mask;
} stm32g4_adc_sqr3_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       sq15      :  5;
		const uint32_t res_5     :  1;
		uint32_t       sq16      :  5;
		const uint32_t res_31_11 : 21;
	};

	uint32_t mask;
} stm32g4_adc_sqr4_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       jl      : 2;
		uint32_t       jextsel : 5;
		uint32_t       jexten  : 2;
		uint32_t       jsq1    : 5;
		const uint32_t res_14  : 1;
		uint32_t       jsq2    : 5;
		const uint32_t res_20  : 1;
		uint32_t       jsq3    : 5;
		const uint32_t res_26  : 1;
		uint32_t       jsq4    : 5;
	};

	uint32_t mask;
} stm32g4_adc_jsqr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       offset    : 12;
		const uint32_t res_23_12 : 12;
		uint32_t       offsetpos :  1;
		uint32_t       saten     :  1;
		uint32_t       offset_ch :  5;
		uint32_t       offset_en :  1;
	};

	uint32_t mask;
} stm32g4_adc_ofr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       calfract_s : 7;
		const uint32_t res_15_7   : 9;
		uint32_t       calfract_d : 7;
		const uint32_t res_31_23  : 9;
	};

	uint32_t mask;
} stm32g4_adc_calfract_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t adrdy_mst : 1;
		const uint32_t eosmp_mst : 1;
		const uint32_t eoc_mst   : 1;
		const uint32_t eos_mst   : 1;
		const uint32_t ovr_mst   : 1;
		const uint32_t jeoc_mst  : 1;
		const uint32_t jeos_mst  : 1;
		const uint32_t awd1_mst  : 1;
		const uint32_t awd2_mst  : 1;
		const uint32_t awd3_mst  : 1;
		const uint32_t jqovf_mst : 1;
		const uint32_t res_15_11 : 5;
		const uint32_t adrdy_slv : 1;
		const uint32_t eosmp_slv : 1;
		const uint32_t eoc_slv   : 1;
		const uint32_t eos_slv   : 1;
		const uint32_t ovr_slv   : 1;
		const uint32_t jeoc_slv  : 1;
		const uint32_t jeos_slv  : 1;
		const uint32_t awd1_slv  : 1;
		const uint32_t awd2_slv  : 1;
		const uint32_t awd3_slv  : 1;
		const uint32_t jqovf_slv : 1;
		const uint32_t res_31_27 : 5;
	};

	uint32_t mask;
} stm32g4_adc_csr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t rdata_mst : 16;
		const uint32_t rdata_slv : 16;
	};

	uint32_t mask;
} stm32g4_adc_cdr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       dual      : 5;
		const uint32_t res_7_5   : 3;
		uint32_t       delay     : 4;
		const uint32_t res_12    : 1;
		uint32_t       dmacfg    : 1;
		uint32_t       mdma      : 2;
		uint32_t       ckmode    : 2;
		uint32_t       presc     : 4;
		uint32_t       vrefen    : 1;
		uint32_t       vsensesel : 1;
		uint32_t       vbatsel   : 1;
		const uint32_t res_31_25 : 7;
	};

	uint32_t mask;
} stm32g4_adc_ccr_t;

typedef struct
__attribute__((__packed__))
{
	volatile uint32_t       isr;
	volatile uint32_t       ier;
	volatile uint32_t       cr;
	volatile uint32_t       cfgr;
	volatile uint32_t       cfgr2;
	volatile uint32_t       smpr1;
	volatile uint32_t       smpr2;
    volatile const uint32_t res_7;
    volatile uint32_t       tr1;
    volatile uint32_t       tr2;
    volatile uint32_t       tr3;
    volatile const uint32_t res_11;
    volatile uint32_t       sqr1;
    volatile uint32_t       sqr2;
    volatile uint32_t       sqr3;
    volatile uint32_t       sqr4;
    volatile const uint32_t dr;
    volatile const uint32_t res_18_17[2];
    volatile uint32_t       jsqr;
	volatile const uint32_t res_23_20[4];
	volatile uint32_t       ofr[4];
	volatile const uint32_t res_31_28[4];
	volatile const uint32_t jdr[4];
	volatile const uint32_t res_39_36[4];
	volatile uint32_t       awd2cr;
	volatile uint32_t       awd3cr;
	volatile const uint32_t res_43_42[2];
	volatile uint32_t       difsel;
	volatile uint32_t       calfact;
	volatile const uint32_t res_47_46[2];
	volatile uint32_t       gcomp;
	volatile const uint32_t res_63_49[15];
} stm32g4_adc_t;

typedef struct
__attribute__((__packed__))
{
    volatile const uint32_t csr;
    volatile const uint32_t res_1;
    volatile uint32_t       ccr;
    volatile const uint32_t cdr;
    volatile const uint32_t res_63_4[60];
} stm32g4_adc_common_t;

typedef struct
__attribute__((__packed__))
{
	volatile stm32g4_adc_t        adc[3];
	volatile stm32g4_adc_common_t common;
} stm32g4_adc_global_t;


static volatile uint16_t * const stm32g4_adc_ts_cal[2] =
{
	(volatile uint16_t*)0x1FFF75A8,
	(volatile uint16_t*)0x1FFF75CA,
};

static volatile uint16_t * const stm32g4_adc_vrefint
	= (volatile uint16_t*)0x1FFF75AA;

static volatile stm32g4_adc_global_t * const stm32g4_adc_global[2] =
{
	(volatile stm32g4_adc_global_t*)0x50000000,
	(volatile stm32g4_adc_global_t*)0x50000400,
};

#define STM32G4_ADC_COUNT          5
#define STM32G4_ADC_GROUP_COUNT    2
#define STM32G4_ADC_PER_GROUP      3
#define STM32G4_ADC_CHANNEL_COUNT 19

typedef enum
{
	STM32G4_ADC1 = 0,
	STM32G4_ADC2,
	STM32G4_ADC3,
	STM32G4_ADC4,
	STM32G4_ADC5,
} stm32g4_adc_e;

typedef enum
{
	STM32G4_ADC12 = 0 ,
	STM32G4_ADC345,
} stm32g4_adc_group_e;

#define STM32G4_ADC_GROUP(x) ((x) <= STM32G4_ADC2 ? STM32G4_ADC12 : STM32G4_ADC345)
#define STM32G4_ADC_GROUP_INDEX(x) ((x) <= STM32G4_ADC2 ? (x) : ((x) - STM32G4_ADC3))

static inline volatile stm32g4_adc_t * stm32g4_adc(stm32g4_adc_e unit)
{
	if (unit >= STM32G4_ADC_COUNT) return NULL;
	return &stm32g4_adc_global[STM32G4_ADC_GROUP(unit)]->adc[STM32G4_ADC_GROUP_INDEX(unit)];
}

#define STM32G4_ADC_FIELD_READ(u, r, f) \
	((stm32g4_adc_##r##_t)stm32g4_adc(u)->r).f
#define STM32G4_ADC_FIELD_WRITE(u, r, f, v) \
	do { stm32g4_adc_##r##_t r = { .mask = stm32g4_adc(u)->r }; \
	r.f = v; stm32g4_adc(u)->r = r.mask; } while (0)

#define STM32G4_ADC_OFR_FIELD_READ(u, o, f) \
	((stm32g4_adc_ofr_t)stm32g4_adc(u)->ofr[o]).f
#define STM32G4_ADC_OFR_FIELD_WRITE(u, o, f, v) \
	do { stm32g4_adc_ofr_t ofr = { .mask = stm32g4_adc(u)->ofr[o] }; \
	ofr.f = v; stm32g4_adc(u)->ofr[o] = ofr.mask; } while (0)

#define STM32G4_ADC_COMMON_FIELD_READ(g, r, f) \
	((stm32g4_adc_##r##_t)stm32g4_adc_global[g]->common.r).f
#define STM32G4_ADC_COMMON_FIELD_WRITE(g, r, f, v) \
	do { stm32g4_adc_##r##_t r = { .mask = stm32g4_adc_global[g]->common.r }; \
	r.f = v; stm32g4_adc_global[g]->common.r = r.mask; } while (0)

typedef enum
{
	STM32G4_ADC1_CHANNEL_VSSA    =  0,
	STM32G4_ADC1_CHANNEL_OPAMP1  = 13,
	STM32G4_ADC1_CHANNEL_TS      = 16,
	STM32G4_ADC1_CHANNEL_BAT     = 17,
	STM32G4_ADC1_CHANNEL_VREFINT = 18,
} stm32g4_adc1_channel_e;

typedef enum
{
	STM32G4_ADC2_CHANNEL_VSSA    =  0,
	STM32G4_ADC2_CHANNEL_OPAMP2  = 16,
	STM32G4_ADC2_CHANNEL_OPAMP3  = 18,
} stm32g4_adc2_channel_e;

typedef enum
{
	STM32G4_ADC3_CHANNEL_VSSA    =  0,
	STM32G4_ADC3_CHANNEL_OPAMP3  = 13,
	STM32G4_ADC3_CHANNEL_BAT     = 17,
	STM32G4_ADC3_CHANNEL_VREFINT = 18,
} stm32g4_adc3_channel_e;

typedef enum
{
	STM32G4_ADC4_CHANNEL_VSSA    =  0,
	STM32G4_ADC4_CHANNEL_OPAMP6  = 17,
	STM32G4_ADC4_CHANNEL_VREFINT = 18,
} stm32g4_adc4_channel_e;

typedef enum
{
	STM32G4_ADC5_CHANNEL_VSSA    =  0,
	STM32G4_ADC5_CHANNEL_OPAMP5  =  3,
	STM32G4_ADC5_CHANNEL_TS      =  4,
	STM32G4_ADC5_CHANNEL_OPAMP4  =  5,
	STM32G4_ADC5_CHANNEL_BAT     = 17,
	STM32G4_ADC5_CHANNEL_VREFINT = 18,
} stm32g4_adc5_channel_e;

typedef enum
{
	STM32G4_ADC_GROUP_MODE_INDEPENDENT           = 0,
	STM32G4_ADC_GROUP_MODE_COMB_REG_SIM_INJ_SIM  = 1,
	STM32G4_ADC_GROUP_MODE_COMB_REG_SIM_ALT_TRIG = 2,
	STM32G4_ADC_GROUP_MODE_COMB_INT_INJ_SIM      = 3,
	STM32G4_ADC_GROUP_MODE_INJ_SIM               = 5,
	STM32G4_ADC_GROUP_MODE_REG_SIM               = 6,
	STM32G4_ADC_GROUP_MODE_INT                   = 7,
	STM32G4_ADC_GROUP_MODE_ALT_TRIG              = 9,
} stm32g4_adc_group_mode_e;

#define STM32G4_TADCVREG_STUP_US 20

#endif
