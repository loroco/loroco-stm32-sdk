#ifndef __STM32G4_REG_CRS_H__
#define __STM32G4_REG_CRS_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       syncokie   :  1;
		uint32_t       syncwarnie :  1;
		uint32_t       errie      :  1;
		uint32_t       esyncie    :  1;
		const uint32_t res_3      :  1;
		uint32_t       cen        :  1;
		uint32_t       autotrimen :  1;
		uint32_t       swsync     :  1;
		uint32_t       trim       :  7;
		const uint32_t res_31_15  : 17;
	};

	uint32_t mask;
} stm32g4_crs_cr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       reload     : 16;
		uint32_t       felim      :  8;
		uint32_t       syncdiv    :  3;
		const uint32_t res_27     :  1;
		uint32_t       syncsrc    :  2;
		const uint32_t res_30     :  1;
		uint32_t       syncpol    :  1;
	};

	uint32_t mask;
} stm32g4_crs_cfgr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t syncokf   :  1;
		const uint32_t syncwarnf :  1;
		const uint32_t errf      :  1;
		const uint32_t esyncf    :  1;
		const uint32_t res_7_4   :  4;
		const uint32_t syncerr   :  1;
		const uint32_t syncmiss  :  1;
		const uint32_t trimovf   :  1;
		const uint32_t res_14_11 :  4;
		const uint32_t fedir     :  1;
		const uint32_t fecap     : 16;
	};

	uint32_t mask;
} stm32g4_crs_isr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       syncokc   :  1;
		uint32_t       syncwarnc :  1;
		uint32_t       errc      :  1;
		uint32_t       esyncc    :  1;
		const uint32_t res_31_4  : 28;
	};

	uint32_t mask;
} stm32g4_crs_icr_t;

typedef struct
{
	volatile uint32_t       cr;
	volatile uint32_t       cfgr;
	volatile const uint32_t isr;
	volatile uint32_t       icr;
} stm32g4_crs_t;

static volatile stm32g4_crs_t * const stm32g4_crs
	= (volatile stm32g4_crs_t*)0x40023000;

#define STM32G4_CRS_FIELD_READ(r, f) \
	((stm32g4_crs_##r##_t)stm32g4_crs->r).f
#define STM32G4_CRS_FIELD_WRITE(r, f, v) \
	do { stm32g4_crs_##r##_t r = { .mask = stm32g4_crs->r }; \
	r.f = v; stm32g4_crs->r = r.mask; } while (0)

#endif
