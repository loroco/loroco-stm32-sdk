#ifndef __STM32G4_REG_DMA_H__
#define __STM32G4_REG_DMA_H__

#include <stdint.h>
#include <stddef.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t gif1   : 1;
		uint32_t tcif1  : 1;
		uint32_t htif1  : 1;
		uint32_t teif1  : 1;
		uint32_t gif2   : 1;
		uint32_t tcif2  : 1;
		uint32_t htif2  : 1;
		uint32_t teif2  : 1;
		uint32_t gif3   : 1;
		uint32_t tcif3  : 1;
		uint32_t htif3  : 1;
		uint32_t teif3  : 1;
		uint32_t gif4   : 1;
		uint32_t tcif4  : 1;
		uint32_t htif4  : 1;
		uint32_t teif4  : 1;
		uint32_t gif5   : 1;
		uint32_t tcif5  : 1;
		uint32_t htif5  : 1;
		uint32_t teif5  : 1;
		uint32_t gif6   : 1;
		uint32_t tcif6  : 1;
		uint32_t htif6  : 1;
		uint32_t teif6  : 1;
		uint32_t gif7   : 1;
		uint32_t tcif7  : 1;
		uint32_t htif7  : 1;
		uint32_t teif7  : 1;
		uint32_t gif8   : 1;
		uint32_t tcif8  : 1;
		uint32_t htif8  : 1;
		uint32_t teif8  : 1;
	};

	uint32_t mask;
} stm32g4_dma_isr_t;

typedef stm32g4_dma_isr_t stm32g4_dma_ifcr_t;

typedef enum
{
    STM32G4_DMA_PL_LOW   = 0,
    STM32G4_DMA_PL_MED   = 1,
    STM32G4_DMA_PL_HIGH  = 2,
    STM32G4_DMA_PL_VHIGH = 3,
} stm32g4_dma_pl_e;

typedef enum
{
	STM32G4_DMA_DIR_PER_MEM = 0,
	STM32G4_DMA_DIR_MEM_PER = 1
} stm32g4_dma_dir_e;

typedef enum
{
	STM32G4_DMA_DATA_SZ_8  = 0,
	STM32G4_DMA_DATA_SZ_16 = 1,
	STM32G4_DMA_DATA_SZ_32 = 2,
	STM32G4_DMA_DATA_SZ_COUNT
} stm32g4_dma_data_sz_e;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       en        :  1;
		uint32_t       tcie      :  1;
		uint32_t       htie      :  1;
		uint32_t       teie      :  1;
		uint32_t       dir       :  1;
		uint32_t       circ      :  1;
		uint32_t       pinc      :  1;
		uint32_t       minc      :  1;
		uint32_t       psize     :  2;
		uint32_t       msize     :  2;
		uint32_t       pl        :  2;
		uint32_t       mem2mem   :  1;
		const uint32_t res_31_15 : 17;
	};

	uint32_t mask;
} stm32g4_dma_ccr_t;


// TODO: can we have an array of channels somehow?
typedef struct
__attribute__((__packed__))
{
	volatile uint32_t       isr;
	volatile uint32_t       ifcr;
	volatile uint32_t       ccr1;
	volatile uint32_t       cndtr1;
	volatile uint32_t       cpar1;
	volatile uint32_t       cmar1;
	volatile const uint32_t res_6;
	volatile uint32_t       ccr2;
	volatile uint32_t       cndtr2;
	volatile uint32_t       cpar2;
	volatile uint32_t       cmar2;
	volatile const uint32_t res_11;
	volatile uint32_t       ccr3;
	volatile uint32_t       cndtr3;
	volatile uint32_t       cpar3;
	volatile uint32_t       cmar3;
	volatile const uint32_t res_16;
	volatile uint32_t       ccr4;
	volatile uint32_t       cndtr4;
	volatile uint32_t       cpar4;
	volatile uint32_t       cmar4;
	volatile const uint32_t res_21;
	volatile uint32_t       ccr5;
	volatile uint32_t       cndtr5;
	volatile uint32_t       cpar5;
	volatile uint32_t       cmar5;
	volatile const uint32_t res_26;
	volatile uint32_t       ccr6;
	volatile uint32_t       cndtr6;
	volatile uint32_t       cpar6;
	volatile uint32_t       cmar6;
	volatile const uint32_t res_31;
	volatile uint32_t       ccr7;
	volatile uint32_t       cndtr7;
	volatile uint32_t       cpar7;
	volatile uint32_t       cmar7;
	volatile const uint32_t res_36;
	volatile uint32_t       ccr8;
	volatile uint32_t       cndtr8;
	volatile uint32_t       cpar8;
	volatile uint32_t       cmar8;
} stm32g4_dma_t;

static volatile stm32g4_dma_t * const stm32g4_dma[2] =
{
	(volatile stm32g4_dma_t*)0x40020000,
	(volatile stm32g4_dma_t*)0x40020400,
};

#define STM32G4_DMA_FIELD_READ(u, r, f) \
	((stm32g4_dma_##r##_t)stm32g4_dma[u]->r).f
#define STM32G4_DMA_FIELD_WRITE(u, r, f, v) \
	do { stm32g4_dma_##r##_t r = { .mask = stm32g4_dma[u]->r }; \
	r.f = v; stm32g4_dma[u]->r = r.mask; } while (0)

#endif
