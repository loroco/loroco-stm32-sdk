#ifndef __STM32G4_REG_DMAMUX_H__
#define __STM32G4_REG_DMAMUX_H__

#include <stdint.h>
#include <stddef.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t dmareq_id  : 7;
		const uint32_t res_7      : 1;
		uint32_t soie       : 1;
		uint32_t ege        : 1;
		const uint32_t res_15_10  : 6;
		uint32_t se         : 1;
		uint32_t spol       : 2;
		uint32_t nbreq      : 5;
		uint32_t sync_id    : 5;
		const uint32_t res_31_29  : 3;
	};

	uint32_t mask;
} stm32g4_dmamux_cxcr_t;


typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t sof0      :  1;
		uint32_t sof1      :  1;
		uint32_t sof2      :  1;
		uint32_t sof3      :  1;
		uint32_t sof4      :  1;
		uint32_t sof5      :  1;
		uint32_t sof6      :  1;
		uint32_t sof7      :  1;
		uint32_t sof8      :  1;
		uint32_t sof9      :  1;
		uint32_t sof10     :  1;
		uint32_t sof11     :  1;
		uint32_t sof12     :  1;
		uint32_t sof13     :  1;
		uint32_t sof14     :  1;
		uint32_t sof15     :  1;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_dmamux_csr_t;

typedef stm32g4_dmamux_csr_t stm32g4_dmamux_cfr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t sid_id    : 5;
		const uint32_t res_7_5   : 3;
		uint32_t oie       : 1;
		const uint32_t res_15_9  : 7;
		uint32_t ge        : 1;
		uint32_t gpol      : 2;
		uint32_t gnbreq    : 5;
		const uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_dmamux_rgxcr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t of0      : 1;
		uint32_t of1      : 1;
		uint32_t of2      : 1;
		uint32_t of3      : 1;
		const uint32_t res_31_4 : 28;
	};

	uint32_t mask;
} stm32g4_dmamux_rgsr_t;

typedef stm32g4_dmamux_rgsr_t stm32g4_dmamux_rgcfr_t;

typedef struct
__attribute__((__packed__))
{
	volatile uint32_t cxcr[16];
	volatile const uint32_t res_31_15[16];
	volatile uint32_t csr;
	volatile uint32_t ccfr;
	volatile const uint32_t res_63_34[30];
	volatile uint32_t rgxcr[4];
	volatile const uint32_t res_79_68[12];
	volatile uint32_t rgsr;
	volatile uint32_t rgcfr;
	volatile const uint32_t res_255_82[174];
} stm32g4_dmamux_t;

static volatile stm32g4_dmamux_t * const stm32g4_dmamux =
	(volatile stm32g4_dmamux_t*)0x40020800;

#define STM32G4_DMAMUX_FIELD_READ(r, f) \
	((stm32g4_dmamux_##r##_t)stm32g4_dmamux->r).f
#define STM32G4_DMAMUX_FIELD_WRITE(r, f, v) \
	do { stm32g4_dmamux_##r##_t r = { .mask = stm32g4_dmamux->r }; \
	r.f = v; stm32g4_dmamux->r = r.mask; } while (0)

#define STM32G4_DMAMUX_FIELD_READ_INDEX(r, i, f) \
	((stm32g4_dmamux_##r##_t)stm32g4_dmamux->r[i]).f
#define STM32G4_DMAMUX_FIELD_WRITE_INDEX(r, i, f, v) \
	do { stm32g4_dmamux_##r##_t r = { .mask = stm32g4_dmamux->r[i] }; \
	r.f = v; stm32g4_dmamux->r[i] = r.mask; } while (0)

#endif
