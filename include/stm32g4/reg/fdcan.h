#ifndef __STM32G4_REG_FDCAN_H__
#define __STM32G4_REG_FDCAN_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t day     : 8;
		const uint32_t mon     : 8;
		const uint32_t year    : 4;
		const uint32_t substep : 4;
		const uint32_t step    : 4;
		const uint32_t rel     : 4;
	};

	uint32_t mask;
} stm32g4_fdcan_crel_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       dsjw      : 4;
		uint32_t       dtseg2    : 4;
		uint32_t       dtseg1    : 5;
		const uint32_t res_15_13 : 3;
		uint32_t       dbrp      : 5;
		const uint32_t res_22_21 : 2;
		uint32_t       tdc       : 1;
		const uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_fdcan_dbtp_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t res_3_0  :  4;
		uint32_t       lbck     :  1;
		uint32_t       tx       :  2;
		const uint32_t rx       :  1;
		const uint32_t res_31_8 : 23;
	};

	uint32_t mask;
} stm32g4_fdcan_test_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       wdc       :  8;
		const uint32_t wdv       :  8;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_fdcan_rwd_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       init      :  1;
		uint32_t       cce       :  1;
		uint32_t       asm_      :  1;
		const uint32_t csa       :  1;
		uint32_t       csr       :  1;
		uint32_t       mon       :  1;
		uint32_t       dar       :  1;
		uint32_t       test      :  1;
		uint32_t       fdoe      :  1;
		uint32_t       brse      :  1;
		const uint32_t res_11_10 :  2;
		uint32_t       pxhd      :  1;
		uint32_t       efbi      :  1;
		uint32_t       txp       :  1;
		uint32_t       niso      :  1;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_fdcan_cccr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       ntseg2    : 7;
		const uint32_t res_7     : 1;
		uint32_t       ntseg1    : 8;
		uint32_t       nbrp      : 9;
		uint32_t       nsjw      : 7;
	};

	uint32_t mask;
} stm32g4_fdcan_nbtp_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       tss       :  2;
		const uint32_t res_15_2  : 14;
		uint32_t       tcp       :  4;
		const uint32_t res_31_20 : 12;
	};

	uint32_t mask;
} stm32g4_fdcan_tscc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t tsc       : 16;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_fdcan_tscv_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       etoc     :  1;
		uint32_t       tos      :  2;
		const uint32_t res_15_3 : 13;
		uint32_t       top      : 16;
	};

	uint32_t mask;
} stm32g4_fdcan_tocc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t toc       : 16;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_fdcan_tocv_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t tec       : 8;
		const uint32_t rec       : 7;
		const uint32_t rp        : 1;
		const uint32_t cel       : 8;
		const uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_fdcan_ecr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t lec       : 3;
		const uint32_t act       : 2;
		const uint32_t ep        : 1;
		const uint32_t ew        : 1;
		const uint32_t bo        : 1;
		const uint32_t dlec      : 3;
		const uint32_t resi      : 1;
		const uint32_t rbrs      : 1;
		const uint32_t redl      : 1;
		const uint32_t pxe       : 1;
		const uint32_t res_15    : 1;
		const uint32_t tdcv      : 7;
		const uint32_t res_31_23 : 9;
	};

	uint32_t mask;
} stm32g4_fdcan_psr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       tdcf      :  7;
		const uint32_t res_7     :  1;
		uint32_t       tdco      :  7;
		const uint32_t res_31_15 : 17;
	};

	uint32_t mask;
} stm32g4_fdcan_tdcr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       rf0n      : 1;
		uint32_t       rf0f      : 1;
		uint32_t       rf0l      : 1;
		uint32_t       rf1n      : 1;
		uint32_t       rf1f      : 1;
		uint32_t       rf1l      : 1;
		uint32_t       hpm       : 1;
		uint32_t       tc        : 1;
		uint32_t       tcf       : 1;
		uint32_t       tfe       : 1;
		uint32_t       tefn      : 1;
		uint32_t       teff      : 1;
		uint32_t       tefl      : 1;
		uint32_t       tsw       : 1;
		uint32_t       mraf      : 1;
		uint32_t       too       : 1;
		uint32_t       elo       : 1;
		uint32_t       ep        : 1;
		uint32_t       ew        : 1;
		uint32_t       bo        : 1;
		uint32_t       wdi       : 1;
		uint32_t       pea       : 1;
		uint32_t       ped       : 1;
		uint32_t       ara       : 1;
		const uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_fdcan_ir_t;

typedef stm32g4_fdcan_ir_t stm32g4_fdcan_ie_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       rxfifo0  :  1;
		uint32_t       rxfifo1  :  1;
		uint32_t       smsg     :  1;
		uint32_t       tferr    :  1;
		uint32_t       misc     :  1;
		uint32_t       berr     :  1;
		uint32_t       perr     :  1;
		const uint32_t res_31_7 : 25;
	};

	uint32_t mask;
} stm32g4_fdcan_ils_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       eint0    :  1;
		uint32_t       eint1    :  1;
		const uint32_t res_31_2 : 30;
	};

	uint32_t mask;
} stm32g4_fdcan_ile_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       rrfe      : 1;
		uint32_t       rrfs      : 1;
		uint32_t       anfe      : 2;
		uint32_t       anfs      : 2;
		const uint32_t res_7_6   : 2;
		uint32_t       f1om      : 1;
		uint32_t       f0om      : 1;
		const uint32_t res_15_10 : 6;
		uint32_t       lss       : 5;
		const uint32_t res_23_21 : 3;
		uint32_t       lse       : 4;
		const uint32_t res_31_28 : 4;
	};

	uint32_t mask;
} stm32g4_fdcan_rxgfc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       eidm      : 29;
		const uint32_t res_31_29 :  3;
	};

	uint32_t mask;
} stm32g4_fdcan_xidam_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t bidx      :  3;
		const uint32_t res_5_3   :  3;
		const uint32_t msi       :  2;
		const uint32_t fidx      :  5;
		const uint32_t res_14_13 :  2;
		const uint32_t flst      :  1;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_fdcan_hpms_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t ffl       : 4;
		const uint32_t res_7_4   : 4;
		const uint32_t fgi       : 2;
		const uint32_t res_15_10 : 6;
		const uint32_t fpi       : 2;
		const uint32_t res_23_18 : 6;
		const uint32_t ff        : 1;
		const uint32_t rfl       : 1;
		const uint32_t res_31_26 : 6;
	};

	uint32_t mask;
} stm32g4_fdcan_rxf0s_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       fai      :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_rxf0a_t;

typedef stm32g4_fdcan_rxf0s_t stm32g4_fdcan_rxf1s_t;
typedef stm32g4_fdcan_rxf0a_t stm32g4_fdcan_rxf1a_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t res_23_0  : 24;
		uint32_t       tfqm      :  1;
		const uint32_t res_31_25 :  7;
	};

	uint32_t mask;
} stm32g4_fdcan_txbc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t tffl      :  3;
		const uint32_t res_7_3   :  5;
		const uint32_t tfgi      :  2;
		const uint32_t res_15_10 :  6;
		const uint32_t tfqpi     :  2;
		const uint32_t res_20_18 :  3;
		const uint32_t tfqf      :  1;
		const uint32_t res_31_22 : 10;
	};

	uint32_t mask;
} stm32g4_fdcan_txfqs_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t trp      :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbrp_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       ar       :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbar_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       cr       :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbcr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t to       :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbto_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t cf       :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbcf_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       tie      :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbtie_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       cfie     :  3;
		const uint32_t res_31_3 : 29;
	};

	uint32_t mask;
} stm32g4_fdcan_txbcie_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t effl      : 3;
		const uint32_t res_7_3   : 5;
		const uint32_t efgi      : 2;
		const uint32_t res_15_10 : 6;
		const uint32_t efpi      : 2;
		const uint32_t res_23_18 : 6;
		const uint32_t eff       : 1;
		const uint32_t tefl      : 1;
		const uint32_t res_31_26 : 6;
	};

	uint32_t mask;
} stm32g4_fdcan_txefs_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       efai     :  2;
		const uint32_t res_31_2 : 30;
	};

	uint32_t mask;
} stm32g4_fdcan_txefa_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       pdiv     :  4;
		const uint32_t res_31_4 : 28;
	};

	uint32_t mask;
} stm32g4_fdcan_ckdiv_t;

typedef struct
{
	volatile const uint32_t crel;
	volatile const uint32_t endn;
	volatile const uint32_t res_2;
	volatile uint32_t       dbtp;
	volatile uint32_t       test;
	volatile uint32_t       rwd;
	volatile uint32_t       cccr;
	volatile uint32_t       nbtp;
	volatile uint32_t       tscc;
	volatile const uint32_t tscv;
	volatile uint32_t       tocc;
	volatile const uint32_t tocv;
	volatile const uint32_t res_15_12[4];
	volatile const uint32_t ecr;
	volatile const uint32_t psr;
	volatile uint32_t       tdcr;
	volatile const uint32_t res_19;
	volatile uint32_t       ir;
	volatile uint32_t       ie;
	volatile uint32_t       ils;
	volatile uint32_t       ile;
	volatile const uint32_t res_31_24[8];
	volatile uint32_t       rxgfc;
	volatile uint32_t       xidam;
	volatile const uint32_t hpms;
	volatile const uint32_t res_35;
	volatile const uint32_t rxf0s;
	volatile uint32_t       rxf0a;
	volatile const uint32_t rxf1s;
	volatile uint32_t       rxf1a;
	volatile const uint32_t res_47_40[8];
	volatile uint32_t       txbc;
	volatile const uint32_t txfqs;
	volatile const uint32_t txbrp;
	volatile uint32_t       txbar;
	volatile uint32_t       txbcr;
	volatile const uint32_t txbto;
	volatile const uint32_t txbcf;
	volatile uint32_t       txbtie;
	volatile uint32_t       txbcie;
	volatile const uint32_t txefs;
	volatile uint32_t       txefa;
	volatile const uint32_t res_63_59[5];
	volatile uint32_t       ckdiv;
} stm32g4_fdcan_t;

typedef enum
{
	STM32G4_FDCAN1 = 0,
	STM32G4_FDCAN2,
	STM32G4_FDCAN3,

	STM32G4_FDCAN_COUNT
} stm32g4_fdcan_e;

#define STM32G4_FDCAN_CREL 0x32141218
#define STM32G4_FDCAN_ENDN 0x87654321

static volatile stm32g4_fdcan_t * const stm32g4_fdcan[STM32G4_FDCAN_COUNT] =
{
	(volatile stm32g4_fdcan_t*)0x40006400,
	(volatile stm32g4_fdcan_t*)0x40006800,
	(volatile stm32g4_fdcan_t*)0x40006C00,
};

#define STM32G4_FDCAN_FIELD_READ(i, r, f) \
	((stm32g4_fdcan_##r##_t)stm32g4_fdcan[i]->r).f
#define STM32G4_FDCAN_FIELD_WRITE(i, r, f, v) \
	do { stm32g4_fdcan_##r##_t r = { .mask = stm32g4_fdcan[i]->r }; \
	r.f = v; stm32g4_fdcan[i]->r = r.mask; } while (0)

#endif
