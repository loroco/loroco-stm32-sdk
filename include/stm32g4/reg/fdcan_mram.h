#ifndef __STM32G4_REG_FDCAN_MRAM_H__
#define __STM32G4_REG_FDCAN_MRAM_H__

#include <stdint.h>


typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       sfid2     : 11;
		const uint32_t res_15_11 :  5;
		uint32_t       sfid1     : 11;
		uint32_t       sfec      :  3;
		uint32_t       sft       :  2;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_standard_filter_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t efid1 : 29;
		uint32_t efec  :  3;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_extended_filter_p0_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       efid2  : 29;
		const uint32_t res_29 :  1;
		uint32_t       efti   :  2;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_extended_filter_p1_t;

typedef struct
__attribute__((__packed__))
{
	uint32_t p0;
	uint32_t p1;
} stm32g4_fdcan_mram_extended_filter_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t id  : 29;
		uint32_t rtr :  1;
		uint32_t xtd :  1;
		uint32_t esi :  1;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_fifo_p0_t;

typedef stm32g4_fdcan_mram_fifo_p0_t stm32g4_fdcan_mram_rx_fifo_0_hd_p0_t;
typedef stm32g4_fdcan_mram_fifo_p0_t stm32g4_fdcan_mram_rx_fifo_1_hd_p0_t;
typedef stm32g4_fdcan_mram_fifo_p0_t stm32g4_fdcan_mram_tx_buffer_hd_p0_t;
typedef stm32g4_fdcan_mram_fifo_p0_t stm32g4_fdcan_mram_tx_event_fifo_p0_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       rxts      : 16;
		uint32_t       dlc       :  4;
		uint32_t       brs       :  1;
		uint32_t       fdf       :  1;
		const uint32_t res_53_54 :  2;
		uint32_t       fidx      :  7;
		uint32_t       anmf      :  1;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_rx_fifo_0_hd_p1_t;

typedef stm32g4_fdcan_mram_rx_fifo_0_hd_p1_t stm32g4_fdcan_mram_rx_fifo_1_hd_p1_t;

#define FDCAN_FIFO_DATA_SIZE 16
/* TODO: the STM32G4 only supports fdcan fifo elements with data length of
 *       64 bytes. Other FDCAN peripherals will support variable - make
 *       this support that.
 */
typedef struct
__attribute__((__packed__))
{
	uint32_t hd_p0;
	uint32_t hd_p1;
	uint32_t data[FDCAN_FIFO_DATA_SIZE];
} stm32g4_fdcan_mram_rx_fifo_t;

typedef stm32g4_fdcan_mram_rx_fifo_t stm32g4_fdcan_mram_rx_fifo_0_t;
typedef stm32g4_fdcan_mram_rx_fifo_t stm32g4_fdcan_mram_rx_fifo_1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t res_32_47 : 16;
		uint32_t       dlc       :  4;
		uint32_t       brs       :  1;
		uint32_t       fdf       :  1;
		const uint32_t res_53    :  1;
		uint32_t       efc       :  1;
		uint32_t       mm        :  8;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_tx_buffer_hd_p1_t;

typedef struct
__attribute__((__packed__))
{
	uint32_t hd_p0;
	uint32_t hd_p1;
	uint32_t data[FDCAN_FIFO_DATA_SIZE];
} stm32g4_fdcan_mram_tx_buffer_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t txts : 16;
		uint32_t dlc  :  4;
		uint32_t brs  :  1;
		uint32_t edl  :  1;
		uint32_t et   :  2;
		uint32_t mm   :  8;
	};

	uint32_t mask;
} stm32g4_fdcan_mram_tx_event_fifo_p1_t;

typedef struct
__attribute__((__packed__))
{
	uint32_t p0;
	uint32_t p1;
} stm32g4_fdcan_mram_tx_event_fifo_t;

#define FDCAN_N_SFILTERS 28
#define FDCAN_N_EFILTERS 8
#define FDCAN_N_RX_ELEMS 3
#define FDCAN_N_TX_ELEMS 3

#define NUMWORDS(t) (sizeof(t) / sizeof(uint32_t))

// TODO: should the following elements be uint32_ts?
typedef struct
{
	volatile uint32_t standard_filter[FDCAN_N_SFILTERS];
	volatile uint32_t extended_filter[FDCAN_N_EFILTERS * NUMWORDS(stm32g4_fdcan_mram_extended_filter_t)];
	volatile uint32_t rx_fifo_0      [FDCAN_N_RX_ELEMS * NUMWORDS(stm32g4_fdcan_mram_rx_fifo_0_t)];
	volatile uint32_t rx_fifo_1      [FDCAN_N_RX_ELEMS * NUMWORDS(stm32g4_fdcan_mram_rx_fifo_1_t)];
	volatile uint32_t tx_event_fifo  [FDCAN_N_TX_ELEMS * NUMWORDS(stm32g4_fdcan_mram_tx_event_fifo_t)];
	volatile uint32_t tx_buffer      [FDCAN_N_TX_ELEMS * NUMWORDS(stm32g4_fdcan_mram_tx_buffer_t)];
} stm32g4_fdcan_mram_t;

// TODO verify that this is the correct mram
static volatile stm32g4_fdcan_mram_t * const stm32g4_fdcan_mram[] =
{
	(volatile stm32g4_fdcan_mram_t*)0x4000A400,
	(volatile stm32g4_fdcan_mram_t*)0x4000A800,
	(volatile stm32g4_fdcan_mram_t*)0x4000AC00,
};

#define STM32G4_FDCAN_MRAM_READ(i, r, e, o) \
	(stm32g4_fdcan_mram[i]->r[e*NUMWORDS(stm32g4_fdcan_mram_##r##_t) + o])

#define STM32G4_FDCAN_MRAM_WRITE(i, r, e, o, v) \
	do { STM32G4_FDCAN_MRAM_READ(i, r, e, o) = v; } while (0)

#endif
