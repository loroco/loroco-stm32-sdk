#ifndef __STM32G4_REG_FLASH_H__
#define __STM32G4_REG_FLASH_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       latency   :  4;
		const uint32_t res_7_4   :  4;
		uint32_t       prften    :  1;
		uint32_t       icen      :  1;
		uint32_t       dcen      :  1;
		uint32_t       icrst     :  1;
		uint32_t       dcrst     :  1;
		uint32_t       run_pd    :  1;
		uint32_t       sleep_pd  :  1;
		const uint32_t res_17_15 :  3;
		uint32_t       dbg_swen  :  1;
		const uint32_t res_31_19 : 13;
	};

	uint32_t mask;
} stm32g4_flash_acr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       eop       :  1;
		uint32_t       operr     :  1;
		const uint32_t res_2     :  1;
		uint32_t       progerr   :  1;
		uint32_t       wrperr    :  1;
		uint32_t       pgaerr    :  1;
		uint32_t       sizerr    :  1;
		uint32_t       pgserr    :  1;
		uint32_t       misserr   :  1;
		uint32_t       fasterr   :  1;
		const uint32_t res_13_10 :  4;
		uint32_t       rderr     :  1;
		uint32_t       optverr   :  1;
		const uint32_t bsy       :  1;
		const uint32_t res_31_17 : 15;
	};

	uint32_t mask;
} stm32g4_flash_sr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       pg         :  1;
		uint32_t       per        :  1;
		uint32_t       mer1       :  1;
		uint32_t       pnb        :  7;
		const uint32_t res_15_10  :  6;
		uint32_t       strt       :  1;
		uint32_t       opt_strt   :  1;
		uint32_t       fstpg      :  1;
		const uint32_t res_23_19  :  5;
		uint32_t       eopie      :  1;
		uint32_t       errie      :  1;
		uint32_t       rderrie    :  1;
		uint32_t       obl_launch :  1;
		uint32_t       sec_prot1  :  1;
		const uint32_t res_29     :  1;
		uint32_t       opt_lock   :  1;
		uint32_t       lock       :  1;
	};

	uint32_t mask;
} stm32g4_flash_cr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t addr_ecc  : 19;
		const uint32_t res_21_19 :  3;
		const uint32_t sysf_ecc  :  1;
		const uint32_t res_23    :  1;
		uint32_t       ecccie    :  1;
		const uint32_t res_29_25 :  5;
		uint32_t       eccc      :  1;
		uint32_t       eccd      :  1;
	};

	uint32_t mask;
} stm32g4_flash_eccr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       rdp         :  8;
		uint32_t       bor_lev     :  3;
		const uint32_t res_11      :  1;
		uint32_t       nrst_stop   :  1;
		uint32_t       nrst_stdby  :  1;
		uint32_t       nrst_shdw   :  1;
		const uint32_t res_15      :  1;
		uint32_t       iwdg_sw     :  1;
		uint32_t       iwdg_stop   :  1;
		uint32_t       iwdg_stdby  :  1;
		uint32_t       wwdg_sw     :  1;
		const uint32_t res_22_20   :  3;
		uint32_t       nboot1      :  1;
		uint32_t       sram_pe     :  1;
		uint32_t       ccmsram_rst :  1;
		uint32_t       nswboot0    :  1;
		uint32_t       nboot0      :  1;
		uint32_t       nrst_mode   :  2;
		uint32_t       irhen       :  1;
		const uint32_t res_31      :  1;
	};

	uint32_t mask;
} stm32g4_flash_optr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       strt        : 15;
		const uint32_t res_31_15   : 17;
	};

	uint32_t mask;
} stm32g4_flash_pcrop1sr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       end         : 15;
		const uint32_t res_30_15   : 16;
		uint32_t       rdp         :  1;
	};

	uint32_t mask;
} stm32g4_flash_pcrop1er_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       start     : 7;
		const uint32_t res_15_7  : 9;
		uint32_t       end       : 7;
		const uint32_t res_31_23 : 9;
	};

	uint32_t mask;
} stm32g4_flash_wrpr_t;

typedef stm32g4_flash_wrpr_t stm32g4_flash_wrp1ar_t;
typedef stm32g4_flash_wrpr_t stm32g4_flash_wrp1br_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       sec_size1 :  8;
		const uint32_t res_15_8  :  8;
		uint32_t       boot_lock :  1;
		const uint32_t res_31_17 : 15;
	};

	uint32_t mask;
} stm32g4_flash_sec1r_t;


typedef struct
{
	volatile uint32_t       acr;
	volatile uint32_t       pdkeyr;
	volatile uint32_t       keyr;
	volatile uint32_t       optkeyr;
	volatile uint32_t       sr;
	volatile uint32_t       cr;
	volatile uint32_t       eccr;
	volatile const uint32_t res_7;
	volatile uint32_t       optr;
	volatile uint32_t       pcrop1sr;
	volatile uint32_t       pcrop1er;
	volatile uint32_t       wrp1ar;
	volatile uint32_t       wrp1br;
	volatile const uint32_t res_13_27[15];
	volatile uint32_t       sec1r;
} stm32g4_flash_t;

static volatile stm32g4_flash_t * const stm32g4_flash
	= (volatile stm32g4_flash_t*)0x40022000;

#define STM32G4_FLASH_FIELD_READ(r, f) \
	((stm32g4_flash_##r##_t)stm32g4_flash->r).f
#define STM32G4_FLASH_FIELD_WRITE(r, f, v) \
	do { stm32g4_flash_##r##_t r = { .mask = stm32g4_flash->r }; \
	r.f = v; stm32g4_flash->r = r.mask; } while (0)

#endif
