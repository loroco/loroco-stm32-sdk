#ifndef __STM32G4_REG_GPIO_H__
#define __STM32G4_REG_GPIO_H__

#include <stdint.h>

typedef enum
{
	STM32G4_GPIO_PORT_A = 0,
	STM32G4_GPIO_PORT_B = 1,
	STM32G4_GPIO_PORT_C = 2,
	STM32G4_GPIO_PORT_D = 3,
	STM32G4_GPIO_PORT_E = 4,
	STM32G4_GPIO_PORT_F = 5,
	STM32G4_GPIO_PORT_G = 6,

	STM32G4_GPIO_PORT_COUNT
} stm32g4_gpio_port_e;
typedef enum
{
	STM32G4_GPIO_MODE_INPUT     = 0,
	STM32G4_GPIO_MODE_OUTPUT    = 1,
	STM32G4_GPIO_MODE_ALTERNATE = 2,
	STM32G4_GPIO_MODE_ANALOG    = 3,
} stm32g4_gpio_mode_e;

typedef enum
{
	STM32G4_GPIO_PUPD_NONE      = 0,
	STM32G4_GPIO_PUPD_PULL_UP   = 1,
	STM32G4_GPIO_PUPD_PULL_DOWN = 2,
} stm32g4_gpio_pupd_e;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t dr        : 16;
		uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_gpio_dr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t bs : 16;
		uint32_t br : 16;
	};

	uint32_t mask;
} stm32g4_gpio_bsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lck       : 16;
		uint32_t lckk      :  1;
		uint32_t res_31_17 : 15;
	};

	uint32_t mask;
} stm32g4_gpio_lckr_t;

typedef struct
{
	volatile uint32_t       moder;
	volatile uint32_t       otyper;
	volatile uint32_t       ospeedr;
	volatile uint32_t       pupdr;
	volatile const uint32_t idr;
	volatile uint32_t       odr;
	volatile uint32_t       bsrr;
	volatile uint32_t       lckr;
	volatile uint32_t       afrl;
	volatile uint32_t       afrh;
	volatile uint32_t       brr;
	volatile uint32_t       reserved[246];
} stm32g4_gpio_t;

static volatile stm32g4_gpio_t * const stm32g4_gpio[STM32G4_GPIO_PORT_COUNT] =
{
	[STM32G4_GPIO_PORT_A] = (volatile stm32g4_gpio_t*)0x48000000,
	[STM32G4_GPIO_PORT_B] = (volatile stm32g4_gpio_t*)0x48000400,
	[STM32G4_GPIO_PORT_C] = (volatile stm32g4_gpio_t*)0x48000800,
	[STM32G4_GPIO_PORT_D] = (volatile stm32g4_gpio_t*)0x48000C00,
	[STM32G4_GPIO_PORT_E] = (volatile stm32g4_gpio_t*)0x48001000,
	[STM32G4_GPIO_PORT_F] = (volatile stm32g4_gpio_t*)0x48001400,
	[STM32G4_GPIO_PORT_G] = (volatile stm32g4_gpio_t*)0x48001800
};

#define STM32G4_GPIO_PORT_FIELD_READ(p, r, f) \
	((stm32g4_gpio_##r##_t)stm32g4_gpio[p]->r).f
#define STM32G4_GPIO_PORT_FIELD_WRITE(p, r, f, v) \
	do { stm32g4_gpio_##r##_t r = { .mask = stm32g4_gpio[p]->r }; \
	r.f = v; stm32g4_gpio[p]->r = r.mask; } while (0)

#endif
