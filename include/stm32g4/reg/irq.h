#ifndef __STM32G4_REG_IRQ_H__
#define __STM32G4_REG_IRQ_H__

typedef enum
{
	STM32G4_IRQ_WWDG                        =   0,
	STM32G4_IRQ_PVD_PVM                     =   1,
	STM32G4_IRQ_RTC_TAMP_STAMP_CSS_LSE      =   2,
	STM32G4_IRQ_RTC_WKUP                    =   3,
	STM32G4_IRQ_FLASH                       =   4,
	STM32G4_IRQ_RCC                         =   5,
	STM32G4_IRQ_EXTI0                       =   6,
	STM32G4_IRQ_EXTI1                       =   7,
	STM32G4_IRQ_EXTI2                       =   8,
	STM32G4_IRQ_EXTI3                       =   9,
	STM32G4_IRQ_EXTI4                       =  10,
	STM32G4_IRQ_DMA1_CH1                    =  11,
	STM32G4_IRQ_DMA1_CH2                    =  12,
	STM32G4_IRQ_DMA1_CH3                    =  13,
	STM32G4_IRQ_DMA1_CH4                    =  14,
	STM32G4_IRQ_DMA1_CH5                    =  15,
	STM32G4_IRQ_DMA1_CH6                    =  16,
	STM32G4_IRQ_DMA1_CH7                    =  17,
	STM32G4_IRQ_ADC_1_2                     =  18,
	STM32G4_IRQ_USB_HP                      =  19,
	STM32G4_IRQ_USB_LP                      =  20,
	STM32G4_IRQ_FDCAN1_IT0                  =  21,
	STM32G4_IRQ_FDCAN1_IT1                  =  22,
	STM32G4_IRQ_EXTI9_5                     =  23,
	STM32G4_IRQ_TIM1_BRK__TIM15             =  24,
	STM32G4_IRQ_TIM1_UP__TIM16              =  25,
	STM32G4_IRQ_TIM1_TRG_COM_DIR_IDX__TIM17 =  26,
	STM32G4_IRQ_TIM1_CC                     =  27,
	STM32G4_IRQ_TIM2                        =  28,
	STM32G4_IRQ_TIM3                        =  29,
	STM32G4_IRQ_TIM4                        =  30,
	STM32G4_IRQ_I2C1_EV                     =  31,
	STM32G4_IRQ_I2C1_ER                     =  32,
	STM32G4_IRQ_I2C2_EV                     =  33,
	STM32G4_IRQ_I2C2_ER                     =  34,
	STM32G4_IRQ_SPI1                        =  35,
	STM32G4_IRQ_SPI2                        =  36,
	STM32G4_IRQ_USART1                      =  37,
	STM32G4_IRQ_USART2                      =  38,
	STM32G4_IRQ_USART3                      =  39,
	STM32G4_IRQ_EXTI15_10                   =  40,
	STM32G4_IRQ_RTC_ALARM                   =  41,
	STM32G4_IRQ_USB_WKUP                    =  42,
	STM32G4_IRQ_TIM8_BRK_TERR_IERR          =  43,
	STM32G4_IRQ_TIM8_UP                     =  44,
	STM32G4_IRQ_TIM8_TRG_COM_DIR_IDX        =  45,
	STM32G4_IRQ_TIM8_CC                     =  46,
	STM32G4_IRQ_ADC3                        =  47,
	STM32G4_IRQ_FSMC                        =  48,
	STM32G4_IRQ_LPTIM1                      =  49,
	STM32G4_IRQ_TIM5                        =  50,
	STM32G4_IRQ_SPI3                        =  51,
	STM32G4_IRQ_UART4                       =  52,
	STM32G4_IRQ_UART5                       =  53,
	STM32G4_IRQ_TIM6_DAC_UNDER              =  54,
	STM32G4_IRQ_TIM7_DAC_UNDER              =  55,
	STM32G4_IRQ_DMA2_CH1                    =  56,
	STM32G4_IRQ_DMA2_CH2                    =  57,
	STM32G4_IRQ_DMA2_CH3                    =  58,
	STM32G4_IRQ_DMA2_CH4                    =  59,
	STM32G4_IRQ_DMA2_CH5                    =  60,
	STM32G4_IRQ_ADC4                        =  61,
	STM32G4_IRQ_ADC5                        =  62,
	STM32G4_IRQ_UCPD1                       =  63,
	STM32G4_IRQ_COMP_1_2_3                  =  64,
	STM32G4_IRQ_COMP_4_5_6                  =  65,
	STM32G4_IRQ_COMP_7                      =  66,
	STM32G4_IRQ_HRTIM_MST                   =  67,
	STM32G4_IRQ_HRTIM_TIMA                  =  68,
	STM32G4_IRQ_HRTIM_TIMB                  =  69,
	STM32G4_IRQ_HRTIM_TIMC                  =  70,
	STM32G4_IRQ_HRTIM_TIMD                  =  71,
	STM32G4_IRQ_HRTIM_TIME                  =  72,
	STM32G4_IRQ_HRTIM_FLT                   =  73,
	STM32G4_IRQ_HRTIM_TIMF                  =  74,
	STM32G4_IRQ_CRS                         =  75,
	STM32G4_IRQ_SAI                         =  76,
	STM32G4_IRQ_TIM20_BRK_TERR_IERR         =  77,
	STM32G4_IRQ_TIM20_UP                    =  78,
	STM32G4_IRQ_TIM20_TRG_COM_DIR_IDX       =  79,
	STM32G4_IRQ_TIM20_CC                    =  80,
	STM32G4_IRQ_FPU                         =  81,
	STM32G4_IRQ_I2C4_EV                     =  82,
	STM32G4_IRQ_I2C4_ER                     =  83,
	STM32G4_IRQ_SPI4                        =  84,
	STM32G4_IRQ_AES                         =  85,
	STM32G4_IRQ_FDCAN2_IT0                  =  86,
	STM32G4_IRQ_FDCAN2_IT1                  =  87,
	STM32G4_IRQ_FDCAN3_IT0                  =  88,
	STM32G4_IRQ_FDCAN3_IT1                  =  89,
	STM32G4_IRQ_RNG                         =  90,
	STM32G4_IRQ_LPUART                      =  91,
	STM32G4_IRQ_I2C3_EV                     =  92,
	STM32G4_IRQ_I2C3_ER                     =  93,
	STM32G4_IRQ_DMAMUX_OVR                  =  94,
	STM32G4_IRQ_QUADSPI                     =  95,
	STM32G4_IRQ_DMA1_CH8                    =  96,
	STM32G4_IRQ_DMA2_CH6                    =  97,
	STM32G4_IRQ_DMA2_CH7                    =  98,
	STM32G4_IRQ_DMA2_CH8                    =  99,
	STM32G4_IRQ_CORDIC                      = 100,
	STM32G4_IRQ_FMAC                        = 101,
} stm32g4_irq_e;

#endif
