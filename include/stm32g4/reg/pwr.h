#ifndef __STM32G4_REG_PWR_H__
#define __STM32G4_REG_PWR_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       lpms      :  3;
		const uint32_t res_7_3   :  5;
		uint32_t       dbp       :  1;
		uint32_t       vos       :  2;
		const uint32_t res_13_11 :  3;
		uint32_t       lpr       :  1;
		const uint32_t res_31_15 : 17;
	};

	uint32_t mask;
} stm32g4_pwr_cr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       pvde     :  1;
		uint32_t       pls      :  3;
		const uint32_t res_5_4  :  2;
		uint32_t       pvmen1   :  1;
		uint32_t       pbmen2   :  1;
		const uint32_t res_31_8 : 24;
	};

	uint32_t mask;
} stm32g4_pwr_cr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       ewup1       :  1;
		uint32_t       ewup2       :  1;
		uint32_t       ewup3       :  1;
		uint32_t       ewup4       :  1;
		uint32_t       ewup5       :  1;
		const uint32_t res_7_5     :  3;
		uint32_t       rrs         :  1;
		const uint32_t res_9       :  1;
		uint32_t       apc         :  1;
		const uint32_t res_12_11   :  2;
		uint32_t       ucpd1_stdby :  1;
		uint32_t       ucpd1_dbdis :  1;
		uint32_t       eiwul       :  1;
		const uint32_t res_31_16   : 16;
	};

	uint32_t mask;
} stm32g4_pwr_cr3_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       wp1       :  1;
		uint32_t       wp2       :  1;
		uint32_t       wp3       :  1;
		uint32_t       wp4       :  1;
		uint32_t       wp5       :  1;
		const uint32_t res_7_5   :  3;
		uint32_t       vbe       :  1;
		uint32_t       vbrs      :  1;
		const uint32_t res_31_10 : 22;
	};

	uint32_t mask;
} stm32g4_pwr_cr4_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t wuf1      :  1;
		const uint32_t wuf2      :  1;
		const uint32_t wuf3      :  1;
		const uint32_t wuf4      :  1;
		const uint32_t wuf5      :  1;
		const uint32_t res_7_5   :  3;
		const uint32_t sbf       :  1;
		const uint32_t res_14_9  :  6;
		const uint32_t wufi      :  1;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_pwr_sr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t res_7_0   :  8;
		const uint32_t reglps    :  1;
		const uint32_t reglpf    :  1;
		const uint32_t vosf      :  1;
		const uint32_t pvdo      :  1;
		const uint32_t res_13_12 :  2;
		const uint32_t pvmo1     :  1;
		const uint32_t pvmo2     :  1;
		const uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_pwr_sr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cwuf1          :  1;
		uint32_t cwuf2          :  1;
		uint32_t cwuf3          :  1;
		uint32_t cwuf4          :  1;
		uint32_t cwuf5          :  1;
		const uint32_t res_7_5  :  3;
		uint32_t csbf           :  1;
		const uint32_t res_31_9 : 23;
	};

	uint32_t mask;
} stm32g4_pwr_scr_t;

#define STM32G4_PWR_R1MODE_NORMAL 1
#define STM32G4_PWR_R1MODE_BOOST  0

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		const uint32_t res_7_0  :  8;
		uint32_t       r1mode   :  1;
		const uint32_t res_31_9 : 23;
	};

	uint32_t mask;
} stm32g4_pwr_cr5_t;


typedef struct
{
	volatile uint32_t       cr1;
	volatile uint32_t       cr2;
	volatile uint32_t       cr3;
	volatile uint32_t       cr4;
	volatile const uint32_t sr1;
	volatile const uint32_t sr2;
	volatile uint32_t       scr;
	volatile const uint32_t res_7;
	volatile uint32_t       pucra;
	volatile uint32_t       pdcra;
	volatile uint32_t       pucrb;
	volatile uint32_t       pdcrb;
	volatile uint32_t       pucrc;
	volatile uint32_t       pdcrc;
	volatile uint32_t       pucrd;
	volatile uint32_t       pdcrd;
	volatile uint32_t       pucre;
	volatile uint32_t       pdcre;
	volatile uint32_t       pucrf;
	volatile uint32_t       pdcrf;
	volatile uint32_t       pucrg;
	volatile uint32_t       pdcrg;
	volatile const uint32_t res_22_31[10];
	volatile uint32_t       cr5;
} stm32g4_pwr_t;

static volatile stm32g4_pwr_t * const stm32g4_pwr
	= (volatile stm32g4_pwr_t*)0x40007000;

#define STM32G4_PWR_FIELD_READ(r, f) \
	((stm32g4_pwr_##r##_t)stm32g4_pwr->r).f
#define STM32G4_PWR_FIELD_WRITE(r, f, v) \
	do { stm32g4_pwr_##r##_t r = { .mask = stm32g4_pwr->r }; \
	r.f = v; stm32g4_pwr->r = r.mask; } while (0)

#endif
