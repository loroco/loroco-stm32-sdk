#ifndef __STM32G4_REG_RCC_H__
#define __STM32G4_REG_RCC_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t res_7_0   : 8;
		uint32_t hsion     : 1;
		uint32_t hsikeron  : 1;
		uint32_t hsirdy    : 1;
		uint32_t res_15_11 : 5;
		uint32_t hseon     : 1;
		uint32_t hserdy    : 1;
		uint32_t hsebyp    : 1;
		uint32_t csson     : 1;
		uint32_t res_23_20 : 4;
		uint32_t pllon     : 1;
		uint32_t pllrdy    : 1;
		uint32_t res_31_26 : 6;
	};

	uint32_t mask;
} stm32g4_rcc_cr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t res_15_0 : 16;
		uint32_t hsical   : 8;
		uint32_t hsitrim  : 7;
		uint32_t res_31   : 1;
	};

	uint32_t mask;
} stm32g4_rcc_icscr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t sw        : 2;
		uint32_t sws       : 2;
		uint32_t hpre      : 4;
		uint32_t ppre1     : 3;
		uint32_t ppre2     : 3;
		uint32_t res_23_14 : 10;
		uint32_t mcosel    : 4;
		uint32_t mcopre    : 3;
		uint32_t res_31    : 1;
	};

	uint32_t mask;
} stm32g4_rcc_cfgr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t pllsrc    : 2;
		uint32_t res_3_2   : 2;
		uint32_t pllm      : 4;
		uint32_t plln      : 7;
		uint32_t res_15    : 1;
		uint32_t pllpen    : 1;
		uint32_t pllp      : 1;
		uint32_t res_19_18 : 2;
		uint32_t pllqen    : 1;
		uint32_t pllq      : 2;
		uint32_t res_23    : 1;
		uint32_t pllren    : 1;
		uint32_t pllr      : 2;
		uint32_t pllpdiv   : 5;
	};

	uint32_t mask;
} stm32g4_rcc_pllcfgr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lsirdyie    : 1;
		uint32_t lserdyie    : 1;
		uint32_t res_2       : 1;
		uint32_t hsirdyie    : 1;
		uint32_t hserdyie    : 1;
		uint32_t pllrdyie    : 1;
		uint32_t res_8_6     : 3;
		uint32_t lsecssie    : 1;
		uint32_t hsi48rdyie  : 1;
		uint32_t res_31_11   : 21;
	};

	uint32_t mask;
} stm32g4_rcc_cier_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lsirdyf    : 1;
		uint32_t lserdyf    : 1;
		uint32_t res_2      : 1;
		uint32_t hsirdyf    : 1;
		uint32_t hserdyf    : 1;
		uint32_t pllrdyf    : 1;
		uint32_t res_7_6    : 2;
		uint32_t cssf       : 1;
		uint32_t lsecssf    : 1;
		uint32_t hsi48rdyf  : 1;
		uint32_t res_31_11  : 21;
	};

	uint32_t mask;
} stm32g4_rcc_cifr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lsirdyc    : 1;
		uint32_t lserdyc    : 1;
		uint32_t res_2      : 1;
		uint32_t hsirdyc    : 1;
		uint32_t hserdyc    : 1;
		uint32_t pllrdyc    : 1;
		uint32_t res_7_6    : 2;
		uint32_t cssc       : 1;
		uint32_t lsecssc    : 1;
		uint32_t hsi48rdyc  : 1;
		uint32_t res_31_11  : 21;
	};

	uint32_t mask;
} stm32g4_rcc_cicr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t dma1     : 1;
		uint32_t dma2     : 1;
		uint32_t dmamux1  : 1;
		uint32_t cordic   : 1;
		uint32_t fmac     : 1;
		uint32_t res_7_5     : 3;
		uint32_t flash    : 1;
		uint32_t res_11_9    : 3;
		uint32_t crc      : 1;
		uint32_t res_31_13   : 19;
	};

	uint32_t mask;
} stm32g4_rcc_ahb1rstr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t gpioa   : 1;
		uint32_t gpiob   : 1;
		uint32_t gpioc   : 1;
		uint32_t gpiod   : 1;
		uint32_t gpioe   : 1;
		uint32_t gpiof   : 1;
		uint32_t gpiog   : 1;
		uint32_t res_12_7   : 6;
		uint32_t adc12   : 1;
		uint32_t adc345  : 1;
		uint32_t res_15     : 1;
		uint32_t dac1    : 1;
		uint32_t dac2    : 1;
		uint32_t dac3    : 1;
		uint32_t dac4    : 1;
		uint32_t res_23_20  : 4;
		uint32_t aes     : 1;
		uint32_t res_25     : 1;
		uint32_t rng     : 1;
		uint32_t res_31_27  : 5;
	};

	uint32_t mask;
} stm32g4_rcc_ahb2rstr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t fmc    : 1;
		uint32_t res_7_1   : 8;
		uint32_t qspi   : 1;
		uint32_t res_31_9  : 22;
	};

	uint32_t mask;
} stm32g4_rcc_ahb3rstr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t tim2      : 1;
		uint32_t tim3      : 1;
		uint32_t tim4      : 1;
		uint32_t tim5      : 1;
		uint32_t tim6      : 1;
		uint32_t tim7      : 1;
		uint32_t res_7_6   : 2;
		uint32_t crs       : 1;
		uint32_t res_13_9  : 5;
		uint32_t spi2      : 1;
		uint32_t spi3      : 1;
		uint32_t res_16    : 1;
		uint32_t usart2    : 1;
		uint32_t usart3    : 1;
		uint32_t uart4     : 1;
		uint32_t uart5     : 1;
		uint32_t i2c1      : 1;
		uint32_t i2c2      : 1;
		uint32_t usb       : 1;
		uint32_t res_24    : 1;
		uint32_t fdcan     : 1;
		uint32_t res_27_26 : 2;
		uint32_t pwrres    : 1;
		uint32_t res_29    : 1;
		uint32_t i2c3      : 1;
		uint32_t lptim1    : 1;
	};

	uint32_t mask;
} stm32g4_rcc_apb1rstr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lpuart1   : 1;
		uint32_t i2c4      : 1;
		uint32_t res_7_2   : 6;
		uint32_t ucpd1     : 1;
		uint32_t res_31_9  : 23;
	};

	uint32_t mask;
} stm32g4_rcc_apb1rstr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t syscfg    : 1;
		uint32_t res_10_1  : 10;
		uint32_t tim1      : 1;
		uint32_t spi1      : 1;
		uint32_t tim8      : 1;
		uint32_t usart1    : 1;
		uint32_t spi4      : 1;
		uint32_t tim15r    : 1;
		uint32_t tim16     : 1;
		uint32_t tim17     : 1;
		uint32_t res_19    : 1;
		uint32_t tim20     : 1;
		uint32_t sai1      : 1;
		uint32_t res_25_22 : 4;
		uint32_t hrtim1    : 1;
		uint32_t res_31_27 : 5;
	};

	uint32_t mask;
} stm32g4_rcc_apb2rstr_t;

typedef stm32g4_rcc_ahb1rstr_t stm32g4_rcc_ahb1enr_t;

typedef stm32g4_rcc_ahb2rstr_t stm32g4_rcc_ahb2enr_t;

typedef stm32g4_rcc_ahb3rstr_t stm32g4_rcc_ahb3enr_t;

typedef stm32g4_rcc_apb1rstr1_t stm32g4_rcc_apb1enr1_t;

typedef stm32g4_rcc_apb1rstr2_t stm32g4_rcc_apb1enr2_t;

typedef stm32g4_rcc_apb2rstr_t stm32g4_rcc_apb2enr_t;

typedef stm32g4_rcc_apb2rstr_t stm32g4_rcc_apb3enr_t;

typedef stm32g4_rcc_ahb1rstr_t stm32g4_rcc_ahb1smenr_t;

typedef stm32g4_rcc_ahb2rstr_t stm32g4_rcc_ahb2smenr_t;

typedef stm32g4_rcc_ahb3rstr_t stm32g4_rcc_ahb3smenr_t;

typedef stm32g4_rcc_apb1rstr1_t stm32g4_rcc_apb1smenr1_t;

typedef stm32g4_rcc_apb1rstr2_t stm32g4_rcc_apb1smenr2_t;

typedef stm32g4_rcc_apb2rstr_t stm32g4_rcc_apb2smenr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t usart1sel   : 2;
		uint32_t usart2sel   : 2;
		uint32_t usart3sel   : 2;
		uint32_t uart4sel    : 2;
		uint32_t uart5sel    : 2;
		uint32_t lpuart1sel  : 2;
		uint32_t i2c1sel     : 2;
		uint32_t i2c2sel     : 2;
		uint32_t i2c3sel     : 2;
		uint32_t lptim1sel   : 2;
		uint32_t sai1sel     : 2;
		uint32_t i2s23sel    : 2;
		uint32_t fdcansel    : 2;
		uint32_t clk48sel    : 2;
		uint32_t adc12sel    : 2;
		uint32_t adc345sel   : 2;
	};

	uint32_t mask;
} stm32g4_rcc_ccipr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lseon      : 1;
		uint32_t lserdy     : 1;
		uint32_t lsebyp     : 1;
		uint32_t lsedrv     : 2;
		uint32_t lsecsson   : 1;
		uint32_t lsecssd    : 1;
		uint32_t res_7      : 1;
		uint32_t rtcsel     : 2;
		uint32_t res_14_10  : 5;
		uint32_t rtcen      : 1;
		uint32_t bdrst      : 1;
		uint32_t res_23_17  : 7;
		uint32_t lscoen     : 1;
		uint32_t lscosel    : 1;
		uint32_t res_31_26  : 6;
	};

	uint32_t mask;
} stm32g4_rcc_bdcr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t lsion      :  1;
		uint32_t lsirdy     :  1;
		uint32_t res_22_2   : 21;
		uint32_t rmvf       :  1;
		uint32_t res_24     :  1;
		uint32_t oblrstf    :  1;
		uint32_t pinrstf    :  1;
		uint32_t borrstf    :  1;
		uint32_t sftrstf    :  1;
		uint32_t iwdgrstf   :  1;
		uint32_t wwdgrstf   :  1;
		uint32_t lpwrrstf   :  1;
	};

	uint32_t mask;
} stm32g4_rcc_csr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t hsi48on    :  1;
		uint32_t hsi48rdy   :  1;
		uint32_t res_6_2    :  5;
		uint32_t hsi48cal   :  9;
		uint32_t res_31_16  : 16;
	};

	uint32_t mask;
} stm32g4_rcc_crrcr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t i2c4sel    :  2;
		uint32_t res_19_2   : 18;
		uint32_t qspisel    :  2;
		uint32_t res_31_22  : 10;
	};

	uint32_t mask;
} stm32g4_rcc_ccipr2_t;



typedef struct
{
	volatile uint32_t cr;
	volatile uint32_t icscr;
	volatile uint32_t cfgr;
	volatile uint32_t pllcfgr;
	volatile uint32_t res_4_5[2];
	volatile uint32_t cier;
	volatile uint32_t cifr;
	volatile uint32_t cicr;
	volatile uint32_t res_9;
	volatile uint32_t ahb1rstr;
	volatile uint32_t ahb2rstr;
	volatile uint32_t ahb3rstr;
	volatile uint32_t res_13;
	volatile uint32_t apb1rstr1;
	volatile uint32_t apb1rstr2;
	volatile uint32_t apb2rstr;
	volatile uint32_t res_17;
	volatile uint32_t ahb1enr;
	volatile uint32_t ahb2enr;
	volatile uint32_t ahb3enr;
	volatile uint32_t res_21;
	volatile uint32_t apb1enr1;
	volatile uint32_t apb1enr2;
	volatile uint32_t apb2enr;
	volatile uint32_t res_25;
	volatile uint32_t ahb1smenr;
	volatile uint32_t ahb2smenr;
	volatile uint32_t ahb3smenr;
	volatile uint32_t res_29;
	volatile uint32_t apb1smenr1;
	volatile uint32_t apb1smenr2;
	volatile uint32_t apb2smenr;
	volatile uint32_t res_33;
	volatile uint32_t ccipr;
	volatile uint32_t res_35;
	volatile uint32_t bdcr;
	volatile uint32_t csr;
	volatile uint32_t crrcr;
	volatile uint32_t ccipr2;
	volatile uint32_t reserved[216];
} stm32g4_rcc_t;

static volatile stm32g4_rcc_t * const stm32g4_rcc
	= (volatile stm32g4_rcc_t*)0x40021000;

#define STM32G4_RCC_FIELD_READ(r, f) \
	((stm32g4_rcc_##r##_t)stm32g4_rcc->r).f
#define STM32G4_RCC_FIELD_WRITE(r, f, v) \
	do { stm32g4_rcc_##r##_t r = { .mask = stm32g4_rcc->r }; \
	r.f = v; stm32g4_rcc->r = r.mask; } while (0)

#endif
