#ifndef __STM32G4_REG_TIMER_H__
#define __STM32G4_REG_TIMER_H__

#include <stdint.h>
#include <stddef.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cen       :  1;
		uint32_t udis      :  1;
		uint32_t urs       :  1;
		uint32_t opm       :  1;
		uint32_t dir       :  1;
		uint32_t cms       :  2;
		uint32_t apre      :  1;
		uint32_t ckd       :  2;
		uint32_t res_10    :  1;
		uint32_t uifremap  :  1;
		uint32_t dithen    :  1;
		uint32_t res_31_13 : 19;
	};

	uint32_t mask;
} stm32g4_timer_cr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t ccpc      : 1;
		uint32_t res_1     : 1;
		uint32_t ccus      : 1;
		uint32_t ccds      : 1;
		uint32_t mms       : 3;
		uint32_t ti1s      : 1;
		uint32_t ois1      : 1;
		uint32_t ois1n     : 1;
		uint32_t ois2      : 1;
		uint32_t ois2n     : 1;
		uint32_t ois3      : 1;
		uint32_t ois3n     : 1;
		uint32_t ois4      : 1;
		uint32_t res_15    : 1;
		uint32_t ois5      : 1;
		uint32_t res_17    : 1;
		uint32_t ois6      : 1;
		uint32_t res_19    : 1;
		uint32_t mms2      : 4;
		uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_timer_cr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t sms       : 3;
		uint32_t occs      : 1;
		uint32_t ts        : 3;
		uint32_t msm       : 1;
		uint32_t etf       : 4;
		uint32_t etps      : 2;
		uint32_t ece       : 1;
		uint32_t etp       : 1;
		uint32_t sms_3     : 1;
		uint32_t res_19_17 : 3;
		uint32_t ts_4_3    : 2;
		uint32_t res_23_22 : 2;
		uint32_t smspe     : 1;
		uint32_t smsps     : 1;
		uint32_t res_31_26 : 6;
	};

	uint32_t mask;
} stm32g4_timer_smcr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t uie       : 1;
		uint32_t cc1ie     : 1;
		uint32_t cc2ie     : 1;
		uint32_t cc3ie     : 1;
		uint32_t cc4ie     : 1;
		uint32_t comie     : 1;
		uint32_t tie       : 1;
		uint32_t bie       : 1;
		uint32_t ude       : 1;
		uint32_t cc1de     : 1;
		uint32_t cc2de     : 1;
		uint32_t cc3de     : 1;
		uint32_t cc4de     : 1;
		uint32_t comde     : 1;
		uint32_t tde       : 1;
		uint32_t res_19_15 : 5;
		uint32_t idxie     : 1;
		uint32_t dirie     : 1;
		uint32_t ierrie    : 1;
		uint32_t terrie    : 1;
		uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_timer_dier_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t uif       : 1;
		uint32_t cc1if     : 1;
		uint32_t cc2if     : 1;
		uint32_t cc3if     : 1;
		uint32_t cc4if     : 1;
		uint32_t comif     : 1;
		uint32_t tif       : 1;
		uint32_t bif       : 1;
		uint32_t b2if      : 1;
		uint32_t cc1of     : 1;
		uint32_t cc2of     : 1;
		uint32_t cc3of     : 1;
		uint32_t cc4of     : 1;
		uint32_t sbif      : 1;
        uint32_t res_15_14 : 2;
		uint32_t cc5if     : 1;
		uint32_t cc6if     : 1;
		uint32_t idxf      : 1;
		uint32_t dirf      : 1;
		uint32_t ierrf     : 1;
		uint32_t terrf     : 1;
		uint32_t res_31_24 : 8;
	};

	uint32_t mask;
} stm32g4_timer_sr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t ug       :  1;
		uint32_t cc1g     :  1;
		uint32_t cc2g     :  1;
		uint32_t cc3g     :  1;
		uint32_t cc4g     :  1;
		uint32_t comg     :  1;
		uint32_t tg       :  1;
		uint32_t bg       :  1;
		uint32_t b2g      :  1;
		uint32_t res_31_9 : 23;
	};

	uint32_t mask;
} stm32g4_timer_egr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cc1s      :  2;
		uint32_t ic1psc    :  2;
		uint32_t ic1f      :  4;
		uint32_t cc2s      :  2;
		uint32_t ic2psc    :  2;
		uint32_t ic2f      :  4;
		uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_timer_ccmr1_ic_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cc1s      : 2;
		uint32_t oc1fe     : 1;
		uint32_t oc1pe     : 1;
		uint32_t oc1m      : 3;
		uint32_t oc1ce     : 1;
		uint32_t cc2s      : 2;
		uint32_t oc2fe     : 1;
		uint32_t oc2pe     : 1;
		uint32_t oc2m      : 3;
		uint32_t oc2ce     : 1;
		uint32_t oc1m_3    : 1;
		uint32_t res_23_17 : 7;
		uint32_t oc2m_3    : 1;
		uint32_t res_31_25 : 7;
	};

	uint32_t mask;
} stm32g4_timer_ccmr1_oc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cc3s      :  2;
		uint32_t ic3psc    :  2;
		uint32_t ic3f      :  4;
		uint32_t cc4s      :  2;
		uint32_t ic4psc    :  2;
		uint32_t ic4f      :  4;
		uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_timer_ccmr2_ic_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cc3s      : 2;
		uint32_t oc3fe     : 1;
		uint32_t oc3pe     : 1;
		uint32_t oc3m      : 3;
		uint32_t oc3ce     : 1;
		uint32_t cc4s      : 2;
		uint32_t oc4fe     : 1;
		uint32_t oc4pe     : 1;
		uint32_t oc4m      : 3;
		uint32_t oc4ce     : 1;
		uint32_t oc3m_3    : 1;
		uint32_t res_23_17 : 7;
		uint32_t oc4m_3    : 1;
		uint32_t res_31_25 : 7;
	};

	uint32_t mask;
} stm32g4_timer_ccmr2_oc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cc1e      :  1;
		uint32_t cc1p      :  1;
		uint32_t cc1ne     :  1;
		uint32_t cc1np     :  1;
		uint32_t cc2e      :  1;
		uint32_t cc2p      :  1;
		uint32_t cc2ne     :  1;
		uint32_t cc2np     :  1;
		uint32_t cc3e      :  1;
		uint32_t cc3p      :  1;
		uint32_t cc3ne     :  1;
		uint32_t cc3np     :  1;
		uint32_t cc4e      :  1;
		uint32_t cc4p      :  1;
		uint32_t cc4ne     :  1;
		uint32_t cc4np     :  1;
		uint32_t cc5e      :  1;
		uint32_t cc5p      :  1;
		uint32_t res_19_18 :  2;
		uint32_t cc6e      :  1;
		uint32_t cc6p      :  1;
		uint32_t res_31_22 : 10;
	};

	uint32_t mask;
} stm32g4_timer_ccer_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t cnt       : 16;
		uint32_t res_30_16 : 15;
		uint32_t uifcpy    :  1;
	};

	uint32_t mask;
} stm32g4_timer_cnt_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t dt        : 8;
		uint32_t lock      : 2;
		uint32_t ossi      : 1;
		uint32_t ossr      : 1;
		uint32_t bke       : 1;
		uint32_t bkp       : 1;
		uint32_t aoe       : 1;
		uint32_t moe       : 1;
		uint32_t bkf       : 4;
		uint32_t bk2f      : 4;
		uint32_t bk2e      : 1;
		uint32_t bk2p      : 1;
		uint32_t bkdsrm    : 1;
		uint32_t bk2dsrm   : 1;
		uint32_t bkbid     : 1;
		uint32_t bk2bid    : 1;
		uint32_t res_31_30 : 2;
	};

	uint32_t mask;
} stm32g4_timer_bdtr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t ccr5      : 16;
		uint32_t res_28_16 : 13;
		uint32_t gc5c1     :  1;
		uint32_t gc5c2     :  1;
		uint32_t gc5c3     :  1;
	};

	uint32_t mask;
} stm32g4_timer_ccr5_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t res_1_0   : 2;
		uint32_t oc5fe     : 1;
		uint32_t oc5pe     : 1;
		uint32_t oc5m      : 3;
		uint32_t oc5ce     : 1;
		uint32_t res_9_8   : 2;
		uint32_t oc6fe     : 1;
		uint32_t oc6pe     : 1;
		uint32_t oc6m      : 3;
		uint32_t oc6ce     : 1;
		uint32_t oc5m_3    : 1;
		uint32_t res_23_17 : 7;
		uint32_t oc6m_3    : 1;
		uint32_t res_31_25 : 7;
	};

	uint32_t mask;
} stm32g4_timer_ccmr3_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t dtgf      :  8;
		uint32_t res_15_8  :  8;
		uint32_t dtae      :  1;
		uint32_t dtpe      :  1;
		uint32_t res_31_18 : 14;
	};

	uint32_t mask;
} stm32g4_timer_dtr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t ie        : 1;
		uint32_t idir      : 2;
		uint32_t res_4_3   : 2;
		uint32_t fidx      : 1;
		uint32_t ipos      : 2;
		uint32_t res_15_8  : 8;
		uint32_t pw        : 8;
		uint32_t pwprsc    : 3;
		uint32_t res_31_27 : 5;
	};

	uint32_t mask;
} stm32g4_timer_erc_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t ti1sel    : 4;
		uint32_t res_7_4   : 4;
		uint32_t ti2sel    : 4;
		uint32_t res_15_12 : 4;
		uint32_t ti3sel    : 4;
		uint32_t res_23_20 : 4;
		uint32_t ti4sel    : 4;
		uint32_t res_31_28 : 4;
	};

	uint32_t mask;
} stm32g4_timer_tisel_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t bkine     :  1;
		uint32_t bkcmp1e   :  1;
		uint32_t bkcmp2e   :  1;
		uint32_t bkcmp3e   :  1;
		uint32_t bkcmp4e   :  1;
		uint32_t bkcmp5e   :  1;
		uint32_t bkcmp6e   :  1;
		uint32_t bkcmp7e   :  1;
		uint32_t res_8     :  1;
		uint32_t bkinp     :  1;
		uint32_t bkcmp1p   :  1;
		uint32_t bkcmp2p   :  1;
		uint32_t bkcmp3p   :  1;
		uint32_t bkcmp4p   :  1;
		uint32_t etrsel    :  4;
		uint32_t res_31_18 : 14;
	};

	uint32_t mask;
} stm32g4_timer_af1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t bk2ine     :  1;
		uint32_t bk2cmp1e   :  1;
		uint32_t bk2cmp2e   :  1;
		uint32_t bk2cmp3e   :  1;
		uint32_t bk2cmp4e   :  1;
		uint32_t bk2cmp5e   :  1;
		uint32_t bk2cmp6e   :  1;
		uint32_t bk2cmp7e   :  1;
		uint32_t res_8      :  1;
		uint32_t bk2inp     :  1;
		uint32_t bk2cmp1p   :  1;
		uint32_t bk2cmp2p   :  1;
		uint32_t bk2cmp3p   :  1;
		uint32_t bk2cmp4p   :  1;
		uint32_t res_15_14  :  2;
		uint32_t ocrsel     :  3;
		uint32_t res_31_19  : 13;
	};

	uint32_t mask;
} stm32g4_timer_af2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t dba       :  5;
		uint32_t res_7_5   :  3;
		uint32_t dbl       :  5;
		uint32_t res_31_13 : 19;
	};

	uint32_t mask;
} stm32g4_timer_dcr_t;

typedef struct
{
	volatile uint32_t cr1;
	volatile uint32_t cr2;
	volatile uint32_t smcr;
	volatile uint32_t dier;
	volatile uint32_t sr;
	volatile uint32_t egr;
	volatile uint32_t ccmr1;
	volatile uint32_t ccmr2;
	volatile uint32_t ccer;
	volatile uint32_t cnt;
    volatile uint32_t psc;
	volatile uint32_t arr;
	volatile uint32_t rcr;
	volatile uint32_t ccr1;
	volatile uint32_t ccr2;
	volatile uint32_t ccr3;
	volatile uint32_t ccr4;
	volatile uint32_t bdtr;
	volatile uint32_t ccr5;
	volatile uint32_t ccr6;
	volatile uint32_t ccmr3;
	volatile uint32_t dtr2;
	volatile uint32_t ecr;
	volatile uint32_t tisel;
	volatile uint32_t af1;
	volatile uint32_t af2;
	volatile uint32_t res_246_26[220];
	volatile uint32_t dcr;
	volatile uint32_t dmar;
} stm32g4_timer_t;

typedef enum
{
	STM32G4_TIMER_TIM1 = 0,
	STM32G4_TIMER_TIM2,
	STM32G4_TIMER_TIM3,
	STM32G4_TIMER_TIM4,
	STM32G4_TIMER_TIM5,
	STM32G4_TIMER_TIM6,
	STM32G4_TIMER_TIM7,
	STM32G4_TIMER_TIM8,
	STM32G4_TIMER_TIM15,
	STM32G4_TIMER_TIM16,
	STM32G4_TIMER_TIM17,
	STM32G4_TIMER_TIM20,

	STM32G4_TIMER_COUNT
} stm32g4_timer_e;

static volatile stm32g4_timer_t * const stm32g4_timer[STM32G4_TIMER_COUNT] =
{
	[STM32G4_TIMER_TIM1 ] = (volatile stm32g4_timer_t*)0x40012C00,
	[STM32G4_TIMER_TIM2 ] = (volatile stm32g4_timer_t*)0x40000000,
	[STM32G4_TIMER_TIM3 ] = (volatile stm32g4_timer_t*)0x40000400,
	[STM32G4_TIMER_TIM4 ] = (volatile stm32g4_timer_t*)0x40000800,
	[STM32G4_TIMER_TIM5 ] = (volatile stm32g4_timer_t*)0x40000C00,
	[STM32G4_TIMER_TIM6 ] = (volatile stm32g4_timer_t*)0x40001000,
	[STM32G4_TIMER_TIM7 ] = (volatile stm32g4_timer_t*)0x40001400,
	[STM32G4_TIMER_TIM8 ] = (volatile stm32g4_timer_t*)0x40013400,
	[STM32G4_TIMER_TIM15] = (volatile stm32g4_timer_t*)0x40014000,
	[STM32G4_TIMER_TIM16] = (volatile stm32g4_timer_t*)0x40014400,
	[STM32G4_TIMER_TIM17] = (volatile stm32g4_timer_t*)0x40014800,
	[STM32G4_TIMER_TIM20] = (volatile stm32g4_timer_t*)0x40015000,
};

#define STM32G4_TIMER_FIELD_READ(i, r, f) \
	((stm32g4_timer_##r##_t)stm32g4_timer[i]->r).f
#define STM32G4_TIMER_FIELD_WRITE(i, r, f, v) \
	do { stm32g4_timer_##r##_t r = { .mask = stm32g4_timer[i]->r }; \
	r.f = v; stm32g4_timer[i]->r = r.mask; } while (0)

#endif
