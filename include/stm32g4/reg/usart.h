#ifndef __STM32G4_REG_USART_H__
#define __STM32G4_REG_USART_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t ue      : 1;
		uint32_t uesm    : 1;
		uint32_t re      : 1;
		uint32_t te      : 1;
		uint32_t idleie  : 1;
		uint32_t rxfneie : 1;
		uint32_t tcie    : 1;
		uint32_t txfnfie : 1;
		uint32_t peie    : 1;
		uint32_t ps      : 1;
		uint32_t pce     : 1;
		uint32_t wake    : 1;
		uint32_t m0      : 1;
		uint32_t mme     : 1;
		uint32_t cmie    : 1;
		uint32_t over8   : 1;
		uint32_t dedt    : 5;
		uint32_t deat    : 5;
		uint32_t rtoie   : 1;
		uint32_t eobie   : 1;
		uint32_t m1      : 1;
		uint32_t fifoen  : 1;
		uint32_t txfeie  : 1;
		uint32_t rxffie  : 1;
	};

	uint32_t mask;
} stm32g4_usart_cr1_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t slven    : 1;
		uint32_t res_2_1  : 2;
		uint32_t dis_nss  : 1;
		uint32_t addm7    : 1;
		uint32_t lbdl     : 1;
		uint32_t lbdie    : 1;
		uint32_t res_7    : 1;
		uint32_t lbcl     : 1;
		uint32_t cpha     : 1;
		uint32_t cpol     : 1;
		uint32_t clken    : 1;
		uint32_t stop     : 2;
		uint32_t linen    : 1;
		uint32_t swap     : 1;
		uint32_t rxinv    : 1;
		uint32_t txinv    : 1;
		uint32_t datainv  : 1;
		uint32_t msbfirst : 1;
		uint32_t abren    : 1;
		uint32_t abrmod   : 2;
		uint32_t rtoen    : 1;
		uint32_t add      : 8;
	};

	uint32_t mask;
} stm32g4_usart_cr2_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t eie       : 1;
		uint32_t iren      : 1;
		uint32_t irlp      : 1;
		uint32_t hdsel     : 1;
		uint32_t nack      : 1;
		uint32_t scen      : 1;
		uint32_t dmar      : 1;
		uint32_t dmat      : 1;
		uint32_t rtse      : 1;
		uint32_t ctse      : 1;
		uint32_t ctsie     : 1;
		uint32_t onebit    : 1;
		uint32_t ovrdis    : 1;
		uint32_t ddre      : 1;
		uint32_t dem       : 1;
		uint32_t dep       : 1;
		uint32_t res_16    : 1;
		uint32_t scarcnt   : 3;
		uint32_t wus       : 2;
		uint32_t wufie     : 1;
		uint32_t txftie    : 1;
		uint32_t tcbgtie   : 1;
		uint32_t rxftcfg   : 3;
		uint32_t rxftie    : 1;
		uint32_t txftcfg   : 3;
	};

	uint32_t mask;
} stm32g4_usart_cr3_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t psc       :  8;
		uint32_t gt        :  8;
		uint32_t res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_usart_gtpr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t rto  : 24;
		uint32_t blen :  8;
	};

	uint32_t mask;
} stm32g4_usart_rtor_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t abrrq     :  1;
		uint32_t sbkrq     :  1;
		uint32_t mmrq      :  1;
		uint32_t rxfrq     :  1;
		uint32_t txfrq     :  1;
		uint32_t res_31_5  : 27;
	};

	uint32_t mask;
} stm32g4_usart_rqr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t pe        : 1;
		uint32_t fe        : 1;
		uint32_t ne        : 1;
		uint32_t ore       : 1;
		uint32_t idle      : 1;
		uint32_t rxfne     : 1;
		uint32_t tc        : 1;
		uint32_t txfnf     : 1;
		uint32_t lbdf      : 1;
		uint32_t ctsif     : 1;
		uint32_t cts       : 1;
		uint32_t rtof      : 1;
		uint32_t eobf      : 1;
		uint32_t udr       : 1;
		uint32_t abre      : 1;
		uint32_t abrf      : 1;
		uint32_t busy      : 1;
		uint32_t cmf       : 1;
		uint32_t sbkf      : 1;
		uint32_t rwu       : 1;
		uint32_t wuf       : 1;
		uint32_t teack     : 1;
		uint32_t reack     : 1;
		uint32_t txfe      : 1;
		uint32_t rxff      : 1;
		uint32_t tcbgt     : 1;
		uint32_t rxft      : 1;
		uint32_t txft      : 1;
		uint32_t res_31_28 : 4;
	};

	uint32_t mask;
} stm32g4_usart_isr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t pe        :  1;
		uint32_t fe        :  1;
		uint32_t ne        :  1;
		uint32_t ore       :  1;
		uint32_t idle      :  1;
		uint32_t txfe      :  1;
		uint32_t tc        :  1;
		uint32_t tcbgt     :  1;
		uint32_t lbd       :  1;
		uint32_t cts       :  1;
		uint32_t res_10    :  1;
		uint32_t rto       :  1;
		uint32_t eob       :  1;
		uint32_t udr       :  1;
		uint32_t res_16_14 :  3;
		uint32_t cm        :  1;
		uint32_t res_19_18 :  2;
		uint32_t wu        :  1;
		uint32_t res_31_21 : 11;
	};

	uint32_t mask;
} stm32g4_usart_icr_t;

typedef struct
{
	volatile uint32_t cr1;
	volatile uint32_t cr2;
	volatile uint32_t cr3;
	volatile uint32_t brr;
	volatile uint32_t gtpr;
	volatile uint32_t rtor;
	volatile uint32_t rqr;
	volatile uint32_t isr;
	volatile uint32_t icr;
	volatile uint32_t rdr;
	volatile uint32_t tdr;
	volatile uint32_t presc;
} stm32g4_usart_t;

#define STM32G4_USART_COUNT 6
static volatile stm32g4_usart_t * const stm32g4_usart[STM32G4_USART_COUNT] =
{
	(volatile stm32g4_usart_t*)0x40008000,
	(volatile stm32g4_usart_t*)0x40013800,
	(volatile stm32g4_usart_t*)0x40004400,
	(volatile stm32g4_usart_t*)0x40004800,
	(volatile stm32g4_usart_t*)0x40004C00,
	(volatile stm32g4_usart_t*)0x40005000
};

static const unsigned stm32g4_usart_prescale[12] =
	{ 1, 2, 4, 6, 8, 10, 12, 16, 32, 64, 128, 256 };

#define STM32G4_USART_FIELD_READ(i, r, f) \
	((stm32g4_usart_##r##_t)stm32g4_usart[i]->r).f
#define STM32G4_USART_FIELD_WRITE(i, r, f, v) \
	do { stm32g4_usart_##r##_t r = { .mask = stm32g4_usart[i]->r }; \
	r.f = v; stm32g4_usart[i]->r = r.mask; } while (0)

#endif
