#ifndef __STM32G4_REG_USB_H__
#define __STM32G4_REG_USB_H__

#include <stdint.h>

/* TODO - Find out where Tstartup is defined. */
#define STM32G4_USB_STARTUP_TIME_MS     2
#define STM32G4_USB_BDCR_SETTLE_TIME_MS 1

#define STM32G4_USB_SRAM_SIZE 1024
#define STM32G4_USB_BTABLE_SIZE 64

#define STM32G4_USB_EP_COUNT 8



typedef enum
{
	STM32G4_USB_EP_TYPE_BULK      = 0,
	STM32G4_USB_EP_TYPE_CONTROL   = 1,
	STM32G4_USB_EP_TYPE_ISO       = 2,
	STM32G4_USB_EP_TYPE_INTERRUPT = 3,
} stm32g4_usb_ep_type_e;

typedef enum
{
	STM32G4_USB_EP_KIND_NONE       = 0,
	STM32G4_USB_EP_KIND_DBL_BUF    = 1,
	STM32G4_USB_EP_KIND_STATUS_OUT = 1,
} stm32g4_usb_ep_kind_e;

typedef enum
{
	STM32G4_USB_EP_STATUS_DISABLED = 0,
	STM32G4_USB_EP_STATUS_STALL    = 1,
	STM32G4_USB_EP_STATUS_NAK      = 2,
	STM32G4_USB_EP_STATUS_VALID    = 3,
} stm32g4_usb_ep_status_e;


typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       ea        :  4;
		uint32_t       stat_tx   :  2;
		uint32_t       dtog_tx   :  1;
		uint32_t       ctr_tx    :  1;
		uint32_t       ep_kind   :  1;
		uint32_t       ep_type   :  2;
		uint32_t       setup     :  1;
		uint32_t       stat_rx   :  2;
		uint32_t       dtog_rx   :  1;
		uint32_t       ctr_rx    :  1;
		uint32_t const res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_usb_epr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       fres      :  1;
		uint32_t       pdwn      :  1;
		uint32_t       lp_mode   :  1;
		uint32_t       fsusp     :  1;
		uint32_t       resume    :  1;
		uint32_t       l1resume  :  1;
		uint32_t const res_6     :  1;
		uint32_t       l1reqm    :  1;
		uint32_t       esofm     :  1;
		uint32_t       sofm      :  1;
		uint32_t       resetm    :  1;
		uint32_t       suspm     :  1;
		uint32_t       wkupm     :  1;
		uint32_t       errm      :  1;
		uint32_t       pmaovrm   :  1;
		uint32_t       ctrm      :  1;
		uint32_t const res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_usb_cntr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t const ep_id     :  4;
		uint32_t const dir       :  1;
		uint32_t const res_6_5   :  2;
		uint32_t       l1req     :  1;
		uint32_t       esof      :  1;
		uint32_t       sof       :  1;
		uint32_t       reset     :  1;
		uint32_t       susp      :  1;
		uint32_t       wkup      :  1;
		uint32_t       err       :  1;
		uint32_t       pmaovr    :  1;
		uint32_t const ctr       :  1;
		uint32_t const res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_usb_istr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t const fn        : 11;
		uint32_t const lsof      :  2;
		uint32_t const lck       :  1;
		uint32_t const rxd       :  2;
		uint32_t const res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_usb_fnr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       add       :  7;
		uint32_t       ef        :  1;
		uint32_t const res_31_8 : 24;
	};

	uint32_t mask;
} stm32g4_usb_daddr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       btable     : 16;
		uint32_t const res_31_16  : 16;
	};

	uint32_t mask;
} stm32g4_usb_btable_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       lpmen    :  1;
		uint32_t       lpmack   :  1;
		uint32_t const res_2    :  1;
		uint32_t const remwake  :  1;
		uint32_t const besl     :  4;
		uint32_t const res_31_8 : 24;
	};

	uint32_t mask;
} stm32g4_usb_lpmcsr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       bcden     :  1;
		uint32_t       dcden     :  1;
		uint32_t       pden      :  1;
		uint32_t       sden      :  1;
		uint32_t const dcdet     :  1;
		uint32_t const pdet      :  1;
		uint32_t const sdet      :  1;
		uint32_t const ps2det    :  1;
		uint32_t const res_14_8  :  7;
		uint32_t       dppu      :  1;
		uint32_t const res_31_16 : 16;
	};

	uint32_t mask;
} stm32g4_usb_bcdr_t;

typedef struct
{
	volatile       uint32_t epr[STM32G4_USB_EP_COUNT];
	volatile const uint32_t res_15_8[8];
	volatile       uint32_t cntr;
	volatile       uint32_t istr;
	volatile const uint32_t fnr;
	volatile       uint32_t daddr;
	volatile       uint32_t btable;
	volatile       uint32_t lpmcsr;
	volatile       uint32_t bcdr;
} stm32g4_usb_t;

static volatile stm32g4_usb_t * const stm32g4_usb =
	(volatile stm32g4_usb_t*)0x40005C00;

static volatile uint16_t * const stm32g4_usb_sram =
	(volatile uint16_t*)0x40006000;

#define STM32G4_USB_FIELD_READ(r, f) \
	((stm32g4_usb_##r##_t)stm32g4_usb->r).f
#define STM32G4_USB_FIELD_WRITE(r, f, v) \
	do { stm32g4_usb_##r##_t r = { .mask = stm32g4_usb->r }; \
	r.f = v; stm32g4_usb->r = r.mask; } while (0)

#define STM32G4_USB_EP_FIELD_READ(i, f) \
	((stm32g4_usb_epr_t)(stm32g4_usb->epr[i])).f

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint16_t count     : 10;
		uint16_t num_block :  5;
		uint16_t blsize    :  1;
	};

	uint16_t mask;
} stm32g4_usb_btable_count_t;

#endif
