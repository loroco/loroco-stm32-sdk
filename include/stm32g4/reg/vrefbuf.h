#ifndef __STM32G4_REG_VREFBUF_H__
#define __STM32G4_REG_VREFBUF_H__

#include <stdint.h>

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       envr     :  1;
		uint32_t       hiz      :  1;
		const uint32_t res_2    :  1;
		const uint32_t vrr      :  1;
		uint32_t       vrs      :  2;
		const uint32_t res_31_6 : 26;
	};

	uint32_t mask;
} stm32g4_vrefbuf_csr_t;

typedef union
__attribute__((__packed__))
{
	struct
	__attribute__((__packed__))
	{
		uint32_t       trim     :  6;
		const uint32_t res_31_6 : 26;
	};

	uint32_t mask;
} stm32g4_vrefbuf_ccr_t;

typedef enum
{
	STM32G4_VREFBUF_VRS_2V048 = 0,
	STM32G4_VREFBUF_VRS_2V5   = 1,
	STM32G4_VREFBUF_VRS_2V9   = 2,
} stm32g4_vrefbuf_vrs_e;


typedef struct
__attribute__((__packed__))
{
	volatile uint32_t csr;
	volatile uint32_t ccr;
} stm32g4_vrefbuf_t;

static volatile stm32g4_vrefbuf_t * const stm32g4_vrefbuf
	= (volatile stm32g4_vrefbuf_t*)0x40010030;

#define STM32G4_VREFBUF_FIELD_READ(r, f) \
	((stm32g4_vrefbuf_##r##_t)stm32g4_vrefbuf->r).f
#define STM32G4_VREFBUF_FIELD_WRITE(r, f, v) \
	do { stm32g4_vrefbuf_##r##_t r = { .mask = stm32g4_vrefbuf->r }; \
	r.f = v; stm32g4_vrefbuf->r = r.mask; } while (0)

#endif
