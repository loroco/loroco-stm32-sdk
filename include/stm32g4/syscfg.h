#ifndef __STM32G4_SYSCFG_H__
#define __STM32G4_SYSCFG_H__

#include <stdbool.h>

void stm32g4_syscfg_reset(void);
void stm32g4_syscfg_enable(bool enable);

bool stm32g4_syscfg_vrefbuf_enable(bool external, unsigned millivolts);
void stm32g4_syscfg_vrefbuf_disable(void);
bool stm32g4_syscfg_vrefbuf_wait_ready(unsigned timeout);

#endif
