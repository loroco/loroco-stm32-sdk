#ifndef __STM32G4_TIMER_H__
#define __STM32G4_TIMER_H__

#include <stdbool.h>
#include <stdint.h>

#include <stm32g4/reg/timer.h>

typedef enum
{
	STM32G4_TIMER_DIR_UP = 0,
	STM32G4_TIMER_DIR_DOWN,
} stm32g4_timer_dir_e;

typedef enum
{
	STM32G4_TIMER_INT_UPDATE,
	STM32G4_TIMER_INT_COM,
	STM32G4_TIMER_INT_TRIGGER,
	STM32G4_TIMER_INT_BREAK,
	STM32G4_TIMER_INT_BREAK2,
	STM32G4_TIMER_INT_SYSTEM_BREAK,

	STM32G4_TIMER_INT_IDX,
	STM32G4_TIMER_INT_DIR,
	STM32G4_TIMER_INT_IERR,
	STM32G4_TIMER_INT_TERR,

	STM32G4_TIMER_INT_CH0,
	STM32G4_TIMER_INT_CH1,
	STM32G4_TIMER_INT_CH2,
	STM32G4_TIMER_INT_CH3,
	STM32G4_TIMER_INT_CH4,
	STM32G4_TIMER_INT_CH5,

	STM32G4_TIMER_INT_COUNT
} stm32g4_timer_int_e;

void stm32g4_timer_reset(stm32g4_timer_e timer);
void stm32g4_timer_enable(stm32g4_timer_e timer, bool enable);

bool stm32g4_timer_interrupt_register(
	stm32g4_timer_e timer, stm32g4_timer_int_e type,
	void (*handler)(void));

bool stm32g4_timer_freq_get(stm32g4_timer_e timer, unsigned* base, unsigned* freq);
bool stm32g4_timer_freq_set(stm32g4_timer_e timer, unsigned target, unsigned* freq);

bool stm32g4_timer_start(stm32g4_timer_e timer,
	bool oneshot, stm32g4_timer_dir_e dir,
	uint16_t count, uint16_t repeat);
void stm32g4_timer_stop(stm32g4_timer_e timer);

void stm32g4_timer_channel_disable(stm32g4_timer_e timer, unsigned channel);

bool stm32g4_timer_channel_pwm_enable(
	stm32g4_timer_e timer, unsigned channel, uint16_t compare);
bool stm32g4_timer_channel_compare_set(
	stm32g4_timer_e timer, unsigned channel, uint16_t compare);

#endif
