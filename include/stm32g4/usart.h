#ifndef __STM32G4_USART_H__
#define __STM32G4_USART_H__

#include <stdbool.h>
#include <stdlib.h>

typedef enum
{
	STM32G4_LPUART1 = 0,
	STM32G4_USART1  = 1,
	STM32G4_USART2  = 2,
	STM32G4_USART3  = 3,
	STM32G4_UART4   = 4,
	STM32G4_UART5   = 5,
	STM32G4_UART_COUNT
} stm32g4_usart_interface_e;

typedef enum
{
	STM32G4_USART_DIR_RX,
	STM32G4_USART_DIR_TX,
	STM32G4_USART_DIR_COUNT
} stm32g4_usart_dir_e;

void stm32g4_usart_reset(unsigned interface);
void stm32g4_usart_enable(unsigned interface, bool enable);

bool stm32g4_usart_config_set(
	unsigned interface,
	unsigned baud,
	unsigned char_len, unsigned stop_bits,
	bool parity, bool parity_odd, bool fifo_en);
bool stm32g4_usart_dma_mode_enable(
	unsigned interface, stm32g4_usart_dir_e direction, bool enable);

/* Optimised in-memory functions */
size_t stm32_usart_tx_acquire(unsigned interface, uint8_t** data);
bool   stm32_usart_tx_commit (unsigned interface, size_t size);

size_t stm32_usart_rx_acquire(unsigned interface, const uint8_t** data);
void   stm32_usart_rx_release(unsigned interface);

/* Simple memcopy functions */
void stm32g4_usart_read (unsigned interface, size_t size, char* data);
void stm32g4_usart_write(unsigned interface, size_t size, const char* data);

#endif
