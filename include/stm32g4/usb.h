#ifndef __STM32G4_USB_H__
#define __STM32G4_USB_H__

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include <protocols/usb.h>


void stm32_usb_reset(void);
void stm32_usb_enable(bool enable);

bool stm32_usb_port_detect(usb_port_type_e* type);

bool stm32_usb_open(
	void* handle,
	void (*configure)(void*),
	void (*suspend  )(void*),
	void (*resume   )(void*));
void stm32_usb_close(void);

bool stm32_usb_endpoint_configure(
	unsigned index, uint8_t addr,
	usb_endpoint_type_e type, bool dbl_buf,
	size_t packet_size,
	void* handle,
	void (*tx)(void*, uint8_t),
	void (*rx)(void*, uint8_t, bool));

void stm32_usb_endpoint_setup_begin(unsigned index, bool is_rx, unsigned count);
void stm32_usb_endpoint_setup_acknowledge(unsigned index, bool success);

/* Optimised in-memory functions */
size_t stm32_usb_endpoint_tx_acquire(unsigned index, void** data);
bool   stm32_usb_endpoint_tx_commit(unsigned index, size_t size);

size_t stm32_usb_endpoint_rx_acquire(unsigned index, const void** data);
void   stm32_usb_endpoint_rx_release(unsigned index);

/* Simple memcopy functions */
bool   stm32_usb_endpoint_write(unsigned index, size_t size, void* data);
size_t stm32_usb_endpoint_read(unsigned index, size_t size, void* data);

bool stm32_usb_addr_set(uint8_t addr);

#endif
