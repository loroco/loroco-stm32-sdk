#ifndef __STRING_H__
#define __STRING_H__

#include <stddef.h>
#include <stdint.h>

void *memcpy(void * restrict s1, const void * restrict s2, size_t n);
void *memset(void *s, int c, size_t n);

size_t strlen(const char *str);

#endif
