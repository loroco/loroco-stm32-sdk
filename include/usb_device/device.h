#ifndef __STM32G4_USB_DEVICE_H__
#define __STM32G4_USB_DEVICE_H__

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include <protocols/usb.h>
#include <stm32g4/usb.h>

typedef struct usb_endpoint_node_s usb_endpoint_node_t;

typedef struct
{
	uint8_t*  buffer;
	uint32_t  count;
	bool      zlp;
	bool      is_control;
	bool      failure;
} usb_transfer_t;

static inline void usb_transfer_reset(usb_transfer_t* transfer)
{
	transfer->buffer     = NULL;
	transfer->count      = 0;
	transfer->zlp        = false;
	transfer->is_control = false;
	transfer->failure    = false;
}

bool usb_transfer_next(usb_transfer_t*      transfer,
                       usb_endpoint_node_t* ep_node,
                       usb_request_dir_e    direction);

bool usb_transfer_is_complete(usb_transfer_t* transfer);

// Interface index doesn't need to map to the HW EP indices,
// as 1 HW EP can have a TX and/or an RX descriptor
typedef enum
{
	USB_ENDPOINT_CTRL,
	USB_ENDPOINT_MAIN,
	USB_ENDPOINT_COUNT
} usb_endpoint_index_e;

typedef enum
{
	USB_DEVICE_STATE_OFF,
	USB_DEVICE_STATE_INITIALISED,
	USB_DEVICE_STATE_ATTACHED,
	USB_DEVICE_STATE_POWERED,
	USB_DEVICE_STATE_SUSPENDED,
	USB_DEVICE_STATE_DEFAULT,
	USB_DEVICE_STATE_ADDRESSED,
	USB_DEVICE_STATE_CONFIGURED,

	USB_DEVICE_STATE_COUNT
} usb_device_state_e;

// TODO: support multiple configs and interfaces
struct usb_endpoint_node_s
{
	usb_endpoint_index_e ep_index;
	size_t               packet_size;
	usb_status_value_t   status_value;
	uint16_t             frame_num;
	usb_transfer_t       transfer[2];
};

typedef struct
{
	usb_endpoint_node_t endpoint_node[USB_ENDPOINT_COUNT];
	usb_setup_packet_t  setup_packet;
	usb_status_value_t  status_value;
} usb_device_node_t;

bool usb_device_open (void);
void usb_device_close(void);

usb_device_state_e usb_device_get_state(void);

void usb_device_request_send_data   (void* data, size_t size);
void usb_device_request_receive_data(void* data, size_t size);
void usb_device_request_cancel      (void);

#endif