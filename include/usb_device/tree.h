#ifndef __STM32G4_USB_TREE_H__
#define __STM32G4_USB_TREE_H__

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include <protocols/usb.h>
#include <usb_device/device.h>
// TODO: support multiple configs and interfaces
#define USB_INTERFACE_NODE_COUNT 1
#define USB_CONFIG_NODE_COUNT    1

/* Endpoint configs */
// Note: we don't have the control endpoint in descriptors, as we never need
//       to send it's descriptor.
// This struct is the layout required when config descriptor is returned
#define USB_CNTRL_PACKET_SIZE  32
#define USB_STREAM_PACKET_SIZE 512 // TODO: check this

typedef enum
{
	USB_ENDPOINT_DESC_INDEX_MAIN_RX,
	USB_ENDPOINT_DESC_INDEX_MAIN_TX,
	USB_ENDPOINT_DESC_INDEX_COUNT
} usb_endpoint_desc_index_e;

typedef struct
__attribute__((__packed__))
{
	usb_device_descriptor_t        device;
	usb_configuration_descriptor_t configuration;
	usb_interface_descriptor_t     interface;
	usb_endpoint_descriptor_t      endpoint[USB_ENDPOINT_DESC_INDEX_COUNT];
} usb_descriptors_t;

typedef struct
{
	usb_descriptors_t descriptors;
	bool              initialised;
} usb_device_tree_t;

#endif

usb_device_tree_t* usb_device_tree_init(void);

bool usb_device_tree_descriptor_ref(
	usb_descriptor_e    descriptor_type,
	usb_setup_packet_t* setup,
	usb_transfer_t*     transfer);
