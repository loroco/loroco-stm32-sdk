#ifndef __UTIL_COMMON_H__
#define __UTIL_COMMON_H__

#define ROUND_UP_DIVIDE(num, den) (((num) + (den - 1)) / den)

#endif
