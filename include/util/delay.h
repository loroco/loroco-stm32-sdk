#ifndef __UTIL_DELAY_H__
#define __UTIL_DELAY_H__

#include <stdbool.h>
#include <stdint.h>

bool delay_us(unsigned duration);
bool delay_ms(unsigned duration);

bool timeout_us(unsigned duration, volatile bool* trigger);
bool timeout_ms(unsigned duration, volatile bool* trigger);
void timeout_cancel(void);

#endif
