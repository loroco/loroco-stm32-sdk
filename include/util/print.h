#ifndef __UTIL_PRINT_H__
#define __UTIL_PRINT_H__

#include <stdbool.h>
#include <stdint.h>

#include <stm32g4/usart.h>

bool uart_print(stm32g4_usart_interface_e interface, const char *nul_term_string);

bool uart_print_uint_base(
	stm32g4_usart_interface_e interface,
	int32_t                   value,
	unsigned                  base);

bool uart_print_int_base(
	stm32g4_usart_interface_e interface,
	int32_t                   value,
	unsigned                  base);

// TODO: support floats
// TODO: Add printf function
// bool uart_printf(stm32g4_usart_interface_e interface, const char *format, ...)
//     __attribute__ ((format (printf, 2, 3)));

#endif