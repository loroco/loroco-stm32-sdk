#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>


/* Linker script variables are symbols not declarations.*/
/* This means that their value is their address not contents. */
extern char _data_flash;
extern char _data_begin;
extern char _data_size;

extern char _bss_begin;
extern char _bss_size;

extern char _stack_end;

extern void __attribute__((noreturn)) _start(void);


__attribute__((section (".text.arm_vector_default")))
void arm_vector_default(void)
{
	while (true);
}

__attribute__((section (".text.arm_vector_reset")))
void arm_vector_reset(void)
{
	__builtin_memcpy(&_data_begin, &_data_flash, (uintptr_t)&_data_size);
	__builtin_memset(&_bss_begin, 0x00, (uintptr_t)&_bss_size);

	_start();

	arm_vector_default();
}

void arm_vector_nmi(void)         __attribute__((weak, alias("arm_vector_default")));
void arm_vector_hard_fault(void)  __attribute__((weak, alias("arm_vector_default")));
void arm_vector_mem_manage(void)  __attribute__((weak, alias("arm_vector_default")));
void arm_vector_bus_fault(void)   __attribute__((weak, alias("arm_vector_default")));
void arm_vector_usage_fault(void) __attribute__((weak, alias("arm_vector_default")));
void arm_vector_svc(void)         __attribute__((weak, alias("arm_vector_default")));
void arm_vector_debug_mon(void)   __attribute__((weak, alias("arm_vector_default")));
void arm_vector_pend_sv(void)     __attribute__((weak, alias("arm_vector_default")));
void arm_vector_system_tick(void) __attribute__((weak, alias("arm_vector_default")));

void stm32_irq_wwdg(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_pvd_pvm(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_rtc_tamp_css_lse(void) __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_rtc_wkup(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_flash(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_rcc(void)              __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_exti0(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_exti1(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_exti2(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_exti3(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_exti4(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch1(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch2(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch3(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch4(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch5(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch6(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch7(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_adc1_2(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_usb_hp(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_usb_lp(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fdcan1_int0(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fdcan1_int1(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_ext9_5(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim1_brk__tim15(void)  __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim1_up__tim16(void)   __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim1_trg_com_dir_idx__tim17(void) __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim1_cc(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim2(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim3(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim4(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c1_ev(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c1_er(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c2_ev(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c2_er(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_spi1(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_spi2(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_usart1(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_usart2(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_usart3(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_ext15_10(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_rtc_alarm(void)        __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_usb_wake_up(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim8_brk_terr_ierr(void) __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim8_up(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim8_trg_com_dir_idx(void) __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim8_cc(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_adc3(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fsmc(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_lptim1(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim5(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_spi3(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_uart4(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_uart5(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim6_dacunder(void)    __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim7_dacunder(void)    __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch1(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch2(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch3(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch4(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch5(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_adc4(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_adc5(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_ucpd1(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_comp1_2_3(void)        __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_comp4_5_6(void)        __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_comp7(void)            __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_master(void)     __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_tima(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_timb(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_timc(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_timd(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_time(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_tim_flt(void)    __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_hrtim_timf(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_crs(void)              __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_sai(void)              __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim20_brk_terr_ierr(void) __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim20_up(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim20_trg_com_dir_idx(void) __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_tim20_cc(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fpu(void)              __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c4_ev(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c4_er(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_spi4(void)             __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_aes(void)              __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fdcan2_int0(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fdcan2_int1(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fdcan3_int0(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fdcan3_int1(void)      __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_rng(void)              __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_lpuart(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c3_ev(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_i2c3_er(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dmamux_ovr(void)       __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_quadspi(void)          __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma1_ch8(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch6(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch7(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_dma2_ch8(void)         __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_cordic(void)           __attribute__((weak, alias("arm_vector_default")));
void stm32_irq_fmac(void)             __attribute__((weak, alias("arm_vector_default")));

#define EXT(x) (x)
#define INT(x) (16 + (x))

__attribute__((section (".arm_vector_table")))
const void* arm_vector_table[] =
{
	[EXT( 0)] = &_stack_end,
	[EXT( 1)] = arm_vector_reset,
	[EXT( 2)] = arm_vector_nmi,
	[EXT( 3)] = arm_vector_hard_fault,
	[EXT( 4)] = arm_vector_mem_manage,
	[EXT( 5)] = arm_vector_bus_fault,
	[EXT( 6)] = arm_vector_usage_fault,
	[EXT( 7)] = NULL,
	[EXT( 8)] = NULL,
	[EXT( 9)] = NULL,
	[EXT(10)] = NULL,
	[EXT(11)] = arm_vector_svc,
	[EXT(12)] = arm_vector_debug_mon,
	[EXT(13)] = NULL,
	[EXT(14)] = arm_vector_pend_sv,
	[EXT(15)] = arm_vector_system_tick,

	[INT(  0)] = stm32_irq_wwdg,
	[INT(  1)] = stm32_irq_pvd_pvm,
	[INT(  2)] = stm32_irq_rtc_tamp_css_lse,
	[INT(  3)] = stm32_irq_rtc_wkup,
	[INT(  4)] = stm32_irq_flash,
	[INT(  5)] = stm32_irq_rcc,
	[INT(  6)] = stm32_irq_exti0,
	[INT(  7)] = stm32_irq_exti1,
	[INT(  8)] = stm32_irq_exti2,
	[INT(  9)] = stm32_irq_exti3,
	[INT( 10)] = stm32_irq_exti4,
	[INT( 11)] = stm32_irq_dma1_ch1,
	[INT( 12)] = stm32_irq_dma1_ch2,
	[INT( 13)] = stm32_irq_dma1_ch3,
	[INT( 14)] = stm32_irq_dma1_ch4,
	[INT( 15)] = stm32_irq_dma1_ch5,
	[INT( 16)] = stm32_irq_dma1_ch6,
	[INT( 17)] = stm32_irq_dma1_ch7,
	[INT( 18)] = stm32_irq_adc1_2,
	[INT( 19)] = stm32_irq_usb_hp,
	[INT( 20)] = stm32_irq_usb_lp,
	[INT( 21)] = stm32_irq_fdcan1_int0,
	[INT( 22)] = stm32_irq_fdcan1_int1,
	[INT( 23)] = stm32_irq_ext9_5,
	[INT( 24)] = stm32_irq_tim1_brk__tim15,
	[INT( 25)] = stm32_irq_tim1_up__tim16,
	[INT( 26)] = stm32_irq_tim1_trg_com_dir_idx__tim17,
	[INT( 27)] = stm32_irq_tim1_cc,
	[INT( 28)] = stm32_irq_tim2,
	[INT( 29)] = stm32_irq_tim3,
	[INT( 30)] = stm32_irq_tim4,
	[INT( 31)] = stm32_irq_i2c1_ev,
	[INT( 32)] = stm32_irq_i2c1_er,
	[INT( 33)] = stm32_irq_i2c2_ev,
	[INT( 34)] = stm32_irq_i2c2_er,
	[INT( 35)] = stm32_irq_spi1,
	[INT( 36)] = stm32_irq_spi2,
	[INT( 37)] = stm32_irq_usart1,
	[INT( 38)] = stm32_irq_usart2,
	[INT( 39)] = stm32_irq_usart3,
	[INT( 40)] = stm32_irq_ext15_10,
	[INT( 41)] = stm32_irq_rtc_alarm,
	[INT( 42)] = stm32_irq_usb_wake_up,
	[INT( 43)] = stm32_irq_tim8_brk_terr_ierr,
	[INT( 44)] = stm32_irq_tim8_up,
	[INT( 45)] = stm32_irq_tim8_trg_com_dir_idx,
	[INT( 46)] = stm32_irq_tim8_cc,
	[INT( 47)] = stm32_irq_adc3,
	[INT( 48)] = stm32_irq_fsmc,
	[INT( 49)] = stm32_irq_lptim1,
	[INT( 50)] = stm32_irq_tim5,
	[INT( 51)] = stm32_irq_spi3,
	[INT( 52)] = stm32_irq_uart4,
	[INT( 53)] = stm32_irq_uart5,
	[INT( 54)] = stm32_irq_tim6_dacunder,
	[INT( 55)] = stm32_irq_tim7_dacunder,
	[INT( 56)] = stm32_irq_dma2_ch1,
	[INT( 57)] = stm32_irq_dma2_ch2,
	[INT( 58)] = stm32_irq_dma2_ch3,
	[INT( 59)] = stm32_irq_dma2_ch4,
	[INT( 60)] = stm32_irq_dma2_ch5,
	[INT( 61)] = stm32_irq_adc4,
	[INT( 62)] = stm32_irq_adc5,
	[INT( 63)] = stm32_irq_ucpd1,
	[INT( 64)] = stm32_irq_comp1_2_3,
	[INT( 65)] = stm32_irq_comp4_5_6,
	[INT( 66)] = stm32_irq_comp7,
	[INT( 67)] = stm32_irq_hrtim_master,
	[INT( 68)] = stm32_irq_hrtim_tima,
	[INT( 69)] = stm32_irq_hrtim_timb,
	[INT( 70)] = stm32_irq_hrtim_timc,
	[INT( 71)] = stm32_irq_hrtim_timd,
	[INT( 72)] = stm32_irq_hrtim_time,
	[INT( 73)] = stm32_irq_hrtim_tim_flt,
	[INT( 74)] = stm32_irq_hrtim_timf,
	[INT( 75)] = stm32_irq_crs,
	[INT( 76)] = stm32_irq_sai,
	[INT( 77)] = stm32_irq_tim20_brk_terr_ierr,
	[INT( 78)] = stm32_irq_tim20_up,
	[INT( 79)] = stm32_irq_tim20_trg_com_dir_idx,
	[INT( 80)] = stm32_irq_tim20_cc,
	[INT( 81)] = stm32_irq_fpu,
	[INT( 82)] = stm32_irq_i2c4_ev,
	[INT( 83)] = stm32_irq_i2c4_er,
	[INT( 84)] = stm32_irq_spi4,
	[INT( 85)] = stm32_irq_aes,
	[INT( 86)] = stm32_irq_fdcan2_int0,
	[INT( 87)] = stm32_irq_fdcan2_int1,
	[INT( 88)] = stm32_irq_fdcan3_int0,
	[INT( 89)] = stm32_irq_fdcan3_int1,
	[INT( 90)] = stm32_irq_rng,
	[INT( 91)] = stm32_irq_lpuart,
	[INT( 92)] = stm32_irq_i2c3_ev,
	[INT( 93)] = stm32_irq_i2c3_er,
	[INT( 94)] = stm32_irq_dmamux_ovr,
	[INT( 95)] = stm32_irq_quadspi,
	[INT( 96)] = stm32_irq_dma1_ch8,
	[INT( 97)] = stm32_irq_dma2_ch6,
	[INT( 98)] = stm32_irq_dma2_ch7,
	[INT( 99)] = stm32_irq_dma2_ch8,
	[INT(100)] = stm32_irq_cordic,
	[INT(101)] = stm32_irq_fmac,
};
