#include <cm4/cm4.h>
#include <cm4/nvic.h>


void cm4_nvic_irq_enable(uint8_t irq, bool enable)
{
	uint32_t mask = (1U << (irq % 32));
	if (enable)
		cm4->nvic_iser[irq / 32] |= mask;
	else
		cm4->nvic_icer[irq / 32] |= mask;
}

void cm4_nvic_irq_pending_set(uint8_t irq, bool pending)
{
	uint32_t mask = (1U << (irq % 32));
	if (pending)
		cm4->nvic_ispr[irq / 32] |= mask;
	else
		cm4->nvic_icpr[irq / 32] |= mask;
}

void cm4_nvic_irq_priority_set(uint8_t irq, uint8_t priority)
{
	if (irq < CM4_NVIC_EXC_COUNT)
		return;
	irq -= CM4_NVIC_EXC_COUNT;

	uint32_t ipr = cm4->nvic_ipr[irq / 4];
	unsigned off = ((irq % 4) * 8);
	ipr &= ~(0xFFU << off);
	ipr |= (priority << off);
	cm4->nvic_ipr[irq / 4] = ipr;
}

bool cm4_nvic_irq_is_enabled(uint8_t irq)
{
	return ((cm4->nvic_iser[irq / 32] >> (irq % 32)) & 1);
}

bool cm4_nvic_irq_is_pending(uint8_t irq)
{
	return ((cm4->nvic_ispr[irq / 32] >> (irq % 32)) & 1);
}

bool cm4_nvic_irq_is_active(uint8_t irq)
{
	return ((cm4->nvic_iabr[irq / 32] >> (irq % 32)) & 1);
}

uint8_t cm4_nvic_irq_priority_get(uint8_t irq)
{
	if (irq < CM4_NVIC_EXC_COUNT)
		return 0;
	irq -= CM4_NVIC_EXC_COUNT;
	return ((cm4->nvic_ipr[irq / 4] >> ((irq % 4) * 8)) & 0xFF);
}
