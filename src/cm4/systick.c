#include <cm4/cm4.h>
#include <cm4/systick.h>
#include <cm4/nvic.h>
#include <stddef.h>


static void (*cm4_systick_callback)(void) = NULL;

bool cm4_systick_enable(uint32_t divide, void (*callback)(void))
{
	if (((divide - 1) >> 24) != 0)
		return false;

	if (CM4_FIELD_READ(syst_csr, enable))
		return false;

	CM4_FIELD_WRITE(syst_rvr, reload, (divide - 1));
	CM4_FIELD_WRITE(syst_cvr, current, 0);

	/* Start counter and enable systick EXT trigger */
	cm4_syst_csr_t csr = { .mask = cm4->syst_csr };
	csr.tickint = true;
	csr.enable  = true;
	cm4->syst_csr = csr.mask;

	cm4_systick_callback = callback;
	return true;
}

void cm4_systick_disable(void)
{
	/* Stop counter and disable systick EXT trigger */
	cm4_syst_csr_t csr = { .mask = cm4->syst_csr };
	csr.tickint = false;
	csr.enable  = false;
	cm4->syst_csr = csr.mask;
}

void arm_vector_system_tick(void)
{
	if (cm4_systick_callback)
		cm4_systick_callback();
}
