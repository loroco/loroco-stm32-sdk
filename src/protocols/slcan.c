#include <protocols/slcan.h>
// TODO: move protocol stuff into protocols/fdcan.h
#include <stm32g4/fdcan.h>

#include <stdlib.h>
#include <string.h>

#define SLCAN_STREAM_SIZE_BASE 2

slcan_frame_t* slcan_frame_create(slcan_header_t header, const uint8_t *data)
{
	if ((header.dlc != 0) && (data == NULL))
		return NULL;

	size_t len = can_fd_dlc_to_size(header.dlc);
	slcan_frame_t *frame = malloc(sizeof(slcan_header_t) + len);
	if (!frame)
		return NULL;

	memcpy(&(frame->data), data, len);
	frame->header = header;

	return frame;
}

void slcan_frame_delete(slcan_frame_t *frame)
{
	if (!frame)
		return;

	free(frame);
}

typedef enum
{
	SLCAN_STREAM_PARSER_STATE_TYPE,
	SLCAN_STREAM_PARSER_STATE_ID,
	SLCAN_STREAM_PARSER_STATE_DLC,
	SLCAN_STREAM_PARSER_STATE_DATA,
	SLCAN_STREAM_PARSER_STATE_COMPLETE,
	SLCAN_STREAM_PARSER_STATE_ERROR,
} slcan_stream_parser_state_e;

static uint8_t hex_to_num(uint8_t hex)
{
	uint8_t start;
	if (('0' <= hex) && ('9' >= hex)) start = '0';
	else if (('a' <= hex) && ('f' >= hex)) start = 'a';
	else if (('A' <= hex) && ('F' >= hex)) start = 'A';
	else return 0;

	return hex - start;
}

static uint8_t num_to_hex(uint8_t num)
{
	if (num <= 9)
		return '0' + num;
	else if (num <= 15)
		return 'A' + (num - 10);
	else
		return 'X';
}

slcan_frame_t* slcan_frame_from_stream(
	const uint8_t *stream, uint32_t stream_len)
{
	if (!stream)
		return NULL;

	slcan_header_t               header = {0};
	slcan_stream_parser_state_e  state  = SLCAN_STREAM_PARSER_STATE_TYPE;
	slcan_frame_t               *frame = NULL;

	// temp values
	uint8_t  value, id_size;
	uint32_t data_index;
	for (unsigned i = 0; i < stream_len; i++)
	{
		switch (state)
		{
			case SLCAN_STREAM_PARSER_STATE_TYPE:
				state = SLCAN_STREAM_PARSER_STATE_ID;
				switch (stream[i])
				{
					case ('t'):
						header.type          = SLCAN_MSG_TYPE_DATA;
						header.address_space = SLCAN_ADDRESS_SPACE_11_BIT;
						break;

					case ('r'):
						header.type          = SLCAN_MSG_TYPE_REMOTE;
						header.address_space = SLCAN_ADDRESS_SPACE_11_BIT;
						break;

					case ('T'):
						header.type          = SLCAN_MSG_TYPE_DATA;
						header.address_space = SLCAN_ADDRESS_SPACE_29_BIT;
						break;

					case ('R'):
						header.type          = SLCAN_MSG_TYPE_REMOTE;
						header.address_space = SLCAN_ADDRESS_SPACE_29_BIT;
						break;

					default:
						state = SLCAN_STREAM_PARSER_STATE_ERROR;
						break;
				}
				break;

			case SLCAN_STREAM_PARSER_STATE_ID:
				id_size = slcan_address_space_size(header.address_space);
				if ((i - 1) >= id_size)
				{
					state = SLCAN_STREAM_PARSER_STATE_ERROR;
				}
				else
				{
					value = hex_to_num(stream[i]);
					header.msg_id += (value << ((id_size - i - 1) * 4));
					if (i == id_size) {
						state = SLCAN_STREAM_PARSER_STATE_DLC;
					}
				}
				break;

			case SLCAN_STREAM_PARSER_STATE_DLC:
				value = hex_to_num(stream[i]);
				header.dlc = hex_to_num(value);
				state = SLCAN_STREAM_PARSER_STATE_DATA;
				break;

			case SLCAN_STREAM_PARSER_STATE_DATA:
				data_index = i;
				state = SLCAN_STREAM_PARSER_STATE_COMPLETE;
				break;

			default:
				state = SLCAN_STREAM_PARSER_STATE_ERROR;
				break;
		}

		if (state == SLCAN_STREAM_PARSER_STATE_ERROR)
			break;
		else if (state == SLCAN_STREAM_PARSER_STATE_COMPLETE)
		{
			size_t len = can_fd_dlc_to_size(header.dlc);

			/* See if stream is binary or ASCII */
			header.binary = (stream[data_index] == '&');
			if (header.binary)
				frame = slcan_frame_create(header, &(stream[data_index + 1]));
			else if ((stream_len - data_index) == (len * 2))
			{
				/* Convert ASCII data to array of bytes after checking that
				 * data is within limits of stream */
				uint8_t data[len];
				for (uint32_t i = 0; i < (len * 2); i += 2) {
					data[i / 2] = (hex_to_num(stream[data_index + i]) << 4)
					            + hex_to_num(stream[data_index + i + 1]);
				}
				frame = slcan_frame_create(header, data);
			}
			break;
		}
	}

	return frame;
}

uint8_t* slcan_frame_to_stream(
	const slcan_frame_t *frame, uint32_t *stream_len)
{
	if (!frame) return NULL;

	/* Get stream size */
	uint8_t id_size = slcan_address_space_size(frame->header.address_space);

	/* Encode frame header into ACII */
	uint8_t type, id[id_size], dlc;

	bool lowercase = (frame->header.address_space == SLCAN_ADDRESS_SPACE_11_BIT);
	if (frame->header.type == SLCAN_MSG_TYPE_DATA)
		type = lowercase ? 't' : 'T';
	else if (frame->header.type == SLCAN_MSG_TYPE_REMOTE)
		type = lowercase ? 'r' : 'R';
	else
		return NULL;

	for (uint8_t i = id_size; i > 0; i--) {
		id[i - 1] = num_to_hex((frame->header.msg_id >> (4 * (i - 1))) & 0xf);
	}

	dlc = num_to_hex(frame->header.dlc);

	*stream_len = SLCAN_STREAM_SIZE_BASE + id_size;
	if (frame->header.binary)
		*stream_len += 1 + can_fd_dlc_to_size(frame->header.dlc);
	else
		*stream_len += (can_fd_dlc_to_size(frame->header.dlc) * 2);

	uint8_t *stream = malloc(*stream_len);
	if (!stream) return NULL;

	/* Write encoded header and data to stream */

	stream[0]           = type;
	memcpy(&(stream[1]), &id, id_size);
	stream[id_size + 1] = dlc;

	uint8_t data_start = id_size + 2;

	if (frame->header.binary)
	{
		stream[data_start] = '&';
		memcpy(&(stream[data_start + 1]), &(frame->data),
			can_fd_dlc_to_size(frame->header.dlc));
	}
	else
	{
		// TODO: work out if there's a SIMD mechanism on stm32
		for (uint32_t i = 0; i < (can_fd_dlc_to_size(frame->header.dlc) * 2); i += 2)
		{
			stream[data_start + i]     = num_to_hex((frame->data[i / 2] >> 4) & 0xf);
			stream[data_start + i + 1] = num_to_hex((frame->data[i / 2] >> 0) & 0xf);
		}
	}

	return stream;
}
