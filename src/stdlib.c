#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

/* Linker script variables are symbols not declarations.*/
/* This means that their value is their address not contents. */
extern char _heap_begin;
extern char _heap_size;


#define HEAP_ALIGN 8

#if HEAP_ALIGN < 8
#error "HEAP_ALIGN must be greater than 8."
#endif

#if (HEAP_ALIGN & (HEAP_ALIGN - 1)) != 0
#warning "HEAP_ALIGN should be a power-of-two for performance reasons."
#endif


typedef struct heap_node_s heap_node_t;

struct __attribute__((__packed__)) heap_node_s
{
	struct
	__attribute__((__packed__))
	{
		bool     alloc    :  1;
		unsigned reserved :  2;
		size_t   size     : 29;
	};

	heap_node_t* prev;

#if HEAP_ALIGN > 8
	uint8_t pad[HEAP_ALIGN - 8];
#endif

	uint8_t   body[0];
};


static heap_node_t* heap = (heap_node_t*)&_heap_begin;
static size_t       heap_size = 0;

#ifndef DISABLE_HEAP_FREE_OPT
static heap_node_t* heap_free = NULL;
#endif


static inline heap_node_t* heap_node_next(heap_node_t* node)
{
	uintptr_t next = (uintptr_t)node + sizeof(*node) + (node->size * HEAP_ALIGN);
	if (next >= ((uintptr_t)heap + heap_size)) return NULL;
	return (heap_node_t*)next;
}

static inline heap_node_t* heap_node_from_body(void* ptr)
{
	if (ptr == NULL) return NULL;
	return (heap_node_t*)((uintptr_t)ptr - sizeof(heap_node_t));
}


void* malloc(size_t size)
{
	size = (size + (HEAP_ALIGN - 1)) / HEAP_ALIGN;

	if (heap_size == 0)
	{
		heap_size = ((size_t)&_heap_size + (HEAP_ALIGN - 1)) / HEAP_ALIGN;

		heap_node_t* node = (heap_node_t*)heap;
		node->size  = (heap_size - sizeof(heap_node_t)) / HEAP_ALIGN;
		node->alloc = false;
		node->prev  = NULL;
	}

#ifndef DISABLE_HEAP_FREE_OPT
	/* If we know of a free node, check that first. */
	if (heap_free && (heap_free->size >= size))
	{
		heap_node_t* node = heap_free;
		node->alloc = true;
		heap_free = NULL;

		/* If there's spare space in the node, we split it. */
		if (node->size > size)
		{
			size_t remain = (node->size - (size + 1));
			node->size = size;

			heap_node_t* split = heap_node_next(node);
			split->size  = remain;
			split->alloc = false;
			split->prev  = node;

			heap_node_t* node_next = heap_node_next(split);
			if (node_next) node_next->prev = split;

			heap_free = split;
		}

		return node->body;
	}
#endif

	heap_node_t* node = heap;
	while (node)
	{
		heap_node_t* node_next = heap_node_next(node);
		if (node->alloc || (node->size < size))
		{
			node = node_next;
			continue;
		}

		node->alloc = true;

		/* If there's spare space in the node, we split it. */
		if (node->size > size)
		{
			size_t remain = (node->size - (size + 1));
			node->size = size;

			heap_node_t* split = heap_node_next(node);
			split->size  = remain;
			split->alloc = false;
			split->prev  = node;

			if (node_next) node_next->prev = split;

#ifndef DISABLE_HEAP_FREE_OPT
			if (!heap_free || (heap_free->size < split->size))
				heap_free = split;
#endif
		}

		return node->body;
	}

	return NULL;
}

void free(void* ptr)
{
	if (ptr == NULL) return;

	heap_node_t* node = heap_node_from_body(ptr);
	node->alloc = false;

	heap_node_t* node_next = heap_node_next(node);

	if (node->prev && !node->prev->alloc)
	{
		node->prev->size += (node->size + 1);
		node = node->prev;

		if (node_next)
			node_next->prev = node;
	}

	if (node_next && !node_next->alloc)
	{
		node->size += (node_next->size + 1);
		node_next = heap_node_next(node);
		if (node_next) node_next->prev = node;
	}

#ifndef DISABLE_HEAP_FREE_OPT
	if (!heap_free || (heap_free->size < node->size))
		heap_free = node;
#endif
}
