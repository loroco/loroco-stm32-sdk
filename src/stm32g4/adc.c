#include <stm32g4/adc.h>
#include <stm32g4/rcc.h>
#include <stm32g4/clock.h>
#include <util/delay.h>


static inline bool stm32_adc_group__rcc(stm32g4_adc_group_e group, stm32g4_rcc_unit_e* rcc)
{
	stm32g4_rcc_unit_e unit;
	switch (group)
	{
		case STM32G4_ADC12:
			unit = STM32G4_RCC_ADC12;
			break;

		case STM32G4_ADC345:
			unit = STM32G4_RCC_ADC345;
			break;

		default:
			return false;
	}

	if (rcc) *rcc = unit;
	return true;
}

void stm32_adc_group_reset(stm32g4_adc_group_e group)
{
	stm32g4_rcc_unit_e unit;
	if (stm32_adc_group__rcc(group, &unit))
		stm32g4_rcc_reset(unit);
}

void stm32_adc_group_enable(stm32g4_adc_group_e group, bool enable)
{
	stm32g4_rcc_unit_e unit;
	if (stm32_adc_group__rcc(group, &unit))
		stm32g4_rcc_enable(unit, enable);
}


void stm32_adc_group_mode_set(stm32g4_adc_group_e group, stm32g4_adc_group_mode_e mode)
{
	if (group >= STM32G4_ADC_GROUP_COUNT)
		return;

	STM32G4_ADC_COMMON_FIELD_WRITE(group, ccr, dual, mode);
}

bool stm32_adc_group_clock_set(stm32g4_adc_group_e group, bool external, unsigned divide)
{
	if (group >= STM32G4_ADC_GROUP_COUNT)
		return false;

	unsigned presc  = 0;
	unsigned ckmode = 0;
	if (external)
	{
		switch (divide)
		{
			case 1: ckmode = 1; break;
			case 2: ckmode = 2; break;
			case 4: ckmode = 3; break;
			default: return false;
		}
	}
	else
	{
		switch (divide)
		{
			case   1: presc =  0; break;
			case   2: presc =  1; break;
			case   4: presc =  2; break;
			case   6: presc =  3; break;
			case   8: presc =  4; break;
			case  10: presc =  5; break;
			case  12: presc =  6; break;
			case  16: presc =  7; break;
			case  32: presc =  8; break;
			case  64: presc =  9; break;
			case 128: presc = 10; break;
			case 256: presc = 11; break;
			default: return false;
		}
	}

	stm32g4_adc_ccr_t ccr = { .mask = stm32g4_adc_global[group]->common.ccr };
	ccr.ckmode = ckmode;
	if (external) ccr.presc = presc;
	stm32g4_adc_global[group]->common.ccr = ccr.mask;
	return true;
}

void stm32_adc_group_enable_internal(stm32g4_adc_group_e group, bool vbat, bool vts, bool vrefint)
{
	if (group >= STM32G4_ADC_GROUP_COUNT)
		return;

	stm32g4_adc_ccr_t ccr = { .mask = stm32g4_adc_global[group]->common.ccr };
	ccr.vbatsel   = vbat;
	ccr.vsensesel = vts;
	ccr.vrefen    = vrefint;
	stm32g4_adc_global[group]->common.ccr = ccr.mask;
}


bool stm32_adc_group_freq(stm32g4_adc_group_e group, unsigned* freq)
{
	stm32g4_clock_e group_clock;
	switch (group)
	{
		case STM32G4_ADC12:
			group_clock = STM32G4_CLOCK_ADC12;
			break;

		case STM32G4_ADC345:
			group_clock = STM32G4_CLOCK_ADC345;
			break;

		default:
			return false;
	}

	stm32g4_adc_ccr_t ccr = { .mask = stm32g4_adc_global[group]->common.ccr };

	unsigned ker_freq;
	if (!stm32g4_clock_freq(
		(ccr.ckmode == 0 ? group_clock : STM32G4_CLOCK_AHB), &ker_freq))
		return false;

	unsigned presc_lut[12] = { 1, 2, 4, 6, 8, 10, 12, 16, 32, 64, 128, 256 };

	unsigned divide;
	switch (ccr.ckmode)
	{
		case 1: divide = 1; break;
		case 2: divide = 2; break;
		case 3: divide = 3; break;

		default:
			if (ccr.presc >= 12)
				return false;
			divide = presc_lut[ccr.presc];
			break;
	}

	if (freq) *freq = ker_freq / divide;
	return true;
}


void stm32_adc_power(stm32g4_adc_e adc, bool enable)
{
	if (adc >= STM32G4_ADC_COUNT)
		return;

	if (enable)
	{
		/* Exit deep power-down state. */
		STM32G4_ADC_FIELD_WRITE(adc, cr, deeppwd, 0);

		/* Enable voltage regulator. */
		STM32G4_ADC_FIELD_WRITE(adc, cr, advregen, 1);

		/* Wait TADCVREG_STUP. */
		delay_us(STM32G4_TADCVREG_STUP_US);
	}
	else
	{
		/* Disable voltage regulator. */
		STM32G4_ADC_FIELD_WRITE(adc, cr, advregen, 0);

		/* Enter deep power-down state. */
		STM32G4_ADC_FIELD_WRITE(adc, cr, deeppwd, 1);
	}
}

static bool stm32_adc_calibrate__wait(stm32g4_adc_e adc)
{
	volatile bool fail = false;
	/* 828us is the worst case for the minimum freq of 1.4kHz on STM32G431. */
	timeout_us(1000, &fail);
	while (!fail && STM32G4_ADC_FIELD_READ(adc, cr, adcal) != 0);
	return !fail;
}

bool stm32_adc_calibrate(stm32g4_adc_e adc, bool single, bool differential)
{
	if (adc >= STM32G4_ADC_COUNT)
		return false;

	stm32g4_adc_group_e group = STM32G4_ADC_GROUP(adc);
	unsigned            index = STM32G4_ADC_GROUP_INDEX(adc);

	if (!single && !differential)
		return true;

	stm32g4_adc_cr_t cr = { .mask = stm32g4_adc_global[group]->adc[index].cr };

	if ((cr.deeppwd != 0)
		|| (cr.advregen == 0)
		|| (cr.aden != 0))
		return false;

	if (single)
	{
		cr.adcaldif = 0;
		cr.adcal    = 1;
		stm32g4_adc_global[group]->adc[index].cr = cr.mask;

		if (!stm32_adc_calibrate__wait(adc))
			return false;
	}

	if (differential)
	{
		cr.adcaldif = 1;
		cr.adcal    = 1;
		stm32g4_adc_global[group]->adc[index].cr = cr.mask;

		if (!stm32_adc_calibrate__wait(adc))
			return false;
	}

	return true;
}

bool stm32_adc_channel_config(
	stm32g4_adc_e adc, unsigned channel,
	bool differential, unsigned sample_period)
{
	if ((adc >= STM32G4_ADC_COUNT)
		|| (channel >= STM32G4_ADC_CHANNEL_COUNT))
		return false;

	unsigned smp;
	switch (sample_period)
	{
		case   2: smp = 0; break;
		case   6: smp = 1; break;
		case  12: smp = 2; break;
		case  24: smp = 3; break;
		case  47: smp = 4; break;
		case  92: smp = 5; break;
		case 247: smp = 6; break;
		case 640: smp = 7; break;
		default: return false;
	}

	stm32g4_adc_group_e group = STM32G4_ADC_GROUP(adc);
	unsigned            index = STM32G4_ADC_GROUP_INDEX(adc);

	uint32_t smpr = (channel < 10
		? stm32g4_adc_global[group]->adc[index].smpr1
		: stm32g4_adc_global[group]->adc[index].smpr2);

	unsigned smpr_offset = (channel % 10) * 3;
	smpr &= ~(0x7U << smpr_offset);
	smpr |= smp << smpr_offset;

	if (channel < 10)
		stm32g4_adc_global[group]->adc[index].smpr1 = smpr;
	else
		stm32g4_adc_global[group]->adc[index].smpr2 = smpr;

	uint32_t difsel = stm32g4_adc_global[group]->adc[index].difsel;
	difsel &= ~(1U << channel);
	difsel |= (!!differential << channel);
	stm32g4_adc_global[group]->adc[index].difsel = difsel;

	return true;
}

bool stm32_adc_channel_sequence_set(
	stm32g4_adc_e adc, bool injected, unsigned count, unsigned* channel)
{
	if ((adc >= STM32G4_ADC_COUNT)
		|| (count == 0)
		|| (count > (injected ? 4 : 16))
		|| !channel)
		return false;

	unsigned c;
	for (c = 0; c < count; c++)
	{
		if (channel[c] >= STM32G4_ADC_CHANNEL_COUNT)
			return false;
	}

	stm32g4_adc_group_e group = STM32G4_ADC_GROUP(adc);
	unsigned            index = STM32G4_ADC_GROUP_INDEX(adc);

	if (injected)
	{
		stm32g4_adc_jsqr_t jsqr = { .mask = stm32g4_adc_global[group]->adc[index].jsqr };

		jsqr.jl = (count - 1);
		if (count >= 1) jsqr.jsq1 = channel[0];
		if (count >= 2) jsqr.jsq2 = channel[1];
		if (count >= 3) jsqr.jsq3 = channel[2];
		if (count >= 4) jsqr.jsq4 = channel[3];

		stm32g4_adc_global[group]->adc[index].jsqr = jsqr.mask;
	}
	else
	{
		stm32g4_adc_sqr1_t sqr1 = { .mask = stm32g4_adc_global[group]->adc[index].sqr1 };
		stm32g4_adc_sqr2_t sqr2 = { .mask = stm32g4_adc_global[group]->adc[index].sqr2 };
		stm32g4_adc_sqr3_t sqr3 = { .mask = stm32g4_adc_global[group]->adc[index].sqr3 };
		stm32g4_adc_sqr4_t sqr4 = { .mask = stm32g4_adc_global[group]->adc[index].sqr4 };

		sqr1.l = (count - 1);
		if (count >=  1) sqr1.sq1  = channel[ 0];
		if (count >=  2) sqr1.sq2  = channel[ 1];
		if (count >=  3) sqr1.sq3  = channel[ 2];
		if (count >=  4) sqr1.sq4  = channel[ 3];

		if (count >=  5) sqr2.sq5  = channel[ 4];
		if (count >=  6) sqr2.sq6  = channel[ 5];
		if (count >=  7) sqr2.sq7  = channel[ 6];
		if (count >=  8) sqr2.sq8  = channel[ 7];
		if (count >=  9) sqr2.sq9  = channel[ 8];

		if (count >= 10) sqr3.sq10 = channel[ 9];
		if (count >= 11) sqr3.sq11 = channel[10];
		if (count >= 12) sqr3.sq12 = channel[11];
		if (count >= 13) sqr3.sq13 = channel[12];
		if (count >= 14) sqr3.sq14 = channel[13];

		if (count >= 15) sqr4.sq15 = channel[14];
		if (count >= 16) sqr4.sq16 = channel[15];

		stm32g4_adc_global[group]->adc[index].sqr1 = sqr1.mask;
		stm32g4_adc_global[group]->adc[index].sqr2 = sqr2.mask;
		stm32g4_adc_global[group]->adc[index].sqr3 = sqr3.mask;
		stm32g4_adc_global[group]->adc[index].sqr4 = sqr4.mask;
	}

	return true;
}


bool stm32_adc_channel_sample_freq(
	stm32g4_adc_e adc, unsigned channel, unsigned* freq)
{
	if ((adc >= STM32G4_ADC_COUNT)
		|| (channel >= STM32G4_ADC_CHANNEL_COUNT))
		return false;

	stm32g4_adc_group_e group = STM32G4_ADC_GROUP(adc);
	unsigned            index = STM32G4_ADC_GROUP_INDEX(adc);

	unsigned smp_lut[8] = { 2, 6, 12, 24, 47, 92, 247, 640 };
	unsigned res_lut[4] = { 12, 10, 8, 6 };

	uint32_t smpr = (channel < 10
		? stm32g4_adc_global[group]->adc[index].smpr1
		: stm32g4_adc_global[group]->adc[index].smpr2);
	unsigned smpr_offset = (channel % 10) * 3;
	unsigned smp = smp_lut[(smpr >> smpr_offset) % 8];
	if (STM32G4_ADC_FIELD_READ(adc, smpr1, smpplus))
		smp += 1;

	unsigned res = res_lut[STM32G4_ADC_FIELD_READ(adc, cfgr, res)];

	unsigned period = smp + res + 1;

	unsigned core_freq;
	if (!stm32_adc_group_freq(group, &core_freq))
		return false;

	if (freq) *freq = core_freq / period;
	return true;
}


bool stm32g4_adc_config(
	stm32g4_adc_e adc, unsigned resolution, unsigned oversample)
{
	if (adc >= STM32G4_ADC_COUNT)
		return false;

	unsigned res;
	switch (resolution)
	{
		case 12: res = 0; break;
		case 10: res = 1; break;
		case  8: res = 2; break;
		case  6: res = 3; break;
		default: return false;
	}

	if (oversample > 8)
		return false;

	unsigned bits  = resolution + oversample;
	unsigned shift = (bits > 16 ? (16 - bits) : 0);

	stm32g4_adc_group_e group = STM32G4_ADC_GROUP(adc);
	unsigned            index = STM32G4_ADC_GROUP_INDEX(adc);

	stm32g4_adc_cfgr_t  cfgr  = { .mask = stm32g4_adc_global[group]->adc[index].cfgr  };
	stm32g4_adc_cfgr2_t cfgr2 = { .mask = stm32g4_adc_global[group]->adc[index].cfgr2 };

	cfgr.res = res;

	cfgr2.rovse = (oversample > 0);
	cfgr2.jovse = (oversample > 0);

	if (oversample > 0)
	{
		cfgr2.ovsr = oversample;
		cfgr2.ovss = shift;
	}

	stm32g4_adc_global[group]->adc[index].cfgr  = cfgr.mask ;
	stm32g4_adc_global[group]->adc[index].cfgr2 = cfgr2.mask;
	return true;
}
