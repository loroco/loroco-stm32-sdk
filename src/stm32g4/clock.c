#include <cm4/cm4.h>
#include <stm32g4/clock.h>
#include <stm32g4/flash.h>
#include <stm32g4/power.h>
#include <stm32g4/reg/crs.h>
#include <stm32g4/reg/rcc.h>
#include <util/delay.h>
#include <stddef.h>


static bool stm32g4_clock_base(stm32g4_clock_e clock, stm32g4_clock_e* base)
{
	stm32g4_clock_e b;
	switch (clock)
	{
		case STM32G4_CLOCK_SYS:
			switch (STM32G4_RCC_FIELD_READ(cfgr, sw))
			{
				case 1:
					b = STM32G4_CLOCK_HSI16;
					break;
				case 2:
					b = STM32G4_CLOCK_HSE;
					break;
				case 3:
					b = STM32G4_CLOCK_PLL_R;
					break;
				default:
					return false;
			}
			break;

		case STM32G4_CLOCK_PLL:
			switch (STM32G4_RCC_FIELD_READ(pllcfgr, pllsrc))
			{
				case 2:
					b = STM32G4_CLOCK_HSI16;
					break;
				case 3:
					b = STM32G4_CLOCK_HSE;
					break;
				default:
					return false;
			}
			break;

		case STM32G4_CLOCK_PLL_P:
		case STM32G4_CLOCK_PLL_Q:
		case STM32G4_CLOCK_PLL_R:
			b = STM32G4_CLOCK_PLL;
			break;

		case STM32G4_CLOCK_MCO:
			switch (STM32G4_RCC_FIELD_READ(cfgr, mcosel))
			{
				case 1:
					b = STM32G4_CLOCK_SYS;
					break;
				case 3:
					b = STM32G4_CLOCK_HSI16;
					break;
				case 4:
					b = STM32G4_CLOCK_HSE;
					break;
				case 5:
					b = STM32G4_CLOCK_PLL_R;
					break;
				case 6:
					b = STM32G4_CLOCK_LSI;
					break;
				case 7:
					b = STM32G4_CLOCK_LSE;
					break;
				case 8:
					b = STM32G4_CLOCK_HSI48;
					break;
				default:
					return false;
			}
			break;

		case STM32G4_CLOCK_AHB:
			b = STM32G4_CLOCK_SYS;
			break;

		case STM32G4_CLOCK_APB1:
		case STM32G4_CLOCK_APB2:
			b = STM32G4_CLOCK_AHB;
			break;

		case STM32G4_CLOCK_RTC:
			switch (STM32G4_RCC_FIELD_READ(bdcr, rtcsel))
			{
				case 1:
					b = STM32G4_CLOCK_LSE;
					break;
				case 2:
					b = STM32G4_CLOCK_LSI;
					break;
				case 3:
					b = STM32G4_CLOCK_HSE;
					break;
				default:
					return false;
			}
			break;

		case STM32G4_CLOCK_USART1:
			switch (STM32G4_RCC_FIELD_READ(ccipr, usart1sel))
			{
				case 0: b = STM32G4_CLOCK_APB2 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_USART2:
			switch (STM32G4_RCC_FIELD_READ(ccipr, usart2sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_USART3:
			switch (STM32G4_RCC_FIELD_READ(ccipr, usart3sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_UART4:
			switch (STM32G4_RCC_FIELD_READ(ccipr, uart4sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_UART5:
			switch (STM32G4_RCC_FIELD_READ(ccipr, uart5sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_LPUART1:
			switch (STM32G4_RCC_FIELD_READ(ccipr, lpuart1sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_I2C1:
			switch (STM32G4_RCC_FIELD_READ(ccipr, i2c1sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_I2C2:
			switch (STM32G4_RCC_FIELD_READ(ccipr, i2c2sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_I2C3:
			switch (STM32G4_RCC_FIELD_READ(ccipr, i2c3sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_LPTIM1:
			switch (STM32G4_RCC_FIELD_READ(ccipr, lptim1sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_LSI  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				case 3: b = STM32G4_CLOCK_LSE  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_SAI1:
			switch (STM32G4_RCC_FIELD_READ(ccipr, sai1sel))
			{
				case 0: b = STM32G4_CLOCK_SYS	  ; break;
				case 1: b = STM32G4_CLOCK_PLL_Q   ; break;
				case 2: b = STM32G4_CLOCK_I2S_CKIN; break;
				case 3: b = STM32G4_CLOCK_HSI16   ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_I2S23:
			switch (STM32G4_RCC_FIELD_READ(ccipr, i2s23sel))
			{
				case 0: b = STM32G4_CLOCK_SYS	  ; break;
				case 1: b = STM32G4_CLOCK_PLL_Q   ; break;
				case 2: b = STM32G4_CLOCK_I2S_CKIN; break;
				case 3: b = STM32G4_CLOCK_HSI16   ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_FDCAN:
			switch (STM32G4_RCC_FIELD_READ(ccipr, fdcansel))
			{
				case 0: b = STM32G4_CLOCK_HSE  ; break;
				case 1: b = STM32G4_CLOCK_PLL_Q; break;
				case 2: b = STM32G4_CLOCK_APB1 ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_CLK48:
			switch (STM32G4_RCC_FIELD_READ(ccipr, clk48sel))
			{
				case 0: b = STM32G4_CLOCK_HSI48; break;
				case 2: b = STM32G4_CLOCK_PLL_Q; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_ADC12:
			switch (STM32G4_RCC_FIELD_READ(ccipr, adc12sel))
			{
				case 1: b = STM32G4_CLOCK_PLL_P; break;
				case 2: b = STM32G4_CLOCK_SYS  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_ADC345:
			switch (STM32G4_RCC_FIELD_READ(ccipr, adc345sel))
			{
				case 1: b = STM32G4_CLOCK_PLL_P; break;
				case 2: b = STM32G4_CLOCK_SYS  ; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_I2C4:
			switch (STM32G4_RCC_FIELD_READ(ccipr2, i2c4sel))
			{
				case 0: b = STM32G4_CLOCK_APB1 ; break;
				case 1: b = STM32G4_CLOCK_SYS  ; break;
				case 2: b = STM32G4_CLOCK_HSI16; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_QSPI:
			switch (STM32G4_RCC_FIELD_READ(ccipr2, qspisel))
			{
				case 0: b = STM32G4_CLOCK_SYS  ; break;
				case 1: b = STM32G4_CLOCK_HSI16; break;
				case 2: b = STM32G4_CLOCK_PLL_Q; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_TIM2:
		case STM32G4_CLOCK_TIM3:
		case STM32G4_CLOCK_TIM4:
		case STM32G4_CLOCK_TIM5:
		case STM32G4_CLOCK_TIM6:
		case STM32G4_CLOCK_TIM7:
			b = STM32G4_CLOCK_APB1;
			break;

		case STM32G4_CLOCK_TIM1:
		case STM32G4_CLOCK_TIM8:
		case STM32G4_CLOCK_TIM15:
		case STM32G4_CLOCK_TIM16:
		case STM32G4_CLOCK_TIM17:
		case STM32G4_CLOCK_TIM20:
			b = STM32G4_CLOCK_APB2;
			break;

		default:
			return false;
	}

	if (base) *base = b;
	return true;
}

static stm32g4_clock_e stm32g4_clock_root(stm32g4_clock_e clock)
{
	stm32g4_clock_e b;
	if (stm32g4_clock_base(clock, &b))
		return stm32g4_clock_root(b);
	return clock;
}


bool stm32g4_clock_enable(stm32g4_clock_e clock, bool enable)
{
	/* TODO - Check if clock is in-use before disabling? */

	switch (clock)
	{
		case STM32G4_CLOCK_LSCO:
		case STM32G4_CLOCK_LSE:
		case STM32G4_CLOCK_RTC:
			if (stm32g4_power_backup_write_protect_get())
				return false;
			break;

		default:
			break;
	}

	switch (clock)
	{
		case STM32G4_CLOCK_HSE:
			STM32G4_RCC_FIELD_WRITE(cr, hseon, enable);
			break;
		case STM32G4_CLOCK_HSI16:
			STM32G4_RCC_FIELD_WRITE(cr, hsion, enable);
			break;
		case STM32G4_CLOCK_HSI16_KER:
			STM32G4_RCC_FIELD_WRITE(cr, hsikeron, enable);
			break;

		case STM32G4_CLOCK_LSCO:
			STM32G4_RCC_FIELD_WRITE(bdcr, lscoen, enable);
			break;
		case STM32G4_CLOCK_LSE:
			STM32G4_RCC_FIELD_WRITE(bdcr, lseon, enable);
			break;
		case STM32G4_CLOCK_RTC:
			STM32G4_RCC_FIELD_WRITE(bdcr, rtcen, enable);
			break;

		case STM32G4_CLOCK_LSI:
			STM32G4_RCC_FIELD_WRITE(csr, lsion, enable);
			break;

		case STM32G4_CLOCK_HSI48:
			STM32G4_RCC_FIELD_WRITE(crrcr, hsi48on, enable);
			break;

		case STM32G4_CLOCK_PLL_P:
			STM32G4_RCC_FIELD_WRITE(pllcfgr, pllpen, enable);
			break;
		case STM32G4_CLOCK_PLL_Q:
			STM32G4_RCC_FIELD_WRITE(pllcfgr, pllqen, enable);
			break;
		case STM32G4_CLOCK_PLL_R:
			STM32G4_RCC_FIELD_WRITE(pllcfgr, pllren, enable);
			break;

		default:
			if (enable) return false;
			return stm32g4_clock_src_set(clock, STM32G4_CLOCK_NONE);
	}

	return true;
}

static unsigned stm32g4_clock_hse_freq = 0;

void stm32g4_clock_hse_set(unsigned freq, bool bypass)
{
	stm32g4_rcc_cr_t cr = { .mask = stm32g4_rcc->cr };
	cr.hseon  = (freq != 0);
	cr.hsebyp = bypass;
	stm32g4_rcc->cr = cr.mask;

	stm32g4_clock_hse_freq = freq;
}


static unsigned stm32g4_clock_lse_freq = 0;

void stm32g4_clock_lse_set(unsigned freq, bool bypass, unsigned drive)
{
	if (drive > 3) drive = 3;

	stm32g4_rcc_bdcr_t bdcr = { .mask = stm32g4_rcc->bdcr };
	bdcr.lseon  = (freq != 0);
	bdcr.lsebyp = bypass;
	bdcr.lsedrv = drive;
	stm32g4_rcc->bdcr = bdcr.mask;

	stm32g4_clock_lse_freq = freq;
}

void stm32g4_clock_hsi48_auto_trim_set(bool auto_trim)
{
	STM32G4_CRS_FIELD_WRITE(cr, autotrimen, auto_trim);
}

static bool stm32g4_clock_sys_set(stm32g4_clock_e src)
{
	stm32g4_clock_e base;
	if (!stm32g4_clock_base(STM32G4_CLOCK_SYS, &base))
		return false;

	unsigned index;
	switch (src)
	{
		case STM32G4_CLOCK_HSI16: index = 1; break;
		case STM32G4_CLOCK_HSE  : index = 2; break;
		case STM32G4_CLOCK_PLL_R: index = 3; break;
		default: return false;
	}

	unsigned freq_from, freq_to;
	if (!stm32g4_clock_freq(base, &freq_from)
		|| !stm32g4_clock_freq(src, &freq_to))
		return false;

	bool     boost_from = stm32g4_power_voltage_boost_get();
	unsigned range_from = stm32g4_power_voltage_scale_get();

	bool     boost_to;
	unsigned range_to;
	if (!stm32g4_power_voltage_scale_calc(freq_to, &range_to, &boost_to))
		return false;

	unsigned latency_from = stm32g4_flash_latency_get();
	unsigned latency_to   = stm32g4_flash_latency_calc(freq_to, range_to);

	unsigned hpre = 0;
	bool presc = false;
	presc = ((freq_from > 80000000)
		!= (freq_to > 80000000));
	if (presc) hpre = STM32G4_RCC_FIELD_READ(cfgr, hpre);

	if ((range_from == 2) && (range_to == 1))
		stm32g4_power_voltage_scale_set(range_to);

	if (presc) STM32G4_RCC_FIELD_WRITE(cfgr, hpre, 8);

	if (latency_to > latency_from)
		stm32g4_flash_latency_set(latency_to);

	if (range_to != range_from) stm32g4_power_voltage_scale_set(range_to);
	if (boost_to != boost_from) stm32g4_power_voltage_boost_set(boost_to);

	STM32G4_RCC_FIELD_WRITE(cfgr, sw, index);

	if (latency_to < latency_from)
		stm32g4_flash_latency_set(latency_to);

	if (presc)
	{
		delay_us(1);
		STM32G4_RCC_FIELD_WRITE(cfgr, hpre, hpre);
	}

	return true;
}

bool stm32g4_clock_src_set(
	stm32g4_clock_e clock,
	stm32g4_clock_e src)
{
	unsigned index;
	switch (clock)
	{
		case STM32G4_CLOCK_SYS:
			if (!stm32g4_clock_sys_set(src))
				return false;
			break;

		case STM32G4_CLOCK_MCO:
			switch (src)
			{
				case STM32G4_CLOCK_NONE : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 3; break;
				case STM32G4_CLOCK_HSE  : index = 4; break;
				case STM32G4_CLOCK_PLL_R: index = 5; break;
				case STM32G4_CLOCK_LSI  : index = 6; break;
				case STM32G4_CLOCK_LSE  : index = 7; break;
				case STM32G4_CLOCK_HSI48: index = 8; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(cfgr, mcosel, index);
			break;

		case STM32G4_CLOCK_LSCO:
			if (stm32g4_power_backup_write_protect_get())
				return false;

			switch (src)
			{
				case STM32G4_CLOCK_LSI: index = 0; break;
				case STM32G4_CLOCK_LSE: index = 1; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(bdcr, lscosel, index);
			break;

		case STM32G4_CLOCK_RTC:
			if (stm32g4_power_backup_write_protect_get())
				return false;

			switch (src)
			{
				case STM32G4_CLOCK_NONE: index = 0; break;
				case STM32G4_CLOCK_LSE : index = 1; break;
				case STM32G4_CLOCK_LSI : index = 2; break;
				case STM32G4_CLOCK_HSE : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(bdcr, rtcsel, index);
			break;

		case STM32G4_CLOCK_USART1:
			switch (src)
			{
				case STM32G4_CLOCK_APB2 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, usart1sel, index);
			break;

		case STM32G4_CLOCK_USART2:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, usart2sel, index);
			break;

		case STM32G4_CLOCK_USART3:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, usart3sel, index);
			break;

		case STM32G4_CLOCK_UART4:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, uart4sel, index);
			break;

		case STM32G4_CLOCK_UART5:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, uart5sel, index);
			break;

		case STM32G4_CLOCK_LPUART1:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, lpuart1sel, index);
			break;

		case STM32G4_CLOCK_I2C1:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, i2c1sel, index);
			break;

		case STM32G4_CLOCK_I2C2:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, i2c2sel, index);
			break;

		case STM32G4_CLOCK_I2C3:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, i2c3sel, index);
			break;

		case STM32G4_CLOCK_LPTIM1:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_LSI  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				case STM32G4_CLOCK_LSE  : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, lptim1sel, index);
			break;

		case STM32G4_CLOCK_SAI1:
			switch (src)
			{
				case STM32G4_CLOCK_SYS     : index = 0; break;
				case STM32G4_CLOCK_PLL_Q   : index = 1; break;
				case STM32G4_CLOCK_I2S_CKIN: index = 2; break;
				case STM32G4_CLOCK_HSI16   : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, sai1sel, index);
			break;

		case STM32G4_CLOCK_I2S23:
			switch (src)
			{
				case STM32G4_CLOCK_SYS     : index = 0; break;
				case STM32G4_CLOCK_PLL_Q   : index = 1; break;
				case STM32G4_CLOCK_I2S_CKIN: index = 2; break;
				case STM32G4_CLOCK_HSI16   : index = 3; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, i2s23sel, index);
			break;

		case STM32G4_CLOCK_FDCAN:
			switch (src)
			{
				case STM32G4_CLOCK_HSE  : index = 0; break;
				case STM32G4_CLOCK_PLL_Q: index = 1; break;
				case STM32G4_CLOCK_APB1 : index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, fdcansel, index);
			break;

		case STM32G4_CLOCK_CLK48:
			switch (src)
			{
				case STM32G4_CLOCK_HSI48: index = 0; break;
				case STM32G4_CLOCK_PLL_Q: index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, clk48sel, index);
			break;

		case STM32G4_CLOCK_ADC12:
			switch (src)
			{
				case STM32G4_CLOCK_NONE : index = 0; break;
				case STM32G4_CLOCK_PLL_P: index = 1; break;
				case STM32G4_CLOCK_SYS  : index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, adc12sel, index);
			break;

		case STM32G4_CLOCK_ADC345:
			switch (src)
			{
				case STM32G4_CLOCK_NONE : index = 0; break;
				case STM32G4_CLOCK_PLL_P: index = 1; break;
				case STM32G4_CLOCK_SYS  : index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr, adc345sel, index);
			break;

		case STM32G4_CLOCK_I2C4:
			switch (src)
			{
				case STM32G4_CLOCK_APB1 : index = 0; break;
				case STM32G4_CLOCK_SYS  : index = 1; break;
				case STM32G4_CLOCK_HSI16: index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr2, i2c4sel, index);
			break;

		case STM32G4_CLOCK_QSPI:
			switch (src)
			{
				case STM32G4_CLOCK_SYS  : index = 0; break;
				case STM32G4_CLOCK_HSI16: index = 1; break;
				case STM32G4_CLOCK_PLL_Q: index = 2; break;
				default: return false;
			}
			STM32G4_RCC_FIELD_WRITE(ccipr2, qspisel, index);
			break;

		default:
			return false;
	}

	return true;
}

bool stm32g4_clock_div_set(
	stm32g4_clock_e clock,
	unsigned div)
{
	unsigned index = 0;
	switch (clock)
	{
		case STM32G4_CLOCK_MCO:
			switch (div)
			{
				case  1: index = 0; break;
				case  2: index = 1; break;
				case  4: index = 2; break;
				case  8: index = 3; break;
				case 16: index = 4; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_AHB:
			switch (div)
			{
				case   1: index =  0; break;
				case   2: index =  8; break;
				case   4: index =  9; break;
				case   8: index = 10; break;
				case  16: index = 11; break;
				case  64: index = 12; break;
				case 128: index = 13; break;
				case 256: index = 14; break;
				case 512: index = 15; break;
				default: return false;
			}
			break;

		case STM32G4_CLOCK_APB1:
		case STM32G4_CLOCK_APB2:
			switch (div)
			{
				case  1: index = 0; break;
				case  2: index = 4; break;
				case  4: index = 5; break;
				case  8: index = 6; break;
				case 16: index = 7; break;
				default: return false;
			}
			break;

		default:
			break;
	}

	switch (clock)
	{
		case STM32G4_CLOCK_MCO:
			STM32G4_RCC_FIELD_WRITE(cfgr, mcopre, index);
			break;

		case STM32G4_CLOCK_AHB:
			STM32G4_RCC_FIELD_WRITE(cfgr, hpre, index);
			break;

		case STM32G4_CLOCK_APB1:
			STM32G4_RCC_FIELD_WRITE(cfgr, ppre1, index);
			break;
		case STM32G4_CLOCK_APB2:
			STM32G4_RCC_FIELD_WRITE(cfgr, ppre2, index);
			break;

		default:
			return false;
	}

    return true;
}

bool stm32g4_clock_pll_set(
	stm32g4_clock_e src,
	unsigned m, unsigned n,
	unsigned p, unsigned q, unsigned r)
{
	if ((m == 0) || (m > 16))
		return false;

	if ((n < 8) || (n > 127))
		return false;

	if ((p == 1) || (p > 31))
		return false;

	if (((q % 2) != 0) || (q > 8))
		return false;

	if (((r % 2) != 0) || (r > 8))
		return false;

	unsigned index;
	switch (src)
	{
		case STM32G4_CLOCK_HSI16: index = 2; break;
		case STM32G4_CLOCK_HSE  : index = 3; break;
		default: return false;
	}

	/* TODO - Switch to HSI clock for sys and switch back when R is stable. */
	stm32g4_clock_e sys;
	if (!stm32g4_clock_base(STM32G4_CLOCK_SYS, &sys)
		|| (sys == STM32G4_CLOCK_PLL_R))
		return false;

	STM32G4_RCC_FIELD_WRITE(cr, pllon, false);
	if ((p == 0) && (q == 0) && (r == 0))
		return true;

	while (stm32g4_clock_is_stable(STM32G4_CLOCK_PLL));

	STM32G4_RCC_FIELD_WRITE(pllcfgr, pllsrc, index);

	stm32g4_rcc_pllcfgr_t pllcfgr = { .mask = stm32g4_rcc->pllcfgr };
	pllcfgr.pllm    = (m - 1);
	pllcfgr.plln    = n;
	pllcfgr.pllpen  = (p != 0);
	pllcfgr.pllp    = (p == 17);
	pllcfgr.pllqen  = (q != 0);
	pllcfgr.pllq    = (q == 0 ? 0 : ((q / 2) - 1));
	pllcfgr.pllren  = (r != 0);
	pllcfgr.pllr    = (r == 0 ? 0 : ((r / 2) - 1));
	pllcfgr.pllpdiv = ((p == 0) || (p == 7) || (p == 17) ? 0 : p);
	stm32g4_rcc->pllcfgr = pllcfgr.mask;
	STM32G4_RCC_FIELD_WRITE(cr, pllon, true);
	return true;
}


bool stm32g4_clock_is_enabled(stm32g4_clock_e clock)
{
	stm32g4_clock_e base;
	if (stm32g4_clock_base(clock, &base)
		&& !stm32g4_clock_is_enabled(base))
		return false;

	switch (clock)
	{
		case STM32G4_CLOCK_HSE:
			return STM32G4_RCC_FIELD_READ(cr, hseon);

		case STM32G4_CLOCK_HSI16:
			return STM32G4_RCC_FIELD_READ(cr, hsion);

		case STM32G4_CLOCK_HSI16_KER:
			return STM32G4_RCC_FIELD_READ(cr, hsikeron);

		case STM32G4_CLOCK_HSI48:
			return STM32G4_RCC_FIELD_READ(crrcr, hsi48on);

		case STM32G4_CLOCK_LSE:
			return STM32G4_RCC_FIELD_READ(bdcr, lseon);

		case STM32G4_CLOCK_LSI:
			return STM32G4_RCC_FIELD_READ(csr, lsion);

		case STM32G4_CLOCK_PLL:
			return STM32G4_RCC_FIELD_READ(cr, pllon);

		case STM32G4_CLOCK_PLL_P:
			return (STM32G4_RCC_FIELD_READ(cr, pllon)
				&& (STM32G4_RCC_FIELD_READ(pllcfgr, pllsrc) >= 2)
				&& STM32G4_RCC_FIELD_READ(pllcfgr, pllpen));

		case STM32G4_CLOCK_PLL_Q:
			return (STM32G4_RCC_FIELD_READ(cr, pllon)
				&& (STM32G4_RCC_FIELD_READ(pllcfgr, pllsrc) >= 2)
				&& STM32G4_RCC_FIELD_READ(pllcfgr, pllqen));

		case STM32G4_CLOCK_PLL_R:
			return (STM32G4_RCC_FIELD_READ(cr, pllon)
				&& (STM32G4_RCC_FIELD_READ(pllcfgr, pllsrc) >= 2)
				&& STM32G4_RCC_FIELD_READ(pllcfgr, pllren));

		case STM32G4_CLOCK_MCO:
			return (STM32G4_RCC_FIELD_READ(cfgr, mcosel) != 0);

		case STM32G4_CLOCK_RTC:
			return (STM32G4_RCC_FIELD_READ(bdcr, rtcen)
				&& (STM32G4_RCC_FIELD_READ(bdcr, rtcsel) != 0));

		// TODO: Return whether peripherals are enabled.

		default:
			break;
	}

	return false;
}

bool stm32g4_clock_is_stable(stm32g4_clock_e clock)
{
	stm32g4_clock_e root = stm32g4_clock_root(clock);
	switch (root)
	{
		case STM32G4_CLOCK_HSE:
			return STM32G4_RCC_FIELD_READ(cr, hserdy);

		case STM32G4_CLOCK_HSI16:
			return STM32G4_RCC_FIELD_READ(cr, hsirdy);

		case STM32G4_CLOCK_HSI48:
			return STM32G4_RCC_FIELD_READ(crrcr, hsi48rdy);

		case STM32G4_CLOCK_LSE:
			return STM32G4_RCC_FIELD_READ(bdcr, lserdy);

		case STM32G4_CLOCK_LSI:
			return STM32G4_RCC_FIELD_READ(csr, lsirdy);

		case STM32G4_CLOCK_SYS:
			return (STM32G4_RCC_FIELD_READ(cfgr, sws)
				== STM32G4_RCC_FIELD_READ(cfgr, sw));

		case STM32G4_CLOCK_PLL:
			return STM32G4_RCC_FIELD_READ(cr, pllrdy);

		default:
			break;
	}

	return false;
}


static void stm32g4_clock__pllfreq(
	unsigned src,
	unsigned *p, unsigned *q, unsigned *r)
{
	stm32g4_rcc_pllcfgr_t pllcfgr = { .mask = stm32g4_rcc->pllcfgr };

	// Avoid 64-bit here as (src * n) shouldn't ever exceed 4GHz.
	unsigned vco = (src * pllcfgr.plln) / (pllcfgr.pllm + 1);

	unsigned pdiv = pllcfgr.pllpdiv;
	if (pdiv == 0) pdiv = (pllcfgr.pllp ? 17 : 7);

	unsigned qdiv = (pllcfgr.pllq + 1) * 2;
	unsigned rdiv = (pllcfgr.pllr + 1) * 2;

	if (p) *p = (pllcfgr.pllpen || (pdiv == 0) ? (vco / pdiv) : 0);
	if (q) *q = (pllcfgr.pllqen ? (vco / qdiv) : 0);
	if (r) *r = (pllcfgr.pllren ? (vco / rdiv) : 0);
}

bool stm32g4_clock_freq(stm32g4_clock_e clock, unsigned* freq)
{
	stm32g4_clock_e base;
	unsigned f, d;
	switch (clock)
	{
		case STM32G4_CLOCK_HSE:
			f = stm32g4_clock_hse_freq;
			break;

		case STM32G4_CLOCK_HSI16:
		case STM32G4_CLOCK_HSI16_KER:
			f = 16000000;
			break;

		case STM32G4_CLOCK_HSI48:
			f = 48000000;
			break;

		case STM32G4_CLOCK_LSE:
			f = stm32g4_clock_lse_freq;
			break;

		case STM32G4_CLOCK_LSI:
			f = 32768;
			break;

		case STM32G4_CLOCK_RTC:
			if (!stm32g4_clock_base(clock, &base)
				|| !stm32g4_clock_freq(base, &f))
				return false;
			if (STM32G4_RCC_FIELD_READ(bdcr, rtcsel) == 3)
				f /= 32;
			break;

		case STM32G4_CLOCK_AHB:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_SYS, &f))
				return false;
			d = STM32G4_RCC_FIELD_READ(cfgr, hpre);
			if (d > 8) f >>= (d - 7);
			break;

		case STM32G4_CLOCK_PLL_P:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_PLL, &f))
				return false;
			stm32g4_clock__pllfreq(f, &f, NULL, NULL);
			break;

		case STM32G4_CLOCK_PLL_Q:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_PLL, &f))
				return false;
			stm32g4_clock__pllfreq(f, NULL, &f, NULL);
			break;

		case STM32G4_CLOCK_PLL_R:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_PLL, &f))
				return false;
			stm32g4_clock__pllfreq(f, NULL, NULL, &f);
			break;

		case STM32G4_CLOCK_MCO:
			if (STM32G4_RCC_FIELD_READ(cfgr, mcopre) == 0)
				return false;
			if (!stm32g4_clock_base(clock, &base)
				|| !stm32g4_clock_freq(base, &f))
				return false;
			f >>= STM32G4_RCC_FIELD_READ(cfgr, mcopre);
			break;

		case STM32G4_CLOCK_SYSTICK:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_AHB, &f))
				return false;
			if (CM4_FIELD_READ(syst_csr, clksource) == 0)
				f >>= 3;
			break;

		case STM32G4_CLOCK_APB1:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_AHB, &f))
				return false;
			d = STM32G4_RCC_FIELD_READ(cfgr, ppre1);
			if (d >= 4) f >>= (d - 3);
			break;

		case STM32G4_CLOCK_APB2:
			if (!stm32g4_clock_freq(STM32G4_CLOCK_AHB, &f))
				return false;
			d = STM32G4_RCC_FIELD_READ(cfgr, ppre2);
			if (d >= 4) f >>= (d - 3);
			break;

		default:
			if (!stm32g4_clock_base(clock, &base)
				|| !stm32g4_clock_freq(base, &f))
				return false;
			break;
	}

	if (freq) *freq = f;
	return true;
}


bool stm32g4_clock_wait_stable(stm32g4_clock_e clock)
{
	volatile bool fail = false;
	timeout_us(500, &fail);
	while (!fail && !stm32g4_clock_is_stable(clock));
	timeout_cancel();
	return !fail;
}
