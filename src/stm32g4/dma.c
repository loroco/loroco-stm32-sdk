#include <stm32g4/dma.h>
#include <stm32g4/reg/dma.h>
#include <stm32g4/reg/irq.h>
#include <stm32g4/rcc.h>

#include <cm4/nvic.h>

#include <stddef.h>

typedef struct
{
	void *user_data;
	void (*tfr_compl)     (void*, stm32g4_dma_instance_e instance, uint8_t channel);
	void (*tfr_half_compl)(void*, stm32g4_dma_instance_e instance, uint8_t channel);
	void (*tfr_err)       (void*, stm32g4_dma_instance_e instance, uint8_t channel);
	/* TODO: fill me in */
} stm32g4_dma_ch_handle_t;

static const stm32g4_rcc_unit_e stm32g4_dma_unit[STM32G4_DMA_COUNT] =
{
	STM32G4_RCC_DMA1,
	STM32G4_RCC_DMA2
};

static const stm32g4_irq_e stm32g4_dma_irq[STM32G4_DMA_COUNT][STM32G4_DMA_CHANNEL_COUNT] =
{
	{
		STM32G4_IRQ_DMA1_CH1,
		STM32G4_IRQ_DMA1_CH2,
		STM32G4_IRQ_DMA1_CH3,
		STM32G4_IRQ_DMA1_CH4,
		STM32G4_IRQ_DMA1_CH5,
		STM32G4_IRQ_DMA1_CH6
	},
	{
		STM32G4_IRQ_DMA2_CH1,
		STM32G4_IRQ_DMA2_CH2,
		STM32G4_IRQ_DMA2_CH3,
		STM32G4_IRQ_DMA2_CH4,
		STM32G4_IRQ_DMA2_CH5,
		STM32G4_IRQ_DMA2_CH6
	}
};

static stm32g4_dma_ch_handle_t stm32g4_dma_context[STM32G4_DMA_COUNT][STM32G4_DMA_CHANNEL_COUNT] = {{{0}}};

static void stm32g4_dma_config_regs(
	stm32g4_dma_instance_e instance, uint8_t channel,
	volatile uint32_t **ccr, volatile uint32_t **cndtr,
	volatile uint32_t **cpar, volatile uint32_t **cmar)
{
	switch (channel)
	{
		case 0:
			*ccr   = &(stm32g4_dma[instance]->ccr1);
			*cndtr = &(stm32g4_dma[instance]->cndtr1);
			*cpar  = &(stm32g4_dma[instance]->cpar1);
			*cmar  = &(stm32g4_dma[instance]->cmar1);
			break;

		case 1:
			*ccr   = &(stm32g4_dma[instance]->ccr2);
			*cndtr = &(stm32g4_dma[instance]->cndtr2);
			*cpar  = &(stm32g4_dma[instance]->cpar2);
			*cmar  = &(stm32g4_dma[instance]->cmar2);
			break;

		case 2:
			*ccr   = &(stm32g4_dma[instance]->ccr3);
			*cndtr = &(stm32g4_dma[instance]->cndtr3);
			*cpar  = &(stm32g4_dma[instance]->cpar3);
			*cmar  = &(stm32g4_dma[instance]->cmar3);
			break;

		case 3:
			*ccr   = &(stm32g4_dma[instance]->ccr4);
			*cndtr = &(stm32g4_dma[instance]->cndtr4);
			*cpar  = &(stm32g4_dma[instance]->cpar4);
			*cmar  = &(stm32g4_dma[instance]->cmar4);
			break;

		case 4:
			*ccr   = &(stm32g4_dma[instance]->ccr5);
			*cndtr = &(stm32g4_dma[instance]->cndtr5);
			*cpar  = &(stm32g4_dma[instance]->cpar5);
			*cmar  = &(stm32g4_dma[instance]->cmar5);
			break;

		case 5:
			*ccr   = &(stm32g4_dma[instance]->ccr6);
			*cndtr = &(stm32g4_dma[instance]->cndtr6);
			*cpar  = &(stm32g4_dma[instance]->cpar6);
			*cmar  = &(stm32g4_dma[instance]->cmar6);
			break;

		case 6:
			*ccr   = &(stm32g4_dma[instance]->ccr7);
			*cndtr = &(stm32g4_dma[instance]->cndtr7);
			*cpar  = &(stm32g4_dma[instance]->cpar7);
			*cmar  = &(stm32g4_dma[instance]->cmar7);
			break;

		case 7:
			*ccr   = &(stm32g4_dma[instance]->ccr8);
			*cndtr = &(stm32g4_dma[instance]->cndtr8);
			*cpar  = &(stm32g4_dma[instance]->cpar8);
			*cmar  = &(stm32g4_dma[instance]->cmar8);
			break;

		default:
			*ccr   = NULL;
			*cndtr = NULL;
			*cpar  = NULL;
			*cmar  = NULL;
			break;
	}
}

void stm32g4_dma_reset (stm32g4_dma_instance_e instance)
{
	if (instance >= STM32G4_DMA_COUNT) return;

	stm32g4_rcc_reset(stm32g4_dma_unit[instance]);
}

void stm32g4_dma_enable(stm32g4_dma_instance_e instance, bool enable)
{
	if (instance >= STM32G4_DMA_COUNT) return;

	stm32g4_rcc_enable(stm32g4_dma_unit[instance], enable);
}

bool stm32g4_dma_channel_config_set_full(
	stm32g4_dma_instance_e instance, uint8_t channel, unsigned data_count,
	uintptr_t p_address, uintptr_t m_address, bool p_inc, bool m_inc,
	stm32g4_dma_data_sz_e p_size, stm32g4_dma_data_sz_e m_size,
	stm32g4_dma_pl_e priority, stm32g4_dma_dir_e dir,
	bool circular, bool mem_to_mem,
	void (*tfr_compl)     (void*, stm32g4_dma_instance_e instance, uint8_t channel),
	void (*tfr_half_compl)(void*, stm32g4_dma_instance_e instance, uint8_t channel),
	void (*tfr_err)       (void*, stm32g4_dma_instance_e instance, uint8_t channel),
	void *user_data)
{
	if ((instance >= STM32G4_DMA_COUNT)
	    || (channel >= STM32G4_DMA_CHANNEL_COUNT)
	    || (!p_address) || (!m_address)
	    || (p_size >= STM32G4_DMA_DATA_SZ_COUNT)
	    || (m_size >= STM32G4_DMA_DATA_SZ_COUNT))
		return false;

	volatile uint32_t *ccrp, *cndtrp, *cparp, *cmarp;

	stm32g4_dma_config_regs(instance, channel, &ccrp, &cndtrp, &cparp, &cmarp);

	if (!ccrp || !cndtrp || !cparp || !cmarp)
		return false;

	*cparp  = p_address;
	*cmarp  = m_address;
	*cndtrp = data_count;

	stm32g4_dma_ccr_t ccr = (stm32g4_dma_ccr_t)(*ccrp);

	if (ccr.en) return false;

	if (circular && mem_to_mem) return false;

	ccr.pinc  = p_inc;
	ccr.minc  = m_inc;
	ccr.psize = p_size;
	ccr.msize = m_size;

	ccr.pl  = priority;
	ccr.dir = dir;

	// TODO: save data_count in context for circular??
	ccr.circ    = circular;
	ccr.mem2mem = mem_to_mem;

	ccr.tcie = !!tfr_compl;
	ccr.htie = !!tfr_half_compl;
	ccr.teie = !!tfr_err;

	stm32g4_dma_context[instance][channel].tfr_compl      = tfr_compl;
	stm32g4_dma_context[instance][channel].tfr_half_compl = tfr_half_compl;
	stm32g4_dma_context[instance][channel].tfr_err        = tfr_err;
	stm32g4_dma_context[instance][channel].user_data      = user_data;

	cm4_nvic_irq_enable(
		stm32g4_dma_irq[instance][channel], (ccr.tcie || ccr.htie || ccr.teie));

	*ccrp = ccr.mask;

	return true;
}


bool stm32g4_dma_channel_enable(
	stm32g4_dma_instance_e instance, uint8_t channel, bool enable)
{
	if ((instance >= STM32G4_DMA_COUNT)
	    || (channel >= STM32G4_DMA_CHANNEL_COUNT))
		return false;

	volatile uint32_t *ccrp;

	stm32g4_dma_config_regs(instance, channel, &ccrp, NULL, NULL, NULL);

	if (!ccrp) return false;

	stm32g4_dma_ccr_t ccr = (stm32g4_dma_ccr_t)(*ccrp);

	ccr.en = enable;

	*ccrp = ccr.mask;

	return true;
}

bool stm32g4_dma_channel_is_enabled(
	stm32g4_dma_instance_e instance, uint8_t channel)
{
	if ((instance >= STM32G4_DMA_COUNT)
	    || (channel >= STM32G4_DMA_CHANNEL_COUNT))
		return false;

	volatile uint32_t *ccrp;

	stm32g4_dma_config_regs(instance, channel, &ccrp, NULL, NULL, NULL);

	if (!ccrp) return false;

	return ((stm32g4_dma_ccr_t)(*ccrp)).en;
}

/* DMA Memory */
// TODO: make a basic stack allocator, or maybe generalise malloc to
//       work on different regions of memory?

// TODO: ensure that memory can be aligned on transfer data size

extern char _dma_begin;
extern char _dma_size;

typedef struct
{
	uint8_t*      data;
	size_t        size;
	volatile bool acquired;
	uint32_t      misses;
} stm32g4_dma_mem_region_t;

static stm32g4_dma_mem_region_t dma_mem_region[STM32G4_DMA_MEM_REGION_COUNT] =
{
	[STM32G4_DMA_MEM_REGION_USART1] =
	{
		.data     = (uint8_t*)&_dma_begin,
		.size     = (size_t)&_dma_size,
		.acquired = false,
		.misses   = 0
	}
};

size_t stm32g4_dma_mem_reacquire(
	stm32g4_dma_mem_region_e region, uint8_t** data)
{
	*data = dma_mem_region[region].data;
	return dma_mem_region[region].size;
}

size_t stm32g4_dma_mem_acquire(
	stm32g4_dma_mem_region_e region, uint8_t** data)
{
	if (region >= STM32G4_DMA_MEM_REGION_COUNT) return 0;

	// TODO: remove me and implement a proper allocator
	while (dma_mem_region[region].acquired);

	if (dma_mem_region[region].acquired)
	{
		dma_mem_region[region].misses++;

		*data = NULL;
		return 0;
	}
	else
	{
		dma_mem_region[region].acquired = true;

		*data = dma_mem_region[region].data;
		return dma_mem_region[region].size;
	}
}

void stm32g4_dma_mem_release(stm32g4_dma_mem_region_e region)
{
	if (region >= STM32G4_DMA_MEM_REGION_COUNT) return;

	dma_mem_region[region].acquired = false;
}

/* Interrupt Handlers */

static void stm32g4_dma_get_flags(
	stm32g4_dma_instance_e instance, uint8_t channel,
	bool *tfr_complete, bool *tfr_half, bool *tfr_error)
{
	switch (channel)
	{
		case 0:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif1);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif1);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif1);
			break;

		case 1:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif2);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif2);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif2);
			break;

		case 2:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif3);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif3);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif3);
			break;

		case 3:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif4);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif4);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif4);
			break;

		case 4:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif5);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif5);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif5);
			break;

		case 5:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif6);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif6);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif6);
			break;

		case 6:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif7);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif7);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif7);
			break;

		case 7:
			*tfr_complete = STM32G4_DMA_FIELD_READ(instance, isr, tcif8);
			*tfr_half     = STM32G4_DMA_FIELD_READ(instance, isr, htif8);
			*tfr_error    = STM32G4_DMA_FIELD_READ(instance, isr, teif8);
			break;

		default:
			*tfr_complete = false;
			*tfr_half     = false;
			*tfr_error    = false;
			break;
	}
}

static void stm32g4_dma_clear_flags(
	stm32g4_dma_instance_e instance, uint8_t channel)
{
	switch (channel)
	{
		case 0:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif1, 1);
			break;

		case 1:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif2, 1);
			break;

		case 2:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif3, 1);
			break;

		case 3:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif4, 1);
			break;

		case 4:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif5, 1);
			break;

		case 5:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif6, 1);
			break;

		case 6:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif7, 1);
			break;

		case 7:
			STM32G4_DMA_FIELD_WRITE(instance, isr, gif8, 1);
			break;

		default:
			break;
	}
}

static void stm32_irq_dma(stm32g4_dma_instance_e instance, uint8_t channel)
{
	if ((instance >= STM32G4_DMA_COUNT)
		|| (channel >= STM32G4_DMA_CHANNEL_COUNT))
		return;

	stm32g4_dma_ch_handle_t* handle = &(stm32g4_dma_context[instance][channel]);

	bool tfr_complete, tfr_half, tfr_error;
	stm32g4_dma_get_flags(
		instance, channel, &tfr_complete, &tfr_half, &tfr_error);

	if (tfr_complete && handle->tfr_compl)
	{
		handle->tfr_compl(handle->user_data, instance, channel);
	}

	if (tfr_half && handle->tfr_half_compl)
	{
		handle->tfr_half_compl(handle->user_data, instance, channel);
	}

	if (tfr_error && handle->tfr_err)
	{
		handle->tfr_err(handle->user_data, instance, channel);
	}

	stm32g4_dma_clear_flags(instance, channel);
}

// DMA1
void stm32_irq_dma1_ch1(void)
{
	stm32_irq_dma(STM32G4_DMA1, 0);
}

void stm32_irq_dma1_ch2(void)
{
	stm32_irq_dma(STM32G4_DMA1, 1);
}

void stm32_irq_dma1_ch3(void)
{
	stm32_irq_dma(STM32G4_DMA1, 2);
}

void stm32_irq_dma1_ch4(void)
{
	stm32_irq_dma(STM32G4_DMA1, 3);
}

void stm32_irq_dma1_ch5(void)
{
	stm32_irq_dma(STM32G4_DMA1, 4);
}

void stm32_irq_dma1_ch6(void)
{
	stm32_irq_dma(STM32G4_DMA1, 5);
}

void stm32_irq_dma1_ch7(void)
{
	stm32_irq_dma(STM32G4_DMA1, 6);
}

void stm32_irq_dma1_ch8(void)
{
	stm32_irq_dma(STM32G4_DMA1, 7);
}

// DMA2

void stm32_irq_dma2_ch1(void)
{
	stm32_irq_dma(STM32G4_DMA2, 0);
}

void stm32_irq_dma2_ch2(void)
{
	stm32_irq_dma(STM32G4_DMA2, 1);
}

void stm32_irq_dma2_ch3(void)
{
	stm32_irq_dma(STM32G4_DMA2, 2);
}

void stm32_irq_dma2_ch4(void)
{
	stm32_irq_dma(STM32G4_DMA2, 3);
}

void stm32_irq_dma2_ch5(void)
{
	stm32_irq_dma(STM32G4_DMA2, 4);
}

void stm32_irq_dma2_ch6(void)
{
	stm32_irq_dma(STM32G4_DMA2, 5);
}

void stm32_irq_dma2_ch7(void)
{
	stm32_irq_dma(STM32G4_DMA2, 6);
}

void stm32_irq_dma2_ch8(void)
{
	stm32_irq_dma(STM32G4_DMA2, 7);
}

