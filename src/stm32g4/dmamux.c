#include <stm32g4/dmamux.h>
#include <stm32g4/reg/dmamux.h>
#include <stm32g4/reg/irq.h>
#include <stm32g4/rcc.h>

#include <cm4/nvic.h>

#include <stddef.h>

typedef struct
{
	void (*sync_overrun)(void*);
	/* TODO: fill me in */
} stm32g4_dmamux_req_ch_handle_t;

static stm32g4_dmamux_req_ch_handle_t stm32g4_dmamux_context[STM32G4_DMAMUX_REQ_CH_COUNT] = {{0}};


void stm32g4_dmamux_reset(void)
{
	stm32g4_rcc_reset(STM32G4_RCC_DMAMUX1);
}


void stm32g4_dmamux_enable(bool enable)
{
	stm32g4_rcc_enable(STM32G4_RCC_DMAMUX1, enable);
}


bool stm32g4_dmamux_request_line_config_set_full(
	uint8_t channel, stm32g4_dmamux_req_line_e request_line,
	bool event_gen_en, uint8_t num_req_fwd,
	bool sync_en, stm32g4_dmamux_sync_polarity_e sync_pol,
	stm32g4_dmamux_trig_sync_input_e sync_input,
	void (*sync_overrun)(void*))
{
	if ((channel >= STM32G4_DMAMUX_REQ_CH_COUNT) ||
	    (request_line >= STM32G4_DMAMUX_REQ_LINE_COUNT) ||
	    (sync_input >= STM32G4_DMAMUX_TRIG_SYNC_INPUT_COUNT))
		return false;

	stm32g4_dmamux_cxcr_t ccr = (stm32g4_dmamux_cxcr_t)(
		stm32g4_dmamux->cxcr[channel]);

	ccr.dmareq_id = request_line;
	ccr.ege       = event_gen_en;

	if ((ccr.se = sync_en))
	{
		ccr.spol    = sync_pol;
		ccr.sync_id = sync_input;
		ccr.nbreq   = num_req_fwd;
		ccr.soie    = !!sync_overrun;
		stm32g4_dmamux_context[channel].sync_overrun = sync_overrun;
	}

	cm4_nvic_irq_enable(STM32G4_IRQ_DMAMUX_OVR, !!sync_overrun);

	stm32g4_dmamux->cxcr[channel] = ccr.mask;
	return true;
}

bool stm32g4_dmamux_request_line_config_basic(
	uint8_t channel, stm32g4_dmamux_req_line_e request_line)
{
	return stm32g4_dmamux_request_line_config_set_full(
		channel, request_line, false, 0,
		false, STM32G4_DMAMUX_SYNC_POL_NONE,
		STM32G4_DMAMUX_TRIG_SYNC_INPUT_EXTI_LN0, NULL);
}

// TODO: Add support for request generation channels
