#include <stm32g4/reg/irq.h>
#include <stm32g4/fdcan.h>
#include <stm32g4/rcc.h>
#include <cm4/nvic.h>
#include <util/common.h>
#include <util/delay.h>
#include <protocols/can.h>

#include <stddef.h>
#include <string.h>

#define FDCAN_RETRY_LIMIT    1000
#define FDCAN_TIMOUT_MS      1000
#define FDCAN_STD_MSG_ID_LEN 11
#define FDCAN_EXT_MSG_ID_LEN 28

static stm32g4_error_e fdcan_handle__configure(fdcan_handle_t *handle, bool enable);

/* Note that for the STM32G4, there is only one FDCAN peripheral */
struct fdcan_handle_s
{
	uint32_t             id;
	fdcan_state_e        state;
	stm32g4_rcc_unit_e   reset_unit;
	uint32_t             mram_index;
	uint32_t             unit;
	fdcan_callback_set_t callbacks;
	int32_t              error;
};

static fdcan_handle_t fdcan_context[FDCAN_UNIT_COUNT] = {
	[FDCAN_UNIT_1] =
	{
		.id         = FDCAN_UNIT_1,
		.reset_unit = STM32G4_RCC_FDCAN,
		.mram_index = STM32G4_FDCAN1,
		.unit       = STM32G4_FDCAN1,
		.error      = STM32G4_ERROR_NONE,
	}
};

fdcan_handle_t* fdcan_handle_open(
	fdcan_unit_e          id,
	fdcan_attr_t         *attr,
	fdcan_callback_set_t *callbacks)
{
	if (id >= FDCAN_UNIT_COUNT) return NULL;
	fdcan_handle_t *handle = &fdcan_context[id];

	if (!handle) return NULL;

	stm32g4_rcc_reset (handle->reset_unit);
	stm32g4_rcc_enable(handle->reset_unit, true);

	stm32g4_error_e error = STM32G4_ERROR_NONE;

	/* Exit from Sleep mode */
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, csr, false);

	volatile bool timeout = false;
	timeout_ms(FDCAN_TIMOUT_MS, &timeout);

	/* Check Sleep mode acknowledge */
	while (!timeout && STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, csa));
	timeout_cancel();
	if (timeout) return NULL;

	if ((error = fdcan_handle__configure(handle, true)) != STM32G4_ERROR_NONE)
		return NULL;

	error = fdcan_handle_configure_callbacks(
		handle, callbacks ? *callbacks : fdcan_callback_default);

	// Configure, if NULL passed; then we just use the default reset register settings
	if (attr) {
		error |= fdcan_handle_set_clkdiv(handle, attr->clkdiv);
		error |= fdcan_handle_set_frame_format(handle, attr->frame_format);
		error |= fdcan_handle_set_op_mode(handle, attr->op_mode);
		error |= fdcan_handle_set_auto_retx(handle, attr->auto_retx);
		error |= fdcan_handle_set_tx_pause(handle, attr->tx_pause);
		error |= fdcan_handle_set_protocol_exception(
			handle, attr->protocol_exception);
		error |= fdcan_handle_set_nominal_time_config(
			handle, attr->nominal_time);
		error |= fdcan_handle_set_data_time_config(handle, attr->data_time);
		error |= fdcan_handle_set_tx_mode(handle, attr->tx_mode);
		error |= fdcan_handle_set_global_filter_config(
			handle, attr->global_filter);
		error |= fdcan_handle_set_ram_watchdog_start(
			handle, attr->ram_watchdog_start);
		error |= fdcan_handle_set_timestamp_config(handle, attr->timestamp);
		error |= fdcan_handle_set_timeout_config(handle, attr->timeout);
		error |= fdcan_handle_set_tx_delay_comp_config(
			handle, attr->tx_delay_comp);
		error |= fdcan_handle_set_iso_mode(handle, attr->iso_mode);
		error |= fdcan_handle_set_edge_filtering(
			handle, attr->edge_filtering);

		if (error != STM32G4_ERROR_NONE)
			return NULL;
	}

	cm4_nvic_irq_enable(STM32G4_IRQ_FDCAN1_IT0, true);
	cm4_nvic_irq_enable(STM32G4_IRQ_FDCAN1_IT1, true);

	handle->state = FDCAN_STATE_OPEN;

	return handle;
}

stm32g4_error_e fdcan_handle_close(fdcan_handle_t* handle)
{
	if (!handle) return STM32G4_ERROR_NONE;

	stm32g4_error_e error = STM32G4_ERROR_NONE;

	error |= fdcan_handle_stop(handle);

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, ile, eint0, false);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, ile, eint1, false);

	handle->state = FDCAN_STATE_CLOSED;
	handle->error = STM32G4_ERROR_NONE;

	cm4_nvic_irq_enable(STM32G4_IRQ_FDCAN1_IT0, false);
	cm4_nvic_irq_enable(STM32G4_IRQ_FDCAN1_IT1, false);

	return error;
}

stm32g4_error_e fdcan_handle_set_powerdown_mode(
	fdcan_handle_t *handle,
	bool            enable)
{
	if (!handle) return STM32G4_ERROR_PARAMETER;

	uint32_t counter = 0;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, csr, enable);

	// TODO: pg 1908 of ref manual says we need to toggle module clocks - what are these?

	while (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, csa) != enable) {
		// TODO: add global tick functions (incremented with sys_tick), to add proper timeout
		if (counter++ > FDCAN_RETRY_LIMIT) {
			return STM32G4_ERROR_RETRIES_EXHAUSTED;
		}
	}

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_configure_callbacks(
	fdcan_handle_t       *handle,
	fdcan_callback_set_t  callbacks)
{
	if (!handle) return STM32G4_ERROR_PARAMETER;

	// TODO: currently enables all interrupts in group (i.e. rxfifo0) - see if we
	//       need finer control
	bool int_line_used[FDCAN_INT_LINE_COUNT] = {false};
	for (unsigned cb_index = 0; cb_index < FDCAN_CB_ID_COUNT; cb_index++) {
		if (callbacks.cb_data[cb_index].callback) {
			// register callback
			fdcan_callback_data_t data          = callbacks.cb_data[cb_index];
			handle->callbacks.cb_data[cb_index] = data;

			// set interrupt line and enable interrupts in mask
			int_line_used[data.int_line] = true;
			switch (cb_index) {
			case FDCAN_CB_ID_RX_FIFO0:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, rf0n, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, rf0f, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, rf0l, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, rxfifo0, data.int_line);
				break;

			case FDCAN_CB_ID_RX_FIFO1:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, rf1n, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, rf1f, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, rf1l, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, rxfifo1, data.int_line);
				break;

			case FDCAN_CB_ID_TX_HIGH_PRIORITY_MSG:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, hpm, data.enabled);
				// Fall-through
			case FDCAN_CB_ID_TX_COMPLETE:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, tc, data.enabled);
				// Fall-through
			case FDCAN_CB_ID_TX_CANCELLED:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, tcf, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, smsg, data.int_line);
				break;

			case FDCAN_CB_ID_TX_FIFO_EMPTY:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, tfe, data.enabled);
				// Fall-through
			case FDCAN_CB_ID_TX_EVENT:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, tefn, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, teff, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, tefl, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, tferr, data.int_line);
				break;

			case FDCAN_CB_ID_TIMESTAMP_WRAP:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, tsw, data.enabled);
				// Fall-through
			case FDCAN_CB_ID_MRAM_ACCESS_FAIL:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, mraf, data.enabled);
				// Fall-through
			case FDCAN_CB_ID_TIMEOUT_OCCURRED:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, too, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, misc, data.int_line);
				break;

			case FDCAN_CB_ID_ERROR:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, ep, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, ew, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, bo, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, wdi, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, ara, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, berr, data.int_line);
				break;

			case FDCAN_CB_ID_PROTOCOL_ERROR:
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, pea, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ie, ped, data.enabled);
				STM32G4_FDCAN_FIELD_WRITE(handle->unit, ils, perr, data.int_line);
				break;

			default:
				break;
			}
		}
	}

	if (int_line_used[FDCAN_INT_LINE_0])
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, ile, eint0, true);

	if (int_line_used[FDCAN_INT_LINE_1])
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, ile, eint1, true);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_deregister_callback(
	fdcan_handle_t      *handle,
	fdcan_callback_id_t  callback_id)
{
	if (!handle || (callback_id >= FDCAN_CB_ID_COUNT)) {
		return STM32G4_ERROR_PARAMETER;
	}

	handle->callbacks.cb_data[callback_id].callback = NULL;

	return STM32G4_ERROR_NONE;
}


/* Config Methods */

static stm32g4_error_e fdcan_handle__configure(fdcan_handle_t *handle, bool enable)
{
	if (!handle) {
		return STM32G4_ERROR_PARAMETER;
	}
	uint32_t counter = 0;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, init, enable);

	// wait for state to change on chip
	while (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, init) != enable) {
		if (counter++ > FDCAN_RETRY_LIMIT) return STM32G4_ERROR_RETRIES_EXHAUSTED;
	}

	if (enable) {
		// cce gets cleared automatically when init is cleared
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, cce, enable);
		while (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, cce) != enable) {
			if (counter++ > FDCAN_RETRY_LIMIT) return STM32G4_ERROR_RETRIES_EXHAUSTED;
		}
	}

	return STM32G4_ERROR_NONE;
}

static bool fdcan_handle__is_configurable(fdcan_handle_t *handle)
{
	if (!handle) return false;

	return (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, init)
	        && STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, cce));
}

stm32g4_error_e fdcan_handle_set_clkdiv(fdcan_handle_t *handle, uint8_t clkdiv)
{
	if (!handle || (clkdiv < 1) || (clkdiv > 30))
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	uint8_t reg_value;
	if (clkdiv == 1)
		reg_value = 0;
	else
		reg_value = clkdiv / 2;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, ckdiv, pdiv, reg_value);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_frame_format(
	fdcan_handle_t       *handle,
	fdcan_frame_format_e  frame_format)
{
	if (!handle || (frame_format >= FDCAN_FRAME_FORMAT_COUNT))
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	switch (frame_format) {
	case FDCAN_FRAME_FORMAT_CLASSIC:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, fdoe, false);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, brse, false);
		break;

	case FDCAN_FRAME_FORMAT_FD_NO_BR_SWITCHING:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, fdoe, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, brse, false);
		break;

	case FDCAN_FRAME_FORMAT_FD_BR_SWITCHING:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, fdoe, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, brse, true);
		break;

	default:
		break;
	}

	return STM32G4_ERROR_NONE;
}

static void fdcan_handle__configure_test(
	fdcan_handle_t *handle,
	bool            enable)
{
	if (!handle) return;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, test, enable);
	while (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, test) != enable);
}

stm32g4_error_e fdcan_handle_set_op_mode(
	fdcan_handle_t  *handle,
	fdcan_op_mode_e  op_mode)
{
	if (!handle || (op_mode >= FDCAN_OP_MODE_COUNT))
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	fdcan_op_mode_e current_mode;
	stm32g4_error_e error = STM32G4_ERROR_NONE;

	// do nothing if current mode is the requested mode
	if (((error = fdcan_handle_get_op_mode(handle, &current_mode))
	    != STM32G4_ERROR_NONE) || (current_mode == op_mode)) {
		return error;
	}

	switch (op_mode) {
	case FDCAN_OP_MODE_NORMAL:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, asm_, false);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, mon, false);
		fdcan_handle__configure_test(handle, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, test, lbck, false);
		fdcan_handle__configure_test(handle, false);
		break;

	case FDCAN_OP_MODE_RESTRICTED:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, asm_, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, mon, false);
		fdcan_handle__configure_test(handle, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, test, lbck, false);
		fdcan_handle__configure_test(handle, false);
		break;

	case FDCAN_OP_MODE_BUS_MONITORING:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, asm_, false);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, mon, true);
		fdcan_handle__configure_test(handle, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, test, lbck, false);
		fdcan_handle__configure_test(handle, false);
		break;

	case FDCAN_OP_MODE_EXT_LOOPBACK:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, asm_, false);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, mon, false);
		fdcan_handle__configure_test(handle, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, test, lbck, true);
		fdcan_handle__configure_test(handle, false);
		break;

	case FDCAN_OP_MODE_INT_LOOPBACK:
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, asm_, false);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, mon, true);
		fdcan_handle__configure_test(handle, true);
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, test, lbck, true);
		fdcan_handle__configure_test(handle, false);
		break;

	default:
		return STM32G4_ERROR_PARAMETER;
	}

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_get_op_mode(
	fdcan_handle_t  *handle,
	fdcan_op_mode_e *op_mode)
{
	if (!handle || !op_mode) return STM32G4_ERROR_PARAMETER;

	if (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, asm_))
		*op_mode = FDCAN_OP_MODE_RESTRICTED;
	else if (STM32G4_FDCAN_FIELD_READ(handle->unit, cccr, mon))
	{
		if (STM32G4_FDCAN_FIELD_READ(handle->unit, test, lbck))
			*op_mode = FDCAN_OP_MODE_INT_LOOPBACK;
		else
			*op_mode = FDCAN_OP_MODE_RESTRICTED;
	}
	else if (STM32G4_FDCAN_FIELD_READ(handle->unit, test, lbck))
		*op_mode = FDCAN_OP_MODE_EXT_LOOPBACK;
	else
		*op_mode = FDCAN_OP_MODE_NORMAL;

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_auto_retx(
	fdcan_handle_t *handle,
	bool            auto_rtx)
{
	if (!handle) return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, dar, auto_rtx);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_tx_pause(
	fdcan_handle_t *handle,
	bool            tx_pause)
{
	if (!handle) return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, txp, tx_pause);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_protocol_exception(
	fdcan_handle_t *handle,
	bool            protocol_exception)
{
	if (!handle) return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, pxhd, protocol_exception);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_nominal_time_config(
	fdcan_handle_t      *handle,
	fdcan_time_config_t  nominal_time)
{
	if (!handle) return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	// TODO: do we need to check the unit values are within range?
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, nbtp, ntseg2, nominal_time.timeseg2 - 1);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, nbtp, ntseg1, nominal_time.timeseg1 - 1);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, nbtp, nbrp, nominal_time.prescaler - 1);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, nbtp, nsjw, nominal_time.sync_jump_width - 1);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_data_time_config(
	fdcan_handle_t      *handle,
	fdcan_time_config_t  data_time)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	// TODO: do we need to check the unit values are within range?
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, dbtp, dtseg2, data_time.timeseg2 - 1);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, dbtp, dtseg1, data_time.timeseg1 - 1);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, dbtp, dbrp, data_time.prescaler - 1);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, dbtp, dsjw, data_time.sync_jump_width - 1);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_tx_mode(
	fdcan_handle_t  *handle,
	fdcan_tx_mode_e  tx_mode)
{
	if (!handle || (tx_mode >= FDCAN_TX_MODE_COUNT))
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, txbc, tfqm, tx_mode);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_global_filter_config(
	fdcan_handle_t               *handle,
	fdcan_global_filter_config_t  global_filter)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, rrfe,
		                             global_filter.reject_remote_frames[FDCAN_FILTER_TYPE_EXT]);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, rrfs,
		                             global_filter.reject_remote_frames[FDCAN_FILTER_TYPE_STD]);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, anfe,
		                             global_filter.filter_non_match[FDCAN_FILTER_TYPE_EXT]);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, anfs,
		                             global_filter.filter_non_match[FDCAN_FILTER_TYPE_STD]);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, f0om,
		                             global_filter.overwrite_on_full[FDCAN_RX_FIFO_0]);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, f1om,
		                             global_filter.filter_non_match[FDCAN_RX_FIFO_1]);
	// TODO: do we need list size?? the size seems fixed in the G4
	// STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, lse,
	// 	                             global_filter.filter_list_size[FDCAN_FILTER_TYPE_EXT]);
	// STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxgfc, lss,
	// 	                             global_filter.filter_list_size[FDCAN_FILTER_TYPE_STD]);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_ram_watchdog_start(
	fdcan_handle_t *handle,
	uint8_t         ram_watchdog_start)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, rwd, wdc, ram_watchdog_start);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_timestamp_config(
	fdcan_handle_t           *handle,
	fdcan_timestamp_config_t  timestamp)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tscc, tss, timestamp.mode);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tscc, tcp, timestamp.prescaler - 1);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_timeout_config(
	fdcan_handle_t         *handle,
	fdcan_timeout_config_t  timeout)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tocc, etoc, timeout.enabled);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tocc, tos, timeout.mode);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tocc, top, timeout.period);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_set_tx_delay_comp_config(
	fdcan_handle_t               *handle,
	fdcan_tx_delay_comp_config_t  tx_delay_comp)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, dbtp, tdc, tx_delay_comp.enabled);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tdcr, tdcf, tx_delay_comp.filter_window_length);
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, tdcr, tdco, tx_delay_comp.offset);

	return STM32G4_ERROR_NONE;
}

/* CAN FD frame format according to ISO11898-1 (0) or Bosch CAN FD Specification V1.0 (1)
 */
stm32g4_error_e fdcan_handle_set_iso_mode(
	fdcan_handle_t *handle,
	bool            iso_mode)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, niso, !iso_mode);

	return STM32G4_ERROR_NONE;
}

/* Two consecutive dominant tq required to detect an edge for hard synchronization (1)
 */
stm32g4_error_e fdcan_handle_set_edge_filtering(
	fdcan_handle_t *handle,
	bool            edge_filtering)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (!fdcan_handle__is_configurable(handle))
		return STM32G4_ERROR_FDCAN_NOT_CONFIGURABLE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, cccr, efbi, edge_filtering);

	return STM32G4_ERROR_NONE;
}

/* Control Methods */

stm32g4_error_e fdcan_handle_get_state(fdcan_handle_t *handle, fdcan_state_e *state)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	*state = handle->state;

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_start(fdcan_handle_t *handle)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (handle->state != FDCAN_STATE_OPEN)
		return STM32G4_ERROR_INCORRECT_STATE;

	stm32g4_error_e error = fdcan_handle__configure(handle, false);

	if (error != STM32G4_ERROR_NONE)
		return error;

	handle->error = STM32G4_ERROR_NONE;
	handle->state = FDCAN_STATE_BUSY;

	return STM32G4_ERROR_NONE;
}


stm32g4_error_e fdcan_handle_stop(fdcan_handle_t *handle)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (handle->state != FDCAN_STATE_OPEN)
		return STM32G4_ERROR_INCORRECT_STATE;

	fdcan_handle__configure(handle, true);

	fdcan_handle_set_powerdown_mode(handle, false);

	handle->state = FDCAN_STATE_OPEN;

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_add_tx_msg_to_fifo(
	fdcan_handle_t                 *handle,
	fdcan_msg_header_t              header_data,
	const uint32_t                 *data,
	uint32_t                       *put_index)
{
	if (!handle)
		return STM32G4_ERROR_PARAMETER;

	if (handle->state != FDCAN_STATE_BUSY)
		return STM32G4_ERROR_INCORRECT_STATE;

	/* Check if fifo/queue is not full */
	if (fdcan_handle_tx_fifo_is_full(handle))
		return STM32G4_ERROR_FDCAN_FIFO_FULL;

	/* Build message in format fdcan unit requires */
	stm32g4_fdcan_mram_tx_buffer_t msg = {0};

	/* Determine if extended id is required */
	stm32g4_fdcan_mram_tx_buffer_hd_p0_t *hd_p0 = (stm32g4_fdcan_mram_tx_buffer_hd_p0_t*)&(msg.hd_p0);
	stm32g4_fdcan_mram_tx_buffer_hd_p1_t *hd_p1 = (stm32g4_fdcan_mram_tx_buffer_hd_p1_t*)&(msg.hd_p1);
	hd_p0->xtd = (header_data.id > (~(1U << FDCAN_STD_MSG_ID_LEN)));

	if (hd_p0->xtd)
		hd_p0->id = header_data.id;
	else
		hd_p0->id = header_data.id << (FDCAN_EXT_MSG_ID_LEN - FDCAN_STD_MSG_ID_LEN + 1);

	hd_p0->rtr = header_data.remote_tx_req;
	hd_p0->esi = header_data.error_bit_tx_recessive;
	hd_p1->dlc = header_data.length_code;
	hd_p1->brs = header_data.bit_rate_switching;
	hd_p1->fdf = header_data.fd_format;
	hd_p1->efc = header_data.store_tx_events;
	hd_p1->mm  = header_data.msg_marker;

	if ((header_data.fd_format) && (header_data.length_code > CAN_DLC_MAX))
		return STM32G4_ERROR_FDCAN_INVALID_LENGTH;

	/* Write msg to TX buffer at put_index */
	*put_index = fdcan_handle_get_tx_put_index(handle);

	// TODO: is this correct?
	STM32G4_FDCAN_MRAM_WRITE(handle->mram_index, tx_buffer, *put_index, 0, (uint32_t)msg.hd_p0);
	STM32G4_FDCAN_MRAM_WRITE(handle->mram_index, tx_buffer, *put_index, 1, (uint32_t)msg.hd_p1);

	for (unsigned i = 0; i < ROUND_UP_DIVIDE(can_fd_dlc_to_size(hd_p1->dlc), 4U); i++)
		STM32G4_FDCAN_MRAM_WRITE(handle->mram_index, tx_buffer, *put_index, 2 + i, (uint32_t)(data[i]));

	/* Request TX */
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, txbar, ar, 1U << *put_index);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_abort_tx(
	fdcan_handle_t *handle,
	uint32_t        buffer_index)
{
	if (!handle || (buffer_index > FDCAN_N_TX_ELEMS))
		return STM32G4_ERROR_PARAMETER;

	if (handle->state != FDCAN_STATE_BUSY)
		return STM32G4_ERROR_INCORRECT_STATE;

	STM32G4_FDCAN_FIELD_WRITE(handle->unit, txbcr, cr, 1U << buffer_index);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_get_tx_event(
	fdcan_handle_t                     *handle,
	stm32g4_fdcan_mram_tx_event_fifo_t *event)
{
	if (!handle || !event) {
		return STM32G4_ERROR_PARAMETER;
	}

	if (handle->state != FDCAN_STATE_BUSY) {
		return STM32G4_ERROR_INCORRECT_STATE;
	}

	uint32_t get_index = STM32G4_FDCAN_FIELD_READ(handle->unit, txefs, efgi);

	// TODO: is this correct??
	event->p0 = STM32G4_FDCAN_MRAM_READ(handle->mram_index, tx_event_fifo, get_index, 0);
	event->p1 = STM32G4_FDCAN_MRAM_READ(handle->mram_index, tx_event_fifo, get_index, 1);

	/* Acknowledge read */
	STM32G4_FDCAN_FIELD_WRITE(handle->unit, txefa, efai, get_index);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_get_rx_msg_from_fifo(
	fdcan_handle_t               *handle,
	fdcan_rx_fifo_e               rx_fifo,
	stm32g4_fdcan_mram_rx_fifo_t *msg)
{
	if (!handle || !msg || (rx_fifo >= FDCAN_RX_FIFO_COUNT)) {
		return STM32G4_ERROR_PARAMETER;
	}

	if (handle->state != FDCAN_STATE_BUSY) {
		return STM32G4_ERROR_INCORRECT_STATE;
	}

	uint32_t get_index, length;
	switch (rx_fifo) {
	case FDCAN_RX_FIFO_0:
		get_index = STM32G4_FDCAN_FIELD_READ(handle->unit, rxf0s, fgi);

		msg->hd_p0 = STM32G4_FDCAN_MRAM_READ(handle->mram_index, rx_fifo_0, get_index, 0);
		msg->hd_p1 = STM32G4_FDCAN_MRAM_READ(handle->mram_index, rx_fifo_0, get_index, 1);
		length = can_fd_dlc_to_size(((stm32g4_fdcan_mram_rx_fifo_0_hd_p1_t)(msg->hd_p1)).dlc);
		for (unsigned i = 0; i < ROUND_UP_DIVIDE(length, 4U); i++)
			msg->data[i] = STM32G4_FDCAN_MRAM_READ(handle->mram_index, rx_fifo_0, get_index, 2 + i);

		/* Acknowledge read */
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxf0a, fai, get_index);
		break;

	case FDCAN_RX_FIFO_1:
		get_index = STM32G4_FDCAN_FIELD_READ(handle->unit, rxf1s, fgi);

		msg->hd_p0 = STM32G4_FDCAN_MRAM_READ(handle->mram_index, rx_fifo_1, get_index, 0);
		msg->hd_p1 = STM32G4_FDCAN_MRAM_READ(handle->mram_index, rx_fifo_1, get_index, 1);
		length = can_fd_dlc_to_size(((stm32g4_fdcan_mram_rx_fifo_1_hd_p1_t)(msg->hd_p1)).dlc);
		for (unsigned i = 0; i < ROUND_UP_DIVIDE(length, 4U); i++)
			msg->data[i] = STM32G4_FDCAN_MRAM_READ(handle->mram_index, rx_fifo_1, get_index, 2 + i);

		/* Acknowledge read */
		STM32G4_FDCAN_FIELD_WRITE(handle->unit, rxf1a, fai, get_index);
		break;

	default:
		return STM32G4_ERROR_VALUE_OUT_OF_RANGE;
	}

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_get_high_priority_msg_status(
	fdcan_handle_t         *handle,
	fdcan_hp_msg_details_t *hp_msg_details)
{
	if (!handle || !hp_msg_details) return STM32G4_ERROR_PARAMETER;

	hp_msg_details->buffer_index = STM32G4_FDCAN_FIELD_READ(
		handle->unit, hpms, bidx);
	hp_msg_details->storage_info = STM32G4_FDCAN_FIELD_READ(
		handle->unit, hpms, msi);
	hp_msg_details->filter_match = STM32G4_FDCAN_FIELD_READ(
		handle->unit, hpms, fidx);
	hp_msg_details->filter_type  = STM32G4_FDCAN_FIELD_READ(
		handle->unit, hpms, flst);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_get_protocol_status(
	fdcan_handle_t        *handle,
	fdcan_procol_status_t *protocol_status)
{
	if (!handle || !protocol_status) return STM32G4_ERROR_PARAMETER;

	protocol_status->last_error               = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, lec);
	protocol_status->last_data_error          = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, dlec);
	protocol_status->activity                 = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, act);
	protocol_status->error_passive            = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, ep);
	protocol_status->warning_status           = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, ew);
	protocol_status->bus_off_status           = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, bo);
	protocol_status->last_esi                 = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, resi);
	protocol_status->last_brs                 = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, rbrs);
	protocol_status->received_msg             = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, redl);
	protocol_status->protocol_exception_event = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, pxe);
	protocol_status->tx_delay_comp            = STM32G4_FDCAN_FIELD_READ(
		handle->unit, psr, tdcv);

	return STM32G4_ERROR_NONE;
}

stm32g4_error_e fdcan_handle_get_error_counters(
	fdcan_handle_t         *handle,
	fdcan_error_counters_t *error_counters)
{
	if (!handle || !error_counters) return STM32G4_ERROR_PARAMETER;

	error_counters->tx_error_count   = STM32G4_FDCAN_FIELD_READ(
		handle->unit, ecr, tec);
	error_counters->rx_error_count   = STM32G4_FDCAN_FIELD_READ(
		handle->unit, ecr, rec);
	error_counters->rx_error_passive = STM32G4_FDCAN_FIELD_READ(
		handle->unit, ecr, rp);
	error_counters->error_logging    = STM32G4_FDCAN_FIELD_READ(
		handle->unit, ecr, cel);

	return STM32G4_ERROR_NONE;
}

uint32_t fdcan_handle_get_rx_fifo_elem_count(
	fdcan_handle_t  *handle,
	fdcan_rx_fifo_e  fifo)
{
	if (!handle || (fifo >= FDCAN_RX_FIFO_COUNT)) return 0;

	uint32_t elem_count;

	switch (fifo) {
	case FDCAN_RX_FIFO_0:
		elem_count = STM32G4_FDCAN_FIELD_READ(handle->unit, rxf0s, ffl);
		break;
	case FDCAN_RX_FIFO_1:
		elem_count = STM32G4_FDCAN_FIELD_READ(handle->unit, rxf1s, ffl);
		break;
	default:
		elem_count = 0;
		break;
	}
	return elem_count;
}

uint32_t fdcan_handle_get_rx_fifo_get_index(
	fdcan_handle_t  *handle,
	fdcan_rx_fifo_e  fifo)
{
	if (!handle || (fifo >= FDCAN_RX_FIFO_COUNT)) return 0;

	uint32_t get_index;

	switch (fifo) {
	case FDCAN_RX_FIFO_0:
		get_index = STM32G4_FDCAN_FIELD_READ(handle->unit, rxf0s, fgi);
		break;
	case FDCAN_RX_FIFO_1:
		get_index = STM32G4_FDCAN_FIELD_READ(handle->unit, rxf1s, fgi);
		break;
	default:
		get_index = 0;
		break;
	}
	return get_index;
}

uint32_t fdcan_handle_get_tx_space(fdcan_handle_t *handle)
{
	if (!handle) return 0;

	return STM32G4_FDCAN_FIELD_READ(handle->unit, txfqs, tffl);
}

bool fdcan_handle_tx_fifo_is_full(fdcan_handle_t *handle)
{
	if (!handle) return true;

	return STM32G4_FDCAN_FIELD_READ(handle->unit, txfqs, tfqf);
}

uint32_t fdcan_handle_get_tx_put_index (fdcan_handle_t *handle)
{
	if (!handle) return 0;

	return STM32G4_FDCAN_FIELD_READ(handle->unit, txfqs, tfqpi);
}

bool fdcan_handle_tx_in_progress(fdcan_handle_t *handle, uint32_t index)
{
	if (!handle) return false;

	return STM32G4_FDCAN_FIELD_READ(handle->unit, txbrp, trp) & (1U << index);
}

/* Default Callbacks - structure can be adopted in client code */

void fdcan_rx_fifo0_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;

	stm32g4_fdcan_ir_t flags = (stm32g4_fdcan_ir_t)(
		stm32g4_fdcan[handle->unit]->ir &
		stm32g4_fdcan[handle->unit]->ie);

	// TODO: maybe set the handle->error in this default handler
	if (flags.rf0n) {
		// RXFIFO0 new msg
	}
	if (flags.rf0f) {
		// RXFIFO0 full
	}
	if (flags.rf0l) {
		// RXFIFO0 msg lost
	}
}

void fdcan_rx_fifo1_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;

	stm32g4_fdcan_ir_t flags = (stm32g4_fdcan_ir_t)(
		stm32g4_fdcan[handle->unit]->ir &
		stm32g4_fdcan[handle->unit]->ie);

	// TODO: maybe set the handle->error in this default handler
	if (flags.rf1n) {
		// RXFIFO0 new msg
	}
	if (flags.rf1f) {
		// RXFIFO0 full
	}
	if (flags.rf1l) {
		// RXFIFO0 msg lost
	}
}

void fdcan_tx_high_priority_msg_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;
}

void fdcan_tx_complete_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;
}

void fdcan_tx_cancelled_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;
}

void fdcan_tx_fifo_empty_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;
}

void fdcan_tx_event_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;

	stm32g4_fdcan_ir_t flags = (stm32g4_fdcan_ir_t)(
		stm32g4_fdcan[handle->unit]->ir &
		stm32g4_fdcan[handle->unit]->ie);

	// TODO: maybe set the handle->error in this default handler
	if (flags.tefn) {
		// TX Event FIFO new msg
	}
	if (flags.teff) {
		// TX Event FIFO full
	}
	if (flags.tefl) {
		// TX Event FIFO msg lost
	}
}

void fdcan_timestamp_wrap_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;
}

void fdcan_mram_access_fail_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;


	fdcan_op_mode_e mode;
	if ((fdcan_handle_get_op_mode(handle, &mode) != STM32G4_ERROR_NONE) &&
	    (mode == FDCAN_OP_MODE_RESTRICTED)) {
		// mram_index access can result in RESTRICTED MODE, we can recover here
		fdcan_handle_set_op_mode(handle,FDCAN_OP_MODE_NORMAL);
	}
}

void fdcan_timeout_occurred_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;
}

void fdcan_error_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;

	stm32g4_fdcan_ir_t flags = (stm32g4_fdcan_ir_t)(
		stm32g4_fdcan[handle->unit]->ir &
		stm32g4_fdcan[handle->unit]->ie);

	// TODO: maybe set the handle->error in this default handler
	if (flags.ep) {
		// Error passive
	}
	if (flags.ew) {
		// Error warning
	}
	if (flags.bo) {
		// Bus off
	}
	if (flags.wdi) {
		// Bus off
	}
	if (flags.ara) {
		// Access to reserved address
	}
}

void fdcan_protocol_error_default_cb(fdcan_handle_t *handle)
{
	if (!handle) return;

	stm32g4_fdcan_ir_t flags = (stm32g4_fdcan_ir_t)(
		stm32g4_fdcan[handle->unit]->ir &
		stm32g4_fdcan[handle->unit]->ie);

	// TODO: maybe set the handle->error in this default handler
	if (flags.pea) {
		// Protocol Error in Arbitration
	}
	if (flags.ped) {
		// Protocol Error in Data Phase
	}
}

/* Interrupt Handlers */

void stm32_irq_fdcan1(void)
{
	fdcan_handle_t *handle = &fdcan_context[FDCAN_UNIT_1];

	stm32g4_fdcan_ir_t flags = (stm32g4_fdcan_ir_t)(
		stm32g4_fdcan[handle->unit]->ir &
		stm32g4_fdcan[handle->unit]->ie);

	// TODO: is there an issue doing the read just once here? Can things change
	//       during interrupt execution????

	if (flags.rf0n || flags.rf0f || flags.rf0l) {
		handle->callbacks.cb_data[FDCAN_CB_ID_RX_FIFO0].callback(handle);
	}
	if (flags.rf1n || flags.rf1f || flags.rf1l) {
		handle->callbacks.cb_data[FDCAN_CB_ID_RX_FIFO1].callback(handle);
	}
	if (flags.hpm) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TX_HIGH_PRIORITY_MSG].callback(handle);
	}
	if (flags.tc) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TX_COMPLETE].callback(handle);
	}
	if (flags.tcf) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TX_CANCELLED].callback(handle);
	}
	if (flags.tfe) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TX_FIFO_EMPTY].callback(handle);
	}
	if (flags.tefn || flags.teff || flags.tefl) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TX_EVENT].callback(handle);
	}
	if (flags.tsw) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TIMESTAMP_WRAP].callback(handle);
	}
	if (flags.mraf) {
		handle->callbacks.cb_data[FDCAN_CB_ID_MRAM_ACCESS_FAIL].callback(handle);
	}
	if (flags.too) {
		handle->callbacks.cb_data[FDCAN_CB_ID_TIMEOUT_OCCURRED].callback(handle);
	}
	if (flags.ep || flags.ew || flags.bo ||flags.wdi || flags.ara) {
		handle->callbacks.cb_data[FDCAN_CB_ID_ERROR].callback(handle);
	}
	if (flags.pea || flags.ped) {
		handle->callbacks.cb_data[FDCAN_CB_ID_PROTOCOL_ERROR].callback(handle);
	}

	// Reset flags
	stm32g4_fdcan[handle->unit]->ir =
		((stm32g4_fdcan_ir_t)stm32g4_fdcan[handle->unit]->ir).mask;
}

void stm32_irq_fdcan1_int0(void)
{
	stm32_irq_fdcan1();
}

void stm32_irq_fdcan1_int1(void)
{
	stm32_irq_fdcan1();
}
