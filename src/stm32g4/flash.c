#include <stm32g4/flash.h>
#include <stm32g4/reg/flash.h>

unsigned stm32g4_flash_latency_calc(unsigned freq, unsigned range)
{
	if (freq == 0)
		return 0;

	unsigned latency;
	if (range == 1)
	{
		latency = (freq - 1) / 20000000;
	}
	else if (range == 2)
	{
		latency = (freq - 1) / 8000000;
	}
	else
	{
		return 15;
	}

	return (latency < 15 ? latency : 15);
}

bool stm32g4_flash_latency_set(unsigned latency)
{
    if (latency >= 16)
		return false;

	STM32G4_FLASH_FIELD_WRITE(acr, latency, latency);
	return true;
}

unsigned stm32g4_flash_latency_get(void)
{
	return STM32G4_FLASH_FIELD_READ(acr, latency);
}
