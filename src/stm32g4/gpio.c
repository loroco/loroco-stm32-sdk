#include <stm32g4/gpio.h>
#include <stm32g4/reg/gpio.h>
#include <stm32g4/rcc.h>
#include <stddef.h>


void stm32g4_gpio_port_reset(stm32g4_gpio_port_e port)
{
	if (port >= STM32G4_GPIO_PORT_COUNT) return;
	stm32g4_rcc_reset(STM32G4_RCC_GPIOA + port);
}

void stm32g4_gpio_port_enable(stm32g4_gpio_port_e port, bool enable)
{
	if (port >= STM32G4_GPIO_PORT_COUNT) return;
	stm32g4_rcc_enable((STM32G4_RCC_GPIOA + port), enable);
}

uint16_t stm32g4_gpio_port_read(stm32g4_gpio_port_e port)
{
	if (port >= STM32G4_GPIO_PORT_COUNT) return 0;
	return stm32g4_gpio[port]->idr;
}

void stm32g4_gpio_port_write(stm32g4_gpio_port_e port, uint16_t value)
{
	if (port >= STM32G4_GPIO_PORT_COUNT) return;
	stm32g4_gpio[port]->odr = value;
}

void stm32g4_gpio_port_set_clear(stm32g4_gpio_port_e port, uint16_t set, uint16_t clear)
{
	if (port >= STM32G4_GPIO_PORT_COUNT) return;
	stm32g4_gpio_bsr_t bsr = { .bs = set, .br = clear };
	stm32g4_gpio[port]->bsrr = bsr.mask;
}

bool stm32g4_gpio_port_lock(stm32g4_gpio_port_e port, uint16_t lock)
{
	if (port >= STM32G4_GPIO_PORT_COUNT) return false;

	stm32g4_gpio[port]->lckr = (0x00010000 | lock);
	stm32g4_gpio[port]->lckr = (0x00000000 | lock);
	stm32g4_gpio[port]->lckr = (0x00010000 | lock);

	bool is_locked = (stm32g4_gpio[port]->lckr >> 16);
	return is_locked;
}


static void stm32g4_gpio_pin_config(
	unsigned              port,
	unsigned              pin,
	stm32g4_gpio_mode_e   mode,
	stm32g4_gpio_otype_e* otype,
	stm32g4_gpio_speed_e* speed,
	stm32g4_gpio_pull_e*  pull,
	unsigned*             alt_func)
{
	if (pin >= 16) return;
	if (port >= STM32G4_GPIO_PORT_COUNT) return;

	uint32_t moder = stm32g4_gpio[port]->moder;
	moder &= ~(3U << (pin * 2));
	moder |= mode << (pin * 2);
	stm32g4_gpio[port]->moder = moder;

	if (otype)
	{
		uint32_t otyper = stm32g4_gpio[port]->otyper;
		otyper &= ~(1U << pin);
		otyper |= *otype << pin;
		stm32g4_gpio[port]->otyper = otyper;
	}

	if (speed)
	{
		uint32_t speedr = stm32g4_gpio[port]->ospeedr;
		speedr &= ~(3U << (pin * 2));
		speedr |= *speed << (pin * 2);
		stm32g4_gpio[port]->ospeedr = speedr;
	}

	if (pull)
	{
		uint32_t pullr = stm32g4_gpio[port]->pupdr;
		pullr &= ~(3U << (pin * 2));
		pullr |= *pull << (pin * 2);
		stm32g4_gpio[port]->pupdr = pullr;
	}

	if (alt_func && (*alt_func < 16))
	{
		if (pin < 8)
		{
			uint32_t afr = stm32g4_gpio[port]->afrl;
			afr &= ~(0xFU << (pin * 4));
			afr |= *alt_func << (pin * 4);
			stm32g4_gpio[port]->afrl = afr;
		}
		else
		{
			uint32_t afr = stm32g4_gpio[port]->afrh;
			afr &= ~(0xFU << ((pin - 8) * 4));
			afr |= *alt_func << ((pin - 8) * 4);
			stm32g4_gpio[port]->afrh = afr;
		}
	}
}

void stm32g4_gpio_pin_config_input(stm32g4_gpio_port_e port, unsigned pin, stm32g4_gpio_pull_e pull)
{
	stm32g4_gpio_pin_config(port, pin,
		STM32G4_GPIO_MODE_INPUT, NULL, NULL, &pull, NULL);
}

void stm32g4_gpio_pin_config_output(stm32g4_gpio_port_e port, unsigned pin,
	stm32g4_gpio_otype_e otype, stm32g4_gpio_speed_e speed, stm32g4_gpio_pull_e pull)
{
	stm32g4_gpio_pin_config(port, pin,
		STM32G4_GPIO_MODE_OUTPUT, &otype, &speed, &pull, NULL);
}

void stm32g4_gpio_pin_config_analog(stm32g4_gpio_port_e port, unsigned pin)
{
	stm32g4_gpio_pin_config(port, pin,
		STM32G4_GPIO_MODE_ANALOG, NULL, NULL, NULL, NULL);
}

void stm32g4_gpio_pin_config_alternate(stm32g4_gpio_port_e port, unsigned pin, unsigned func)
{
	stm32g4_gpio_pin_config(port, pin,
		STM32G4_GPIO_MODE_ALTERNATE, NULL, NULL, NULL, &func);
}

bool stm32g4_gpio_pin_read(stm32g4_gpio_port_e port, unsigned pin)
{
	if (pin >= 16) return false;

	return (((stm32g4_gpio_port_read(port) >> pin) & 1) != 0);
}

void stm32g4_gpio_pin_write(stm32g4_gpio_port_e port, unsigned pin, bool value)
{
	if (pin >= 16) return;

	uint16_t set = 0, clear = 0;
	if (value)
		set   |= (1U << pin);
	else
		clear |= (1U << pin);
	stm32g4_gpio_port_set_clear(port, set, clear);
}
