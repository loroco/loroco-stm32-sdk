#include <stm32g4/power.h>
#include <stm32g4/reg/pwr.h>


void stm32g4_power_backup_write_protect_set(bool enable)
{
	STM32G4_PWR_FIELD_WRITE(cr1, dbp, !enable);
}

bool stm32g4_power_backup_write_protect_get(void)
{
	return !STM32G4_PWR_FIELD_READ(cr1, dbp);
}


bool stm32g4_power_voltage_scale_calc(unsigned freq, unsigned* range, bool* boost)
{
	unsigned r = 1;
	bool     b = false;

	if (freq <= 26000000)
		r = 2;
	else if (freq >= 150000000)
		b = true;

	if (range) *range = r;
	if (boost) *boost = b;
	return true;
}


bool stm32g4_power_voltage_scale_set(unsigned range)
{
	if ((range == 0) || (range > 2))
		return false;
	STM32G4_PWR_FIELD_WRITE(cr1, vos, range);
	while (STM32G4_PWR_FIELD_READ(sr2, vosf));
	return true;
}

unsigned stm32g4_power_voltage_scale_get(void)
{
	return STM32G4_PWR_FIELD_READ(cr1, vos);
}


void stm32g4_power_voltage_boost_set(bool boost)
{
	STM32G4_PWR_FIELD_WRITE(cr5, r1mode,
		(boost ? STM32G4_PWR_R1MODE_BOOST : STM32G4_PWR_R1MODE_NORMAL));
}

bool stm32g4_power_voltage_boost_get(void)
{
	return (STM32G4_PWR_FIELD_READ(cr5, r1mode) == STM32G4_PWR_R1MODE_BOOST);
}
