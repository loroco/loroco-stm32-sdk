#include <stm32g4/rcc.h>
#include <stm32g4/reg/rcc.h>

void stm32g4_rcc_reset(stm32g4_rcc_unit_e unit)
{
    volatile uint32_t* rstr = &stm32g4_rcc->ahb1rstr;
	rstr[unit >> 5] = (1U << (unit & 0x1F));

	// TODO: Add a delay here?
	__asm__ __volatile__("nop");
	__asm__ __volatile__("nop");

	rstr[unit >> 5] = 0;
}

void stm32g4_rcc_enable(stm32g4_rcc_unit_e unit, bool enable)
{
	volatile uint32_t* enr = &stm32g4_rcc->ahb1enr;
	uint32_t mask = (1U << (unit & 0x1F));

	if (enable)
		enr[unit >> 5] |= mask;
	else
		enr[unit >> 5] &= ~mask;

	__asm__ __volatile__("nop");
	__asm__ __volatile__("nop");
}
