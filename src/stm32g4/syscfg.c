#include <stm32g4/syscfg.h>
#include <stm32g4/rcc.h>
#include <stm32g4/reg/vrefbuf.h>
#include <util/delay.h>


void stm32g4_syscfg_reset(void)
{
	stm32g4_rcc_reset(STM32G4_RCC_SYSCFG);
}

void stm32g4_syscfg_enable(bool enable)
{
	stm32g4_rcc_enable(STM32G4_RCC_SYSCFG, enable);
}


static unsigned stm32g4_syscfg_vrefbuf__millivolts = 0;

bool stm32g4_syscfg_vrefbuf_enable(bool external, unsigned millivolts)
{
	stm32g4_vrefbuf_csr_t csr = { .mask = stm32g4_vrefbuf->csr };

	csr.hiz  =  external;
	csr.envr = !external;

	if (!external)
	{
		switch (millivolts)
		{
			case 2048:
				csr.vrs = STM32G4_VREFBUF_VRS_2V048;
				break;
			case 2500:
				csr.vrs = STM32G4_VREFBUF_VRS_2V5;
				break;
			case 2900:
				csr.vrs = STM32G4_VREFBUF_VRS_2V9;
				break;
			default:
				return false;
		}
	}

	stm32g4_vrefbuf->csr = csr.mask;

	stm32g4_syscfg_vrefbuf__millivolts = millivolts;
	return true;
}

void stm32g4_syscfg_vrefbuf_disable(void)
{
	stm32g4_vrefbuf_csr_t csr = { .mask = stm32g4_vrefbuf->csr };
	csr.hiz  = 0;
	csr.envr = 0;
	stm32g4_vrefbuf->csr = csr.mask;
}

bool stm32g4_syscfg_vrefbuf_wait_ready(unsigned timeout)
{
	volatile bool fail = false;

	if (timeout != 0)
		timeout_us(timeout, &fail);

	while (!fail && STM32G4_VREFBUF_FIELD_READ(csr, vrr));

	return !fail;
}
