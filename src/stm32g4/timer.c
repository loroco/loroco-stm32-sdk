#include <stm32g4/timer.h>
#include <stm32g4/rcc.h>
#include <stm32g4/clock.h>
#include <stm32g4/reg/irq.h>
#include <stm32g4/reg/timer.h>
#include <cm4/nvic.h>


typedef struct
{
	void (*isr[STM32G4_TIMER_INT_COUNT])(void);
} stm32g4_timer_context_t;

static stm32g4_timer_context_t stm32g4_timer_context[STM32G4_TIMER_COUNT] = {0};

#define STM32G4_TIMER_RCC_MAP(x) [STM32G4_TIMER_##x] = STM32G4_RCC_##x
static const stm32g4_rcc_unit_e stm32g4_timer_unit[STM32G4_TIMER_COUNT] =
{
	STM32G4_TIMER_RCC_MAP(TIM1),
	STM32G4_TIMER_RCC_MAP(TIM2),
	STM32G4_TIMER_RCC_MAP(TIM3),
	STM32G4_TIMER_RCC_MAP(TIM4),
	STM32G4_TIMER_RCC_MAP(TIM5),
	STM32G4_TIMER_RCC_MAP(TIM6),
	STM32G4_TIMER_RCC_MAP(TIM7),
	STM32G4_TIMER_RCC_MAP(TIM8),
	STM32G4_TIMER_RCC_MAP(TIM15),
	STM32G4_TIMER_RCC_MAP(TIM16),
	STM32G4_TIMER_RCC_MAP(TIM17),
	STM32G4_TIMER_RCC_MAP(TIM20),
};


static bool stm32g4_timer_is_advanced(stm32g4_timer_e timer)
{
	switch (timer)
	{
		case STM32G4_TIMER_TIM1:
		case STM32G4_TIMER_TIM8:
		case STM32G4_TIMER_TIM20:
			return true;

		default:
			break;
	}
	return false;
}


void stm32g4_timer_reset(stm32g4_timer_e timer)
{
	if (timer >= STM32G4_TIMER_COUNT) return;
	stm32g4_rcc_unit_e unit = stm32g4_timer_unit[timer];
	if (unit == 0) return;
	stm32g4_rcc_reset(unit);
}


void stm32g4_timer_enable(stm32g4_timer_e timer, bool enable)
{
	if (timer >= STM32G4_TIMER_COUNT) return;
	stm32g4_rcc_unit_e unit = stm32g4_timer_unit[timer];
	if (unit == 0) return;
	stm32g4_rcc_enable(unit, enable);

	if (stm32g4_timer_is_advanced(timer))
	{
		stm32g4_timer[timer]->af1 = 0x00000000;
		stm32g4_timer[timer]->af2 = 0x00000000;
	}
}


static bool stm32g4_timer__has_interrupt(stm32g4_timer_e timer)
{
	unsigned i;
	for (i = 0; i < STM32G4_TIMER_INT_COUNT; i++)
	{
		if (stm32g4_timer_context[timer].isr[i])
			return true;
	}
	return false;
}

bool stm32g4_timer_interrupt_register(
	stm32g4_timer_e timer, stm32g4_timer_int_e type,
	void (*handler)(void))
{
	switch (timer)
	{
		case STM32G4_TIMER_TIM1:
		case STM32G4_TIMER_TIM8:
		case STM32G4_TIMER_TIM20:
			switch (type)
			{
				case STM32G4_TIMER_INT_UPDATE:
				case STM32G4_TIMER_INT_COM:
				case STM32G4_TIMER_INT_TRIGGER:
				case STM32G4_TIMER_INT_BREAK:
				case STM32G4_TIMER_INT_BREAK2:
				case STM32G4_TIMER_INT_SYSTEM_BREAK:
				case STM32G4_TIMER_INT_IDX:
				case STM32G4_TIMER_INT_DIR:
				case STM32G4_TIMER_INT_IERR:
				case STM32G4_TIMER_INT_TERR:
				case STM32G4_TIMER_INT_CH0:
				case STM32G4_TIMER_INT_CH1:
				case STM32G4_TIMER_INT_CH2:
				case STM32G4_TIMER_INT_CH3:
				case STM32G4_TIMER_INT_CH4:
				case STM32G4_TIMER_INT_CH5:
					break;
				default:
					return false;
			}
			break;

		case STM32G4_TIMER_TIM2:
		case STM32G4_TIMER_TIM3:
		case STM32G4_TIMER_TIM4:
		case STM32G4_TIMER_TIM5:
			switch (type)
			{
				case STM32G4_TIMER_INT_UPDATE:
				case STM32G4_TIMER_INT_TRIGGER:
				case STM32G4_TIMER_INT_IDX:
				case STM32G4_TIMER_INT_DIR:
				case STM32G4_TIMER_INT_IERR:
				case STM32G4_TIMER_INT_TERR:
				case STM32G4_TIMER_INT_CH0:
				case STM32G4_TIMER_INT_CH1:
				case STM32G4_TIMER_INT_CH2:
				case STM32G4_TIMER_INT_CH3:
					break;
				default:
					return false;
			}
			break;

		case STM32G4_TIMER_TIM6:
		case STM32G4_TIMER_TIM7:
			switch (type)
			{
				case STM32G4_TIMER_INT_UPDATE:
					break;
				default:
					return false;
			}
			break;

		case STM32G4_TIMER_TIM15:
			switch (type)
			{
				case STM32G4_TIMER_INT_UPDATE:
				case STM32G4_TIMER_INT_COM:
				case STM32G4_TIMER_INT_TRIGGER:
				case STM32G4_TIMER_INT_BREAK:
				case STM32G4_TIMER_INT_CH0:
				case STM32G4_TIMER_INT_CH1:
					break;
				default:
					return false;
			}
			break;

		case STM32G4_TIMER_TIM16:
		case STM32G4_TIMER_TIM17:
			switch (type)
			{
				case STM32G4_TIMER_INT_UPDATE:
				case STM32G4_TIMER_INT_COM:
				case STM32G4_TIMER_INT_BREAK:
				case STM32G4_TIMER_INT_CH0:
					break;
				default:
					return false;
			}
			break;

		default:
			return false;
	}

	bool enable = (handler != NULL);
	stm32g4_timer_dier_t dier = { .mask = stm32g4_timer[timer]->dier };

	switch (type)
	{
		case STM32G4_TIMER_INT_UPDATE:
			dier.uie = enable;
			break;

		case STM32G4_TIMER_INT_COM:
			dier.comie = enable;
			break;

		case STM32G4_TIMER_INT_TRIGGER:
			dier.tie = enable;
			break;

		case STM32G4_TIMER_INT_BREAK:
		case STM32G4_TIMER_INT_BREAK2:
		case STM32G4_TIMER_INT_SYSTEM_BREAK:
			dier.bie = enable;
			break;

		case STM32G4_TIMER_INT_IDX:
			dier.idxie = enable;
			break;

		case STM32G4_TIMER_INT_DIR:
			dier.dirie = enable;
			break;

		case STM32G4_TIMER_INT_IERR:
			dier.ierrie = enable;
			break;

		case STM32G4_TIMER_INT_TERR:
			dier.terrie = enable;
			break;

		case STM32G4_TIMER_INT_CH0:
			dier.cc1ie = enable;
			break;

		case STM32G4_TIMER_INT_CH1:
			dier.cc2ie = enable;
			break;

		case STM32G4_TIMER_INT_CH2:
			dier.cc3ie = enable;
			break;

		case STM32G4_TIMER_INT_CH3:
			dier.cc4ie = enable;
			break;

		default:
			break;
	}

	if (enable)
	{
		stm32g4_timer_context[timer].isr[type] = handler;

		switch (timer)
		{
			case STM32G4_TIMER_TIM1:
				switch (type)
				{
					case STM32G4_TIMER_INT_UPDATE:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_UP__TIM16, true);
						break;

					case STM32G4_TIMER_INT_BREAK:
					case STM32G4_TIMER_INT_BREAK2:
					case STM32G4_TIMER_INT_SYSTEM_BREAK:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_BRK__TIM15, true);
						break;

					case STM32G4_TIMER_INT_TRIGGER:
					case STM32G4_TIMER_INT_COM:
					case STM32G4_TIMER_INT_DIR:
					case STM32G4_TIMER_INT_IDX:
						cm4_nvic_irq_enable(
						    STM32G4_IRQ_TIM1_TRG_COM_DIR_IDX__TIM17, true);
						break;

					case STM32G4_TIMER_INT_CH0:
					case STM32G4_TIMER_INT_CH1:
					case STM32G4_TIMER_INT_CH2:
					case STM32G4_TIMER_INT_CH3:
					case STM32G4_TIMER_INT_CH4:
					case STM32G4_TIMER_INT_CH5:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_CC, true);
						break;

					default:
						break;
				}
				break;

			case STM32G4_TIMER_TIM2:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM2, true);
				break;

			case STM32G4_TIMER_TIM3:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM3, true);
				break;

			case STM32G4_TIMER_TIM4:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM4, true);
				break;

			case STM32G4_TIMER_TIM5:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM5, true);
				break;

			case STM32G4_TIMER_TIM6:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM6_DAC_UNDER, true);
				break;

			case STM32G4_TIMER_TIM7:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM7_DAC_UNDER, true);
				break;

			case STM32G4_TIMER_TIM8:
				switch (type)
				{
					case STM32G4_TIMER_INT_UPDATE:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_UP, true);
						break;

					case STM32G4_TIMER_INT_BREAK:
					case STM32G4_TIMER_INT_BREAK2:
					case STM32G4_TIMER_INT_SYSTEM_BREAK:
					case STM32G4_TIMER_INT_TERR:
					case STM32G4_TIMER_INT_IERR:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_BRK_TERR_IERR, true);
						break;

					case STM32G4_TIMER_INT_TRIGGER:
					case STM32G4_TIMER_INT_COM:
					case STM32G4_TIMER_INT_DIR:
					case STM32G4_TIMER_INT_IDX:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_TRG_COM_DIR_IDX, true);
						break;

					case STM32G4_TIMER_INT_CH0:
					case STM32G4_TIMER_INT_CH1:
					case STM32G4_TIMER_INT_CH2:
					case STM32G4_TIMER_INT_CH3:
					case STM32G4_TIMER_INT_CH4:
					case STM32G4_TIMER_INT_CH5:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_CC, true);
						break;

					default:
						break;
				}
				break;

			case STM32G4_TIMER_TIM15:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_BRK__TIM15, true);
				break;

			case STM32G4_TIMER_TIM16:
				cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_UP__TIM16, true);
				break;

			case STM32G4_TIMER_TIM17:
				cm4_nvic_irq_enable(
					STM32G4_IRQ_TIM1_TRG_COM_DIR_IDX__TIM17, true);
				break;

			case STM32G4_TIMER_TIM20:
				switch (type)
				{
					case STM32G4_TIMER_INT_UPDATE:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM20_UP, true);
						break;

					case STM32G4_TIMER_INT_BREAK:
					case STM32G4_TIMER_INT_BREAK2:
					case STM32G4_TIMER_INT_SYSTEM_BREAK:
					case STM32G4_TIMER_INT_TERR:
					case STM32G4_TIMER_INT_IERR:
						cm4_nvic_irq_enable(
						    STM32G4_IRQ_TIM20_BRK_TERR_IERR, true);
						break;

					case STM32G4_TIMER_INT_TRIGGER:
					case STM32G4_TIMER_INT_COM:
					case STM32G4_TIMER_INT_DIR:
					case STM32G4_TIMER_INT_IDX:
						cm4_nvic_irq_enable(
						    STM32G4_IRQ_TIM20_TRG_COM_DIR_IDX, true);
						break;

					case STM32G4_TIMER_INT_CH0:
					case STM32G4_TIMER_INT_CH1:
					case STM32G4_TIMER_INT_CH2:
					case STM32G4_TIMER_INT_CH3:
					case STM32G4_TIMER_INT_CH4:
					case STM32G4_TIMER_INT_CH5:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM20_CC, true);
						break;

					default:
						break;
				}
				break;

			default:
				break;
		}

		stm32g4_timer[timer]->dier = dier.mask;
	}
	else
	{
		stm32g4_timer[timer]->dier = dier.mask;
		stm32g4_timer_context[timer].isr[type] = NULL;

		switch (timer)
		{
			case STM32G4_TIMER_TIM1:
				switch (type)
				{
					case STM32G4_TIMER_INT_UPDATE:
						if (!stm32g4_timer__has_interrupt(STM32G4_TIMER_TIM16))
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_UP__TIM16, false);
						break;

					case STM32G4_TIMER_INT_BREAK:
					case STM32G4_TIMER_INT_BREAK2:
					case STM32G4_TIMER_INT_SYSTEM_BREAK:
						if (!stm32g4_timer__has_interrupt(STM32G4_TIMER_TIM15)
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_BREAK]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_BREAK2]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_SYSTEM_BREAK])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_BRK__TIM15, false);
						break;

					case STM32G4_TIMER_INT_TRIGGER:
					case STM32G4_TIMER_INT_COM:
						if (!stm32g4_timer__has_interrupt(STM32G4_TIMER_TIM17)
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_TRIGGER]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_COM])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_TRG_COM_DIR_IDX__TIM17, false);
						break;

					case STM32G4_TIMER_INT_CH0:
					case STM32G4_TIMER_INT_CH1:
					case STM32G4_TIMER_INT_CH2:
					case STM32G4_TIMER_INT_CH3:
					case STM32G4_TIMER_INT_CH4:
					case STM32G4_TIMER_INT_CH5:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH0]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH1]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH2]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH3]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH4]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH5])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_CC, false);
						break;

					default:
						break;
				}
				break;

			case STM32G4_TIMER_TIM2:
				if (!stm32g4_timer__has_interrupt(timer))
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM2, false);
				break;

			case STM32G4_TIMER_TIM3:
				if (!stm32g4_timer__has_interrupt(timer))
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM3, false);
				break;

			case STM32G4_TIMER_TIM4:
				if (!stm32g4_timer__has_interrupt(timer))
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM4, false);
				break;

			case STM32G4_TIMER_TIM5:
				if (!stm32g4_timer__has_interrupt(timer))
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM5, false);
				break;

			case STM32G4_TIMER_TIM6:
				// TODO: Handle collision with DAC_UNDER
				if (!stm32g4_timer__has_interrupt(timer))
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM6_DAC_UNDER, false);
				break;

			case STM32G4_TIMER_TIM7:
				// TODO: Handle collision with DAC_UNDER
				if (!stm32g4_timer__has_interrupt(timer))
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM7_DAC_UNDER, false);
				break;

			case STM32G4_TIMER_TIM8:
				switch (type)
				{
					case STM32G4_TIMER_INT_UPDATE:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_UP, false);
						break;

					case STM32G4_TIMER_INT_BREAK:
					case STM32G4_TIMER_INT_BREAK2:
					case STM32G4_TIMER_INT_SYSTEM_BREAK:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_BREAK]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_BREAK2]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_SYSTEM_BREAK]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_TERR]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_IERR])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_BRK_TERR_IERR, false);
						break;

					case STM32G4_TIMER_INT_TRIGGER:
					case STM32G4_TIMER_INT_COM:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_TRIGGER]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_COM]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_DIR]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_IDX])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_TRG_COM_DIR_IDX, false);
						break;

					case STM32G4_TIMER_INT_CH0:
					case STM32G4_TIMER_INT_CH1:
					case STM32G4_TIMER_INT_CH2:
					case STM32G4_TIMER_INT_CH3:
					case STM32G4_TIMER_INT_CH4:
					case STM32G4_TIMER_INT_CH5:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH0]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH1]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH2]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH3]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH4]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH5])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM8_CC, false);
						break;

					default:
						break;
				}
				break;

			case STM32G4_TIMER_TIM15:
				if (!stm32g4_timer__has_interrupt(timer)
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_BREAK]
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_BREAK2]
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_SYSTEM_BREAK])
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_BRK__TIM15, false);
				break;

			case STM32G4_TIMER_TIM16:
				if (!stm32g4_timer__has_interrupt(timer)
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_UPDATE])
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_UP__TIM16, false);
				break;

			case STM32G4_TIMER_TIM17:
				if (!stm32g4_timer__has_interrupt(timer)
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_TRIGGER]
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_COM]
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_DIR]
					&& !stm32g4_timer_context[STM32G4_TIMER_TIM1].isr[STM32G4_TIMER_INT_IDX])
					cm4_nvic_irq_enable(STM32G4_IRQ_TIM1_TRG_COM_DIR_IDX__TIM17, false);
				break;

			case STM32G4_TIMER_TIM20:
				switch (type)
				{
					case STM32G4_TIMER_INT_UPDATE:
						cm4_nvic_irq_enable(STM32G4_IRQ_TIM20_UP, false);
						break;

					case STM32G4_TIMER_INT_BREAK:
					case STM32G4_TIMER_INT_BREAK2:
					case STM32G4_TIMER_INT_SYSTEM_BREAK:
					case STM32G4_TIMER_INT_TERR:
					case STM32G4_TIMER_INT_IERR:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_BREAK]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_BREAK2]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_SYSTEM_BREAK]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_TERR]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_IERR])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM20_BRK_TERR_IERR, false);
						break;

					case STM32G4_TIMER_INT_TRIGGER:
					case STM32G4_TIMER_INT_COM:
					case STM32G4_TIMER_INT_DIR:
					case STM32G4_TIMER_INT_IDX:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_TRIGGER]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_COM]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_DIR]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_IDX])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM20_TRG_COM_DIR_IDX, false);
						break;

					case STM32G4_TIMER_INT_CH0:
					case STM32G4_TIMER_INT_CH1:
					case STM32G4_TIMER_INT_CH2:
					case STM32G4_TIMER_INT_CH3:
					case STM32G4_TIMER_INT_CH4:
					case STM32G4_TIMER_INT_CH5:
						if (!stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH0]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH1]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH2]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH3]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH4]
							&& !stm32g4_timer_context[timer].isr[STM32G4_TIMER_INT_CH5])
							cm4_nvic_irq_enable(STM32G4_IRQ_TIM20_CC, false);
						break;

					default:
						break;
				}
				break;

			default:
				break;
		}
	}

	return true;
}

#define STM32G4_TIMER_CLOCK_MAP(x) case STM32G4_TIMER_##x: clock = STM32G4_CLOCK_##x; break
bool stm32g4_timer_freq(stm32g4_timer_e timer, unsigned* base, unsigned* freq)
{
	stm32g4_clock_e clock;
	switch (timer)
	{
		STM32G4_TIMER_CLOCK_MAP(TIM1);
		STM32G4_TIMER_CLOCK_MAP(TIM2);
		STM32G4_TIMER_CLOCK_MAP(TIM3);
		STM32G4_TIMER_CLOCK_MAP(TIM4);
		STM32G4_TIMER_CLOCK_MAP(TIM5);
		STM32G4_TIMER_CLOCK_MAP(TIM6);
		STM32G4_TIMER_CLOCK_MAP(TIM7);
		STM32G4_TIMER_CLOCK_MAP(TIM8);
		STM32G4_TIMER_CLOCK_MAP(TIM15);
		STM32G4_TIMER_CLOCK_MAP(TIM16);
		STM32G4_TIMER_CLOCK_MAP(TIM17);
		STM32G4_TIMER_CLOCK_MAP(TIM20);
		default: return false;
	}

	unsigned b;
	if (!stm32g4_clock_freq(clock, &b))
	     return false;

	unsigned f = b / (stm32g4_timer[timer]->psc + 1);

	if (base) *base = b;
	if (freq) *freq = f;
	return true;
}

bool stm32g4_timer_freq_set(stm32g4_timer_e timer, unsigned target, unsigned* freq)
{
	unsigned base = 0;
	if (!stm32g4_timer_freq(timer, &base, NULL))
		return false;

	if (target > base)
		return false;

	unsigned prescale = (base / target);
	if ((prescale == 0) || (prescale > 65535))
		return false;

	stm32g4_timer[timer]->psc = (prescale - 1);
	if (freq) *freq = (base / prescale);
	return true;
}


bool stm32g4_timer_start(stm32g4_timer_e timer,
	bool oneshot, stm32g4_timer_dir_e dir,
	uint16_t count, uint16_t repeat)
{
	if ((timer >= STM32G4_TIMER_COUNT)
		|| STM32G4_TIMER_FIELD_READ(timer, cr1, cen))
		return false;

	if (dir > 1) return false;

	stm32g4_timer[timer]->arr = count;
	stm32g4_timer[timer]->rcr = repeat;

	stm32g4_timer_cr1_t cr1 = { .mask = stm32g4_timer[timer]->cr1 };
	cr1.dir = dir;
	cr1.opm = oneshot;
	cr1.cen = true;
	stm32g4_timer[timer]->cr1 = cr1.mask;
	return true;
}

void stm32g4_timer_stop(stm32g4_timer_e timer)
{
	if (timer < STM32G4_TIMER_COUNT)
		STM32G4_TIMER_FIELD_WRITE(timer, cr1, cen, false);
}

bool stm32g4_timer_channel_compare_set(
	stm32g4_timer_e timer, unsigned channel, uint16_t compare)
{
	if (timer >= STM32G4_TIMER_COUNT)
		return false;

	switch (channel)
	{
		case 0:
			stm32g4_timer[timer]->ccr1 = compare;
			break;

		case 1:
			stm32g4_timer[timer]->ccr2 = compare;
			break;

		case 2:
			stm32g4_timer[timer]->ccr3 = compare;
			break;

		case 3:
			stm32g4_timer[timer]->ccr4 = compare;
			break;

		case 4:
			STM32G4_TIMER_FIELD_WRITE(timer, ccr5, ccr5, compare);
			break;

		case 5:
			stm32g4_timer[timer]->ccr6 = compare;
			break;

		default:
			return false;
	}

	return true;
}

bool stm32g4_timer_channel_pwm_enable(
	stm32g4_timer_e timer, unsigned channel, uint16_t compare)
{
	if ((timer >= STM32G4_TIMER_COUNT)
		|| (channel >= 6))
		return false;

    unsigned ccs  = 0;
	bool     fe   = false;
	bool     pe   = false;
	unsigned mode = 6;
	bool     ce   = false;

	stm32g4_timer_ccmr1_oc_t ccmr1 = { .mask = 0 };
	ccmr1.cc1s   = ccs;
	ccmr1.oc1fe  = fe;
	ccmr1.oc1pe  = pe;
	ccmr1.oc1m   = (mode & 7);
	ccmr1.oc1ce  = ce;
	ccmr1.oc1m_3 = (mode >> 3);

	uint32_t mask  = 0x000100FF;
	if ((channel & 1) != 0)
	{
		mask       <<= 8;
		ccmr1.mask <<= 8;
	}

	switch (channel)
	{
		case 0:
		case 1:
			stm32g4_timer[timer]->ccmr1
				= (stm32g4_timer[timer]->ccmr1 & ~mask) | ccmr1.mask;
			break;

		case 2:
		case 3:
			stm32g4_timer[timer]->ccmr2
				= (stm32g4_timer[timer]->ccmr2 & ~mask) | ccmr1.mask;
			break;

		default:
			stm32g4_timer[timer]->ccmr3
				= (stm32g4_timer[timer]->ccmr3 & ~mask) | ccmr1.mask;
			break;
	}

	stm32g4_timer_channel_compare_set(timer, channel, compare);

	stm32g4_timer[timer]->ccer |= (0x1U << (channel * 4));

	if (stm32g4_timer_is_advanced(timer))
		STM32G4_TIMER_FIELD_WRITE(timer, bdtr, moe, true);
	return true;
}

void stm32g4_timer_channel_disable(stm32g4_timer_e timer, unsigned channel)
{
	if (timer < STM32G4_TIMER_COUNT)
		stm32g4_timer[timer]->ccer &= ~(0x5U << (channel * 4));
}



static inline void stm32g4_global_timer_irq(
	stm32g4_timer_e timer,
	bool update, bool com, bool trigger,
	bool brk, bool brk2, bool sysbrk,
	bool dir, bool idx, bool ierr, bool terr,
	unsigned channels)
{
	stm32g4_timer_context_t* context = &stm32g4_timer_context[timer];

	stm32g4_timer_sr_t sr = { .mask = stm32g4_timer[timer]->sr };

	if (update)
	{
		if (sr.uif && context->isr[STM32G4_TIMER_INT_UPDATE])
			context->isr[STM32G4_TIMER_INT_UPDATE]();
		sr.uif = false;
	}

	if (com)
	{
		if (sr.comif && context->isr[STM32G4_TIMER_INT_COM])
			context->isr[STM32G4_TIMER_INT_COM]();
		sr.comif = false;
	}

	if (trigger)
	{
		if (sr.tif && context->isr[STM32G4_TIMER_INT_TRIGGER])
			context->isr[STM32G4_TIMER_INT_TRIGGER]();
		sr.tif = false;
	}

	if (brk)
	{
		if (sr.bif && context->isr[STM32G4_TIMER_INT_BREAK])
			context->isr[STM32G4_TIMER_INT_BREAK]();
		sr.bif = false;
	}

	if (brk2)
	{
		if (sr.b2if && context->isr[STM32G4_TIMER_INT_BREAK2])
			context->isr[STM32G4_TIMER_INT_BREAK2]();
		sr.b2if = false;
	}

	if (sysbrk)
	{
		if (sr.sbif && context->isr[STM32G4_TIMER_INT_SYSTEM_BREAK])
			context->isr[STM32G4_TIMER_INT_SYSTEM_BREAK]();
		sr.sbif = false;
	}

	if (dir)
	{
		if (sr.dirf && context->isr[STM32G4_TIMER_INT_DIR])
			context->isr[STM32G4_TIMER_INT_DIR]();
		sr.dirf = false;
	}

	if (idx)
	{
		if (sr.idxf && context->isr[STM32G4_TIMER_INT_IDX])
			context->isr[STM32G4_TIMER_INT_IDX]();
		sr.idxf = false;
	}

	if (ierr)
	{
		if (sr.ierrf && context->isr[STM32G4_TIMER_INT_IERR])
			context->isr[STM32G4_TIMER_INT_IERR]();
		sr.ierrf = false;
	}

	if (terr)
	{
		if (sr.terrf && context->isr[STM32G4_TIMER_INT_TERR])
			context->isr[STM32G4_TIMER_INT_TERR]();
		sr.terrf = false;
	}

	if (channels >= 1)
	{
		if (sr.cc1if && context->isr[STM32G4_TIMER_INT_CH0])
			context->isr[STM32G4_TIMER_INT_CH0]();
		sr.cc1if = false;
	}

	if (channels >= 2)
	{
		if (sr.cc2if && context->isr[STM32G4_TIMER_INT_CH1])
			context->isr[STM32G4_TIMER_INT_CH1]();
		sr.cc2if = false;
	}

	if (channels >= 3)
	{
		if (sr.cc3if && context->isr[STM32G4_TIMER_INT_CH2])
			context->isr[STM32G4_TIMER_INT_CH2]();
		sr.cc3if = false;
	}

	if (channels >= 4)
	{
		if (sr.cc4if && context->isr[STM32G4_TIMER_INT_CH3])
			context->isr[STM32G4_TIMER_INT_CH3]();
		sr.cc4if = false;
	}

	if (channels >= 5)
	{
		if (sr.cc5if && context->isr[STM32G4_TIMER_INT_CH4])
			context->isr[STM32G4_TIMER_INT_CH4]();
		sr.cc5if = false;
	}

	if (channels >= 6)
	{
		if (sr.cc6if && context->isr[STM32G4_TIMER_INT_CH5])
			context->isr[STM32G4_TIMER_INT_CH5]();
		sr.cc6if = false;
	}

	stm32g4_timer[timer]->sr = sr.mask;
}


void stm32_irq_tim1_brk__tim15(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM1 , 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0);
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM15, 1, 1 ,1, 1, 0, 0, 0, 0, 0, 0, 2);
}

void stm32_irq_tim1_up__tim16(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM1 , 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM16, 1, 1 ,0, 1, 0, 0, 0, 0, 0, 0, 1);
}

void stm32_irq_tim1_trg_com_dir_idx__tim17(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM1 , 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0);
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM17, 1, 1 ,0, 1, 0, 0, 0, 0, 0, 0, 1);
}

void stm32_irq_tim1_cc(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6);
}

void stm32_irq_tim2(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM2, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 4);
}

void stm32_irq_tim3(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM3, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 4);
}

void stm32_irq_tim4(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM4, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 4);
}

void stm32_irq_tim8_brk_terr_ierr(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM8, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0);
}

void stm32_irq_tim8_up(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

void stm32_irq_tim8_trg_com_dir_idx(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM8, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0);
}

void stm32_irq_tim8_cc(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6);
}

void stm32_irq_tim5(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM5, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 4);
}

void stm32_irq_tim6_dacunder(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

void stm32_irq_tim7_dacunder(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

void stm32_irq_tim20_brk_terr_ierr(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM20, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0);
}

void stm32_irq_tim20_up(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM20, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

void stm32_irq_tim20_trg_com_dir_idx(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM20, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0);
}

void stm32_irq_tim20_cc(void)
{
	stm32g4_global_timer_irq(STM32G4_TIMER_TIM20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6);
}
