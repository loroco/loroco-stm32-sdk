#include <stm32g4/clock.h>
#include <stm32g4/dma.h>
#include <stm32g4/dmamux.h>
#include <stm32g4/rcc.h>
#include <stm32g4/usart.h>
#include <stm32g4/reg/irq.h>
#include <stm32g4/reg/usart.h>

#include <cm4/nvic.h>

#include <string.h>

typedef struct
{
	void* user_data;
	void (*tx_compl)(void*, unsigned);
	/* TODO: fill me in */
} stm32g4_usart_handle_t;

static stm32g4_usart_handle_t stm32g4_usart_context[STM32G4_UART_COUNT] = {{0}};

static const stm32g4_rcc_unit_e stm32g4_usart_unit[STM32G4_UART_COUNT] =
{
	[STM32G4_LPUART1] = STM32G4_RCC_LPUART1,
	[STM32G4_USART1]  = STM32G4_RCC_USART1,
	[STM32G4_USART2]  = STM32G4_RCC_USART2,
	[STM32G4_USART3]  = STM32G4_RCC_USART3,
	[STM32G4_UART4]   = STM32G4_RCC_UART4,
	[STM32G4_UART5]   = STM32G4_RCC_UART5
};

static const stm32g4_irq_e stm32g4_usart_irq[STM32G4_UART_COUNT] =
{
	[STM32G4_LPUART1] = STM32G4_IRQ_LPUART,
	[STM32G4_USART1]  = STM32G4_IRQ_USART1,
	[STM32G4_USART2]  = STM32G4_IRQ_USART2,
	[STM32G4_USART3]  = STM32G4_IRQ_USART3,
	[STM32G4_UART4]   = STM32G4_IRQ_UART4,
	[STM32G4_UART5]   = STM32G4_IRQ_UART5
};

static stm32g4_dma_mem_region_e usart_dma_mem_map[STM32G4_UART_COUNT] =
{
	[STM32G4_LPUART1] = STM32G4_DMA_MEM_REGION_NONE,
	[STM32G4_USART1]  = STM32G4_DMA_MEM_REGION_USART1,
	[STM32G4_USART2]  = STM32G4_DMA_MEM_REGION_NONE,
	[STM32G4_USART3]  = STM32G4_DMA_MEM_REGION_NONE,
	[STM32G4_UART4]   = STM32G4_DMA_MEM_REGION_NONE,
	[STM32G4_UART5]   = STM32G4_DMA_MEM_REGION_NONE,
};

static stm32g4_dmamux_req_line_e usart_dmamux_req_map[STM32G4_USART_DIR_COUNT][STM32G4_UART_COUNT] =
{
	[STM32G4_USART_DIR_RX] =
	{
		[STM32G4_LPUART1] = STM32G4_DMAMUX_REQ_LINE_LPUART1_RX,
		[STM32G4_USART1]  = STM32G4_DMAMUX_REQ_LINE_USART1_RX,
		[STM32G4_USART2]  = STM32G4_DMAMUX_REQ_LINE_USART2_RX,
		[STM32G4_USART3]  = STM32G4_DMAMUX_REQ_LINE_USART3_RX,
		[STM32G4_UART4]   = STM32G4_DMAMUX_REQ_LINE_UART4_RX,
		[STM32G4_UART5]   = STM32G4_DMAMUX_REQ_LINE_UART5_RX
	},
	[STM32G4_USART_DIR_TX] =
	{
		[STM32G4_LPUART1] = STM32G4_DMAMUX_REQ_LINE_LPUART1_TX,
		[STM32G4_USART1]  = STM32G4_DMAMUX_REQ_LINE_USART1_TX,
		[STM32G4_USART2]  = STM32G4_DMAMUX_REQ_LINE_USART2_TX,
		[STM32G4_USART3]  = STM32G4_DMAMUX_REQ_LINE_USART3_TX,
		[STM32G4_UART4]   = STM32G4_DMAMUX_REQ_LINE_UART4_TX,
		[STM32G4_UART5]   = STM32G4_DMAMUX_REQ_LINE_UART5_TX
	}
};

void stm32g4_usart_reset(unsigned interface)
{
	if (interface >= STM32G4_UART_COUNT) return;
	stm32g4_rcc_reset(stm32g4_usart_unit[interface]);
}

void stm32g4_usart_enable(unsigned interface, bool enable)
{
	if (interface >= STM32G4_UART_COUNT) return;
	stm32g4_rcc_enable(stm32g4_usart_unit[interface], enable);
}


static unsigned stm32g4_usart_prescale_get(unsigned interface)
{
	uint32_t p = stm32g4_usart[interface]->presc;
	if (p > 11) return 0;
	return stm32g4_usart_prescale[p];
}

static stm32g4_clock_e stm32g4_usart_clock_get(unsigned interface)
{
	switch (interface)
	{
		case STM32G4_LPUART1: return STM32G4_CLOCK_LPUART1;
		case STM32G4_USART1:  return STM32G4_CLOCK_USART1;
		case STM32G4_USART2:  return STM32G4_CLOCK_USART2;
		case STM32G4_USART3:  return STM32G4_CLOCK_USART3;
		case STM32G4_UART4:   return STM32G4_CLOCK_UART4;
		case STM32G4_UART5:   return STM32G4_CLOCK_UART5;

		default: break;
	}
	return STM32G4_CLOCK_NONE;
}

unsigned stm32g4_usart_baud_get(unsigned interface)
{
	if (interface >= STM32G4_UART_COUNT)
		return 0;

	stm32g4_clock_e clock = stm32g4_usart_clock_get(interface);

	unsigned freq;
	if (!stm32g4_clock_freq(clock, &freq))
		return 0;

	unsigned clk = (freq * 256) / stm32g4_usart_prescale_get(interface);
	return (clk / stm32g4_usart[interface]->brr);
}

static bool stm32g4_usart_baud_set(unsigned interface, unsigned* baud)
{
	if (!baud || (*baud == 0))
		return false;

	/* TODO - Don't loop over prescale. */

	stm32g4_clock_e clock = stm32g4_usart_clock_get(interface);

	unsigned freq;
	if (!stm32g4_clock_freq(clock, &freq))
		return false;

	unsigned fmul     = (interface == 0 ?   256 :  1);
	unsigned divmin   = (interface == 0 ? 0x300 :  0);
	unsigned brr_size = (interface == 0 ?    20 : 16);

	unsigned p = 0;
	for (p = 0; p < 12; p++)
	{
		unsigned clk = (freq * fmul) / stm32g4_usart_prescale[p];
		unsigned div = (clk / *baud);

		unsigned over = (clk / div) - *baud;
		unsigned under = (clk / (div + 1)) - *baud;

		if (over > under) div += 1;

		if (((div >> brr_size) == 0) && (div >= divmin))
		{
			*baud = (clk / div);
			stm32g4_usart[interface]->presc = p;
			stm32g4_usart[interface]->brr = div;
			return true;
		}
	}

	return false;
}

bool stm32g4_usart_config_set(
	unsigned interface,
	unsigned baud,
	unsigned char_len, unsigned stop_bits,
	bool parity, bool parity_odd, bool fifo_en)
{
	if (interface >= STM32G4_UART_COUNT)
		return false;

	stm32g4_usart_cr1_t cr1 = { .mask = stm32g4_usart[interface]->cr1 };
	stm32g4_usart_cr2_t cr2 = { .mask = stm32g4_usart[interface]->cr2 };

	STM32G4_USART_FIELD_WRITE(interface, cr1, ue, false);

	if ((char_len < 7) || (char_len > 9))
		return false;

	if ((stop_bits < 1) || (stop_bits > 2))
		return false;

	cr1.m0 = (char_len == 9);
	cr1.m1 = (char_len == 7);
	cr1.pce = parity;
	cr1.ps  = parity_odd;

	cr1.te = true;
	cr1.re = true;
	cr1.ue = true;

	cr1.fifoen = fifo_en;

	cr2.stop = ((stop_bits - 1) << 1);

	if (!stm32g4_usart_baud_set(interface, &baud))
		return false;

	stm32g4_usart[interface]->cr2 = cr2.mask;
	stm32g4_usart[interface]->cr1 = cr1.mask;
	return true;
}

/* Release dma memory for future acquire calls */
static void stm32g4_usart__transfer_complete(
	void* user_data, unsigned interface)
{
	(void)user_data;
	stm32g4_dma_mem_release(usart_dma_mem_map[interface]);
	// Disable dma channel for next config
	stm32g4_dma_channel_enable(STM32G4_DMA1, 0, false);
}

bool stm32g4_usart_dma_mode_enable(
	unsigned interface, stm32g4_usart_dir_e direction, bool enable)
{
	if ((interface >= STM32G4_UART_COUNT) ||
	    (direction >= STM32G4_USART_DIR_COUNT))
		return false;

	switch (direction)
	{
		case STM32G4_USART_DIR_TX:
		{
			// Populate context and enable relevant interrupts
			stm32g4_usart_handle_t* handle = &(stm32g4_usart_context[interface]);

			// TODO: support composite cbs (combining client tx_compl and
			//       the one required for dma)
			if (enable && handle->tx_compl)
				return false;

			handle->tx_compl = enable ? stm32g4_usart__transfer_complete : NULL;
			STM32G4_USART_FIELD_WRITE(interface, cr1, tcie, enable);
			cm4_nvic_irq_enable(stm32g4_usart_irq[interface], enable);

			STM32G4_USART_FIELD_WRITE(interface, cr3, dmat, enable);
			break;
		}

		case STM32G4_USART_DIR_RX:
			STM32G4_USART_FIELD_WRITE(interface, cr3, dmar, enable);
			break;

		default:
			break;
	}

	return true;
}

/* Optimised in-memory functions */
size_t stm32_usart_tx_acquire(unsigned interface, uint8_t** data)
{
	if (interface >= STM32G4_UART_COUNT)
		return 0;

	size_t size = stm32g4_dma_mem_acquire(usart_dma_mem_map[interface], data);

	return data != NULL ? size : 0;
}

bool stm32_usart_tx_commit(unsigned interface, size_t size)
{
	if (interface >= STM32G4_UART_COUNT)
		return 0;

	// Get src (dma sram) and target (usart->tdr reg) memory addresses
	uint8_t* dma_data;
	size_t   dma_size = stm32g4_dma_mem_reacquire(
		usart_dma_mem_map[interface], &dma_data);

	if (size > dma_size) return false;

	uint8_t* target = (uint8_t*)(&(stm32g4_usart[interface]->tdr));

	// TODO: work out how we'll allocate dma channels
	if (stm32g4_dma_channel_is_enabled(STM32G4_DMA1, 0))
		return false;

	// Configure the dma
	if (!stm32g4_dma_channel_config_set_full(
			STM32G4_DMA1, 0, size, (uintptr_t)target, (uintptr_t)dma_data,
			false, true, STM32G4_DMA_DATA_SZ_8, STM32G4_DMA_DATA_SZ_8,
			STM32G4_DMA_PL_VHIGH, STM32G4_DMA_DIR_MEM_PER, false,
			false, NULL, NULL, NULL, NULL))
		return false;

	if (!stm32g4_dmamux_request_line_config_basic(
			0, usart_dmamux_req_map[STM32G4_USART_DIR_TX][interface]))
		return false;

	// Clear transmission complete flag and enable DMA
	STM32G4_USART_FIELD_WRITE(interface, isr, tc, 0);
	if (!stm32g4_dma_channel_enable(STM32G4_DMA1, 0, true))
		return false;

	return true;
}

/* Helper, memcpy based functions */
void stm32g4_usart_read(unsigned interface, size_t size, char* data)
{
	unsigned i;
	for (i = 0; i < size; i++)
	{
		while (STM32G4_USART_FIELD_READ(interface, isr, rxfne) == 0);
		data[i] = stm32g4_usart[interface]->rdr;
	}
}

void stm32g4_usart_write(unsigned interface, size_t size, const char* data)
{
	if (STM32G4_USART_FIELD_READ(interface, cr3, dmat))
	{
		// Async TX using DMA
		uint8_t* dma_data;
		size_t   dma_data_cap = stm32_usart_tx_acquire(interface, &dma_data);
		if (!dma_data || (dma_data_cap < size))
			return;

		memcpy(dma_data, data, size);
		stm32_usart_tx_commit(interface, size);
	}
	else
	{
		unsigned i;
		for (i = 0; i < size; i++)
		{
			while (STM32G4_USART_FIELD_READ(interface, isr, txfnf) == 0);
			stm32g4_usart[interface]->tdr = data[i];
		}
		while (STM32G4_USART_FIELD_READ(interface, isr, tc) == 0);
	}
}

/* Interrupt Handlers */

static void stm32_irq_usart(unsigned interface)
{
	stm32g4_usart_isr_t isr = { .mask = stm32g4_usart[interface]->isr };
	stm32g4_usart_handle_t* handle = &(stm32g4_usart_context[interface]);

	if (isr.tc && handle->tx_compl)
	{
		handle->tx_compl(handle->user_data, interface);
		// clear interrupt flag
		STM32G4_USART_FIELD_WRITE(interface, icr, tc, 1);
	}

	// TODO: support other interrupts
}


void stm32_irq_lpuart(void)
{
	stm32_irq_usart(STM32G4_LPUART1);
}

void stm32_irq_usart1(void)
{
	stm32_irq_usart(STM32G4_USART1);
}

void stm32_irq_usart2(void)
{
	stm32_irq_usart(STM32G4_USART2);
}

void stm32_irq_usart3(void)
{
	stm32_irq_usart(STM32G4_USART3);
}

void stm32_irq_uart4(void)
{
	// TODO: separate cb for uarts?
}

void stm32_irq_uart5(void)
{
	// TODO: separate cb for uarts?
}
