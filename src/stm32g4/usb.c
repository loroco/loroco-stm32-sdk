#include <stm32g4/reg/usb.h>
#include <stm32g4/reg/irq.h>
#include <stm32g4/usb.h>
#include <stm32g4/rcc.h>
#include <stm32g4/clock.h>
#include <cm4/nvic.h>
#include <util/delay.h>


volatile uint16_t* stm32_usb_sram_btable;
volatile uint16_t* stm32_usb_sram_ptr;
size_t             stm32_usb_sram_avail = 0;

static void stm32_usb_btable_init(void)
{
	/* Initialize SRAM */
	stm32_usb_sram_ptr   = stm32g4_usb_sram;
	stm32_usb_sram_avail = STM32G4_USB_SRAM_SIZE;

	/* Allocate BTABLE */
	stm32_usb_sram_btable = stm32_usb_sram_ptr;
	stm32_usb_sram_ptr    = &stm32_usb_sram_ptr[STM32G4_USB_BTABLE_SIZE / 2];
	stm32_usb_sram_avail -= STM32G4_USB_BTABLE_SIZE;

	stm32g4_usb->btable = ((uintptr_t)stm32_usb_sram_btable - (uintptr_t)stm32g4_usb_sram);
}

static bool stm32_usb_btable_alloc(
	unsigned index, size_t packet_size, bool tx, bool rx, bool dbl_buf)
{
	index %= 8;

	/* TODO - Support 2 to 512 byte TX buffers? */

	if ((packet_size == 0)
		|| (packet_size > 480))
		return false;

	stm32g4_usb_btable_count_t count;
	count.count = 0;

	if (packet_size <= 62)
	{
		if (packet_size % 2) packet_size++;

		count.blsize    = 0;
		count.num_block = (packet_size / 2);
	}
	else
	{
		unsigned remain = (packet_size % 32);
		if (remain > 0) packet_size += (32 - remain);

		count.blsize    = 1;
		count.num_block = (packet_size / 32) - 1;
	}

	size_t size = 0;
	if (rx) size += packet_size;
	if (tx) size += packet_size;
	if (dbl_buf) size <<= 1;

	if (size > stm32_usb_sram_avail)
		return false;

	if (dbl_buf)
	{
		stm32_usb_sram_btable[(index * 4) + 0] = ((uintptr_t)stm32_usb_sram_ptr & 0xFFFF);
		stm32_usb_sram_btable[(index * 4) + 1] = count.mask;
		stm32_usb_sram_ptr = &stm32_usb_sram_ptr[packet_size / 2];

		stm32_usb_sram_btable[(index * 4) + 2] = ((uintptr_t)stm32_usb_sram_ptr & 0xFFFF);
		stm32_usb_sram_btable[(index * 4) + 3] = count.mask;
		stm32_usb_sram_ptr = &stm32_usb_sram_ptr[packet_size / 2];
	}
	else
	{
		stm32_usb_sram_btable[(index * 4) + 1] = 0;
		if (tx)
		{
			stm32_usb_sram_btable[(index * 4) + 0] = ((uintptr_t)stm32_usb_sram_ptr & 0xFFFF);
			stm32_usb_sram_btable[(index * 4) + 1] = count.mask;
			stm32_usb_sram_ptr = &stm32_usb_sram_ptr[packet_size / 2];
		}

		stm32_usb_sram_btable[(index * 4) + 3] = 0;
		if (rx)
		{
			stm32_usb_sram_btable[(index * 4) + 2] = ((uintptr_t)stm32_usb_sram_ptr & 0xFFFF);
			stm32_usb_sram_btable[(index * 4) + 3] = count.mask;
			stm32_usb_sram_ptr = &stm32_usb_sram_ptr[packet_size / 2];
		}
	}

	stm32_usb_sram_avail -= size;
	return true;
}

static void stm32_usb_btable_get(unsigned index, unsigned offset,
	void** data, size_t* size, size_t* count)
{
	index %= 8;
	offset = (offset & 1) << 1;

	uint16_t addr = stm32_usb_sram_btable[(index * 4) + offset + 0];

	stm32g4_usb_btable_count_t bcount = {
		.mask = stm32_usb_sram_btable[(index * 4) + offset + 1] };

	if (data ) *data  = (void*)((uintptr_t)stm32g4_usb_sram + addr);
	if (size ) *size  = bcount.num_block * (bcount.blsize ? 32 : 2);
	if (count) *count = bcount.count;
}

static bool stm32_usb_btable_count_set(unsigned index, unsigned offset, size_t count)
{
	offset = (offset & 1) << 1;

	stm32g4_usb_btable_count_t bcount = {
		.mask = stm32_usb_sram_btable[(index * 4) + offset + 1] };

	size_t size = bcount.num_block * (bcount.blsize ? 32 : 2);
	if (count > size) return false;

	bcount.count = count;
	stm32_usb_sram_btable[(index * 4) + offset + 1] = bcount.mask;
	return true;
}


static void stm32_usb_sram_memcpy(uint8_t* restrict dst, const uint8_t* restrict src, size_t size)
{
	uint8_t* end = &dst[size];

	if ((((uintptr_t)dst % 2) == 0)
		&& (((uintptr_t)src % 2) == 0))
	{
		uint16_t* dst16 = (uint16_t*)dst;
		const uint16_t* src16 = (const uint16_t*)src;
		uint16_t* end16 = &dst16[size / 2];

		while (dst16 != end16)
			*dst16++ = *src16++;

		src = (uint8_t*)src16;
		dst = (uint8_t*)dst16;
	}

	while (dst != end)
		*dst++ = *src++;
}


typedef struct
{
	void* handle;
	void (*tx_cb)(void*, uint8_t);
	void (*rx_cb)(void*, uint8_t, bool);

	volatile unsigned tx_queued, rx_queued;
	volatile unsigned tx_setup , rx_setup ;
	volatile bool     rx_setup_ack;
} stm32_usb_endpoint_t;

stm32_usb_endpoint_t stm32_usb_endpoint[STM32G4_USB_EP_COUNT] = {0};

bool stm32_usb_endpoint_configure(
	unsigned index, uint8_t addr,
	usb_endpoint_type_e type, bool dbl_buf,
	size_t packet_size,
	void* handle,
	void (*tx)(void*, uint8_t),
	void (*rx)(void*, uint8_t, bool))
{
	if (index >= STM32G4_USB_EP_COUNT)
		return false;

	if (addr >= 0x10) return false;

	/* TODO - Support half-duplex control endpoint? */

	stm32g4_usb_ep_type_e ep_type;
	switch (type)
	{
		case USB_ENDPOINT_TYPE_BULK:
			ep_type = STM32G4_USB_EP_TYPE_BULK;
			break;

		case USB_ENDPOINT_TYPE_CONTROL:
			ep_type = STM32G4_USB_EP_TYPE_CONTROL;
			break;

		case USB_ENDPOINT_TYPE_ISOCHRONOUS:
			ep_type = STM32G4_USB_EP_TYPE_ISO;
			break;

		case USB_ENDPOINT_TYPE_INTERRUPT:
			ep_type = STM32G4_USB_EP_TYPE_INTERRUPT;
			break;

		default:
			return false;
	}

	switch (ep_type)
	{
		case STM32G4_USB_EP_TYPE_CONTROL:
		case STM32G4_USB_EP_TYPE_ISO:
		case STM32G4_USB_EP_TYPE_INTERRUPT:
			if (dbl_buf) return false;
			break;

		default:
			break;
	}

	stm32g4_usb_ep_kind_e ep_kind = STM32G4_USB_EP_KIND_NONE;
	if (dbl_buf) ep_kind = STM32G4_USB_EP_KIND_DBL_BUF;

	/* Isochronous endpoints are always functionally double buffered. */
	if (ep_type == STM32G4_USB_EP_TYPE_ISO)
		dbl_buf = true;

	/* Can't be double-buffered and bi-directional. */
	if (dbl_buf && tx && rx)
		return false;

	if (!stm32_usb_btable_alloc(index, packet_size, !!tx, !!rx, dbl_buf))
		return false;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	endpoint->handle    = handle;
	endpoint->tx_cb     = tx;
	endpoint->rx_cb     = rx;
	endpoint->tx_queued = 0;
	endpoint->rx_queued = 0;
	endpoint->tx_setup  = 0;
	endpoint->rx_setup  = 0;
	endpoint->rx_setup_ack = false;

	stm32g4_usb_ep_status_e stat_rx = STM32G4_USB_EP_STATUS_DISABLED;
	if (rx) stat_rx = STM32G4_USB_EP_STATUS_VALID;

	stm32g4_usb_ep_status_e stat_tx = STM32G4_USB_EP_STATUS_DISABLED;
	if (tx)
	{
		switch (ep_type)
		{
			case STM32G4_USB_EP_TYPE_ISO:
				/* For ISO endpoints keep it disabled until first transaction. */
				stat_tx = STM32G4_USB_EP_STATUS_DISABLED;
				break;

			default:
				stat_tx = STM32G4_USB_EP_STATUS_NAK;
				break;
		}
	}

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };

	epr.ea      = addr;
	epr.ep_type = ep_type;
	epr.ep_kind = ep_kind;

	epr.stat_rx ^= stat_rx;
	epr.stat_tx ^= stat_tx;

	/* Clear any pending interrupts. */
	epr.ctr_tx = 0;
	epr.ctr_rx = 0;

	/* Toggle dtog to zero ready for first packet. */
	epr.dtog_tx ^= epr.dtog_tx;
	epr.dtog_rx ^= epr.dtog_rx;

	stm32g4_usb->epr[index] = epr.mask;
	return true;
}

static inline bool stm32_usb_endpoint_is_high_priority(unsigned index)
{
	if (index >= STM32G4_USB_EP_COUNT)
		return false;

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };
	switch (epr.ep_type)
	{
		case STM32G4_USB_EP_TYPE_ISO:
			return true;

		case STM32G4_USB_EP_TYPE_BULK:
			return (epr.ep_kind == STM32G4_USB_EP_KIND_DBL_BUF);

		default:
			break;
	}

	return false;
}

static void stm32_usb_endpoint_irq(unsigned ep)
{
	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[ep] };

	/* Set toggle bits to zero so we don't change state. */
	stm32g4_usb_epr_t eprm = epr;
	eprm.stat_tx = 0;
	eprm.dtog_tx = 0;
	eprm.stat_rx = 0;
	eprm.dtog_rx = 0;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[ep];

	if (epr.ctr_tx)
	{
		if ((endpoint->tx_queued == 2)
			&& (epr.ep_type == STM32G4_USB_EP_TYPE_BULK)
			&& (epr.ep_kind == STM32G4_USB_EP_KIND_DBL_BUF))
			epr.dtog_rx = 1;

		if (endpoint->tx_queued > 0)
			endpoint->tx_queued--;

		if (endpoint->tx_setup > 0)
			endpoint->tx_setup--;

		if (endpoint->tx_cb)
			endpoint->tx_cb(endpoint->handle, epr.ea);

		/* Clear the STATUS_OUT bit when we've sent an acknowledge. */
		if (epr.ep_type == STM32G4_USB_EP_TYPE_CONTROL)
			eprm.ep_kind = STM32G4_USB_EP_KIND_NONE;

		eprm.ctr_tx = 0;
	}

	if (epr.ctr_rx)
	{
		endpoint->rx_queued++;

		if ((endpoint->rx_queued > 1)
			&& (epr.ep_kind == STM32G4_USB_EP_TYPE_ISO))
			endpoint->rx_queued = 1;

		if (endpoint->rx_setup == 1)
		{
			eprm.stat_tx = epr.stat_tx ^ STM32G4_USB_EP_STATUS_NAK;
			eprm.stat_rx = epr.stat_rx ^ STM32G4_USB_EP_STATUS_STALL;
			endpoint->rx_setup_ack = true;
		}

		if (endpoint->rx_setup > 0)
			endpoint->rx_setup--;

		if (endpoint->rx_cb)
			endpoint->rx_cb(endpoint->handle, epr.ea, epr.setup);

		eprm.ctr_rx = 0;
	}

	stm32g4_usb->epr[ep] = eprm.mask;
}

void stm32_usb_endpoint_setup_begin(unsigned index, bool is_rx, unsigned count)
{
	if (index >= STM32G4_USB_EP_COUNT) return;

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };

	epr.dtog_tx = 0;
	epr.dtog_rx = 0;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	if (is_rx)
	{
		epr.stat_rx ^= (count > 0 ? STM32G4_USB_EP_STATUS_VALID : STM32G4_USB_EP_STATUS_STALL);
		epr.stat_tx ^= (count > 0 ? STM32G4_USB_EP_STATUS_STALL : STM32G4_USB_EP_STATUS_NAK  );
		endpoint->rx_setup = count;
		if (count == 0) endpoint->rx_setup_ack = true;
	}
	else
	{
		epr.stat_tx ^= (count > 0 ? STM32G4_USB_EP_STATUS_NAK   : STM32G4_USB_EP_STATUS_STALL);
		epr.stat_rx ^= (count > 0 ? STM32G4_USB_EP_STATUS_STALL : STM32G4_USB_EP_STATUS_NAK  );
		endpoint->tx_setup = count;
	}

	stm32g4_usb->epr[index] = epr.mask;
}

static void stm32_usb_endpoint__send_zlp(unsigned index)
{
	stm32_usb_endpoint_tx_acquire(index, NULL);
	stm32_usb_endpoint_tx_commit (index, 0);
}

void stm32_usb_endpoint_setup_acknowledge(unsigned index, bool success)
{
	if (index >= STM32G4_USB_EP_COUNT) return;

	stm32g4_usb_ep_status_e stat = (success
		? STM32G4_USB_EP_STATUS_VALID
		: STM32G4_USB_EP_STATUS_STALL);

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };
	epr.dtog_tx = 0;
	epr.dtog_rx = 0;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	if (endpoint->rx_setup_ack)
	{
		stm32_usb_endpoint__send_zlp(index);
		epr.stat_tx ^= stat;
		epr.stat_rx  = 0;
	}
	else
	{
		epr.ep_kind  = STM32G4_USB_EP_KIND_STATUS_OUT;
		epr.stat_tx  = 0;
		epr.stat_rx ^= stat;
	}

	stm32g4_usb->epr[index] = epr.mask;
	endpoint->rx_setup_ack = false;
}

size_t stm32_usb_endpoint_tx_acquire(unsigned index, void** data)
{
	if (index >= STM32G4_USB_EP_COUNT) return 0;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	if (!endpoint->tx_cb) return 0;

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };

	bool dbl_buf = ((epr.ep_type == STM32G4_USB_EP_TYPE_BULK)
		&& (epr.ep_kind == STM32G4_USB_EP_KIND_DBL_BUF));

	unsigned txqmax = (dbl_buf ? 2 : 1);
	if (endpoint->tx_queued >= txqmax) return 0;

	unsigned sw_buf = 0;
	if (epr.ep_type == STM32G4_USB_EP_TYPE_ISO)
		sw_buf = !epr.dtog_tx;
	else if (dbl_buf)
		sw_buf = epr.dtog_rx;

	size_t size;
	stm32_usb_btable_get(index, sw_buf, data, &size, NULL);
	return size;
}

bool stm32_usb_endpoint_tx_commit(unsigned index, size_t size)
{
	if (index >= STM32G4_USB_EP_COUNT) return false;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	if (!endpoint->tx_cb) return false;

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };
	bool dbl_buf = ((epr.ep_type == STM32G4_USB_EP_TYPE_BULK)
		&& (epr.ep_kind == STM32G4_USB_EP_KIND_DBL_BUF));

	unsigned txqmax = (dbl_buf ? 2 : 1);
	if (endpoint->tx_queued >= txqmax) return false;

	unsigned sw_buf = 0;
	if (epr.ep_type == STM32G4_USB_EP_TYPE_ISO)
		sw_buf = !epr.dtog_tx;
	else if (dbl_buf)
		sw_buf = epr.dtog_rx;

	if (!stm32_usb_btable_count_set(index, sw_buf, size))
		return false;

	stm32g4_usb_ep_status_e stat_rx = epr.stat_rx;
	if (endpoint->tx_setup == 1)
		stat_rx = STM32G4_USB_EP_STATUS_NAK;

	epr.stat_tx ^= STM32G4_USB_EP_STATUS_VALID;
	epr.stat_rx ^= stat_rx;

	epr.dtog_tx = 0;
	epr.dtog_rx = (dbl_buf && (epr.dtog_tx == epr.dtog_rx));

	endpoint->tx_queued++;

	stm32g4_usb->epr[index] = epr.mask;
	return true;
}

bool stm32_usb_endpoint_write(unsigned index, size_t size, void* data)
{
	void* buff = NULL;
	size_t max_size = stm32_usb_endpoint_tx_acquire(index, &buff);
	if (size > max_size) return false;

	stm32_usb_sram_memcpy(buff, data, size);

	stm32_usb_endpoint_tx_commit(index, size);
	return true;
}


size_t stm32_usb_endpoint_rx_acquire(unsigned index, const void** data)
{
	if (index >= STM32G4_USB_EP_COUNT) return 0;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	if (!endpoint->rx_cb        ) return 0;
	if (endpoint->rx_queued == 0) return 0;

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };

	unsigned sw_buf = 1;
	if (epr.ep_type == STM32G4_USB_EP_TYPE_ISO)
		sw_buf = !epr.dtog_rx;
	else if ((epr.ep_type == STM32G4_USB_EP_TYPE_BULK)
		&& (epr.ep_kind == STM32G4_USB_EP_KIND_DBL_BUF))
		sw_buf = epr.dtog_tx;

	size_t size;
	stm32_usb_btable_get(index, sw_buf, (void**)data, NULL, &size);
	return size;
}

void stm32_usb_endpoint_rx_release(unsigned index)
{
	if (index >= STM32G4_USB_EP_COUNT) return;

	stm32_usb_endpoint_t* endpoint = &stm32_usb_endpoint[index];
	if (!endpoint->rx_cb        ) return;
	if (endpoint->rx_queued == 0) return;

	endpoint->rx_queued--;

	stm32g4_usb_epr_t epr = { .mask = stm32g4_usb->epr[index] };
	bool dbl_buf = ((epr.ep_type == STM32G4_USB_EP_TYPE_BULK)
		&& (epr.ep_kind == STM32G4_USB_EP_KIND_DBL_BUF));

	epr.stat_tx = 0;
	epr.stat_rx = 0;

	if (!endpoint->rx_setup_ack)
		epr.stat_tx ^= STM32G4_USB_EP_STATUS_VALID;

	epr.dtog_tx = dbl_buf;
	epr.dtog_rx = 0;

	stm32g4_usb->epr[index] = epr.mask;
}

size_t stm32_usb_endpoint_read(unsigned index, size_t size, void* data)
{
	const void* buff = NULL;
	size_t packet_size = stm32_usb_endpoint_rx_acquire(index, &buff);
	if (packet_size > size) return 0;

	stm32_usb_sram_memcpy(data, buff, packet_size);

	stm32_usb_endpoint_rx_release(index);
	return packet_size;
}


void stm32_usb_reset(void)
{
	STM32G4_USB_FIELD_WRITE(cntr, fres, true);
	stm32g4_rcc_reset(STM32G4_RCC_USB);
}

void stm32_usb_enable(bool enable)
{
	stm32g4_rcc_enable(STM32G4_RCC_USB, enable);

	STM32G4_USB_FIELD_WRITE(cntr, pdwn, !enable);

	if (!enable) return;

	delay_ms(STM32G4_USB_STARTUP_TIME_MS);
	STM32G4_USB_FIELD_WRITE(cntr, fres, false);

	cm4_nvic_irq_enable(STM32G4_IRQ_USB_HP  , enable);
	cm4_nvic_irq_enable(STM32G4_IRQ_USB_LP  , enable);
	cm4_nvic_irq_enable(STM32G4_IRQ_USB_WKUP, enable);
}


bool stm32_usb_port_detect(usb_port_type_e* type)
{
	stm32g4_usb_bcdr_t bcdr = { .mask = stm32g4_usb->bcdr };

	/* We can't detect port type while connected. */
	if (bcdr.dppu) return false;

	bcdr.bcden = true;
	bcdr.dcden = true;
	bcdr.pden  = false;
	bcdr.sden  = false;

	stm32g4_usb->bcdr = bcdr.mask;
	delay_ms(STM32G4_USB_BDCR_SETTLE_TIME_MS);
	bcdr.mask = stm32g4_usb->bcdr;
	bcdr.dcden = false;

	usb_port_type_e t = USB_PORT_TYPE_SDP;
	if (bcdr.dcdet)
	{
		bcdr.pden = true;

		stm32g4_usb->bcdr = bcdr.mask;
		/* TODO - Find out if multiple delays needed. */
		delay_ms(STM32G4_USB_BDCR_SETTLE_TIME_MS);
		bcdr.mask = stm32g4_usb->bcdr;
		bcdr.pden = false;

		if (bcdr.ps2det)
		{
			t = USB_PORT_TYPE_PS2;
		}
		else if (bcdr.pdet)
		{
			bcdr.sden = true;

			stm32g4_usb->bcdr = bcdr.mask;
			/* TODO - Find out if multiple delays needed. */
			delay_ms(STM32G4_USB_BDCR_SETTLE_TIME_MS);
			bcdr.mask = stm32g4_usb->bcdr;
			bcdr.sden = false;

			t = (bcdr.sdet ? USB_PORT_TYPE_DCP : USB_PORT_TYPE_CDP);
		}
	}

	bcdr.bcden = false;
	stm32g4_usb->bcdr = bcdr.mask;

	if (type) *type = t;
	return true;
}


bool stm32_usb_addr_set(uint8_t addr)
{
	if (addr >= 0x80) return false;
	STM32G4_USB_FIELD_WRITE(daddr, add, addr);
	return true;
}


static void* stm32_usb_callback_handle = NULL;
static void (*stm32_usb_callback_configure)(void*) = NULL;
static void (*stm32_usb_callback_suspend  )(void*) = NULL;
static void (*stm32_usb_callback_resume   )(void*) = NULL;

bool stm32_usb_open(
	void* handle,
	void (*configure)(void*),
	void (*suspend  )(void*),
	void (*resume   )(void*))
{
	if (!configure) return false;

	stm32_usb_callback_handle    = handle;
	stm32_usb_callback_configure = configure;
	stm32_usb_callback_suspend   = suspend;
	stm32_usb_callback_resume    = resume;

	stm32_usb_btable_init();

	/* Clear all pending interrutps. */
	stm32g4_usb->istr = 0;

	stm32g4_usb_cntr_t cntr = { .mask = stm32g4_usb->cntr };
	cntr.l1reqm  = false;
	cntr.esofm   = false;
	cntr.sofm    = false;
	cntr.resetm  = true;
	cntr.suspm   = (suspend != NULL);
	cntr.wkupm   = (resume  != NULL);
	cntr.errm    = false;
	cntr.pmaovrm = false;
	cntr.ctrm    = true;
	stm32g4_usb->cntr = cntr.mask;

	STM32G4_USB_FIELD_WRITE(daddr, ef, true);

	STM32G4_USB_FIELD_WRITE(bcdr, dppu, true);
	return true;
}

void stm32_usb_close(void)
{
	STM32G4_USB_FIELD_WRITE(bcdr, dppu, false);

	STM32G4_USB_FIELD_WRITE(daddr, ef, false);

	stm32g4_usb_cntr_t cntr = { .mask = stm32g4_usb->cntr };
	cntr.l1reqm  = false;
	cntr.esofm   = false;
	cntr.sofm    = false;
	cntr.resetm  = false;
	cntr.suspm   = false;
	cntr.wkupm   = false;
	cntr.errm    = false;
	cntr.pmaovrm = false;
	cntr.ctrm    = false;
	stm32g4_usb->cntr = cntr.mask;

	/* Clear all pending interrutps. */
	stm32g4_usb->istr = 0;
}


void stm32_irq_usb_hp(void)
{
	stm32g4_usb_istr_t istr = { .mask = stm32g4_usb->istr };

	while (istr.ctr && stm32_usb_endpoint_is_high_priority(istr.ep_id))
	{
		stm32_usb_endpoint_irq(istr.ep_id);
		istr.mask = stm32g4_usb->istr;
	}

	if (istr.reset)
	{
		if (stm32_usb_callback_configure)
			stm32_usb_callback_configure(stm32_usb_callback_handle);
		istr.reset = false;
	}

	if (istr.susp)
	{
		if (stm32_usb_callback_suspend)
			stm32_usb_callback_suspend(stm32_usb_callback_handle);

		STM32G4_USB_FIELD_WRITE(cntr, fsusp, true);

		istr.susp = false;
	}

	/* Clear handled interrupts. */
	stm32g4_usb->istr = istr.mask;
}

void stm32_irq_usb_lp(void)
{
	stm32g4_usb_istr_t istr = { .mask = stm32g4_usb->istr };

	while (istr.ctr)
	{
		stm32_usb_endpoint_irq(istr.ep_id);
		istr.mask = stm32g4_usb->istr;
	}

	/* TODO - Handle l1req. */
	/* TODO - Handle esof. */
	/* TODO - Handle sof, pass FNR value to callback. */
	/* TODO - Handle err. */
	/* TODO - Handle pmaovr. */

	stm32g4_usb->istr = istr.mask;
}

void stm32_irq_usb_wake_up(void)
{
	/* TODO - Work out what to do with RXDM and RXDP here. */

	if (stm32_usb_callback_resume)
		stm32_usb_callback_resume(stm32_usb_callback_handle);

	stm32g4_usb_istr_t istr = { 0 };
	istr.wkup = true;
	stm32g4_usb->istr = istr.mask;
}
