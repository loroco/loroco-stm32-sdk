#include <string.h>

void *memcpy(void * restrict s1, const void * restrict s2, size_t n)
{
	uint8_t *us1 = s1;
	const uint8_t *us2 = s2;
	while (n--) *(us1++) = *(us2++);
	return us1;
}

void *memset(void *s, int c, size_t n)
{
	uint8_t *us = s;
	while (n--) *(us++) = c;
	return us;
}

size_t strlen(const char *str)
{
	size_t nul;
	for (nul = 0; str[nul] != '\0'; nul++);

	return nul;
}
