#include <string.h>

#include <stm32g4/usb.h>
#include <stm32g4/reg/usb.h>

#include <usb_device/device.h>
#include <usb_device/tree.h>

#include <util/print.h>

typedef struct
{
	usb_device_node_t           device_node;
	usb_device_tree_t*          descriptor_tree;
	uint8_t                     address;
	bool                        address_pending;
	uint8_t                     active_config;
	volatile usb_device_state_e state;
} usb_device_context_t;

static usb_device_context_t usb_device_context = {0};

#define USB_DEVICE_DEV_N    (&(usb_device_context.device_node))
#define USB_DEVICE_EP_N(i)  (&(USB_DEVICE_DEV_N->endpoint_node[i]))


static bool usb_device__setup_cb    (usb_transfer_t* transfer, uint8_t ep_index);
static void usb_device__configure_cb(void* context);
static void usb_device__suspend_cb  (void* context);
static void usb_device__resume_cb   (void* context);


static int16_t usb_endpoint__get_index(uint8_t ep_address)
{
	int16_t ep_index = -1;
	for (unsigned i = 0; i < USB_ENDPOINT_COUNT; i++) {
		if (STM32G4_USB_EP_FIELD_READ(i, ea) == ep_address) {
			ep_index = i;
			break;
		}
	}

	return ep_index;
}


static void usb_endpoint__tx_cb(
	void* context_in, uint8_t ep_address)
{
	usb_device_context_t* context = (usb_device_context_t*)context_in;
	if (!context) return;

	int16_t ep_index = usb_endpoint__get_index(ep_address);

	if (ep_index == -1) return;

	usb_endpoint_node_t* ep_node  = &(context->device_node.endpoint_node[ep_index]);
	usb_transfer_t*      transfer = &(ep_node->transfer[USB_REQUEST_DIR_DEVICE_TO_HOST]);

	if (usb_transfer_is_complete(transfer))
	{
		usb_transfer_reset(transfer);
		if (transfer->is_control)
			stm32_usb_endpoint_setup_acknowledge(ep_index, !transfer->failure);
	}
	else
		usb_transfer_next(transfer, ep_node, USB_REQUEST_DIR_DEVICE_TO_HOST);
}

static void usb_endpoint__rx_cb(
	void* context_in, uint8_t ep_address, bool is_setup)
{
	uart_print(STM32G4_USART1, "usb_endpoint__rx_cb\n\r");
	usb_device_context_t* context = (usb_device_context_t*)context_in;
	if (!context) return;

	int16_t ep_index = usb_endpoint__get_index(ep_address);

	if (ep_index == -1) return;

	usb_endpoint_node_t* ep_node  = &(context->device_node.endpoint_node[ep_index]);
	usb_transfer_t*      transfer = &(ep_node->transfer[USB_REQUEST_DIR_HOST_TO_DEVICE]);

	if (is_setup)
		usb_device__setup_cb(transfer, ep_index);
	else
	{
		if (usb_transfer_is_complete(transfer))
		{
			usb_transfer_reset(transfer);
			if (transfer->is_control)
				stm32_usb_endpoint_setup_acknowledge(ep_index, !transfer->failure);
		}
		else
			usb_transfer_next(transfer, ep_node, USB_REQUEST_DIR_DEVICE_TO_HOST);
	}
}

static void usb_device__configure_cb(void* context)
{
	if (!stm32_usb_open(context, usb_device__configure_cb,
	                    usb_device__suspend_cb, usb_device__resume_cb))
	{
		uart_print(STM32G4_USART1, "Fails to open\n\r");
		return;
	}

	// ctrl endpoint configure
	if (!stm32_usb_endpoint_configure(
		USB_ENDPOINT_CTRL, 0, USB_ENDPOINT_TYPE_CONTROL, false,
		USB_CNTRL_PACKET_SIZE, context,
		usb_endpoint__tx_cb, usb_endpoint__rx_cb))
	{
		uart_print(STM32G4_USART1, "Fails to configure\n\r");
		return;
	}

	// begin setup

	// wait for setup to end

	// return setup success
}


static void usb_device__suspend_cb(void* context)
{
	(void)context;
}


static void usb_device__resume_cb(void* context)
{
	(void)context;
}


static void usb_device__init(void)
{
	stm32_usb_reset();
	stm32_usb_enable(true);

	usb_device_context.active_config   = 0;
	usb_device_context.address         = 0;
	usb_device_context.address_pending = 0;

	/* Device */
	USB_DEVICE_DEV_N->status_value.mask = 0x0000;
	memset(&(USB_DEVICE_DEV_N->setup_packet), 0, sizeof(usb_setup_packet_t));

	/* Endpoints */
	usb_endpoint_node_t *ep_ctrl = USB_DEVICE_EP_N(USB_ENDPOINT_CTRL);
	ep_ctrl->ep_index            = USB_ENDPOINT_CTRL;
	ep_ctrl->packet_size         = USB_CNTRL_PACKET_SIZE;
	ep_ctrl->status_value.mask   = 0x0000;
	ep_ctrl->frame_num           = 0;

	usb_endpoint_node_t *ep_strm = USB_DEVICE_EP_N(USB_ENDPOINT_MAIN);
	ep_strm->ep_index            = USB_ENDPOINT_MAIN;
	ep_strm->packet_size         = USB_STREAM_PACKET_SIZE;
	ep_strm->status_value.mask   = 0x0000;
	ep_strm->frame_num           = 0;

	/* Descriptor Tree */
	usb_device_context.descriptor_tree = usb_device_tree_init();

	usb_device_context.state = USB_DEVICE_STATE_INITIALISED;
}


bool usb_device_open(void)
{
	usb_device__init();

	bool connected = false;
	usb_port_type_e port_type;
	if (stm32_usb_port_detect(&port_type)
		&& usb_port_type_has_data(port_type))
	{
		uart_print(STM32G4_USART1, "Ready to go!\n\r");
		usb_device__configure_cb(&usb_device_context);
		connected = true;
	}
	else
	{
		uart_print(STM32G4_USART1, "Ready to go!\n\r");
		/* TODO - Poll for USB data connection. */
		stm32_usb_enable(false);
	}

	return connected;
}


void usb_device_close(void)
{
	stm32_usb_close();
	stm32_usb_enable(false);
}


usb_device_state_e usb_device_get_state(void)
{
	return usb_device_context.state;
}


void usb_device_request_send_data(void* data, size_t size)
{
	(void)data;
	(void)size;
}

void usb_device_request_receive_data(void* data, size_t size)
{
	(void)data;
	(void)size;
}


void usb_device_request_cancel(void)
{
	return;
}


static bool usb_device__get_status_ref(
	usb_request_recipient_e recipient,
	usb_setup_packet_t*     setup,
	usb_transfer_t*         transfer)
{
	if (!transfer) return false;

	bool success = true;

	switch (recipient) {
		case USB_REQUEST_RECIPIENT_DEVICE:
			transfer->buffer = (uint8_t*)&(USB_DEVICE_DEV_N->status_value);
			// TODO: update and populate device status
			break;

		case USB_REQUEST_RECIPIENT_ENDPOINT:
			if ((usb_device_context.state == USB_DEVICE_STATE_ADDRESSED)
				&& (setup->wIndex.epNum != 0))
				success = false;
			else
			{
				int16_t ep_index = usb_endpoint__get_index(setup->wIndex.epNum);
				if (ep_index == -1)
					success = false;
				else
				{
					usb_status_value_t* status = &(
						USB_DEVICE_EP_N(setup->wIndex.epNum)->status_value);
					if (setup->wIndex.direction == USB_REQUEST_DIR_HOST_TO_DEVICE) {
						// RX
						status->halt = (STM32G4_USB_EP_FIELD_READ(
							ep_index, stat_rx) == STM32G4_USB_EP_STATUS_STALL);
					}
					else if (setup->wIndex.direction == USB_REQUEST_DIR_DEVICE_TO_HOST) {
						// TX
						status->halt = (STM32G4_USB_EP_FIELD_READ(
							ep_index, stat_tx) == STM32G4_USB_EP_STATUS_STALL);
					}
					transfer->buffer = (uint8_t*)status;
				}
			}
			break;

		case USB_REQUEST_RECIPIENT_INTERFACE:
		case USB_REQUEST_RECIPIENT_OTHER:
		default:
			success = false;
			break;
	}

	if (success)
		transfer->count  = sizeof(usb_status_value_t);

	return true;
}



static bool usb_device__standard_request(
	usb_setup_packet_t* setup,
	usb_transfer_t*     transfer)
{
	if (!setup || !transfer) return false;

	bool success = true;

	switch (setup->bRequest) {
	case USB_REQUEST_GET_STATUS:
		success = usb_device__get_status_ref(
			USB_REQUEST_RECIPIENT_DEVICE, setup, transfer);
		break;

	case USB_REQUEST_CLEAR_FEATURE:
	case USB_REQUEST_SET_FEATURE:
		// TODO: add support for features
		success = false;
		break;

	case USB_REQUEST_SET_ADDRESS:
		// NB: standard says not to set address in HW til end of status stage
		if (setup->wValue.mask > 127)
			success = false;
		else {
			usb_device_context.address         = setup->wValue.mask;
			usb_device_context.address_pending = true;
		}

		if (usb_device_context.address == 0) {
			// TODO: deconfigure device
			success = false;
		}
		// NB: we listen for status stage of setup and write to HW
		break;

	case USB_REQUEST_GET_DESCRIPTOR:
		success = usb_device_tree_descriptor_ref(
			setup->wValue.descType, setup, transfer);
		break;

	case USB_REQUEST_SET_DESCRIPTOR:
		// Not supported
		success = false;
		break;

	case USB_REQUEST_GET_CONFIGURATION:
		transfer->count  = sizeof(uint8_t);
		transfer->buffer = &(usb_device_context.active_config);
		break;

	case USB_REQUEST_SET_CONFIGURATION:
		/* Check value is valid */
		if (setup->wValue.mask == 0) {
			/* Deactivate */
			// TODO: see what we need to do here
			usb_device_context.state         = USB_DEVICE_STATE_INITIALISED;
			usb_device_context.active_config = 0;
		}
		else {
			/* See if wValue is present in descriptors */
			usb_device_tree_t* tree = usb_device_context.descriptor_tree;
			if (setup->wValue.mask != tree->descriptors.configuration.bConfigurationValue)
				success = false;
			else
			{
				usb_device_context.active_config = setup->wValue.mask;
				usb_device_context.state         = USB_DEVICE_STATE_CONFIGURED;
			}
		}
		break;

	default:
		success = false;
		break;
	}

	return success;
}

static bool usb_interface__standard_request(
	usb_setup_packet_t* setup,
	usb_transfer_t*     transfer)
{
	if (!setup || !transfer) return false;

	bool success = true;

	switch (setup->bRequest) {

	case USB_REQUEST_GET_INTERFACE:
		if (setup->wIndex.mask < USB_INTERFACE_NODE_COUNT) {
			transfer->count  = sizeof(uint8_t);
			transfer->buffer = &(usb_device_context.descriptor_tree->descriptors.interface.bAlternateSetting);
		}
		else {
			success = false;
		}
		break;

	case USB_REQUEST_GET_STATUS:
	case USB_REQUEST_CLEAR_FEATURE:
	case USB_REQUEST_SET_FEATURE:
	case USB_REQUEST_SET_INTERFACE:
	default:
		success = false;
		break;
	}

	return success;
}


static bool usb_endpoint__standard_request(
	usb_setup_packet_t* setup,
	usb_transfer_t*     transfer)
{
	if (!setup || !transfer) return false;

	bool success = true;
	bool set_feature = false;
	uint8_t ep_num = setup->wIndex.epNum;

	switch (setup->bRequest) {
	case USB_REQUEST_GET_STATUS:
		if ((usb_device_context.state == USB_DEVICE_STATE_ADDRESSED)
		     && (ep_num != 0)) {
			success = false;
		}
		else {
			success &= usb_device__get_status_ref(
				USB_REQUEST_RECIPIENT_ENDPOINT, setup, transfer);
		}
		break;

	case USB_REQUEST_SET_FEATURE:
		set_feature = true;
		// Fall-through
	case USB_REQUEST_CLEAR_FEATURE:
		// used to HALT (stall) an endpoint
		if (setup->wValue.mask != USB_FEATURE_SELECTOR_EP_HALT)
			success = false;
		else {
			stm32g4_usb_ep_status_e new_status;
			if (set_feature) {
				new_status = STM32G4_USB_EP_STATUS_STALL;
			}
			else {
				new_status = STM32G4_USB_EP_STATUS_VALID;
			}
			(void)new_status;

			// TODO: work out how to respond to set / clear feature
			// int16_t ep_index = usb_endpoint__get_index(setup->wIndex.epNum);

			// if (ep_index == -1) {
			// 	success = false;
			// }
			// else {
			// 	if (setup->wIndex.direction == USB_REQUEST_DIR_HOST_TO_DEVICE) {
			// 		STM32G4_USB_EP_FIELD_TOGGLE_TO(ep_index, stat_rx, new_status);
			// 	}
			// 	else if (setup->w_index.data_dir == usb_REQUEST_DATA_DIR_HOST_IN) {
			// 		STM32G4_USB_EP_FIELD_TOGGLE_TO(ep_index, stat_tx, new_status);
			// 	}
			// }

		}
		break;

	case USB_REQUEST_SYNCH_FRAME: {
		int16_t ep_index = usb_endpoint__get_index(ep_num);
		if (ep_index == -1)
			success = false;
		else
		{
			uint16_t* frame_num = &(
				USB_DEVICE_EP_N(setup->wIndex.epNum)->frame_num);
			*frame_num = STM32G4_USB_FIELD_READ(fnr, fn);

			transfer->count  = sizeof(uint16_t);
			transfer->buffer = (uint8_t*)frame_num;
		}
		break;
	}

	default:
		success = false;
		break;
	}

	return success;
}


static bool usb_device__setup_cb(usb_transfer_t* transfer, uint8_t ep_index)
{
	uart_print(STM32G4_USART1, "usb_device__setup_cb\n\r");
	if (ep_index != USB_ENDPOINT_CTRL)
		return false;
	// TODO: need to support transfers properly - should be simple
	//       usb_transfer_start|complete
	//       Maybe test with single buffer transfers first
	/* Copy in SETUP Packet and parse */
	usb_setup_packet_t* setup = &(usb_device_context.device_node.setup_packet);
	usb_endpoint_node_t* ctrl_node = USB_DEVICE_EP_N(USB_ENDPOINT_CTRL);

	size_t packet_size = stm32_usb_endpoint_read(
		USB_ENDPOINT_CTRL, sizeof(usb_setup_packet_t), setup);

	if (packet_size != sizeof(usb_setup_packet_t))
		return false;

	// TODO: add support for class and vendor requests
	// unsupported
	if (setup->bmRequestType.type != USB_REQUEST_TYPE_STANDARD)
		return false;

	usb_transfer_reset(transfer);
	transfer->zlp        = true;
	transfer->is_control = true;

	bool success = true;

	switch (setup->bmRequestType.recipient) {
	case USB_REQUEST_RECIPIENT_DEVICE:
		success = usb_device__standard_request(setup, transfer);
		break;

	case USB_REQUEST_RECIPIENT_INTERFACE:
		success = usb_interface__standard_request(setup, transfer);
		break;

	case USB_REQUEST_RECIPIENT_ENDPOINT:
		success = usb_endpoint__standard_request(setup, transfer);
		break;

	default:
		success = false;
		break;
	}

	if (success) {
		stm32_usb_endpoint_setup_begin(
			USB_ENDPOINT_CTRL,
			setup->bmRequestType.direction == USB_REQUEST_DIR_HOST_TO_DEVICE,
			setup->wLength);
		// Do we want to acknowledge setup here if len == 0??

		if ((setup->wLength != 0) && (transfer->buffer))
			success = usb_transfer_next(
				transfer, ctrl_node,
				setup->bmRequestType.direction);

		if ((setup->wLength == 0) || (usb_transfer_is_complete(transfer)))
		{
			stm32_usb_endpoint_setup_acknowledge(USB_ENDPOINT_CTRL, success);
			usb_transfer_reset(transfer);
		}
	}

	uart_print(STM32G4_USART1, "success = %u\n\r");
	uart_print_int_base(STM32G4_USART1, success, 10);
	return success;
}

