#include <stm32g4/usb.h>
#include <usb_device/device.h>

bool usb_transfer_next(usb_transfer_t*      transfer,
                       usb_endpoint_node_t* ep_node,
                       usb_request_dir_e    direction)
{
	// TODO: rework architecture to support exposing USB SRAM directly, and
	//       maybe handling transfers with DMA
	if (!transfer) return false;

	bool success = true;

	switch (direction)
	{
		case USB_REQUEST_DIR_HOST_TO_DEVICE:
		{
			size_t rx_size = stm32_usb_endpoint_read(
				ep_node->ep_index, ep_node->packet_size, transfer->buffer);
			transfer->count -= rx_size;
			break;
		}

		case USB_REQUEST_DIR_DEVICE_TO_HOST:
		{
			size_t tx_size = (transfer->count < ep_node->packet_size)
				? transfer->count
				: ep_node->packet_size;
			// TODO: support double-buffered endpoints
			if ((tx_size > 0) || (transfer->zlp))
				success = stm32_usb_endpoint_write(
					ep_node->ep_index, tx_size, transfer->buffer);
			if ((tx_size == 0) && (transfer->zlp))
				transfer->zlp = false;
			if (success)
				transfer->count -= tx_size;
			break;
		}

		default:
			success = false;
			break;
	}

	transfer->failure &= !success;

	return success;
}


bool usb_transfer_is_complete(usb_transfer_t* transfer)
{
	if (!transfer) return false;

	return (transfer->count == 0) && (!transfer->zlp);
}
