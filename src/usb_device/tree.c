#include <usb_device/tree.h>
#include <protocols/usb.h>

/* This is probably a temporary solution - it contains all of the configuration
   for the device (namely the descriptor tree)*/

#define USB_CONFIG_DESC_TOTAL_LEN sizeof(usb_descriptors_t)

static usb_device_tree_t usb_device_tree =
{
	.descriptors = {{0}},
	.initialised = false
};

#define USB_DESCRIPTORS (&(usb_device_tree.descriptors))

usb_device_tree_t* usb_device_tree_init(void)
{
	// TODO: improve this initialisation
	/* Device */
	usb_device_descriptor_t *dev_desc = &(USB_DESCRIPTORS->device);
	dev_desc->bLength             = sizeof(usb_device_descriptor_t);
	dev_desc->bDescriptorType     = USB_DESCRIPTOR_DEVICE;
	dev_desc->bcdUSB              = USB_VERSION_2_0;
	dev_desc->bDeviceClass        = USB_DEVICE_CLASS_DEVICE;
	dev_desc->bDeviceSubClass     = 0x00;
	dev_desc->bDeviceProtocol     = 0x00;
	dev_desc->bMaxPacketSize      = USB_CNTRL_PACKET_SIZE; // TODO: tune this
	dev_desc->idVendor            = 0x0000;                // TODO: get one
	dev_desc->idProduct           = 0x0000;                // TODO: get one
	dev_desc->bcdDevice           = 0x0100;
	dev_desc->iManufaturer        = 0;
	dev_desc->iProduct            = 0;
	dev_desc->iSerialNumber       = 0;
	dev_desc->bNumConfigurations  = USB_CONFIG_NODE_COUNT;

	/* Configuration */
	usb_configuration_descriptor_t *conf_desc = &(USB_DESCRIPTORS->configuration);
	conf_desc->bLength               = sizeof(usb_configuration_descriptor_t);
	conf_desc->bDescriptorType      = USB_DESCRIPTOR_CONFIGURATION;
	conf_desc->wTotalLength         = USB_CONFIG_DESC_TOTAL_LEN;
	conf_desc->bNumInterfaces       = USB_INTERFACE_NODE_COUNT;
	conf_desc->bConfigurationValue  = 1;
	conf_desc->iConfiguration       = 0;
	conf_desc->bmAttributes.mask    = 0;
	conf_desc->bmAttributes.res_7   = 1;
	conf_desc->bMaxPower            = 25; // TODO: assumes 50mA

	/* Interface */
	usb_interface_descriptor_t *inter_desc = &(USB_DESCRIPTORS->interface);
	inter_desc->bLength              = sizeof(usb_interface_descriptor_t);
	inter_desc->bDescriptorType      = USB_DESCRIPTOR_INTERFACE;
	inter_desc->bInterfaceNum        = 0;
	inter_desc->bAlternateSetting    = 0;
	inter_desc->bNumEndpoints        = USB_ENDPOINT_DESC_INDEX_COUNT;
	inter_desc->bInterfaceClass      = 0x00;
	inter_desc->bInterfaceSubClass   = 0x00;
	inter_desc->bInterfaceProtocol   = 0x00;
	inter_desc->iInterface           = 0;

	/* Endpoints */
	usb_endpoint_descriptor_t *ep_strm_desc_rx = &(
		USB_DESCRIPTORS->endpoint[USB_ENDPOINT_DESC_INDEX_MAIN_RX]);
	ep_strm_desc_rx->bLength                    = sizeof(usb_endpoint_descriptor_t);
	ep_strm_desc_rx->bDescriptorType            = USB_DESCRIPTOR_ENDPOINT;
	ep_strm_desc_rx->bEndpointAddress.number    = USB_ENDPOINT_MAIN;
	ep_strm_desc_rx->bEndpointAddress.reserved  = 0;
	ep_strm_desc_rx->bEndpointAddress.direction = 0;
	ep_strm_desc_rx->bmAttributes.type          = USB_ENDPOINT_TYPE_INTERRUPT;
	ep_strm_desc_rx->bmAttributes.sync          = 0;
	ep_strm_desc_rx->bmAttributes.usage         = 0;
	ep_strm_desc_rx->bmAttributes.reserved      = 0;
	ep_strm_desc_rx->wMaxPacketSize             = USB_STREAM_PACKET_SIZE;
	ep_strm_desc_rx->bInterval                  = 0x0A;

	usb_endpoint_descriptor_t *ep_strm_desc_tx = &(
		USB_DESCRIPTORS->endpoint[USB_ENDPOINT_DESC_INDEX_MAIN_TX]);
	ep_strm_desc_tx->bLength                    = sizeof(usb_endpoint_descriptor_t);
	ep_strm_desc_tx->bDescriptorType            = USB_DESCRIPTOR_ENDPOINT;
	ep_strm_desc_tx->bEndpointAddress.number    = USB_ENDPOINT_MAIN;
	ep_strm_desc_tx->bEndpointAddress.reserved  = 0;
	ep_strm_desc_tx->bEndpointAddress.direction = 1;
	ep_strm_desc_tx->bmAttributes.type          = USB_ENDPOINT_TYPE_INTERRUPT;
	ep_strm_desc_tx->bmAttributes.sync          = 0;
	ep_strm_desc_tx->bmAttributes.usage         = 0;
	ep_strm_desc_tx->bmAttributes.reserved      = 0;
	ep_strm_desc_tx->wMaxPacketSize             = USB_STREAM_PACKET_SIZE;
	ep_strm_desc_tx->bInterval                  = 0x0A;

	usb_device_tree.initialised = true;

	return &usb_device_tree;
}

/* Setup methods */
static uint32_t usb__get_desc_tree_size(void)
{
	// Currently, the tree size is defined at compile, but in future, could
	// be dynamic
	return USB_CONFIG_DESC_TOTAL_LEN;
}

bool usb_device_tree_descriptor_ref(
	usb_descriptor_e    descriptor_type,
	usb_setup_packet_t* setup,
	usb_transfer_t*     transfer)
{
	bool success = true;
	switch (descriptor_type) {
		case USB_DESCRIPTOR_DEVICE:
			transfer->buffer = (uint8_t*)&(USB_DESCRIPTORS->device);
			transfer->count  = sizeof(usb_device_descriptor_t);
			break;

		case USB_DESCRIPTOR_CONFIGURATION:
			if (setup->wValue.index >= USB_CONFIG_NODE_COUNT)
				success = false;
			transfer->buffer = (uint8_t*)USB_DESCRIPTORS;
			transfer->count  = usb__get_desc_tree_size();
			break;

		case USB_DESCRIPTOR_INTERFACE:
		case USB_DESCRIPTOR_ENDPOINT:
			// not valid requests
		case USB_DESCRIPTOR_STRING:
			// TODO: add support (optional)
		default:
			success = false;
			break;
		}

	return success;
}
