#include <util/delay.h>
#include <cm4/systick.h>
#include <stm32g4/clock.h>
#include <stddef.h>


volatile unsigned delay_time;
volatile unsigned delay_target;
volatile bool*    delay_timeout = NULL;
volatile bool     delay_complete = true;

static void delay_callback(void)
{
	if (++delay_time >= delay_target)
	{
		cm4_systick_disable();
		delay_complete = true;
		if (delay_timeout) *delay_timeout = true;
		delay_timeout = NULL;
		return;
	}
}


#define ROUNDING_DIVIDE(x, y) (((x) + ((y) - 1)) / (y))

static bool delay_timeout_unit(unsigned duration, unsigned unit, volatile bool* trigger)
{
	if (!delay_complete)
		return false;

	unsigned freq;
	if (!stm32g4_clock_freq(STM32G4_CLOCK_SYSTICK, &freq))
		return false;

	if (duration == 0) return true;

	/* TODO - Detect overflow and handle. */
	unsigned fd = ((freq / 1000) * duration);

	delay_time     = 0;
	delay_target   = 1;
	delay_complete = false;
	delay_timeout  = trigger;

	unsigned ticks = ROUNDING_DIVIDE(fd, unit);
	while (ticks >> 24)
	{
		delay_target <<= 1;
		ticks >>= 1;
	}

	if (!cm4_systick_enable(ticks, delay_callback))
	{
		delay_complete = true;
		return false;
	}

	if (!trigger)
	{
		while (!delay_complete)
			__asm("wfi");
	}

	return true;
}


bool delay_us(unsigned duration)
{
	return delay_timeout_unit(duration, 1000, NULL);
}

bool delay_ms(unsigned duration)
{
	return delay_timeout_unit(duration, 1, NULL);
}

bool timeout_us(unsigned duration, volatile bool* trigger)
{
	return delay_timeout_unit(duration, 1000, trigger);
}

bool timeout_ms(unsigned duration, volatile bool* trigger)
{
	return delay_timeout_unit(duration, 1, trigger);
}

void timeout_cancel(void)
{
	cm4_systick_disable();
	delay_complete = true;
	delay_timeout  = NULL;
}
