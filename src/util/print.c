#include <util/print.h>

#include <stdarg.h>
#include <string.h>

#define PRINT_MAX_WIDTH            79
#define PRINT_TEMP_PRINTF_BUFFER   256

bool uart_print(stm32g4_usart_interface_e interface, const char *nul_term_string)
{
	if (interface >= STM32G4_UART_COUNT) return false;
	stm32g4_usart_write(interface, strlen(nul_term_string), nul_term_string);

	return true;
}


bool uart_print_uint_base(
	stm32g4_usart_interface_e interface,
	int32_t                   value,
	unsigned                  base)
{
	if (interface >= STM32G4_UART_COUNT) return false;

	if ((base == 0) || (base > 36))
		return false;

	char buffer[PRINT_MAX_WIDTH];

	int32_t index = PRINT_MAX_WIDTH, remaining = value, current;
	while ((index >= 0) && (remaining != 0))
	{
		current   = remaining % base;
		remaining = remaining / base;

		if (current < 10)
			buffer[index--] = '0' + current;
		else
			buffer[index--] = 'A' + current - 10;
	}

	stm32g4_usart_write(interface, PRINT_MAX_WIDTH - index + 1, &buffer[index]);
	return true;
}


bool uart_print_int_base(
	stm32g4_usart_interface_e interface,
	int32_t                   value,
	unsigned                  base)
{
	if (value < 0)
	{
		stm32g4_usart_write(interface, 1, "-");
		value = -value;
	}

	return uart_print_uint_base(interface, value, base);
}

// TODO: add printf function
